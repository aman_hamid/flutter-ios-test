import 'package:json_annotation/json_annotation.dart';

part 'signup_transfer_image_response.g.dart';

@JsonSerializable()

class SignupTransferImageResponse{

  String originalPath;
  String resizedPath;


  SignupTransferImageResponse(this.originalPath, this.resizedPath);

  factory SignupTransferImageResponse.fromJson(Map<String, dynamic> json) => _$SignupTransferImageResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SignupTransferImageResponseToJson(this);
}