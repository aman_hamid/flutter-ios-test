import 'dart:core';
import 'package:json_annotation/json_annotation.dart';
part 'prescription_document.g.dart';

@JsonSerializable()
class PrescriptionDocument {
  String documentPath;
  String thumbnailPath;
  String documentType;

  factory PrescriptionDocument.fromJson(Map<String, dynamic> json) => _$PrescriptionDocumentFromJson(json);

  Map<String, dynamic> toJson() => _$PrescriptionDocumentToJson(this);

  PrescriptionDocument(this.documentPath, this.thumbnailPath, this.documentType);
}
