import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/success/base_success_screen.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';

class AddMemberCompleteWidget extends BaseStatelessWidget {
  static const routeName = 'add_member_complete';
  final UserPatient userPatient;

  AddMemberCompleteWidget({Key key, this.userPatient}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseSuccessScreen(
      source: BaseStepperSource.ADD_PATIENT,
      transactionSuccessEnum: TransactionSuccessEnum.ADD_MEMBER_SUCCESS,
    );
  }
}
