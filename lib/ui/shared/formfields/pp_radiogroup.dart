import 'package:flutter/material.dart';
import 'package:pocketpills/ui/shared/buttons/radio_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';

class PPRadioGroup<T> extends StatefulWidget {
  final _RadioGroupState<T> stateObj = _RadioGroupState();
  final List<T> radioOptions;
  final String labelText;
  final String errorText;
  T initialValue;
  final Function onChange;
  final Function onChangeCallback;
  final String initialSelectedText;

  PPRadioGroup(
      {@required this.radioOptions,
      this.labelText = "Options",
      this.errorText = "This is a required field",
      this.initialValue,
      this.onChange,
      this.onChangeCallback,
      this.initialSelectedText});

  bool validate() {
    return stateObj.validate();
  }

  T getValue() {
    return stateObj.value;
  }

  @override
  _RadioGroupState<T> createState() => stateObj;
}

class _RadioGroupState<T> extends State<PPRadioGroup<T>> {
  T value;
  bool errorState;

  bool validate() {
    if (value == null) {
      setState(() {
        errorState = true;
      });
      return false;
    }
    return true;
  }

  @override
  void initState() {
    value = widget.initialValue;
    super.initState();
  }

  Widget getRowChild(T radioOption) {
    return value != radioOption
        ? RadioButton(
            text: radioOption.toString(),
            isSelected: false,
            onPressed: () => setState(() {
                  value = radioOption;
                  errorState = false;
                  widget.onChangeCallback ;
                  if (widget.onChange != null) widget.onChange(value);
                }), )
        : RadioButton(
            text: radioOption.toString(),
            isSelected: true,
            onPressed: () {
              setState(() {
                value = radioOption;
                errorState = false;
                widget.onChangeCallback;
                if (widget.onChange != null) widget.onChange(value);
              });
            });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> rowChildren = [];
    for (int i = 0; i < widget.radioOptions.length; i++) {
      rowChildren.add(getRowChild(widget.radioOptions[i]));
      if (i < widget.radioOptions.length - 1)
        rowChildren.add(SizedBox(width: PPUIHelper.HorizontalSpaceMedium));
    }
    return Container(
      child: Column(
        children: [
          PPTexts.getFormLabel(widget.labelText, isBold: true),
          PPUIHelper.verticalSpaceSmall(),
          Row(children: rowChildren),
          errorState == true
              ? PPTexts.getFormError(widget.errorText, color: errorColor)
              : SizedBox(height: MEDIUM_XXX)
        ],
      ),
    );
  }
}
