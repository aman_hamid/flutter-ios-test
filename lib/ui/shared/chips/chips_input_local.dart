import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pocketpills/ui/views/profile/chips/chips_input_internal.dart';


typedef ChipsInputSuggestions<T> = FutureOr<List<T>> Function(String query);
typedef ChipSelected<T> = void Function(T data, bool selected);
typedef ChipsBuilder<T> = Widget Function(BuildContext context, ChipsInputState<T> state, T data);

class ChipsInputLocal<T> extends ChipsInput<T> {
  ChipsInputLocal({
    Key key,
    this.initialValue = const [],
    this.decoration = const InputDecoration(),
    this.enabled = true,
    @required this.chipBuilder,
    @required this.suggestionBuilder,
    @required this.findSuggestions,
    @required this.onChanged,
    this.onChipTapped,
    this.maxChips,
    this.textStyle,
    this.textOverflow = TextOverflow.ellipsis,
  })  : assert(maxChips == null || initialValue.length <= maxChips),
        super(
            key: key,
            initialValue: initialValue,
            decoration: decoration,
            enabled: enabled,
            chipBuilder: chipBuilder,
            suggestionBuilder: suggestionBuilder,
            findSuggestions: findSuggestions,
            onChanged: onChanged,
            onChipTapped: onChipTapped,
            maxChips: maxChips,
            textStyle: textStyle,
            textOverflow: textOverflow);

  final InputDecoration decoration;
  final TextStyle textStyle;
  final bool enabled;
  final ChipsInputSuggestions<T> findSuggestions;
  final ValueChanged<List<T>> onChanged;
  final ValueChanged<T> onChipTapped;
  final ChipsBuilder<T> chipBuilder;
  final ChipsBuilder<T> suggestionBuilder;
  final List<T> initialValue;
  final int maxChips;
  final ChipsInputState state = ChipsInputState<T>();
  final TextOverflow textOverflow;

  @override
  ChipsInputState<T> createState() {
    return state;
  }
}
