import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/views/pillreminder/component/pill_reminder_tick.dart';

class PillReminderDayWiseIconStatus extends StatelessWidget {
  final PocketPackDetails pocketPackDetail;

  PillReminderDayWiseIconStatus(this.pocketPackDetail);

  @override
  Widget build(BuildContext context) {
    switch (pocketPackDetail.getPillStatusType()) {
      case PillStatusType.TAKEN:
        return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(LARGE_XXX)),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.all(SMALL_XX),
              child: PillReminderTick(PillStatusType.TAKEN),
            ));
        break;
      case PillStatusType.MISSED:
        return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(LARGE_XXX)),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.all(SMALL_XX),
              child: PillReminderTick(PillStatusType.MISSED),
            ));
        break;
      case PillStatusType.FUTURE:
      case PillStatusType.NONE:
        return PPContainer.emptyContainer();
        break;
    }
  }
}
