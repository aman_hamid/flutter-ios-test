// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_document.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderDocument _$OrderDocumentFromJson(Map<String, dynamic> json) {
  return OrderDocument(
    json['documentPath'] as String,
    json['thumbnailPath'] as String,
    json['documentType'] as String,
  );
}

Map<String, dynamic> _$OrderDocumentToJson(OrderDocument instance) =>
    <String, dynamic>{
      'documentPath': instance.documentPath,
      'thumbnailPath': instance.thumbnailPath,
      'documentType': instance.documentType,
    };
