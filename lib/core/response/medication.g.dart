// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medication.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Medication _$MedicationFromJson(Map<String, dynamic> json) {
  return Medication(
    json['rxId'] as int,
    json['medicationId'] as int,
    json['drug'] as String,
    json['drugId'] as String,
    json['refillsLeft'] as int,
    json['drugType'] as String,
    json['quantityLeft'] as num,
    json['quantityFilled'] as num,
    json['quantityPerFill'] as num,
    json['daysSupply'] as int,
    json['totalQuantity'] as num,
    json['initialDispensedQuantity'] as num,
    json['sig'] as String,
    json['validUntil'] as String,
    json['doctor'] as String,
    json['message'] as String,
    json['statusText'] as String,
    json['type'] as int,
    json['dgType'] as String,
    json['refillQuantity'] as String,
    json['variantId'] as int,
    json['isSmartPack'] as int,
  )..id = json['id'] as int;
}

Map<String, dynamic> _$MedicationToJson(Medication instance) =>
    <String, dynamic>{
      'id': instance.id,
      'rxId': instance.rxId,
      'medicationId': instance.medicationId,
      'drug': instance.drug,
      'drugId': instance.drugId,
      'refillsLeft': instance.refillsLeft,
      'drugType': instance.drugType,
      'quantityLeft': instance.quantityLeft,
      'quantityFilled': instance.quantityFilled,
      'quantityPerFill': instance.quantityPerFill,
      'daysSupply': instance.daysSupply,
      'totalQuantity': instance.totalQuantity,
      'initialDispensedQuantity': instance.initialDispensedQuantity,
      'sig': instance.sig,
      'validUntil': instance.validUntil,
      'doctor': instance.doctor,
      'message': instance.message,
      'statusText': instance.statusText,
      'type': instance.type,
      'dgType': instance.dgType,
      'refillQuantity': instance.refillQuantity,
      'variantId': instance.variantId,
      'isSmartPack': instance.isSmartPack,
    };
