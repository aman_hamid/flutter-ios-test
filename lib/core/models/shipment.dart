import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/shipment_item.dart';

part 'shipment.g.dart';

@JsonSerializable()
class Shipment{
  String dueDate;
  int id;
  String shipmentNo;
  String createdDateTime;
  String updatedDateTime;
  String order;
  String provider;
  String status;
  String providerShipmentId;
  List<ShipmentItem> items;
  num weight;
  String deliveryInstructions;
  String packageSize;
  String service;
  String labelSize;
  String expectedDeliveryDate;
  String shipmentDate;
  String deliveryDate;
  String attemptedDeliveryDate;
  String returnDate;
  String createdBy;
  String modifiedBy;
  bool isTrackingAvailable;

  Shipment({this.status,this.id,this.updatedDateTime,this.modifiedBy,this.dueDate,this.createdDateTime,this.createdBy,this.attemptedDeliveryDate,this.deliveryDate,this.deliveryInstructions,this.expectedDeliveryDate,this.isTrackingAvailable,this.items,this.labelSize,this.order,this.packageSize,this.provider,this.providerShipmentId,this.returnDate,this.service,this.shipmentDate,this.shipmentNo,this.weight
  });

  factory Shipment.fromJson(Map<String, dynamic> json) => _$ShipmentFromJson(json);

  Map<String, dynamic> toJson() => _$ShipmentToJson(this);
}