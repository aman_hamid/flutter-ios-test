// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_patient_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserPatientResponse _$UserPatientResponseFromJson(Map<String, dynamic> json) {
  return UserPatientResponse(
    userPatientList: (json['patientList'] as List)
        ?.map((e) =>
            e == null ? null : UserPatient.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$UserPatientResponseToJson(
        UserPatientResponse instance) =>
    <String, dynamic>{
      'patientList': instance.userPatientList,
    };
