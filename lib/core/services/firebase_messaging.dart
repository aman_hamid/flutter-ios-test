import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/request/notification_device_body.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'notification_service.dart';

class FirebaseMessagingService {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final DataStoreService dataStore = locator<DataStoreService>();
  final NotificationService notificationService = locator<NotificationService>();
  final HttpApi _api = locator<HttpApi>();

  String deviceType;
  String deviceId;

  String deviceName;

  FirebaseMessagingService() {
    firebaseCloudMessaging_Listeners();
    getDeviceInfo();
  }

  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token) async {
      await dataStore.writeString(DataStoreService.FIREBASE_TOKEN, token);
      await _api.putUpdateFirebaseToken(RequestWrapper(
          body: NotificationDeviceBody(
              userId: dataStore.getUserId(),
              token: token.toString(),
              deviceId: this.deviceId,
              deviceName: this.deviceName,
              deviceType: this.deviceType,
              oneSignalId: dataStore.readString(DataStoreService.ONESIGNAL_PLAYERID),
              oneSignalToken: dataStore.readString(DataStoreService.ONESIGNAL_TOKEN))));
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        Map notification = message['notification'];
        print('on message $message');
        notificationService.showNotification(notification['title'], notification['body']);
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future<void> getDeviceInfo() async {
    if (Platform.isAndroid) {
      await _getAndroidInfo();
    } else if (Platform.isIOS) {
      await _getIOSInfo();
    }
  }

  _getAndroidInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    this.deviceType = 'ANDROID';
    this.deviceId = androidInfo.androidId;
    this.deviceName = androidInfo.manufacturer + ' ' + androidInfo.model;
  }

  _getIOSInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    this.deviceType = 'IOS';
    this.deviceId = iosInfo.identifierForVendor;
    this.deviceName = iosInfo.localizedModel;
  }
}
