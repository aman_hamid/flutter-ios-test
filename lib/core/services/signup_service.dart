import 'dart:convert';

import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/signup_request.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/locator.dart';

import 'login_service.dart';

class SignupService {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  final LoginService loginService = locator<LoginService>();

  final Base64Codec base64 = Base64Codec();

  Future<bool> signup(String phoneNumber, String password, String firstName, String lastName, String date, String month,
      String year) async {
    SignupRequest signupRequest =
        SignupRequest(firstName: firstName, lastName: lastName, dob: year + "-" + month + "-" + date);

    var response = await _api.signup(RequestWrapper(body: signupRequest));

    return await loginService.registerUser(response);
  }
}
