import 'dart:io';

import 'package:exif/exif.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

class RotateAndCompressImage {
  Future<File> rotateAndCompressAndSaveImage(File image) async {
    int rotate = 0;
    try {
      List<int> imageBytes = await image.readAsBytes();
      Map<String, IfdTag> exifData = await readExifFromBytes(imageBytes);

      if (exifData != null && exifData.isNotEmpty && exifData.containsKey("Image Orientation")) {
        IfdTag orientation = exifData["Image Orientation"];
        int orientationValue = orientation.values[0];

        if (orientationValue == 3) {
          rotate = 180;
        }

        if (orientationValue == 6) {
          rotate = -90;
        }

        if (orientationValue == 8) {
          rotate = 90;
        }
      }

      List<int> result = await FlutterImageCompress.compressWithList(imageBytes, quality: 100, rotate: 0);

      await image.writeAsBytes(result);

      return image;
    } catch (ex) {
      Crashlytics.instance.log(ex.toString());
    }
  }
}
