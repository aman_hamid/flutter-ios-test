import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/add_member_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/add_patient_responses.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class AddPatientModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();
  ConnectivityResult connectivityResult;

  Future<UserPatient> addPatient({Patient patient, String relation}) async {
    setState(ViewState.Busy);
    AddMemberRequest request = AddMemberRequest();
    request.patient = patient;
    request.relation = relation;
    var response = await _api.addPatient(RequestWrapper(body: request));
    setState(ViewState.Idle);
    return parseAddMemberResponse(response);
  }

  Future<UserPatient> addPatientFromRequest({AddMemberRequest request}) async {
    setState(ViewState.Busy);
    var response = await _api.addPatient(RequestWrapper(body: request));
    setState(ViewState.Idle);
    return parseAddMemberResponse(response);
  }

  UserPatient parseAddMemberResponse(var response) {
    if (response != null) {
      BaseResponse<AddPatientResponse> res = BaseResponse<AddPatientResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      AddPatientResponse apRes = res.response;
      return PatientUtils.getUserPatientFromList(apRes.patientList, apRes.patientId);
    } else {
      return null;
    }
  }

  Future<bool> updatePatientDetails({BuildContext context, String province, String gender, bool selfMedication, UserPatient userPatient, bool pocketPacks}) async {
    setState(ViewState.Busy);
    MemberAboutYouRequest request = MemberAboutYouRequest();
    request.gender = gender;
    request.province = province.toUpperCase();
    request.hasDailyMedication = selfMedication;
    request.pocketPacks = pocketPacks;
    int patientId = userPatient != null ? userPatient.patientId : dataStore.getPatientId();
    Provider.of<DashboardModel>(context).memberPatientId = patientId;
    var response = await _api.updatePatient(patientId, RequestWrapper(body: request));
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
