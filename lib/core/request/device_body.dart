import 'dart:io';

import 'package:json_annotation/json_annotation.dart';

import 'package:device_info/device_info.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';

part 'device_body.g.dart';

@JsonSerializable(explicitToJson: true)
class DeviceBody extends BaseRequest {

  @JsonKey(includeIfNull: false)
  int userId;

  String deviceType;
  String deviceId;

  String deviceName;
  String notificationToken;

  final DataStoreService dataStore = locator<DataStoreService>();

  init() async {
    if (Platform.isAndroid) {
      await _getAndroidInfo();
    } else if (Platform.isIOS) {
      await _getIOSInfo();
    }
  }

  DeviceBody(
      {this.userId, this.deviceType, this.deviceId, this.deviceName, this.notificationToken});

  factory DeviceBody.fromJson(Map<String, dynamic> json) => _$DeviceBodyFromJson(json);

  Map<String, dynamic> toJson() => _$DeviceBodyToJson(this);

  _getAndroidInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    this.deviceType = 'ANDROID';
    this.deviceId = androidInfo.androidId;
    this.deviceName = androidInfo.manufacturer + ' ' + androidInfo.model;
    this.notificationToken = dataStore.readString(DataStoreService.FIREBASE_TOKEN);
  }

  _getIOSInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    this.deviceType = 'IOS';
    this.deviceId = iosInfo.identifierForVendor;
    this.deviceName = iosInfo.localizedModel;
    this.notificationToken = dataStore.readString(DataStoreService.FIREBASE_TOKEN);
  }

}