import 'package:pocketpills/core/request/base_request.dart';

class AddProvinceRequest extends BaseRequest {
  String province;

  AddProvinceRequest({
    this.province,
  });

  Map<String, dynamic> toJson() => <String, dynamic>{
        'province': this.province,
      };
}
