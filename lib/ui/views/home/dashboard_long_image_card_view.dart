import 'package:flutter/material.dart';
import 'package:pocketpills/core/dashboard/dashboard_item.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';

class DashboardLongImageCardView extends StatelessWidget {
  final DashboardItem dashboardItem;
  final HomeModel homeModel;

  DashboardLongImageCardView(
      {@required this.dashboardItem, @required this.homeModel});

  @override
  Widget build(BuildContext context) {
    return PPCard(
      onTap: () {
        homeModel.handleDashboardRoute(dashboardItem.getActionType(), context,
            dashboardItem: dashboardItem);
      },
      margin: EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM),
      padding: EdgeInsets.all(0.0),
      child: Container(
        child: Row(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(SMALL_X),
                  bottomLeft: Radius.circular(SMALL_X)),
              child: Container(
                color: brandColor,
                child: Image.network(
                  dashboardItem.image ?? "",
                  width: 84,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: MEDIUM_X,
                    top: SMALL_X,
                    bottom: SMALL_X,
                    right: MEDIUM),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      dashboardItem.title ?? "",
                      style: MEDIUM_XXX_PRIMARY_BOLD,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    SizedBox(
                      height: SMALL,
                    ),
                    Text(
                      dashboardItem.description ?? "",
                      style: MEDIUM_XX_PRIMARY,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
