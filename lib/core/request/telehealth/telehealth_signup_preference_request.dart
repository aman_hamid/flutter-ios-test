import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';
part 'telehealth_signup_preference_request.g.dart';

@JsonSerializable()

class TelehealthSignUpPreferenceRequest extends BaseRequest {
  String type;
  String prescriptionComment;
  bool prescriptionFilledByExternalPharmacy;
  String prescriptionState;
  String prescriptionRequestReason;
  String prescriptionRequestCategory;
  String telehealthRequestedMedications;
  String prescriptionMedicalConditions;
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime appointmentTime;

  TelehealthSignUpPreferenceRequest(
      {this.type,
      this.prescriptionComment,
      this.prescriptionFilledByExternalPharmacy,
      this.prescriptionState,
      this.prescriptionRequestReason,
      this.prescriptionRequestCategory,
      this.telehealthRequestedMedications,
      this.prescriptionMedicalConditions,
      this.appointmentTime
      });

  factory TelehealthSignUpPreferenceRequest.fromJson(Map<String, dynamic> data) =>
      _$TelehealthSignUpPreferenceRequestFromJson(data);

  Map<String, dynamic> toJson() => _$TelehealthSignUpPreferenceRequestToJson(this);
}
