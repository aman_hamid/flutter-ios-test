// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_request_update.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthRequestUpdate _$TelehealthRequestUpdateFromJson(
    Map<String, dynamic> json) {
  return TelehealthRequestUpdate(
    id: json['id'] as int,
    appointmentTime: PPDateUtils.fromStr(json['appointmentTime'] as String),
  );
}

Map<String, dynamic> _$TelehealthRequestUpdateToJson(
        TelehealthRequestUpdate instance) =>
    <String, dynamic>{
      'id': instance.id,
      'appointmentTime': PPDateUtils.toStr(instance.appointmentTime),
    };
