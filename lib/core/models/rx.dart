import 'package:json_annotation/json_annotation.dart';

part 'rx.g.dart';

@JsonSerializable()
class Rx {
  int medicationId;
  int rxId;
  int prescriptionId;
  String createdDate;
  String rxStop;
  String validTill;
  String drug;
  String din;
  String doctorFirstName;
  String doctorLastName;
  String phnNumber;
  bool refillAvailable;
  num remainingQty;
  num filledQty;
  num qty;
  String sig;
  int preApprovedRefillCount;
  int days;
  int refillId;
  num quantity;
  num copay;
  num pharmaCare;
  num insurance;
  num cost;

  Rx(
      {this.createdDate,
      this.days,
      this.din,
      this.doctorFirstName,
      this.doctorLastName,
      this.drug,
      this.filledQty,
      this.medicationId,
      this.phnNumber,
      this.preApprovedRefillCount,
      this.prescriptionId,
      this.qty,
      this.refillAvailable,
      this.remainingQty,
      this.rxId,
      this.rxStop,
      this.sig,
      this.validTill,
      this.copay,
      this.cost,
      this.insurance,
      this.pharmaCare,
      this.quantity,
      this.refillId});

  factory Rx.fromJson(Map<String, dynamic> json) => _$RxFromJson(json);

  Map<String, dynamic> toJson() => _$RxToJson(this);
}
