import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/health_info.dart';
part 'patient_health_response.g.dart';

@JsonSerializable()
class PatientHealthResponse {
  HealthInfo healthInfo;

  PatientHealthResponse({this.healthInfo});

  factory PatientHealthResponse.fromJson(Map<String, dynamic> json) => _$PatientHealthResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PatientHealthResponseToJson(this);
}
