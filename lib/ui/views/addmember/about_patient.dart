import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/add_patient_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';

class AboutPatientWidget extends StatefulWidget {
  static const routeName = 'aboutpatient';

  final String snackBarMessage;
  final UserPatient userPatient;
  final String gender;

  AboutPatientWidget({Key key, this.snackBarMessage, this.userPatient, this.gender}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AboutPatientState();
  }
}

class AboutPatientState extends BaseState<AboutPatientWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();

  PPRadioGroup<String> genderRadioGroup;
  PPRadioGroup<String> selfMedicationRadioGroup;

  final _formKey = GlobalKey<FormState>();
  String dropDownValue = null;
  String dropDownError;

  bool manitobaCheck = true;

  bool isFormValid() {
    bool dp = dropDownValue != null;
    //bool gr = genderRadioGroup.validate();
    bool sm = selfMedicationRadioGroup.validate();
    return (dp && sm);
  }

  //String gender;
  String province;
  bool hasDailyMedication;

  AddPatientModel model;
  bool initialize = false;
  @override
  void initState() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_about_them);
    super.initState();
    model = AddPatientModel();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: myFutureMethodOverall(model),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getProvider();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Widget getProvider() {
    if (!initialize) {
      initializeContent();
      initialize = true;
    }

    return ChangeNotifierProvider(
      create: (_) => AddPatientModel(),
      child: Consumer<AddPatientModel>(builder: (BuildContext context, AddPatientModel model, Widget child) {
        return getMainView(model);
      }),
    );
  }

  Future myFutureMethodOverall(AddPatientModel model) async {
    Future<Map<String, dynamic>> future1 = model.getLocalization(["common", "addmember", "signup"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(AddPatientModel model) {
    return Scaffold(
      appBar: InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString("common", "common.label.add-a-member"),
        appBar: AppBar(),
        backButtonIcon: Icon(
          Icons.home,
          color: whiteColor,
        ),
        leadingBackButton: () {
          Navigator.pop(context);
        },
      ),
      body: SafeArea(
        child: Container(
          child: Builder(
            builder: (BuildContext context) {
              return Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              PPUIHelper.verticalSpaceLarge(),
                              // PPTexts.getHeading(LocalizationUtils.getSingleValueString("signup", "signup.almostdone.title-employer")),
                              // PPTexts.getDescription(LocalizationUtils.getSingleValueString("signup", "signup.almostdone.description-employer")),
                              pharmacistFlow(),
                              PPUIHelper.verticalSpaceLarge(),
                              PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("signup", "signup.fields.provience-label"), isBold: true),
                              PPUIHelper.verticalSpaceSmall(),
                              getDropDown(),
                              dropDownError == null ? SizedBox(height: 20) : PPTexts.getFormError(dropDownError, color: errorColor),
//                              PPUIHelper.verticalSpaceSmall(),
//                              getGenderUI(),
                              PPUIHelper.verticalSpaceSmall(),
                              getTakeMedicationUI(),
                              getManitobaCheck()
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Builder(
                    builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                      child: model.state == ViewState.Busy
                          ? ViewConstants.progressIndicator
                          : PrimaryButton(
                              text: LocalizationUtils.getSingleValueString("common", "common.button.continue"),
                              onPressed: () {
                                onNextClick(context, model);
                              },
                              fullWidth: true),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget getManitobaCheck() {
    if (dropDownValue == "manitoba") {
      return InkWell(
        onTap: () {
          setState(() {
            manitobaCheck = !manitobaCheck;
          });
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: Checkbox(
                  value: manitobaCheck,
                  activeColor: brandColor,
                  onChanged: (bool value) {
                    setState(() {
                      manitobaCheck = !manitobaCheck;
                    });
                  }),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Text(
                  LocalizationUtils.getSingleValueString("signup", "signup.fields.packpermission-label"),
                  overflow: TextOverflow.clip,
                  style: TextStyle(height: 1.4, color: secondaryColor, fontSize: 14, fontWeight: FontWeight.w400),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget pharmacistFlow() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Center(
          child: Avatar(displayImage: "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            LocalizationUtils.getSingleValueString("addmember", "addmember.almostdone.description"),
            style: TextStyle(
              color: lightBlue,
              fontWeight: FontWeight.w500,
              fontSize: 16.0,
              height: 1.5,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
      ],
    );
  }

  void onNextClick(context, AddPatientModel model) async {
    var connectivityResult = await model.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }
    if (_formKey.currentState.validate() && isFormValid()) {
      //&& genderRadioGroup.validate()
      //gender = getGenderEnglish(genderRadioGroup.getValue());
      province = dropDownValue;
      hasDailyMedication = selfMedicationRadioGroup.getValue() == LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase() ? true : false;
      var success = await model.updatePatientDetails(
          context: context,
          province: province,
          gender: widget.gender,
          selfMedication: hasDailyMedication,
          userPatient: widget.userPatient,
          pocketPacks: dropDownValue == "manitoba" ? manitobaCheck : null);
      if (success) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.member_transfer_new);
        Provider.of<DashboardModel>(context).clearUserPatientList();
        Navigator.pushReplacementNamed(context, TransferWidget.routeName,
            arguments: TransferArguments(
              source: BaseStepperSource.ADD_PATIENT,
              userPatient: widget.userPatient,
            ));
      } else
        onFail(context, errMessage: model.errorMessage);
    } else if (dropDownValue == null)
      setState(() {
        dropDownError = LocalizationUtils.getSingleValueString("signup", "signup.fields.select-province-error");
      });
  }

  Widget getDropDown() {
    if (dropDownError == null)
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        items: getStateMap().map((state) => DropdownMenuItem<String>(value: state["key"], child: Text(state["value"]))).toList(),
        onChanged: (selectedItem) => setState(() {
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
    else
      return PPFormFields.getDropDown(
        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.province"),
        fullWidth: true,
        value: dropDownValue,
        isError: true,
        items: getStateMap().map((state) => DropdownMenuItem<String>(value: state["key"], child: Text(state["value"]))).toList(),
        onChanged: (selectedItem) => setState(() {
          dropDownValue = selectedItem;
          setState(() {
            dropDownError = null;
          });
        }),
      );
  }

//  Widget getGenderUI() {
//    return genderRadioGroup;
//  }

  Widget getTakeMedicationUI() {
    return selfMedicationRadioGroup;
  }

  void initializeContent() {
//    genderRadioGroup = PPRadioGroup(
//      radioOptions: [
//        LocalizationUtils.getSingleValueString(
//                "signup", "signup.fields.gender-male")
//            .toUpperCase(),
//        LocalizationUtils.getSingleValueString(
//                "signup", "signup.fields.gender-female")
//            .toUpperCase(),
//        LocalizationUtils.getSingleValueString(
//                "signup", "signup.fields.gender-other")
//            .toUpperCase()
//      ],
//      labelText: LocalizationUtils.getSingleValueString(
//          "signup", "signup.fields.gender-label"),
//      errorText: LocalizationUtils.getSingleValueString(
//          "signup", "signup.fields.error-required"),
//    );

    selfMedicationRadioGroup = PPRadioGroup(
        radioOptions: [
          LocalizationUtils.getSingleValueString("common", "common.all.yes").toUpperCase(),
          LocalizationUtils.getSingleValueString("common", "common.all.no").toUpperCase()
        ],
        labelText: LocalizationUtils.getSingleValueString("addmember", "addmember.fields.medication-daily-other"),
        errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*");
  }
}

//getGenderEnglish(String value) {
//  if (value ==
//      LocalizationUtils.getSingleValueString(
//              "signup", "signup.fields.gender-male")
//          .toUpperCase()) {
//    return "MALE";
//  } else if (value ==
//      LocalizationUtils.getSingleValueString(
//              "signup", "signup.fields.gender-female")
//          .toUpperCase()) {
//    return "FEMALE";
//  } else if (value ==
//      LocalizationUtils.getSingleValueString(
//              "signup", "signup.fields.gender-other")
//          .toUpperCase()) {
//    return "OTHER";
//  }
//}
