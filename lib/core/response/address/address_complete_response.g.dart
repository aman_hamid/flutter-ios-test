// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_complete_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressCompleteResponse _$AddressCompleteResponseFromJson(
    Map<String, dynamic> json) {
  return AddressCompleteResponse(
    json['userMessage'] as String,
    json['success'] as bool,
    json['message'] as String,
    json['details'] == null
        ? null
        : AddressComplete.fromJson(json['details'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AddressCompleteResponseToJson(
        AddressCompleteResponse instance) =>
    <String, dynamic>{
      'userMessage': instance.userMessage,
      'success': instance.success,
      'message': instance.message,
      'details': instance.addressComplete,
    };
