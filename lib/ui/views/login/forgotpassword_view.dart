import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/viewmodels/login/forgot_password_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/loginverification_view.dart';
import 'package:pocketpills/ui/views/signup/verification_arguments.dart';
import 'package:pocketpills/ui/views/start/start_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:sms_autofill/sms_autofill.dart';

class ForgotPasswdWidget extends StatefulWidget {
  static const routeName = 'forgotpassword';

  final String phoneNumber;

  ForgotPasswdWidget({this.phoneNumber});

  @override
  State<StatefulWidget> createState() {
    return ForgotPasswdState();
  }
}

class ForgotPasswdState extends BaseState<ForgotPasswdWidget> {
  final TextEditingController _phoneNumberController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  FocusNode _phoneNumberFocusNode;

  @override
  void initState() {
    _phoneNumberFocusNode = FocusNode();
    _autoFillOtpInit();
    if (widget.phoneNumber != null && widget.phoneNumber != "") {
      _phoneNumberController.text = widget.phoneNumber;
    }
    super.initState();
  }

  void _autoFillOtpInit() async {
    await SmsAutoFill().listenForCode;
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ForgotPasswordModel>(
          create: (_) => ForgotPasswordModel(),
        )
      ],
      child: Consumer<ForgotPasswordModel>(builder: (BuildContext context, ForgotPasswordModel model, Widget child) {
        return FutureBuilder(
            future: myFutureMethodOverall(model),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Future myFutureMethodOverall(ForgotPasswordModel model) async {
    Future<Map<String, dynamic>> future1 = model.getLocalization(["forgot", "login"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(ForgotPasswordModel model) {
    return BaseScaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: MEDIUM_XXX),
          child: Builder(
            builder: (BuildContext context) {
              return SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: LARGE_XX,
                          ),
                          Image.asset(
                            getLogoImage(),
                            height: LARGE_XX,
                          ),
                          SizedBox(
                            height: LARGE_XX,
                          ),
                          Text(
                            LocalizationUtils.getSingleValueString("forgot", "forgot.all.title"),
                            style: REGULAR_XXX_PRIMARY_BOLD,
                          ),
                          SizedBox(
                            height: SMALL_X,
                          ),
                          Text(
                            LocalizationUtils.getSingleValueString("forgot", "forgot.all.description"),
                            style: MEDIUM_XXX_SECONDARY,
                          ),
                          SizedBox(
                            height: LARGE,
                          ),
                        ],
                      ),
                      getUsernameField(),
                      PPUIHelper.verticalSpaceLarge(),
                      model.state == ViewState.Busy
                          ? ViewConstants.progressIndicator
                          : Row(
                              children: <Widget>[
                                SecondaryButton(
                                  text: LocalizationUtils.getSingleValueString("forgot", "forgot.all.otp-back"),
                                  onPressed: () {
                                    Navigator.of(context).pushNamedAndRemoveUntil(StartView.routeName, (Route<dynamic> route) => false);
                                  },
                                ),
                                SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                                PrimaryButton(
                                  text: LocalizationUtils.getSingleValueString("common", "common.button.next"),
                                  onPressed: () {
                                    onNextPressed(context, model);
                                  },
                                ),
                              ],
                            ),
                      PPUIHelper.verticalSpaceLarge(),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  onNextPressed(context, model) async {
    var connectivityResult = await model.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: model.noInternetConnection);
      return;
    }
    if (_formKey.currentState.validate()) {
      var success = await model.getOtp(_phoneNumberController.text);
      if (success) {
        Navigator.pushNamed(
          context,
          LoginVerificationWidget.routeName,
          arguments: VerificationArguments(
            phoneNo: int.parse(_phoneNumberController.text),
          ),
        );
      } else
        onFail(context, errMessage: model.errorMessage);
    }
  }

  TextFormField getUsernameField() {
    return PPFormFields.getNumericFormField(
        focusNode: _phoneNumberFocusNode,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.phone,
        maxLength: 10,
        minLength: 10,
        labelText: LocalizationUtils.getSingleValueString("forgot", "forgot.all.phone-label"),
        errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
        controller: _phoneNumberController,
        onFieldSubmitted: (value) {},
        validator: (value) {
          if (value.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
          if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error");
          if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error2");
          ;
          return null;
        });
  }
}
