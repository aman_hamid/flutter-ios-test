import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/vitamins/shopping_cart_item.dart';

part 'shopping_cart.g.dart';

@JsonSerializable()
class ShoppingCart {
  int id;
  bool disabled;
  int version;
  int patientId;
  List<ShoppingCartItem> shoppingCartItems;
  int addressId;
  int ccId;
  num listPrice;
  num totalPrice;

  ShoppingCart(this.id, this.disabled, this.version, this.patientId, this.shoppingCartItems, this.addressId, this.ccId,
      this.listPrice, this.totalPrice);

  factory ShoppingCart.fromJson(Map<String, dynamic> json) => _$ShoppingCartFromJson(json);
}
