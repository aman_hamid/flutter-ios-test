import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class EmptyImageContainer extends StatelessWidget {
  final String iconText;
  final Function onTap;
  final double boxHeight;
  final double boxwidth;

  EmptyImageContainer({Key key, @required this.onTap, this.iconText = "", this.boxHeight = 148, this.boxwidth = 156}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 2),
        height: boxHeight,
        width: boxwidth,
        decoration: BoxDecoration(shape: BoxShape.rectangle, borderRadius: BorderRadius.all(Radius.circular(4.0))),
        child: DottedBorder(
          dashPattern: [6, 4],
          strokeWidth: 1,
          color: tertiaryColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                Icon(
                  Icons.add_a_photo,
                  color: primaryColor,
                ),
              ]),
              PPUIHelper.verticalSpaceSmall(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      iconText,
                      style: TextStyle(
                        fontSize: 16,
                        color: primaryColor,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
