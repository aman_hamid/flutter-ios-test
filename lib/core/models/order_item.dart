import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/rx.dart';

part 'order_item.g.dart';

@JsonSerializable()
class OrderItem {
  int id;
  int orderId;
  String status;
  String lastName;
  Rx rx;
  Rx refill;
  int refundId;
  num quantity;
  int medicationId;
  num shippedQuantity;
  String din;
  String drugName;
  String plan;

  OrderItem(
      {this.quantity,
      this.medicationId,
      this.din,
      this.id,
      this.status,
      this.lastName,
      this.drugName,
      this.orderId,
      this.plan,
      this.refill,
      this.refundId,
      this.rx,
      this.shippedQuantity});

  factory OrderItem.fromJson(Map<String, dynamic> json) => _$OrderItemFromJson(json);

  Map<String, dynamic> toJson() => _$OrderItemToJson(this);
}
