import 'package:enum_to_string/enum_to_string.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/dashboard/color_schema.dart';
import 'package:pocketpills/utils/route/dashboard_route.dart';

part 'dashboard_item.g.dart';

@JsonSerializable()
class DashboardItem {
  String title;
  String description;
  String action;
  String cardType;
  String icon;
  String tagName;
  String image;
  String itemId;
  int id;
  bool trackActionComplete;
  ColorSchema colorSchema;

  CardType getCardType() {
    return EnumToString.fromString(CardType.values, cardType);
  }

  ActionType getActionType() {
    return EnumToString.fromString(ActionType.values, action);
  }

  DashboardItem(this.title, this.description, this.action, this.cardType, this.icon, this.tagName, this.image,
      this.itemId, this.id, this.trackActionComplete, this.colorSchema);

  factory DashboardItem.fromJson(Map<String, dynamic> json) => _$DashboardItemFromJson(json);

  Map<String, dynamic> toJson() => _$DashboardItemToJson(this);
}
