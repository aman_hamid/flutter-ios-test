import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/models/telehealth_medical_need.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/response/appointment_slot_list_response.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/oms_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/telehealth/medicalCondition_suggestion.dart';
import 'package:pocketpills/core/response/telehealth/medicalCondition_suggestion_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_create_response.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference_bottom_sheet.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:async/async.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/response/insurance_response.dart';

class PreferenceModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  List<AppointmentTime> appointmentsList = [];
  static List<MedicalNeeds> players;
  ConnectivityResult connectivityResult;
  SuccessDetails successDetails;
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();

  Future<bool> sendPreferences(TelehealthRequest telehealthRequest) async {
    setState(ViewState.Busy);
    int prescriptionId = await _getPrescriptionId();
    Response response =
        await _api.updateTelehealth(prescriptionId, telehealthRequest);
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<TelehealthCreateResponse> baseResponse =
          BaseResponse<TelehealthCreateResponse>.fromJson(response.data);
      if (!baseResponse.status) {
        errorMessage = baseResponse.getErrorMessage();
        return false;
      }
      this.successDetails = baseResponse.response.successDetails;
      GlobalVariable().prescriptionId = baseResponse.response.omsPrescriptionId;
      GlobalVariable().userFlow = PrescriptionTypeConstant.TELEHEALTH;
      return true;
    } else {
      return false;
    }
  }

  AsyncMemoizer<Insurance> _memoizer = AsyncMemoizer();

  int prevPatientId;
  Future<Insurance> _futureInsurance;
  List<Patient> copyPrimaryInsuranceList;
  List<Patient> copySecondaryInsuranceList;

  Insurance curInsurance;
  String curImageUploading = "";

  Future<Insurance> fetchInsuranceData(int patientId) {
    if (prevPatientId != null && prevPatientId == patientId)
      return _futureInsurance;
    this._memoizer = AsyncMemoizer();
    _futureInsurance = this._memoizer.runOnce(() async {
      prevPatientId = patientId;
      curInsurance = await this.getPatientInsurance();
      return curInsurance;
    });
    return _futureInsurance;
  }

  Future<bool> sendPreferencesSignUp(
      TelehealthSignUpPreferenceRequest telehealthRequest) async {
    // int prescriptionId = await _getPrescriptionId();

    setState(ViewState.Busy);
    var response = await _api
        .uploadPrescriptionSignUp(RequestWrapper(body: telehealthRequest));
    setState(ViewState.Idle);
    if (response != null) {
    } else {
      return false;
    }
    setState(ViewState.Idle);
    return true;
  }

  showCallAndChatBottomSheet(
    BuildContext context,
    TelehealthSignUpPreferenceRequest telehealthRequestSignUp,
    TelehealthRequest telehealthRequest,
    PreferenceModel model,
    String from,
    SignUpTransferModel modelSignUp,
    UserPatient userPatient,
    BaseStepperSource source,
  ) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return PreferenceBottomSheet(context, telehealthRequest, model,
              telehealthRequestSignUp, from, modelSignUp, source, userPatient);
        });
  }

  Future<Insurance> getPatientInsurance() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }

    Response response = await _api.getPatientInsurance();
    if (response != null) {
      BaseResponse<InsuranceResponse> res =
          BaseResponse<InsuranceResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      curInsurance = res.response.insurance;
      notifyListeners();
      return curInsurance;
    } else {
      return null;
    }
  }

  Future<List<AppointmentTime>> getAppointmentDate() async {
    Response response = await _api.getAppointmentSlots();
    if (response != null) {
      OMSResponse<AppointmentSlotListResponse> res = OMSResponse<AppointmentSlotListResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      appointmentsList = res.response.items;
      return res.response.items;
    } else {
      return null;
    }
  }

  static Future loadPlayers() async {
    try {
      players = new List<MedicalNeeds>();
      String jsonString = await rootBundle.loadString('assets/players.json');
      Map parsedJson = json.decode(jsonString);
      var categoryJson = parsedJson['players'] as List;
      for (int i = 0; i < categoryJson.length; i++) {
        players.add(new MedicalNeeds.fromJson(categoryJson[i]));
      }
    } catch (e) {
      print(e);
    }
  }

  Future<String> setValue(String keyword) async {

      return keyword;
    }


  Future<List<MedicalConditionSuggestion>> searchMedicalConditions(String keyword) async {
    Response response = await _api.searchMedicalConditions(keyword);
    if (response != null) {
      BaseResponse<MedicalConditionSuggestionResponse> res = BaseResponse<MedicalConditionSuggestionResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      return res.response.items;
    } else {
      return null;
    }
  }

  Future<int> _getPrescriptionId() async {
    var response =
        await _api.getPrescriptionId(PrescriptionTypeConstant.TELEHEALTH);
    if (response != null &&
        response.data != null &&
        response.data['details'] != null &&
        response.data['details']['prescriptionId'] != null)
      return response.data['details']['prescriptionId'];
    return null;
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData =
            LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request =
              LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }

}
