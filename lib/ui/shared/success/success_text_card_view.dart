import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/signup/transaction_success_field.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';

class SuccessTextCardView extends BaseStatelessWidget {
  final TransactionSuccessField field;

  SuccessTextCardView(this.field);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: MEDIUM_X, bottom: MEDIUM_X, right: MEDIUM_XXX),
      child: Container(
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                color: bghighlight2,
                width: MEDIUM,
              ),
              SizedBox(
                width: MEDIUM_X,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: SMALL_XXX,
                    ),
                    Text(
                      field.title ?? "",
                      style: MEDIUM_XXX_PRIMARY_BOLD,
                    ),
                    SizedBox(
                      height: SMALL,
                    ),
                    Text(
                      field.description ?? "",
                      style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 6,
                    ),
                    SizedBox(
                      height: SMALL_XXX,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
    ;
  }
}
