import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/request/device_body.dart';

part 'reset_password_request.g.dart';

@JsonSerializable()

class ResetPasswordRequest extends BaseRequest{

  String password;
  String resetKey;
  int phone;
  DeviceBody loginBody;

  ResetPasswordRequest({this.password, this.resetKey, this.phone, this.loginBody});

  Map<String, dynamic> toJson() => _$ResetPasswordRequestToJson(this);

}