import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class StepperModel extends BaseModel {
  int currentStep = 0;
  BaseStepperSource source;
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  ConnectivityResult connectivityResult;
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();

  StepperModel({this.currentStep, this.source}) {
    this.source = source;
    currentStep = currentStep == null ? currentStep : currentStep;
    sendAnalytics();
  }

  setCurrentStep({int step = 0}) {
    currentStep = step;
    sendAnalytics();
    notifyListeners();
  }

  getCurrentStep() {
    if (currentStep != null) return currentStep;
    currentStep = 0;
    return 0;
  }

  resetWithoutNotify() {
    currentStep = 5;
    sendAnalytics();
    currentStep = 0;
  }

  incrCurrentStep() {
    currentStep++;
    sendAnalytics();
    notifyListeners();
  }

  decrCurrentStep() {
    currentStep--;
    sendAnalytics();
    notifyListeners();
  }

  sendAnalytics() {
    switch (currentStep) {
      case 0:
        sendInsuranceEvent();
        break;
      case 1:
        sendAddressEvent();
        break;
      case 2:
        sendPaymentEvent();
        break;
      case 3:
        sendHealthEvent();
        break;
      default:
        sendFinalEvent();
        break;
    }
  }

  sendHealthEvent() {
    switch (source) {
      case BaseStepperSource.TRANSFER_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_health);
        break;
      case BaseStepperSource.REFILL_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.refill_health);
        break;
      case BaseStepperSource.VITAMINS_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.vitamins_health);
        break;
      case BaseStepperSource.UPLOAD_PRESCRIPTION_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.upload_health);
        break;
      case BaseStepperSource.TELEHEALTH_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.telehealth_health);
        break;
      default:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.new_user_health);
        break;
    }
  }

  sendInsuranceEvent() {
    switch (source) {
      case BaseStepperSource.TRANSFER_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_insurance);
        break;
      case BaseStepperSource.REFILL_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.refill_insurance);
        break;
      case BaseStepperSource.VITAMINS_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.vitamins_insurance);
        break;
      case BaseStepperSource.UPLOAD_PRESCRIPTION_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.upload_insurance);
        break;
      case BaseStepperSource.TELEHEALTH_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.telehealth_insurance);
        break;
      default:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.new_user_insurance);
        break;
    }
  }

  sendAddressEvent() {
    switch (source) {
      case BaseStepperSource.TRANSFER_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_address);
        break;
      case BaseStepperSource.REFILL_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.refill_address);
        break;
      case BaseStepperSource.VITAMINS_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.vitamins_address);
        break;
      case BaseStepperSource.UPLOAD_PRESCRIPTION_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.upload_address);
        break;
      case BaseStepperSource.TELEHEALTH_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.telehealth_address);
        break;
      default:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.new_user_address);
        break;
    }
  }

  sendPaymentEvent() {
    switch (source) {
      case BaseStepperSource.TRANSFER_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_payment);
        break;
      case BaseStepperSource.REFILL_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.refill_payment);
        break;
      case BaseStepperSource.VITAMINS_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.vitamins_payment);
        break;
      case BaseStepperSource.UPLOAD_PRESCRIPTION_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.upload_payment);
        break;
      case BaseStepperSource.TELEHEALTH_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.telehealth_payment);
        break;
      default:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.new_user_payment);
        break;
    }
  }

  sendFinalEvent() {
    switch (source) {
      case BaseStepperSource.TRANSFER_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_final);
        break;
      case BaseStepperSource.REFILL_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.refill_final);
        break;
      case BaseStepperSource.VITAMINS_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.vitamins_final);
        break;
      case BaseStepperSource.UPLOAD_PRESCRIPTION_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.upload_final);
        break;
      case BaseStepperSource.TELEHEALTH_SCREEN:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.telehealth_final);
        break;
      default:
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.new_user_final);
        break;
    }
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
