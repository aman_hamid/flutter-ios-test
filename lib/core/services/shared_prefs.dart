import 'dart:convert';

import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/locator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataStoreService {
  static const String USERID = "user_id";
  static const String PATIENTID = "patient_id";
  static const String AUTHCOOKIE = "authorization_cookie";
  static const String AUTHTOKEN = "authorization_token";
  static const String IS_LOGGED_IN = "is_logged_in";
  static const String FIREBASE_TOKEN = "firebase_token";
  static const String RESET_KEY = "reset_key";
  static const String SET_PASSWORD_TOKEN = "set_password_token";
  static const String IS_APP_REVIEW = "is_app_review";
  static const String APP_REVIEW_CLICK_DATE = "review_click_date";
  static const String APP_VERSION = "app_version";
  static const String PHONE = "phone";
  static const String EMAIL = "email";
  static const String LANGUAGE = "locale";
  static const String LANGUAGE_KEYS = "locale_keys";
  static const String LANGUAGE_UPDATE = "locale_update";
  static const String FIRSTNAME = "firstname";
  static const String LASTNAME = "lastname";
  static const String DATE_OF_BIRTH = "date_of_birth";
  static const String GENDER = "gender";
  static const String ZIP = "zip";
  static const String SELECTED_PATIENT = "selected_patient";
  static const String ONESIGNAL_TOKEN = "onesignal_token";
  static const String ONESIGNAL_PLAYERID = "onesignal_playerid";

  static const String COMPLETE_SIGN_UP_STEPPER = "complete_sign_up_stepper";

  static const String HAS_PASSWORD = "has_password";
  static const String VERIFIED = "verified";

  static const String COMPLETE_TRANSFER_MODULE = "complete_transfer_module";
  static const String SIGNUP_TELEHEALTH = "signup_telehealth";
  static const String DASHBOARD_TELEHEALTH = "dashboard_telehealth";
  static const String SIGNUP_PRESCRIPTION = "signup_prescription";

  static const String COMPLETE_ABOUT_YOU = "complete_about_you";
  static const String COMPLETE_OTP_PASSWORD = "complete_otp_password";

  static const String PROVINCE = "province";

  static const String HAS_DAILY_MEDICATION = "has_daily_medication";

  static const String IS_CARE_GIVER = "is_care_giver";

  static const String COMPLETE_ALMOST_DONE = "complete_almost_done";

  static const String COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD =
      "complete_tele_healthcard";

  static const String TRANSFER_SKIPPED = "transfer_skipped";

  static const String TRANSFER_DONE = "transfer_done";

  static const String IS_SELECTED_PATIENT_CAREGIVER =
      "is_selected_patient_caregiver";

  static const String DOCTOR_NAME = "doctor_name";

  static const String APPOINTMENT_TIME = "appointment_time";

  static const String APPOINTMENT_DATE = "appointment_date";

  static const String REFERRAL_CODE = "referral_code";
  static const String INVITATION_CODE = "invitation_code";
  static const String UTM_CAMPAIGN = "utm_campaign";
  static const String UTM_MEDIUM = "utm_content";
  static const String UTM_SOURCE = "utm_source";
  static const String UTM_CONTENT = "utm_content";
  static const String UTM_TERM = "utm_term";
  static const String ADVERTISING_ID = "advertising_id";
  static const String IDFA_ID = "idfa_id";
  static const String DEVICE_ID = "idfa_id";
  static const String PP_DISTINCT_ID = "pp_distinct_id";
  static const String GET_STARTED_WITHOUT_PHONE_NUMBER =
      "get_started_without_phone_number";

  //CHAMBERS
  static const String GROUP = "group";
  static const String TOKEN = "token";
  static const String MAIL_VERIFIED = "mail_verified";
  static const String CHAMBERS_FLOW = 'chambers_flow';
  static const String REFRESH_DASHBOARD = 'refresh_dashboard';

  final prefs = locator<SharedPreferences>();

  Future<void> saveResetToken(String resetKey) async {
    await prefs.setString(RESET_KEY, resetKey);
  }

  Future<void> saveSetPasswordToken(String setKey) async {
    await prefs.setString(SET_PASSWORD_TOKEN, setKey);
  }

  Future<bool> logOut() async {
    return await prefs.clear();
  }

  Future<void> saveAuthorizationCookie(String cookie) async {
    await prefs.setString(AUTHCOOKIE, cookie);
  }

  Future<void> saveReviewClickDate(String reviewDate) async {
    await prefs.setString(APP_REVIEW_CLICK_DATE, reviewDate);
  }

  Future<void> saveAuthorizationToken(String token) async {
    await prefs.setString(AUTHTOKEN, token);
  }

  Future<void> logoutUser() async {
    await prefs.clear();
  }

  Future<void> savePatient(Patient selectedPatient) async {
    await prefs.setString(
        SELECTED_PATIENT, json.encode(selectedPatient.toJson()));
  }

  Future<void> markSelectedPatientAsCaregiver(bool mark) async {
    await prefs.setBool(IS_SELECTED_PATIENT_CAREGIVER, mark);
  }

  String getResetToken() {
    return prefs.getString(RESET_KEY);
  }

  String getReviewClickDateToken() {
    return prefs.getString(APP_REVIEW_CLICK_DATE);
  }

  String getSetPasswordToken() {
    return prefs.getString(SET_PASSWORD_TOKEN);
  }

  String getAuthorizationCookie() {
    return prefs.get(AUTHCOOKIE);
  }

  String getAuthorizationToken() {
    return prefs.get(AUTHTOKEN);
  }

  String getPatientZipCode() {
    return prefs.get(ZIP);
  }

  /// Generic Methods

  String readString(String key) {
    return prefs.get(key);
  }

  String readCheckedString(String key) {
    return prefs.get(key) != null ? prefs.get(key) : "";
  }

  bool readBoolean(String key) {
    return prefs.getBool(key) == null ? false : prefs.getBool(key);
  }

  bool readBooleanDefault(String key) {
    return prefs.getBool(key);
  }

  int readInteger(String key) {
    return prefs.getInt(key);
  }

  double readDouble(String key) {
    return prefs.getDouble(key);
  }

  Future<bool> clearSharedPrefs() async {
    return await prefs.clear();
  }

  Future<void> writeString(String key, String value) async {
    await prefs.setString(key, value);
  }

  Future<void> writeInt(String key, int value) async {
    await prefs.setInt(key, value);
  }

  Future<bool> writeBoolean(String key, bool value) async {
    return await prefs.setBool(key, value);
  }

  int getPatientId() {
    return readInteger(DataStoreService.PATIENTID);
  }

  int getUserId() {
    return readInteger(DataStoreService.USERID);
  }
}
