import 'package:flutter/material.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PPBottomBars {
  static getBottomBoxDecoration() {
    return BoxDecoration(color: Colors.white, boxShadow: <BoxShadow>[
      BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.11), offset: Offset(0, -3), blurRadius: 2.0)
    ]);
  }

  static getButtonedBottomBar({Widget child, double height = 80, bool showBorder = true}) {
    BoxDecoration bd;
    if (showBorder) bd = getBottomBoxDecoration();
    return BottomAppBar(
      color: Colors.white,
      elevation: 0.0,
      child: Container(
        decoration: bd,
        height: height,
        child: Padding(padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium), child: child),
      ),
    );
  }
}
