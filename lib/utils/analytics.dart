import 'dart:io';

import 'package:flutter_branch_io_plugin/flutter_branch_io_plugin.dart';
import 'package:package_info/package_info.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/facebook_analytics_events.dart';
import 'package:pocketpills/utils/mix_panel_analytics_events.dart';
import 'firebase_analytics_events.dart';
import 'package:device_info/device_info.dart';

class Analytics {
  FireBaseAnalyticsEvent _fireBaseAnalyticsEvent;
  MixPanelAnalyticsEvents _mixPanelAnalyticsEvents;
  FacebookAnalyticsEvents _facebookAnalyticsEvents;

  String _deviceId;
  String _deviceName;

  final DataStoreService dataStore = locator<DataStoreService>();

  Analytics() {
    _fireBaseAnalyticsEvent = FireBaseAnalyticsEvent();
    _mixPanelAnalyticsEvents = MixPanelAnalyticsEvents();
    _facebookAnalyticsEvents = FacebookAnalyticsEvents();
    _getDevicenfo();
    _initPackageInfo();
    setBranchDistinctID();
  }

  Future<void> sendAnalyticsEvent(String eventName, [Map<String, dynamic> bundle]) async {
    if (bundle == null) {
      bundle = Map();
      getAnalyticsParams(bundle);
    } else {
      bundle.addAll(bundle);
      getAnalyticsParams(bundle);
    }

    await _fireBaseAnalyticsEvent.sendAnalyticsEvent(eventName, bundle);
    await _mixPanelAnalyticsEvents.sendAnalyticsEvent(eventName, bundle);
    await _facebookAnalyticsEvents.sendAnalyticsEvent(eventName, bundle);
  }

  void getAnalyticsParams(Map<String, dynamic> bundle) {
    try {
      bundle["user_id"] = _getUserId() != null ? _getUserId().toString() : " ";
      bundle["device_id"] = _deviceId.toString();
      bundle["device_name"] = _deviceName.toString();
      bundle["patient_id"] = _getPatientId() != null ? _getPatientId().toString() : " ";
    } catch (ex) {}
  }

  Future<void> setUserIdentifiers() async {
    await _fireBaseAnalyticsEvent.setAnalyticsEventIdentify();
    await _mixPanelAnalyticsEvents.setAnalyticsEventIdentify();
  }

  Future<void> sendInitiatedCheckoutEvent({int prescriptionId = 0}) async {
    if (Platform.isIOS) {
      await _facebookAnalyticsEvents.sendInitiatedEvent(prescriptionId);
    } else {
      await _facebookAnalyticsEvents.sendInitiatedEvent(prescriptionId);
    }
  }

  Future<void> setPeopleProperties(Map map) async {
    await _mixPanelAnalyticsEvents.setMixpanelPeoplePeoperty(map);
  }

  Future<dynamic> flush() async {
    await _mixPanelAnalyticsEvents.flush();
  }

  Future<void> mixPanelIdentifier() async {
    await _mixPanelAnalyticsEvents.setMixpanelIdentifier();
  }

  Future<dynamic> reset() async {
    await _mixPanelAnalyticsEvents.reset();
    setBranchDistinctID();
  }

  Future<dynamic> logCompleteRegistrationEvent() async {
    await _facebookAnalyticsEvents.logCompleteRegistrationEvent();
  }

  String _getUserId() {
    return dataStore.getUserId() != null ? dataStore.getUserId().toString() : null;
  }

  String _getPatientId() {
    return dataStore.getPatientId() != null ? dataStore.getPatientId().toString() : null;
  }

  Future<void> _getDevicenfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      _deviceId = androidInfo.androidId;
      _deviceName = androidInfo.manufacturer + ' ' + androidInfo.model;
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      _deviceId = iosInfo.identifierForVendor;
      _deviceName = iosInfo.localizedModel;
    }
    dataStore.writeString(DataStoreService.DEVICE_ID, _deviceId);
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    dataStore.writeString(DataStoreService.APP_VERSION, info.version);
  }

  Future<void> setBranchDistinctID() async {
    String distinctID = await _mixPanelAnalyticsEvents.getMixpanelDistinctID();
    FlutterBranchIoPlugin.setRequestMetadata(distinctID);
  }

  Future<String> getMixpanelDistinctId() async {
    String distinctID = await _mixPanelAnalyticsEvents.getMixpanelDistinctID();
    return distinctID;
  }
}
