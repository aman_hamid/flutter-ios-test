import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/response/signup/transaction_successful_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/success/transaction_successful_model.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/appbars/singup_stepper_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class SignUpSuccessWidget extends StatefulWidget {
  static const routeName = 'signup/success';

  BaseStepperSource source;

  SignUpSuccessWidget({this.source = BaseStepperSource.UNKNOWN_SCREEN});

  @override
  State<StatefulWidget> createState() {
    return SignUpSuccessState();
  }
}

class SignUpSuccessState extends BaseState<SignUpSuccessWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserContactModel>(create: (_) => UserContactModel()),
        ChangeNotifierProvider<TransactionSuccessfulModel>(create: (_) => TransactionSuccessfulModel())
      ],
      child: Consumer2<TransactionSuccessfulModel, UserContactModel>(
        builder: (BuildContext context, TransactionSuccessfulModel successfulModel, UserContactModel userContactModel, Widget child) {
          return Scaffold(
              appBar: SignUpStepperAppBar(
                appBar: AppBar(),
              ),
              bottomNavigationBar: getBottomBar(successfulModel, userContactModel),
              body: FutureBuilder(
                future: successfulModel.getTransactionSuccessData(TransactionSuccessEnum.SIGN_UP_SUCCESS),
                builder: (BuildContext context, AsyncSnapshot<TransactionSuccessfulResponse> snapshot) {
                  if (successfulModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                    return NoInternetScreen(
                      onClickRetry: successfulModel.clearAsyncMemoizer,
                    );
                  }

                  if (snapshot.hasData && successfulModel.connectivityResult != ConnectivityResult.none) {
                    return afterFutureResolved(context, snapshot.data, userContactModel);
                  } else if (snapshot.hasError && successfulModel.connectivityResult != ConnectivityResult.none) {
                    Crashlytics.instance.log(snapshot.hasError.toString());
                    return ErrorScreen();
                  }

                  if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                    return LoadingScreen();
                  }
                  return PPContainer.emptyContainer();
                },
              ));
        },
      ),
    );
  }

  Widget afterFutureResolved(BuildContext context, TransactionSuccessfulResponse response, UserContactModel userContactModel) {
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        UserContactState().getMainView(userContactModel, response.successDetails),
        SizedBox(
          height: REGULAR_XXX,
        ),
      ],
    ));
  }

  Widget getBottomBar(TransactionSuccessfulModel successfulModel, UserContactModel userContactModel) {
    return Builder(
        builder: (BuildContext context) => PPTexts.getBottomBarHeading(
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(
                      height: SMALL_XX,
                    ),
                    (successfulModel.successfulResponse != null &&
                            successfulModel.successfulResponse.successDetails.helpText != null &&
                            successfulModel.successfulResponse.successDetails.helpText != "")
                        ? Text(
                            successfulModel.successfulResponse.successDetails.helpText ?? "",
                            style: MEDIUM_XX_PRIMARY,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          )
                        : PPContainer.emptyContainer(),
                    SizedBox(
                      height: MEDIUM_X,
                    ),
                    userContactModel.state == ViewState.Busy
                        ? ViewConstants.progressIndicator
                        : PrimaryButton(
                            fullWidth: true,
                            text: LocalizationUtils.getSingleValueString("common", "common.navbar.continue"),
                            onPressed: () {
                              Provider.of<SignUpModel>(context).markSummarySeen();
                              getNextRoute(userContactModel);
                            },
                          ),
                    SizedBox(
                      height: MEDIUM_XXX,
                    ),
                  ],
                ),
              ),
            ));
  }

  getNextRoute(UserContactModel userContactModel) async {
    await userContactModel.putContact();
    Navigator.of(context).pushNamedAndRemoveUntil(OrderStepper.routeName, (Route<dynamic> route) => false, arguments: BaseStepperArguments());
  }
}
