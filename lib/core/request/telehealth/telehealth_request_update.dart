import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'telehealth_request_update.g.dart';

@JsonSerializable()
class TelehealthRequestUpdate extends BaseRequest {
  int id;
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime appointmentTime;

  TelehealthRequestUpdate(
      {this.id,
      this.appointmentTime,
      });

  factory TelehealthRequestUpdate.fromJson(Map<String, dynamic> data) =>
      _$TelehealthRequestUpdateFromJson(data);

  Map<String, dynamic> toJson() => _$TelehealthRequestUpdateToJson(this);
}
