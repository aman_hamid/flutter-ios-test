// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'color_schema.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ColorSchema _$ColorSchemaFromJson(Map<String, dynamic> json) {
  return ColorSchema(
    name: json['name'],
  );
}

Map<String, dynamic> _$ColorSchemaToJson(ColorSchema instance) =>
    <String, dynamic>{
      'name': instance.name,
    };
