import 'package:flutter/material.dart';
import 'package:pocketpills/core/dashboard/dashboard_item.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class DashboardLongButtonCardView extends StatelessWidget {
  final DashboardItem dashboardItem;
  final HomeModel homeModel;

  DashboardLongButtonCardView({@required this.dashboardItem, @required this.homeModel});

  @override
  Widget build(BuildContext context) {
    return PPCard(
      onTap: () async {
        homeModel.handleDashboardRoute(dashboardItem.getActionType(), context, dashboardItem: dashboardItem);
        if (dashboardItem.trackActionComplete == true) {
          await homeModel.trackDashboardCard(dashboardItem.id);
        }
      },
      cardColor: lightBlueColor,
      margin: EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM),
      padding: EdgeInsets.only(left: MEDIUM_X, top: MEDIUM_X, bottom: MEDIUM_X),
      child: Container(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  dashboardItem.title ?? "",
                  style: MEDIUM_XXX_PRIMARY_BOLD,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                ),
                SizedBox(
                  height: SMALL_X,
                ),
                Text(
                  dashboardItem.description ?? "",
                  style: MEDIUM_XX_PRIMARY,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                ),
              ],
            ),
          ),
          FlatButton(
            color: Colors.transparent,
            child: getSelectedLanguage() == ViewConstants.languageIdEn
                ? Text(
                    dashboardItem.tagName ?? "",
                    style: MEDIUM_XX_LINK,
                  )
                : Icon(Icons.arrow_forward_ios_rounded, color: linkColor),
            onPressed: () {
              homeModel.handleDashboardRoute(dashboardItem.getActionType(), context, dashboardItem: dashboardItem);
            },
          )
        ],
      )),
    );
  }
}
