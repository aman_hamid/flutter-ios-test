import 'package:flutter/material.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
export 'package:pocketpills/utils/analytics_event_constant.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T> {
  String errMessage = "Please address the issues above to proceed.";
  String successMessage = "Action completed successfully.";
  Widget content;
  bool showingError = false;

  final Analytics analyticsEvents = locator<Analytics>();
  final DataStoreService dataStore = locator<DataStoreService>();

  @override
  void dispose() {
    analyticsEvents.flush();
    super.dispose();
  }

  onFail(context, {errMessage, content}) {
    if (errMessage == null || errMessage == "") errMessage = LocalizationUtils.getSingleValueString("common", "common.label.please-address-issue");
    if (content == null) content = Text(errMessage);
    final snackBar = SnackBar(
      content: content,
      duration: const Duration(seconds: 3),
    );
    Scaffold.of(context).removeCurrentSnackBar();
    Scaffold.of(context).showSnackBar(snackBar);
  }

  showOnSnackBar(context, {successMessage}) {
    if (successMessage == null || successMessage == "") successMessage = LocalizationUtils.getSingleValueString("common", "common.label.action-completed-msg");
    final snackBar = SnackBar(
      content: Text(successMessage),
      duration: const Duration(seconds: 3),
    );
    Scaffold.of(context).removeCurrentSnackBar();
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
