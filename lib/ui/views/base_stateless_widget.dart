import 'package:flutter/material.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

abstract class BaseStatelessWidget extends StatelessWidget {
  final String errMessage = "Please address the issues above to proceed.";
  final String successMessage = "Action completed successfully.";

  BaseStatelessWidget({Key key}) : super(key: key);

  final Analytics analyticsEvents = locator<Analytics>();

  onFail(context, {errMessage}) {
    if (errMessage == null || errMessage == "") errMessage = LocalizationUtils.getSingleValueString("common", "common.label.please-address-issue");
    final snackBar = SnackBar(
      content: Text(errMessage),
      duration: const Duration(seconds: 10),
    );
    Scaffold.of(context).removeCurrentSnackBar();
    Scaffold.of(context).showSnackBar(snackBar);
  }

  showOnSnackBar(context, {successMessage}) {
    if (successMessage == null || successMessage == "") successMessage = LocalizationUtils.getSingleValueString("common", "common.label.action-completed-msg");
    final snackBar = SnackBar(
      content: Text(successMessage),
      duration: const Duration(seconds: 5),
    );
    Scaffold.of(context).removeCurrentSnackBar();
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
