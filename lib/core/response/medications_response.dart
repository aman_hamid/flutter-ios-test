import 'package:json_annotation/json_annotation.dart';

import 'medication.dart';

part 'medications_response.g.dart';

@JsonSerializable()
class MedicationsResponse{
  Map<String, List<Medication>> medicines;

  MedicationsResponse(this.medicines);
  factory MedicationsResponse.fromJson(Map<String, dynamic> json) =>
      _$MedicationsResponseFromJson(json);
}