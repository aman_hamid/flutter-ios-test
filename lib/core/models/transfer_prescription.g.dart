// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transfer_prescription.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransferPrescription _$TransferPrescriptionFromJson(Map<String, dynamic> json) {
  return TransferPrescription(
    json['prescriptionState'] as String,
    json['pharmacyName'] as String,
    json['pharmacyAddress'] as String,
    json['pharmacyPhone'] as int,
    json['omsPrescriptionId'] as int,
  );
}

Map<String, dynamic> _$TransferPrescriptionToJson(
        TransferPrescription instance) =>
    <String, dynamic>{
      'pharmacyName': instance.pharmacyName,
      'pharmacyAddress': instance.pharmacyAddress,
      'pharmacyPhone': instance.pharmacyPhone,
      'prescriptionState': instance.prescriptionState,
      'omsPrescriptionId': instance.omsPrescriptionId,
    };
