// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prescription.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Prescription _$PrescriptionFromJson(Map<String, dynamic> json) {
  return Prescription(
    json['id'] as int,
    json['createDateTime'] == null
        ? null
        : DateTime.parse(json['createDateTime'] as String),
    json['type'] as String,
    json['status'] as String,
    json['exPharmacyName'] as String,
    json['exPharmacyPhone'] as String,
    json['exPharmacyAddress'] as String,
    json['doctorsName'] as String,
    (json['mappedRxs'] as List)
        ?.map((e) => e == null
            ? null
            : PrescriptionMedication.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['documents'] as List)
        ?.map((e) => e == null
            ? null
            : PrescriptionDocument.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$PrescriptionToJson(Prescription instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createDateTime': instance.createDateTime?.toIso8601String(),
      'type': instance.type,
      'status': instance.status,
      'exPharmacyName': instance.exPharmacyName,
      'exPharmacyPhone': instance.exPharmacyPhone,
      'exPharmacyAddress': instance.exPharmacyAddress,
      'doctorsName': instance.doctorsName,
      'mappedRxs': instance.medications,
      'documents': instance.documents,
    };
