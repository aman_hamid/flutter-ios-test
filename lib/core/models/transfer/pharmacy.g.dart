// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pharmacy.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pharmacy _$PharmacyFromJson(Map<String, dynamic> json) {
  return Pharmacy(
    pharmacyName: json['pharmacyName'] as String,
    pharmacyPlaceId: json['pharmacyPlaceId'] as String,
    pharmacyAddress: json['pharmacyAddress'] as String,
  );
}

Map<String, dynamic> _$PharmacyToJson(Pharmacy instance) => <String, dynamic>{
      'pharmacyName': instance.pharmacyName,
      'pharmacyAddress': instance.pharmacyAddress,
      'pharmacyPlaceId': instance.pharmacyPlaceId,
    };
