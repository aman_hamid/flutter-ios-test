import 'package:json_annotation/json_annotation.dart';
part 'address_delete_response.g.dart';

@JsonSerializable()
class AddressDeleteResponse{

  bool isDeleted;

  AddressDeleteResponse({this.isDeleted});

  factory AddressDeleteResponse.fromJson(Map<String, dynamic> json) => _$AddressDeleteResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AddressDeleteResponseToJson(this);

}
