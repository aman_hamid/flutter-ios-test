import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/response/address/pincode_suggestion.dart';

import 'medicalCondition_suggestion.dart';

part 'medicalCondition_suggestion_response.g.dart';

@JsonSerializable()
class MedicalConditionSuggestionResponse {
  @JsonKey(name: 'items')
  List<MedicalConditionSuggestion> items;

  MedicalConditionSuggestionResponse(this.items);

  factory MedicalConditionSuggestionResponse.fromJson(Map<String, dynamic> json) => _$MedicalConditionSuggestionResponseFromJson(json);
}
