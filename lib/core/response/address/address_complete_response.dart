import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/response/address/address_complete.dart';
part 'address_complete_response.g.dart';

@JsonSerializable()
class AddressCompleteResponse {
  @JsonKey(name: 'userMessage')
  String userMessage;

  @JsonKey(name: 'success')
  bool success;

  @JsonKey(name: 'message')
  String message;

  @JsonKey(name: 'details')
  AddressComplete addressComplete;

  AddressCompleteResponse(this.userMessage, this.success, this.message, this.addressComplete);

  factory AddressCompleteResponse.fromJson(Map<String, dynamic> json) => _$AddressCompleteResponseFromJson(json);
}
