import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/payment_card.dart';
part 'patient_card_response.g.dart';

@JsonSerializable()
class PatientCardResponse {
  List<PaymentCard> cards;

  PatientCardResponse({this.cards});

  factory PatientCardResponse.fromJson(Map<String, dynamic> json) => _$PatientCardResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PatientCardResponseToJson(this);
}
