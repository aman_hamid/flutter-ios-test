import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/analytics_event.dart';

class FireBaseAnalyticsEvent implements AnalyticsEvents {
  static FirebaseAnalytics _firebaseAnalytics;
  final DataStoreService dataStore = locator<DataStoreService>();

  FireBaseAnalyticsEvent() {
    _firebaseAnalytics = FirebaseAnalytics();
  }

  static FirebaseAnalyticsObserver getFireBaseObserver() {
    return FirebaseAnalyticsObserver(analytics: _firebaseAnalytics);
  }

  @override
  Future<void> sendAnalyticsEvent(String eventName, [Map<String, dynamic> bundle]) async {
    await _firebaseAnalytics.logEvent(name: eventName, parameters: bundle);
  }

  @override
  Future<void> setAnalyticsEventIdentify() async {
    if (dataStore.getUserId() != null) {
      _firebaseAnalytics.setAnalyticsCollectionEnabled(true);
      _firebaseAnalytics.setUserId(dataStore.getUserId().toString());
    }
  }
}
