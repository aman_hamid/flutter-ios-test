import 'package:pocketpills/core/models/user_patient.dart';

class ConsentArguments {
  final UserPatient userPatient;

  ConsentArguments({this.userPatient});
}
