// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) {
  return Order(
    orderPayment: json['orderPayment'] == null
        ? null
        : OrderPayment.fromJson(json['orderPayment'] as Map<String, dynamic>),
    status: json['status'] as String,
    patientId: json['patientId'] as int,
    patient: json['patient'] == null
        ? null
        : Patient.fromJson(json['patient'] as Map<String, dynamic>),
    pharmacyCode: json['pharmacyCode'] as String,
    address: json['address'] == null
        ? null
        : Address.fromJson(json['address'] as Map<String, dynamic>),
    caregiver: json['caregiver'] == null
        ? null
        : User.fromJson(json['caregiver'] as Map<String, dynamic>),
    caregiverId: json['caregiverId'] as int,
    consultationBy: json['consultationBy'] as String,
    consultationOn: PPDateUtils.fromStr(json['consultationOn'] as String),
    consultationStatus: json['consultationStatus'] as String,
    createdBy: json['createdBy'] as String,
    createdDateTime: PPDateUtils.fromStr(json['createdDateTime'] as String),
    dueDate: PPDateUtils.fromStr(json['dueDate'] as String),
    id: json['id'] as int,
    modifiedBy: json['modifiedBy'] as String,
    notes: json['notes'] as String,
    orderedDateTime: PPDateUtils.fromStr(json['orderedDateTime'] as String),
    orderItems: (json['orderItems'] as List)
        ?.map((e) =>
            e == null ? null : OrderItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    orderNo: json['orderNo'] as String,
    pharmacyId: json['pharmacyId'] as int,
    shipments: (json['shipments'] as List)
        ?.map((e) =>
            e == null ? null : Shipment.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    transactions: (json['transactions'] as List)
        ?.map((e) =>
            e == null ? null : Transaction.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    updatedDateTime: PPDateUtils.fromStr(json['updatedDateTime'] as String),
  );
}

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      'dueDate': PPDateUtils.toStr(instance.dueDate),
      'id': instance.id,
      'orderNo': instance.orderNo,
      'patientId': instance.patientId,
      'patient': instance.patient,
      'caregiver': instance.caregiver,
      'caregiverId': instance.caregiverId,
      'pharmacyId': instance.pharmacyId,
      'pharmacyCode': instance.pharmacyCode,
      'address': instance.address,
      'status': instance.status,
      'orderedDateTime': PPDateUtils.toStr(instance.orderedDateTime),
      'orderItems': instance.orderItems,
      'shipments': instance.shipments,
      'notes': instance.notes,
      'transactions': instance.transactions,
      'orderPayment': instance.orderPayment,
      'createdDateTime': PPDateUtils.toStr(instance.createdDateTime),
      'updatedDateTime': PPDateUtils.toStr(instance.updatedDateTime),
      'createdBy': instance.createdBy,
      'modifiedBy': instance.modifiedBy,
      'consultationBy': instance.consultationBy,
      'consultationOn': PPDateUtils.toStr(instance.consultationOn),
      'consultationStatus': instance.consultationStatus,
    };
