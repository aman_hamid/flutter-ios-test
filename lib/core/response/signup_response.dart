import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/signup_dto.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';

import 'package:pocketpills/core/response/base_response.dart';
part 'signup_response.g.dart';

@JsonSerializable()
class SignupResponse extends BaseResponse {
  @JsonKey(name: 'userId')
  int userId;

  @JsonKey(name: 'patientId')
  int patientId;

  @JsonKey(name: 'prescription')
  TransferPrescription prescription;

  @JsonKey(name: 'signupDto')
  SignupDto signupDto;

  SignupResponse(this.userId, this.patientId, this.prescription, this.signupDto);

  factory SignupResponse.fromJson(Map<String, dynamic> json) => _$SignupResponseFromJson(json);
}
