import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/vitamins/coupon.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

part 'coupon_list_response.g.dart';

@JsonSerializable()
class CouponListResponse {
  @JsonKey(name: 'success')
  bool status;

  @JsonKey(name: 'userMessage')
  String errMessage;

  @JsonKey(name: 'message')
  String apiMessage;

  @JsonKey(name: 'details')
  List<Coupon> coupons;

  CouponListResponse({this.status, this.errMessage, this.apiMessage, this.coupons});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory CouponListResponse.fromJson(Map<String, dynamic> json) => _$CouponListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CouponListResponseToJson(this);
}
