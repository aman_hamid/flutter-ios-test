import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'chambers_active_body.g.dart';

@JsonSerializable()
class ChambersRequest extends BaseRequest {
  String token;
  String group;
  String userIdentifier;

  ChambersRequest({
    this.token,
    this.group,
    this.userIdentifier,
  });

  factory ChambersRequest.fromJson(Map<String, dynamic> data) =>
      _$ChambersRequestFromJson(data);

  Map<String, dynamic> toJson() => _$ChambersRequestToJson(this);
}
