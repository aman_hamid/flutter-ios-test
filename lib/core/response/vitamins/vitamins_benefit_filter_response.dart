import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/vitamins/item_group_filter_list.dart';
part 'vitamins_benefit_filter_response.g.dart';

@JsonSerializable()
class VitaminsBenefitFilterResponse {
  List<ItemGroupFilterList> itemGroupFilterList;

  VitaminsBenefitFilterResponse(this.itemGroupFilterList);

  factory VitaminsBenefitFilterResponse.fromJson(Map<String, dynamic> json) =>
      _$VitaminsBenefitFilterResponseFromJson(json);
}
