import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'signup_request.g.dart';

@JsonSerializable()
class SignupRequest extends BaseRequest {
  String firstName;
  String lastName;
  @JsonKey(name: "birthDate")
  String dob;
  String gender;

  SignupRequest({this.firstName, this.dob, this.lastName, this.gender});

  factory SignupRequest.fromJson(Map<String, dynamic> json) => _$SignupRequestFromJson(json);

  Map<String, dynamic> toJson() => _$SignupRequestToJson(this);
}
