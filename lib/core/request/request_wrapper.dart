import 'package:meta/meta.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/string_utils.dart';

class RequestWrapper<T extends BaseRequest> {
  String requestId;
  T body;
  ClientDetails clientDetails = ClientDetails();

  RequestWrapper({@required this.body});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> jsonMap = Map();
    jsonMap['id'] = StringUtils.generateRandomRequestId();
    jsonMap['body'] = (body == null)
        ? {}
        : body is List
            ? body
            : body.toJson();
    jsonMap['clientDetails'] = clientDetails.toJson();
    return jsonMap;
  }
}

class ClientDetails {
  int userId;
  int patientId;
  int clientId;
  final DataStoreService dataStore = locator<DataStoreService>();

  ClientDetails() {
    int u = dataStore.getUserId();
    userId = u == null ? -1 : u;
    int p = dataStore.getPatientId();
    patientId = p == null ? -1 : p;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> jsonMap = Map();
    jsonMap['userId'] = this.userId.toString();
    jsonMap['patientId'] = this.patientId.toString();
    jsonMap['clientUserId'] = -1;
    return jsonMap;
  }
}
