import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/request/medication_request.dart';

part 'refill_request.g.dart';

@JsonSerializable(explicitToJson: true)
class RefillRequest extends BaseRequest {
  String comment;
  List<MedicationRequest> medications;

  RefillRequest({this.comment, this.medications});

  Map<String, dynamic> toJson() => _$RefillRequestToJson(this);
}
