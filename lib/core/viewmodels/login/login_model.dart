import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:package_info/package_info.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/login_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/phone_verify_response.dart';
import 'package:pocketpills/core/response/signup_phonenumber_response.dart';
import 'package:pocketpills/core/response/telehealth/localization_list_response.dart';
import 'package:pocketpills/core/response/telehealth/localization_update_response.dart';
import 'package:pocketpills/core/response/version_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:sms_autofill/sms_autofill.dart';

class LoginModel extends BaseModel {
  final LoginService loginService = locator<LoginService>();
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<VersionResponse> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();
  AsyncMemoizer<List<LocalizationUpdateResponse>> contentUpdateMemoizer = AsyncMemoizer();

  ConnectivityResult connectivityResult;

  Future<bool> login(String phoneNumber, String password) async {
    setState(ViewState.Busy);
    Response response = await loginService.login(phoneNumber, password);
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  cleanMemorizer() {
    _memoizer = AsyncMemoizer();
    contentUpdateMemoizer = AsyncMemoizer();
    notifyListeners();
  }

  Future<PhoneVerification> verifyPhone(String phoneNo) async {
    setState(ViewState.Busy);
    var response = await _api.checkPhone(phoneNo);
    setState(ViewState.Idle);
    if (response != null) {
      PhoneVerifyResponse res = PhoneVerifyResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      await dataStore.writeInt(DataStoreService.USERID, res.response.userId);
      await dataStore.writeString(DataStoreService.PHONE, phoneNo);
      await analyticsEvents.mixPanelIdentifier();
      return res.response;
    } else {
      return null;
    }
  }

  Future<SignupPhoneNumberResponse> phoneNumberSignup(String phoneNo) async {
    setState(ViewState.Busy);
    var response = await _api.phoneNumberSignup(phoneNo);
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<SignupPhoneNumberResponse> res = BaseResponse<SignupPhoneNumberResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      await dataStore.writeInt(DataStoreService.USERID, res.response.userId);
      return res.response;
    } else {
      return null;
    }
  }

  Future<bool> getOtp(String phoneNo) async {
    setState(ViewState.Busy);
    await SmsAutoFill().listenForCode;
    var response = await _api.getOtp(phoneNo);
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<LoginResponse> res = BaseResponse<LoginResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  Future<VersionResponse> onVersionCallCheck() async {
    return this._memoizer.runOnce(() async {
      PackageInfo version = await PackageInfo.fromPlatform();
      String platform = Platform.isAndroid ? "android" : "ios";
      connectivityResult = await checkInternet();
      if (connectivityResult == ConnectivityResult.none) {
        return null;
      }
      Response response = await _api.checkVersion(version.version, platform);
      if (response != null) {
        VersionResponse res = VersionResponse.fromJson(response.data);
        return res;
      }
      return null;
    });
  }

  Future<List<LocalizationUpdateResponse>> checkContentEngineUpdates() async {
    return this.contentUpdateMemoizer.runOnce(() async {
      connectivityResult = await checkInternet();
      if (connectivityResult == ConnectivityResult.none) {
        return null;
      }
      var response = await _api.getUpdatesContentEngine();
      if (response != null) {
        BaseResponse<LocalizationListResponse> res = BaseResponse<LocalizationListResponse>.fromJson(response.data);
        var data = res.response;
        return LocalizationUtils.saveUpdateDate(data.dataList);
      } else
        return null;
    });
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
