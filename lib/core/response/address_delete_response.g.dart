// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_delete_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressDeleteResponse _$AddressDeleteResponseFromJson(
    Map<String, dynamic> json) {
  return AddressDeleteResponse(
    isDeleted: json['isDeleted'] as bool,
  );
}

Map<String, dynamic> _$AddressDeleteResponseToJson(
        AddressDeleteResponse instance) =>
    <String, dynamic>{
      'isDeleted': instance.isDeleted,
    };
