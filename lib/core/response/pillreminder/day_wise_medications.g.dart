// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'day_wise_medications.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DayWiseMedications _$DayWiseMedicationsFromJson(Map<String, dynamic> json) {
  return DayWiseMedications(
    json['date'] as String,
    json['firstName'] as String,
    json['title'] as String,
    json['missedCount'] as int,
    json['takenCount'] as int,
    json['pendingCount'] as int,
    (json['pocketPackDetails'] as List)
        ?.map((e) => e == null
            ? null
            : PocketPackDetails.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$DayWiseMedicationsToJson(DayWiseMedications instance) =>
    <String, dynamic>{
      'date': instance.date,
      'firstName': instance.firstName,
      'title': instance.title,
      'missedCount': instance.missedCount,
      'takenCount': instance.takenCount,
      'pendingCount': instance.pendingCount,
      'pocketPackDetails': instance.pocketPackDetails,
    };
