import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PrimaryButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final bool fullWidth;
  final bool isSmall;
  final IconData iconData;
  final bool isExpanded;
  final Color butttonColor;
  final bool disabled;
  PrimaryButton(
      {@required this.text,
      @required this.onPressed,
      this.fullWidth = false,
      this.iconData = null,
      this.butttonColor = brandColor,
      this.isExpanded = true,
      this.disabled = false,
      this.isSmall});

  @override
  Widget build(BuildContext context) {
    double fontSize = isSmall == null ? PPUIHelper.FontSizeMedium : PPUIHelper.FontSizeSmall;

    Widget buttonChild = Text(
      this.text.toUpperCase(),
      style: TextStyle(fontSize: fontSize, color: whiteColor),
      textAlign: TextAlign.center,
    );
    if (this.iconData != null)
      buttonChild = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            this.text.toUpperCase(),
            style: TextStyle(fontSize: fontSize, color: whiteColor),
            textAlign: TextAlign.center,
          ),
          SizedBox(width: 8.0),
          new Icon(this.iconData, size: 24, color: whiteColor),
        ],
      );

    Widget button = RaisedButton(
      color: butttonColor,
      disabledColor: secondaryColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SMALL_XX),
      ),
      child: buttonChild,
      onPressed: this.onPressed,
    );
    if (isExpanded) button = Expanded(child: button);
    if (!this.fullWidth)
      return button;
    else
      return Container(
        color: Colors.white,
        child: Row(
          children: <Widget>[button],
        ),
      );
  }
}
