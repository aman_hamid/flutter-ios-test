import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/order_item.dart';

part 'shipment_item.g.dart';

@JsonSerializable()
class ShipmentItem {
  int id;
  num quantity;
  OrderItem orderItem;
  ShipmentItem({this.quantity, this.id, this.orderItem});

  factory ShipmentItem.fromJson(Map<String, dynamic> json) => _$ShipmentItemFromJson(json);

  Map<String, dynamic> toJson() => _$ShipmentItemToJson(this);
}
