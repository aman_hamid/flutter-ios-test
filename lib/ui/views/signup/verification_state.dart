import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/viewmodels/signup/verification_model.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

abstract class VerificationState<T extends StatefulWidget> extends BaseState<T> {
  final formKey = GlobalKey<FormState>();
  final List<TextEditingController> otpNums = [TextEditingController(), TextEditingController(), TextEditingController(), TextEditingController()];
  List<String> otpLengths = ["", "", "", ""];
  List<String> prevOtpLengths = ["", "", "", ""];
  List<FocusNode> otpFocusNodes = [FocusNode(), FocusNode(), FocusNode(), FocusNode()];
  Timer _timer;
  int start = 30;
  VerificationModel innerModel;
  BuildContext innerContext;
  String lastNum;
  bool stopTimer = false;

  @override
  void initState() {
    super.initState();
    this.startTimer();
//    for (int i = 0; i < 4; i++) {
//      //otpNums[i].addListener(shiftFocus(i));
//      otpNums[i].addListener(() {
//        if (otpLengths[i] != otpNums[i].text) {
//          otpLengths[i] = otpNums[i].text;
//          if (otpNums[i].text.length == 1) {
//            if (i < 3)
//              FocusScope.of(context).requestFocus(otpFocusNodes[i + 1]);
//          } else if (otpNums[i].text.length == 0) {
//            if (i != 0)
//              FocusScope.of(context).requestFocus(otpFocusNodes[i - 1]);
//          }
//        }
//      });
//      otpNums[i].addListener(areOtpComplete);
//    }
  }

  @override
  void dispose() {
    for (int i = 0; i < 4; i++) {
      //otpNums[i].removeListener(shiftFocus(i));
      // otpNums[i].removeListener(areOtpComplete);
    }

    _timer.cancel();
    super.dispose();
  }

  shiftFocus(int i) {
    print("i is - " + i.toString());
    if (otpNums[i].text.length == 1 && otpLengths[i] != otpNums[i].text) {
      otpLengths[i] = otpNums[i].text;
      if (i < 3) FocusScope.of(context).requestFocus(otpFocusNodes[i + 1]);
    }
  }

//  void areOtpComplete() {
//    if (innerModel == null || innerContext == null) return;
//    var model = innerModel;
//    int count = 0;
//    String curNum = "";
//    for (int i = 0; i < 4; i++) {
//      if (otpNums[i].text.length == 1) {
//        count++;
//        curNum += otpNums[i].text;
//      }
//    }
//    if (count == 4 && curNum != lastNum) {
//      lastNum = curNum;
//      print(" SENDING BACKEND CALL ");
//      Future.delayed(
//        const Duration(milliseconds: 100),
//        () {
//          onPressed(innerContext, model );
//        },
//      );
//    }else if(count<4)
//      lastNum="";
//  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = Timer.periodic(oneSec, (Timer timer) {
      if (start < 1 || stopTimer == true)
        _timer.cancel();
      else {
        setState(() {
          start--;
        });
      }
    });
  }

  submitOtp(String otp) {
    if (innerModel == null || innerContext == null) return;
    var model = innerModel;
    Future.delayed(
      const Duration(milliseconds: 100),
      () {
        onPressed(innerContext, model, otp);
      },
    );
  }

  onPressed(BuildContext context, VerificationModel model, String otp) async {}

  List<Widget> getOTPBoxes() {
    List<Widget> ret = [];
    for (int i = 0; i < 4; i++) {
      Widget ch = PPFormFields.getNumericFormField(
          textInputAction: i == 3 ? TextInputAction.done : TextInputAction.next,
          centeredText: true,
          maxLength: 1,
          controller: otpNums[i],
          focusNode: otpFocusNodes[i],
          decoration: PPInputDecor.getDecoration(labelText: '', hintText: "", helperText: ""));
      ret.add(Expanded(child: ch));
      if (i != 3) ret.add(SizedBox(width: PPUIHelper.HorizontalSpaceMedium));
    }
    return ret;
  }

  Widget getBottomWidget(BuildContext context) {
    return PPBottomBars.getButtonedBottomBar(
      height: 160,
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          PPUIHelper.verticalSpaceMedium(),
          PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("forgot", "forgot.all.otp-resend")),
          PPTexts.getDescription(LocalizationUtils.getSingleValueString("forgot", "forgot.all.resend-or-call")),
          PPUIHelper.verticalSpaceLarge(),
          innerModel.state == ViewState.Busy
              ? CircularProgressIndicator()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          start > 0
                              ? PPTexts.getDescription(LocalizationUtils.getSingleValueString("forgot", "forgot.all.retry"))
                              : PPTexts.getDescription(LocalizationUtils.getSingleValueString("forgot", "forgot.all.retry"), color: Colors.white),
                          start > 0
                              ? PPTexts.getDescription(start.toString() + " " + LocalizationUtils.getSingleValueString("forgot", "forgot.all.retry-sec"), isBold: true)
                              : PPTexts.getDescription(start.toString() + " " + LocalizationUtils.getSingleValueString("forgot", "forgot.all.retry-sec"), color: Colors.white)
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Row(
                        children: <Widget>[
                          SecondaryButton(
                            text: LocalizationUtils.getSingleValueString("forgot", "forgot.all.resend-call"),
                            isSmall: true,
                            onPressed: start > 0 ? null : onGetCallPressed(context, innerModel),
                          ),
                          PPUIHelper.horizontalSpaceMedium(),
                          SecondaryButton(
                            text: LocalizationUtils.getSingleValueString("forgot", "forgot.all.resend-sms"),
                            isSmall: true,
                            onPressed: start > 0 ? null : onResendPressed(context, innerModel),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
        ],
      ),
    );
  }

  VoidCallback onResendPressed(BuildContext context, VerificationModel model) {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_resend_sms);
    return () {};
  }

  VoidCallback onGetCallPressed(BuildContext context, VerificationModel model) {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_get_call);
    return () {};
  }
}
