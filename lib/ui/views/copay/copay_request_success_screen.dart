import 'package:flutter/material.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/success/base_success_screen.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';

class CopayRequestSuccessScreen extends StatelessWidget {
  static const String routeName = 'copayRequestSuccessScreen';

  @override
  Widget build(BuildContext context) {
    return BaseSuccessScreen(
      source: BaseStepperSource.COPAY_REQUEST,
      transactionSuccessEnum: TransactionSuccessEnum.COPAY_REQUEST_SUCCESS,
    );
  }
}
