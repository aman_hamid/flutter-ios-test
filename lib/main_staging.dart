import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/res/colors.dart';
import 'app_config.dart';
import 'locator.dart';
import 'main.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: brandColor, // Color for Android
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness:
          Brightness.light // Dark == white status bar -- for IOS.
      ));

  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  await setupSharedPreferences();
  await initPlatformState();
  setupLocator();

  var configuredApp = new AppConfig(
    appName: 'STGPocketPills',
    flavorName: 'staging',
    apiBaseUrl: 'https://c7.api.pocketpills.com',
    webviewBaseUrl: 'https://testapp.pocketpills.com',
    mixPanelToken: '6a4886648afb69fca0a05c2649c6149d',
    oneSignalToken: 'cbb37f0e-8394-4c2c-aa36-041a471abade',
    child: new MyApp(),
  );
  setupAppConfig(configuredApp);
  PPApplication.initApplication();
  runZoned<Future<void>>(() async {
    runApp(configuredApp);
  }, onError: FirebaseCrashlytics.instance.recordError);
}
