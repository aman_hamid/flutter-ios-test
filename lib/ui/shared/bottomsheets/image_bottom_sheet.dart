import 'package:flutter/material.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';

class ImageBottomSheet {
  static void getBottomSheet(BuildContext context, {Function onCameraClick, Function onGalleryClick}) {
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: PPTexts.getHeading('CHOOSE AN OPTION'),
            ),
            PPDivider(),
            ListTile(
              leading: new Icon(Icons.photo_camera),
              title: new Text(
                'Take a photo',
                style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
              ),
              onTap: onCameraClick,
            ),
            ListTile(
                leading: new Icon(Icons.photo_library),
                title: new Text('Choose from gallery', style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM),
                onTap: onGalleryClick),
            SizedBox(
              height: 20,
            ),
          ],
        );
      },
    );
  }
}
