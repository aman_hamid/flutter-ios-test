// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopping_cart_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShoppingCartItem _$ShoppingCartItemFromJson(Map<String, dynamic> json) {
  return ShoppingCartItem(
    json['patientId'] as int,
    json['disabled'] as bool,
    json['variantId'] as int,
    json['monthsSupply'] as int,
    json['quantity'] as num,
    json['userSigCode'] as String,
    json['totalPrice'] as num,
    json['listPrice'] as num,
  )..drugDTO = json['drugDTO'] == null
      ? null
      : Medicine.fromJson(json['drugDTO'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ShoppingCartItemToJson(ShoppingCartItem instance) =>
    <String, dynamic>{
      'patientId': instance.patientId,
      'disabled': instance.disabled,
      'variantId': instance.variantId,
      'monthsSupply': instance.monthsSupply,
      'quantity': instance.quantity,
      'userSigCode': instance.userSigCode,
      'totalPrice': instance.totalPrice,
      'listPrice': instance.listPrice,
      'drugDTO': instance.drugDTO,
    };
