// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'patient.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Patient _$PatientFromJson(Map<String, dynamic> json) {
  return Patient(
    id: json['id'] as int,
    disabled: json['disabled'] as bool,
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    birthDate: json['birthDate'] as String,
    countryCode: json['countryCode'] as int,
    phn: json['phn'] as String,
    emailVerificationToken: json['emailVerificationToken'] as String,
    province: json['province'] as String,
    insuranceDetailsSet: json['insuranceDetailsSet'] as List,
    hasDailyMedication: json['hasDailyMedication'] as bool,
    hasViewedAboutYouUser: json['hasViewedAboutYouUser'] as bool,
    hasViewedAboutYouPatient: json['hasViewedAboutYouPatient'] as bool,
    hasViewedAlmostDone: json['hasViewedAlmostDone'] as bool,
    hasViewedUserRegistrationSummary:
        json['hasViewedUserRegistrationSummary'] as bool,
    maritalStatus: json['maritalStatus'] as String,
    pharmacyCode: json['pharmacyCode'] as String,
    gender: json['gender'] as String,
    phone: json['phone'] as int,
    email: json['email'] as String,
    isMailVerified: json['isMailVerified'] as bool,
    employerDisplayName: json['employerDisplayName'] as String,
  )..invitationCode = json['invitationCode'] as String;
}

Map<String, dynamic> _$PatientToJson(Patient instance) => <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'birthDate': instance.birthDate,
      'countryCode': instance.countryCode,
      'phn': instance.phn,
      'emailVerificationToken': instance.emailVerificationToken,
      'province': instance.province,
      'insuranceDetailsSet': instance.insuranceDetailsSet,
      'hasDailyMedication': instance.hasDailyMedication,
      'hasViewedAboutYouUser': instance.hasViewedAboutYouUser,
      'hasViewedAboutYouPatient': instance.hasViewedAboutYouPatient,
      'hasViewedAlmostDone': instance.hasViewedAlmostDone,
      'hasViewedUserRegistrationSummary':
          instance.hasViewedUserRegistrationSummary,
      'maritalStatus': instance.maritalStatus,
      'invitationCode': instance.invitationCode,
      'pharmacyCode': instance.pharmacyCode,
      'gender': instance.gender,
      'employerDisplayName': instance.employerDisplayName,
      'phone': instance.phone,
      'email': instance.email,
      'isMailVerified': instance.isMailVerified,
    };
