import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/services/user_service.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:pocketpills/utils/http_api_utils.dart';

class DashboardModel extends BaseModel {
  final DataStoreService dataStore = locator<DataStoreService>();
  UserService userService = locator<UserService>();
  int _memberPatientId;

  int _dashboardIndex = 0;
  int showBottomBar = 1;

  UserPatient _selectedPatient;
  DayWiseMedications _dayWiseMedications;

  bool _applicationStart = false;

  List<UserPatient> _patientList;

  ConnectivityResult connectivityResult;

  AsyncMemoizer<List<UserPatient>> _memoizer = AsyncMemoizer();

  AsyncMemoizer<void> _geoIpMemoizer = AsyncMemoizer();
  HttpApiUtils httpApiUtils = HttpApiUtils();
  bool quebecCheck = false;

  DashboardModel() {
    getProvinceName();
  }

  Future<void> getProvinceName() async {
    return this._geoIpMemoizer.runOnce(() async {
      if (httpApiUtils != null) {
        try {
          String subdivisionName = await httpApiUtils.getSubdivisionName();
          quebecCheck =
              subdivisionName.toUpperCase().contains('QUEBEC') ? true : false;
          notifyListeners();
        } catch (ex) {}
      }
    });
  }

  int get selectedPatientId {
    if (_selectedPatient != null)
      return _selectedPatient.patientId;
    else
      return dataStore.getPatientId();
  }

  int get dashboardIndex => _dashboardIndex;

  set dashboardIndex(int value) {
    checknetworkConnectivity();
    _dashboardIndex = value;
    notifyListeners();
  }

  UserPatient get selectedPatient => _selectedPatient;

  List<UserPatient> get patientList => _patientList;

  void displayBottomBar() {
    showBottomBar = 1;
    notifyListeners();
  }

  void hideBottomBar() {
    showBottomBar = 0;
    notifyListeners();
  }

  void setEmail(String emailID) {
    if (_selectedPatient != null) {
      _selectedPatient.patient.email = emailID;
      notifyListeners();
    }
  }

  Future<List<UserPatient>> getAndSetUserPatientList() async {
    print('AMANGETANDSETCALLED');

    if (dataStore.readBoolean(DataStoreService.REFRESH_DASHBOARD)) {
      print(
          'AMANCHMABERSDASHBOARD:${dataStore.readBoolean(DataStoreService.REFRESH_DASHBOARD)}');
      clearAsyncMemoizer();
      dataStore.writeBoolean(DataStoreService.REFRESH_DASHBOARD, false);
      return await this._memoizer.runOnce(() async {
        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        List<UserPatient> ret = await userService.getUserPatientList();
        if (ret != null && ret.length == 0) return ret;
        _patientList = ret;
        UserPatient p;
        for (int i = 0; i < ret.length; i++) {
          if (ret.elementAt(i).primary == true) p = ret.elementAt(i);
          if (ret.elementAt(i).patientId == dataStore.getPatientId()) {
            p = ret.elementAt(i);
            break;
          }
        }
        if (p != null) await setSelectedPatient(p);
        return ret;
      });
    }

    return this._memoizer.runOnce(() async {
      connectivityResult = await checkInternet();
      if (connectivityResult == ConnectivityResult.none) {
        return null;
      }
      List<UserPatient> ret = await userService.getUserPatientList();
      if (ret != null && ret.length == 0) return ret;
      _patientList = ret;
      UserPatient p;
      for (int i = 0; i < ret.length; i++) {
        if (ret.elementAt(i).primary == true) p = ret.elementAt(i);
        if (ret.elementAt(i).patientId == dataStore.getPatientId()) {
          p = ret.elementAt(i);
          break;
        }
      }
      if (p != null) await setSelectedPatient(p);
      return ret;
    });
  }

  clearUserPatientList() {
    clearAsyncMemoizer();
    notifyListeners();
  }

  clearAsyncMemoizer() {
    this._memoizer = AsyncMemoizer();
    _dashboardIndex = 0;
  }

  clearAsyncMemorizer() {
    _memoizer = AsyncMemoizer();
    checknetworkConnectivity();
    notifyListeners();
  }

  Future<void> setSelectedPatient(UserPatient patient) async {
    _selectedPatient = patient;
    await dataStore.writeInt(
        DataStoreService.PATIENTID, _selectedPatient.patientId);
    await dataStore.savePatient(_selectedPatient.patient);
    if (patient.primary == true)
      await dataStore.markSelectedPatientAsCaregiver(true);
    else
      await dataStore.markSelectedPatientAsCaregiver(false);
    notifyListeners();
  }

  Future<void> setSelectedPatientId(int patientId) async {
    analyticsEvents
        .sendAnalyticsEvent(AnalyticsEventConstant.click_member_item);
    for (int i = 0; i < _patientList.length; i++) {
      if (_patientList.elementAt(i).patientId == patientId) {
        _selectedPatient = _patientList.elementAt(i);
        await dataStore.writeInt(
            DataStoreService.PATIENTID, _selectedPatient.patientId);
        await dataStore.savePatient(_selectedPatient.patient);
        if (_selectedPatient.primary == true)
          await dataStore.markSelectedPatientAsCaregiver(true);
        else
          await dataStore.markSelectedPatientAsCaregiver(false);
        notifyListeners();
        return;
      }
    }
  }

  String getCurrentScreenName() {
    String eventName = null;
    switch (dashboardIndex) {
      case 0:
        eventName = AnalyticsEventConstant.dashboard;
        break;
      case 1:
        eventName = AnalyticsEventConstant.medications;
        break;
      case 2:
        eventName = AnalyticsEventConstant.click_prescriptions;
        break;
      case 3:
        eventName = AnalyticsEventConstant.click_orders;
        break;
      default:
        eventName = AnalyticsEventConstant.dashboard;
        break;
    }
    return eventName;
  }

  checknetworkConnectivity() async {
    connectivityResult = await checkInternet();
    notifyListeners();
  }

  int get memberPatientId => _memberPatientId;

  set memberPatientId(int value) {
    _memberPatientId = value;
  }

  bool get applicationStart => _applicationStart;

  set applicationStart(bool value) {
    _applicationStart = value;
    notifyListeners();
  }

  DayWiseMedications get dayWiseMedications => _dayWiseMedications;

  set dayWiseMedications(DayWiseMedications value) {
    _dayWiseMedications = value;
  }
}
