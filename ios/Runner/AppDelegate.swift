import UIKit
import Flutter
import Firebase
import FBSDKCoreKit

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate{
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
        ) -> Bool {
        
        var filePath:String!

        if let bundleDisplayName = Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as? String {
            if bundleDisplayName.starts(with: "com.Pocketpills.staging") {
                filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist", inDirectory: "config/staging")
            } else {
                filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist", inDirectory: "config/prod")
            }
        }
        
        let options = FirebaseOptions.init(contentsOfFile: filePath)!
        FirebaseApp.configure(options: options)
        
        AppEvents.activateApp()
        Settings.isAutoLogAppEventsEnabled = true
        
        let controller : FlutterBinaryMessenger = window?.rootViewController as! FlutterBinaryMessenger
        let facebookChannel = FlutterMethodChannel(name: "plugins.flutter.io/facebook_analytics",
                                                   binaryMessenger: controller)
        facebookChannel.setMethodCallHandler({
            (call: FlutterMethodCall, result: FlutterResult) -> Void in
            // Note: this method is invoked on the UI thread.

            if call.method == "logEvent" {
                guard let args = call.arguments else {
                    return
                }
                if let myArgs = args as? [String: Any],
                    let eventName = myArgs["name"] as? String,
                    let caps: [String:String] = myArgs["parameters"] as? [String: String]{
                    AppEvents.logEvent(AppEvents.Name(eventName), parameters: caps)
                    result("Log event success")
                } else {
                    result("iOS could not extract flutter arguments in method: (sendParams)")
                }
            } else if call.method == "initiatedEvent" {
                guard let args = call.arguments else {
                    return
                }
                if let myArgs = args as? [String: Any],
                    let caps: [String:String] = myArgs["initiatedEvent"] as? [String: String]{
            
                    AppEvents.logEvent(AppEvents.Name("fb_mobile_initiated_checkout"), parameters: caps)
                   
                    result("Log event success")
                } else {
                    result("iOS could not extract flutter arguments in method: (sendParams)")
                }
            } else if call.method == "completeRegistration" {
                    AppEvents.logEvent(AppEvents.Name("fb_mobile_complete_registration"))
                    result("Log event success")
            } else if call.method == "userData" {
                guard let args = call.arguments else {
                    return
                }
                if let myArgs = args as? [String: Any],
                    let caps: [String:String] = myArgs["userData"] as? [String: String]{
                    let email = caps["email"]
                    let firstName = caps["firstName"]
                    let lastName = caps["lastName"]
                    let phone = caps["phone"]
                    let dateOfBirth = caps["dateOfBirth"]
                    let gender = caps["gender"]
                    let city = caps["city"]
                    let state = caps["state"]
                    let zip = caps["zip"]
                    let country = caps["country"]
                    dump(caps)
                    //AppEvents.setUser(email: email, firstName: firstName, lastName: lastName, phone: phone, dateOfBirth: dateOfBirth, gender: gender, city: city, state: state, zip: zip, country: country)
                    
                    result("User data success")
                } else {
                    result("iOS could not extract flutter arguments in method: (sendParams)")
                }
            } else {
                result("Flutter method not implemented on iOS")
            }
        });
        
        GeneratedPluginRegistrant.register(with: self)

        //For Displaying download complete notification
        if #available(iOS 10.0, *) {
          UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
        }
        return super.application(application, didFinishLaunchingWithOptions: launchOptions);
    }
    
}

