import 'dart:core';

import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/telehealth/localization_update_response.dart';

part 'localization_list_response.g.dart';

@JsonSerializable()
class LocalizationListResponse {
  @JsonKey(name: "data")
  List<LocalizationUpdateResponse> dataList;

  LocalizationListResponse(this.dataList);

  factory LocalizationListResponse.fromJson(Map<String, dynamic> json) => _$LocalizationListResponseFromJson(json);
}
