import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';

class ChoiceChips extends StatefulWidget {
  final List chipName;
  final Function(String) selectedItem;
  ChoiceChips({Key key, this.chipName,@required this.selectedItem}) : super(key: key);

  @override
  _ChoiceChipsState createState() => _ChoiceChipsState();
}

class _ChoiceChipsState extends State<ChoiceChips> {
  String _isSelected = "";

  _buildChoiceList() {
    List<Widget> choices = List();
    widget.chipName.forEach((item) {
      choices.add(Container(
        child: ChoiceChip(
          label: Text(item),
          labelStyle: TextStyle(color: Colors.black),
          backgroundColor: headerBgColor,
          selectedColor:headerBgColor ,
          selected: _isSelected == item,
          onSelected: (selected) {
            setState(() {
              _isSelected = item;
              widget.selectedItem(_isSelected);
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 5.0,
      runSpacing: 3.0,
      children: _buildChoiceList(),
    );
  }
}
