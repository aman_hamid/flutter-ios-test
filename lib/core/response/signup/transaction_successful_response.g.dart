// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_successful_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionSuccessfulResponse _$TransactionSuccessfulResponseFromJson(
    Map<String, dynamic> json) {
  return TransactionSuccessfulResponse(
    json['successDetails'] == null
        ? null
        : SuccessDetails.fromJson(
            json['successDetails'] as Map<String, dynamic>),
  )
    ..status = json['success'] as bool
    ..errMessage = json['userMessage'] as String
    ..apiMessage = json['message'] as String
    ..prescriptionType = json['prescriptionType'] as String
    ..prescriptionPreference = json['prescriptionPreference'] as String
    ..deliveryBy = json['deliveryBy'] as String
    ..dueDate = json['dueDate'] as bool;
}

Map<String, dynamic> _$TransactionSuccessfulResponseToJson(
        TransactionSuccessfulResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'prescriptionType': instance.prescriptionType,
      'prescriptionPreference': instance.prescriptionPreference,
      'deliveryBy': instance.deliveryBy,
      'dueDate': instance.dueDate,
      'successDetails': instance.successDetails,
    };
