import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/response/activate_patient_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/date_utils.dart';
import 'package:pocketpills/core/viewmodels/consent_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/appbar_icons.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/views/addmember/about_patient.dart';
import 'package:pocketpills/ui/views/addmember/add_member_signup.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/consent/activate_patient.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class DashboardAppBar extends StatefulWidget implements PreferredSizeWidget {
  final AppBar appBar;

  /// you can add more fields that meet your needs

  DashboardAppBar({Key key, this.appBar}) : super(key: key);

  @override
  State<DashboardAppBar> createState() {
    return DashboardAppBarState();
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}

class DashboardAppBarState extends BaseState<DashboardAppBar> {
  int curValue;
  final DataStoreService dataStore = locator<DataStoreService>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<DashboardModel>(
      builder: (BuildContext context, DashboardModel dashboardModel, Widget child) {
        return AppBar(
            titleSpacing: 8,
            backgroundColor: Colors.white,
            centerTitle: true,
            elevation: 2,
            iconTheme: IconThemeData(
              color: primaryColor,
            ),
            title: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  PPTexts.getDescription(LocalizationUtils.getSingleValueString("common", "common.navbar.view-label").toUpperCase()),
                  InkWell(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          dashboardModel.selectedPatient.patient != null ? dashboardModel.selectedPatient.patient.firstName : "",
                          textAlign: TextAlign.left,
                          style: TextStyle(color: primaryColor, fontSize: 20, height: 1, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(width: 6),
                        Icon(Icons.arrow_drop_down, size: 30),
                      ],
                    ),
                    onTap: () {
                      showBottomSheet(context);
                    },
                  )
                ],
              ),
            ),
            actions: AppBarIcons.getAppBarIcons(context));
      },
    );
  }

  showBottomSheet(BuildContext context) {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_member);
    showModalBottomSheet<Null>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Consumer<DashboardModel>(
          builder: (BuildContext context, DashboardModel dashboardModel, Widget child) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 6,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              PPTexts.getHeading(LocalizationUtils.getSingleValueString("modal", "modal.patientsdropdown.title").toUpperCase()),
                              PPTexts.getDescription(LocalizationUtils.getSingleValueString("modal", "modal.patientsdropdown.subtitle"))
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: OutlineButton(
                            color: Colors.white,
                            disabledTextColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              side: BorderSide(color: tertiaryColor),
                              borderRadius: BorderRadius.circular(SMALL_XX),
                            ),
                            child: Text(
                              LocalizationUtils.getSingleValueString("common", "common.navbar.add-member").toUpperCase(),
                              textAlign: TextAlign.center,
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, AddMemberSignupWidget.routeName);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  PPDivider(),
                  ConstrainedBox(
                    constraints: new BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height / 2,
                    ),
                    child: ListView(
                      shrinkWrap: true,
                      children: dashboardModel.patientList.map(
                        (UserPatient value) {
                          Widget titleWidget = PPTexts.getHeading(value.patient.firstName, isDisabled: value.consent != null && !value.consent);
                          if (value.primary != null && value.primary == true) {
                            titleWidget = PPTexts.getHeading(value.patient.firstName,
                                isDisabled: value.consent != null && !value.consent,
                                child: PPChip(label: LocalizationUtils.getSingleValueString("common", "common.navbar.primary")),
                                isChildLeading: false);
                          }
                          if (value.consent != null && !value.consent) {
                            if (value.patientId == -1) {
                              titleWidget = PPTexts.getHeading(value.patient.firstName,
                                  isDisabled: value.consent != null && !value.consent,
                                  child: Container(
                                    child: PPTexts.getHyperLink(
                                        LocalizationUtils.getSingleValueString("common", "common.navbar.activate").toUpperCase(), onActivateClick(dashboardModel, value)),
                                  ),
                                  isChildEnd: true);
                            } else
                              titleWidget = PPTexts.getHeading(value.patient.firstName,
                                  isDisabled: value.consent != null && !value.consent,
                                  child: Container(
                                    child: PPTexts.getHyperLink(LocalizationUtils.getSingleValueString("common", "common.navbar.askconsent").toUpperCase(), onConsentClick(value)),
                                  ),
                                  isChildEnd: true);
                          }
                          return RadioListTile(
                            title: titleWidget,
                            groupValue: dashboardModel.selectedPatientId,
                            value: value.patientId,
                            onChanged: value.consent != null && value.consent == false
                                ? null
                                : (value) async {
                                    print(value);
                                    await dashboardModel.setSelectedPatientId(value);
                                    Navigator.pop(context);
                                  },
                          );
                        },
                      ).toList(),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  onActivateClick(DashboardModel dashboardModel, UserPatient userPatient) {
    return () async {
      bool isAbove19 = PPDateUtils.isAbove19(userPatient.patient.birthDate);
      if (isAbove19 == true)
        Navigator.pushNamed(context, ActivatePatientWidget.routeName,
            arguments: ConsentArguments(
              userPatient: userPatient,
            ));
      else {
        ConsentModel consentModel = locator<ConsentModel>();
        ActivatePatientResponse res = await consentModel.activatePatient(userPatient, "");
        if (res != null) {
          userPatient.patient = res.patient;
          Provider.of<DashboardModel>(context).clearUserPatientList();
          await Provider.of<DashboardModel>(context).getAndSetUserPatientList();
          await dashboardModel.setSelectedPatientId(res.patient.id);
          if (res.patient.hasViewedAboutYouPatient == null || res.patient.hasViewedAboutYouPatient == false) {
            Navigator.pushReplacementNamed(context, AboutPatientWidget.routeName,
                arguments: TransferArguments(userPatient: userPatient, snackBarMessage: "We have activated preferred pricing on " + userPatient.patient.firstName + "'s account."));
          } else {
            Navigator.pop(context);
          }
        }
      }
    };
  }

  onConsentClick(UserPatient userPatient) {
    return () {
      if (userPatient.hasViewedTransferRefills != null && userPatient.hasViewedTransferRefills == true)
        Navigator.pushNamed(context, WaitConsentWidget.routeName,
            arguments: ConsentArguments(
              userPatient: userPatient,
            ));
      else if (userPatient.patient.hasViewedAboutYouPatient != null && userPatient.patient.hasViewedAboutYouPatient == true)
        Navigator.pushNamed(context, TransferWidget.routeName, arguments: TransferArguments(userPatient: userPatient, source: BaseStepperSource.CONSENT_FLOW));
      else
        Navigator.pushNamed(context, AboutPatientWidget.routeName, arguments: TransferArguments(userPatient: userPatient, source: BaseStepperSource.CONSENT_FLOW));
    };
  }
}
