import 'package:flutter/material.dart';
import 'package:pocketpills/core/dashboard/dashboard_item.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';

class DashboardLongErrorCardView extends StatelessWidget {
  final DashboardItem dashboardItem;
  final HomeModel homeModel;

  DashboardLongErrorCardView({@required this.dashboardItem, @required this.homeModel});

  @override
  Widget build(BuildContext context) {
    return PPCard(
      onTap: () {
        homeModel.handleDashboardRoute(dashboardItem.getActionType(), context, dashboardItem: dashboardItem);
      },
      margin: EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM),
      padding: EdgeInsets.only(left: MEDIUM_X, right: MEDIUM_XXX, top: MEDIUM_X, bottom: MEDIUM_X),
      roundedRectangleBorder: RoundedRectangleBorder(
        side: BorderSide(color: errorColor),
        borderRadius: BorderRadius.circular(SMALL_X),
      ),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    dashboardItem.title ?? "",
                    style: MEDIUM_XXX_PRIMARY_BOLD,
                  ),
                  SizedBox(
                    height: SMALL_X,
                  ),
                  Text(
                    dashboardItem.description ?? "",
                    style: MEDIUM_XX_PRIMARY,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 4,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: MEDIUM_XXX),
              child: Icon(
                Icons.error_outline,
                color: errorColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
