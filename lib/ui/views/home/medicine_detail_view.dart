import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/viewmodels/medications_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/app_expansion_tile_State.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/search_medicine.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/copay/copay_request_arguments.dart';
import 'package:pocketpills/ui/views/copay/copay_request_view.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class MedicineDetailWidget extends StatefulWidget {
  static const routeName = 'medicinedetail';
  BaseStepperSource source;
  String medicineName;
  String from;
  UserPatient userPatient;
  final SignUpTransferModel model;

  MedicineDetailWidget({Key key, this.source, this.medicineName, this.from, this.model, this.userPatient}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MedicineDetailState();
  }
}

class MedicineDetailState extends BaseState<MedicineDetailWidget> {
  Medicine medicine;
  TextEditingController _quantityController = new TextEditingController();
  num totalCost;
  num drugCost;
  SignUpModel model;
  String dropDownValue = "80";
  bool coverDispeningFee = true;
  final GlobalKey<MedicineDetailState> expansionTile = new GlobalKey();

  @override
  void initState() {
    model = SignUpModel();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  num getDrugCost() {
    int quantity = 0;
    if (_quantityController.text.length > 0) {
      quantity = int.parse(_quantityController.text);
    } else
      return 0;
    num cost = medicine.getDrugCost(quantity: quantity);
    return cost;
  }

  num getTotalCost() {
    int quantity = 0;
    if (_quantityController.text.length > 0) {
      quantity = int.parse(_quantityController.text);
    } else
      return 0;
    num cost = medicine.getTotalCost(coverDispensingFee: coverDispeningFee, quantity: quantity, insuranceCover: int.parse(dropDownValue));
    return cost;
  }

  void onQuantityChange() {
    setState(() {
      drugCost = getDrugCost();
      totalCost = getTotalCost();
    });
  }

  void onInsuranceCoverageChange() {
    setState(() {
      totalCost = getTotalCost();
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: model.getLocalization(["search"]),
        builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  Widget getMainView() {
    return WillPopScope(
      onWillPop: widget.from == "telehealth"
          ? widget.source == BaseStepperSource.NEW_USER
              ? () async {
                  Navigator.pop(context);
                  return true;
                }
              : () => _onBackPressed(context)
          : () async {
              Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
              return true;
            },
      child: Scaffold(
        bottomNavigationBar: medicine != null
            ? widget.source == BaseStepperSource.NEW_USER
                ? getButtons()
                : Container(
                    height: 0.0,
                    width: 0.0,
                  )
            : Container(
                height: 0.0,
                width: 0.0,
              ),
        appBar: InnerAppBar(
          titleText: LocalizationUtils.getSingleValueString("search", "search.all.search"),
          appBar: AppBar(),
          leadingBackButton: () {
            widget.from == "telehealth" ? Navigator.pop(context) : Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          },
        ),
        body: Builder(
          builder: (BuildContext context) => Container(
            child: Builder(
              builder: (BuildContext context) {
                return SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      PPUIHelper.verticalSpaceMedium(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                        child: SearchMedicine(
                          onSubmit: onMedicineSubmit,
                        ),
                      ),
                      medicine != null
                          ? Column(
                              children: <Widget>[
                                PPDivider(),
                                Padding(
                                  padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: MEDIUM_XXX, bottom: MEDIUM_XXX),
                                  child: Container(child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: getMedicineDetails())),
                                ),
                                Container(color: headerBgColor, child: Column(children: getCostDetails())),
                                widget.source != BaseStepperSource.NEW_USER
                                    ? Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(SMALL_X)),
                                          color: headerBgColor,
                                        ),
                                        margin: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: MEDIUM_XXX),
                                        padding: EdgeInsets.all(MEDIUM),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              LocalizationUtils.getSingleValueString("search", "search.all.disclaimer-primary"),
                                              style: MEDIUM_X_SECONDARY,
                                            ),
                                            PPUIHelper.verticalSpaceSmall(),
                                            Text(
                                              LocalizationUtils.getSingleValueString("search", "search.all.disclaimer-secondary"),
                                              style: MEDIUM_X_SECONDARY,
                                            ),
                                            PPUIHelper.verticalSpaceSmall(),
                                            Text(
                                              LocalizationUtils.getSingleValueString("search", "search.all.disclaimer-tertiary"),
                                              style: MEDIUM_X_SECONDARY,
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                SizedBox(
                                  height: MEDIUM_XX,
                                ),
                                widget.source != BaseStepperSource.NEW_USER ? getRequestForCopayCardView() : Container(),
                                SizedBox(
                                  height: MEDIUM_XXX,
                                ),
                                widget.source != BaseStepperSource.NEW_USER ? getReDirectionCard() : Container(),
                                SizedBox(
                                  height: MEDIUM_XXX,
                                ),
                                widget.source != BaseStepperSource.NEW_USER
                                    ? Padding(
                                        padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: REGULAR_XXX),
                                        child: PPTexts.getDescription(LocalizationUtils.getSingleValueString("search", "search.all.disclaimer-bc")),
                                      )
                                    : Container(),
                              ],
                            )
                          : Container(height: 0.0, width: 0.0),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget getButtons() {
    return Builder(
        builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SecondaryButton(
                    isSmall: true,
                    text: LocalizationUtils.getSingleValueString("search", "search.all.button-skip"),
                    onPressed: () {
                      onSkipClick(widget.model);
                    },
                  ),
                  SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                  PrimaryButton(
                    text: LocalizationUtils.getSingleValueString("search", "search.all.button-transfer"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              ),
            ));
  }

  onSkipClick(SignUpTransferModel model) async {
    if (widget.source == BaseStepperSource.NEW_USER || widget.source == BaseStepperSource.ADD_PATIENT) {
      bool success = await model.uploadPrescriptionSignUp();
      print("here");
      if (success == true) {
        print("success");
        dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, true);
        goToNextScreen(model);
      }
    } else
      Navigator.pop(context);
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      Navigator.of(context)
          .pushNamedAndRemoveUntil(SignUpAlmostDoneWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.TRANSFER_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }

  Widget getRequestForCopayCardView() {
    return PPCard(
        padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: MEDIUM_XXX),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  LocalizationUtils.getSingleValueString("search", "search.all.request-copay-title"),
                  style: MEDIUM_XXX_PRIMARY_BOLD,
                ),
                SizedBox(
                  height: MEDIUM_XX,
                ),
                Text(
                  LocalizationUtils.getSingleValueString("search", "search.all.request-copay-description"),
                  style: MEDIUM_XX_PRIMARY,
                ),
              ],
            ),
            PPUIHelper.verticalSpaceSmall(),
            TransparentButton(
              text: LocalizationUtils.getSingleValueString('search', "search.all.request-copay-button"),
              onPressed: () {
                SystemChannels.textInput.invokeMethod('TextInput.hide');
                Navigator.pushNamed(context, CopayRequestView.routeName, arguments: CopayRequestArguments(medicine: this.medicine, quantity: int.parse(_quantityController.text)));
              },
            ),
          ],
        ),
        onTap: () {});
  }

  Widget getReDirectionCard() {
    return PPCard(
        padding: EdgeInsets.all(0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(MEDIUM_XXX),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    LocalizationUtils.getSingleValueString("search", "search.all.prescription-title"),
                    style: MEDIUM_XXX_PRIMARY_BOLD,
                  ),
                  SizedBox(
                    height: MEDIUM_XX,
                  ),
                  PPTexts.getSecondaryHeading("\u2022  " + LocalizationUtils.getSingleValueString("search", "search.all.usp1"), isBold: false, isChildLeading: true),
                  SizedBox(
                    height: SMALL,
                  ),
                  PPTexts.getSecondaryHeading("\u2022  " + LocalizationUtils.getSingleValueString("search", "search.all.usp2"), isBold: false),
                  SizedBox(
                    height: SMALL,
                  ),
                  PPTexts.getSecondaryHeading("\u2022  " + LocalizationUtils.getSingleValueString("search", "search.all.usp3"), isBold: false),
                  SizedBox(
                    height: SMALL,
                  ),
                  PPTexts.getSecondaryHeading("\u2022  " + LocalizationUtils.getSingleValueString("search", "search.all.usp4"), isBold: false),
                  SizedBox(
                    height: SMALL,
                  ),
                  PPTexts.getSecondaryHeading("\u2022  " + LocalizationUtils.getSingleValueString("search", "search.all.usp5"), isBold: false)
                ],
              ),
            ),
            PPUIHelper.verticalSpaceSmall(),
            Container(
              color: headerBgColor,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: MEDIUM_XXX),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Center(
                        child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, TransferWidget.routeName, arguments: TransferArguments(source: BaseStepperSource.MAIN_SCREEN));
                            },
                            child: Text(
                              LocalizationUtils.getSingleValueString("search", "search.all.transfer-button"),
                              style: MEDIUM_XX_LINK,
                              textAlign: TextAlign.center,
                            )),
                      ),
                    ),
                    Expanded(
                      child: Center(
                        child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, UploadPrescription.routeName);
                            },
                            child: Text(
                              LocalizationUtils.getSingleValueString("search", "search.all.upload-button"),
                              style: MEDIUM_XX_LINK,
                              textAlign: TextAlign.center,
                            )),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
        onTap: () {});
  }

  List<Widget> getCostDetails() {
    List<Widget> ret = [];
    ret.add(PPDivider());
    ret.add(
      Padding(
        padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: MEDIUM_XXX),
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      LocalizationUtils.getSingleValueString("search", "search.all.qty-label"),
                      style: MEDIUM_XX_PRIMARY_BOLD,
                    ),
                    medicine.description != null
                        ? Text(
                            LocalizationUtils.getSingleValueString("search", "search.all.qty-description-no-tablet") + " " + medicine.description,
                            style: MEDIUM_X_SECONDARY_BOLD_MEDIUM,
                          )
                        : SizedBox(
                            height: 0.0,
                          ),
                  ],
                ),
                SizedBox(
                  width: PPUIHelper.HorizontalSpaceLarge,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                  child: Container(
                    color: Colors.white,
                    width: MediaQuery.of(context).size.width * 0.26,
                    height: 52,
                    child: PPFormFields.getNumericFormField(
                        textInputAction: TextInputAction.done,
                        maxLength: 3,
                        controller: _quantityController,
                        centeredText: true,
                        onTextChanged: (value) {
                          onQuantityChange();
                        },
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(4.0),
                            borderSide: BorderSide(color: brandColor),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(4.0),
                            borderSide: BorderSide(color: tertiaryColor),
                          ),
                          fillColor: Colors.white,
                        )),
                  ),
                )
              ],
            ),
            SizedBox(
              height: MEDIUM_XXX,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        LocalizationUtils.getSingleValueString("search", "search.all.insurance-label"),
                        style: MEDIUM_XX_PRIMARY_BOLD,
                      ),
                      Flexible(
                        child: Text(
                          LocalizationUtils.getSingleValueString("search", "search.all.insurance-description"),
                          style: MEDIUM_X_SECONDARY_BOLD_MEDIUM,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.26,
                  color: whiteColor,
                  height: 52,
                  child: PPFormFields.getDropDown(
                    labelText: "0%",
                    fullWidth: false,
                    value: dropDownValue,
                    items: ViewConstants.insuranceCover
                        .map((String key, String value) {
                          return MapEntry(
                              key,
                              DropdownMenuItem<String>(
                                value: key,
                                child: Text(value),
                              ));
                        })
                        .values
                        .toList(),
                    onChanged: (selectedItem) => setState(() {
                      dropDownValue = selectedItem;
                      onInsuranceCoverageChange();
                    }),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
    ret.add(Divider(
      height: SMALL,
      thickness: SMALL,
      color: whiteColor,
    ));
    ret.add(Container(
        color: headerBgColor,
        padding: EdgeInsets.only(top: SMALL_X, bottom: SMALL_X),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Checkbox(
                value: coverDispeningFee,
                activeColor: brandColor,
                onChanged: (bool value) {
                  setState(() {
                    coverDispeningFee = !coverDispeningFee;
                    onInsuranceCoverageChange();
                  });
                }),
            Text(
              LocalizationUtils.getSingleValueString("search", "search.all.dispensing-cover-label"),
              style: MEDIUM_XX_PRIMARY_MEDIUM_BOLD,
            )
          ],
        )));
    ret.add(getCostHeading());
    return ret;
  }

  List<Widget> getMedicineDetails() {
    List<Widget> ret = [];
    if (medicine.name != null) {
      ret.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                medicine.description ?? "",
                style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
              ),
              Text(
                medicine.name ?? "",
                style: MEDIUM_XXX_PRIMARY_BOLD,
              ),
            ],
          ),
          Image.network(medicine.getDrugTypeSource(), height: LARGE)
        ],
      ));
      ret.add(SizedBox(
        height: MEDIUM,
      ));
    }

    if (medicine.trade != null) {
      ret.add(
        RichText(
          text: TextSpan(
            text: LocalizationUtils.getSingleValueString("search", "search.all.generic-label") + "  ",
            style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
            children: <TextSpan>[
              TextSpan(text: medicine?.trade?.toUpperCase() ?? "", style: MEDIUM_XX_PRIMARY_BOLD),
            ],
          ),
        ),
      );
      ret.add(SizedBox(
        height: SMALL_XXX,
      ));
    }

    if (medicine.manufacturer != null) {
      ret.add(
        RichText(
          text: TextSpan(
            text: LocalizationUtils.getSingleValueString("search", "search.all.manufacturer-label") + "  ",
            style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
            children: <TextSpan>[
              TextSpan(text: medicine?.manufacturer?.toUpperCase() ?? "", style: MEDIUM_XX_PRIMARY_BOLD),
            ],
          ),
        ),
      );
    }
    return ret;
  }

  Widget getCostHeading() {
    return Container(
      color: whiteColor,
      padding: EdgeInsets.only(top: MEDIUM_XXX),
      child: AppExpansionTile(
        key: expansionTile,
        suffixIcon: false,
        onExpansionChanged: (value) {
          if (value == true) analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.search_breakup_click);
        },
        isListTile: false,
        title: Container(
          margin: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  PPUIHelper.verticalSpaceSmall(),
                  PPTexts.getHeading(LocalizationUtils.getSingleValueString("search", "search.all.copay-label").toUpperCase()),
                  SizedBox(
                    height: SMALL_X,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        LocalizationUtils.getSingleValueString("search", "search.all.breakup-button").toUpperCase(),
                        style: TextStyle(fontSize: 12, color: linkColor),
                      ),
                      Icon(Icons.keyboard_arrow_down, size: 14, color: linkColor)
                    ],
                  ),
                  PPUIHelper.verticalSpaceSmall()
                ],
              ),
              if (medicine != null && totalCost != null)
                Container(
                  width: 100.0,
                  height: 50.0,
                  child: PPTexts.getMainViewHeading('\$' + totalCost.toStringAsFixed(2), textAlign: TextAlign.right, mainAxisAlignment: MainAxisAlignment.end),
                )
              else
                Container(
                  width: 100.0,
                  height: 50.0,
                  child: PPTexts.getMainViewHeading("-", textAlign: TextAlign.right, mainAxisAlignment: MainAxisAlignment.end),
                )
            ],
          ),
        ),
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: SMALL_X,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      PPTexts.getDescription(LocalizationUtils.getSingleValueString("search", "search.all.drugcost-label"), isExpanded: false),
                      Text(
                        (medicine != null && drugCost != null) ? '\$' + drugCost.toStringAsFixed(2) : "_",
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  SizedBox(
                    height: SMALL_XXX,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      PPTexts.getDescription(LocalizationUtils.getSingleValueString("search", "search.all.dispensing-label"), isExpanded: false),
                      Text(
                        (medicine != null && medicine.dispensingFee != null) ? '\$' + medicine.dispensingFee.toStringAsFixed(2) : "_",
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  SizedBox(
                    height: SMALL_XXX,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      PPTexts.getDescription(LocalizationUtils.getSingleValueString("search", "search.all.delivery-label"), isExpanded: false),
                      Text(
                        LocalizationUtils.getSingleValueString("common", "common.all.free").toUpperCase(),
                        style: MEDIUM_XX_GREEN_BOLD_MEDIUM,
                      )
                    ],
                  ),
                  SizedBox(
                    height: SMALL_XXX,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      PPTexts.getDescription(LocalizationUtils.getSingleValueString("search", "search.all.insurance-label"), isExpanded: false),
                      Text(
                        (medicine != null && totalCost != null && drugCost != null && medicine.dispensingFee != null)
                            ? '\$' + (medicine.dispensingFee + drugCost - totalCost).toStringAsFixed(2)
                            : "-",
                        style: MEDIUM_XX_GREEN_BOLD_MEDIUM,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: SMALL_XXX,
          ),
          Divider(),
        ],
      ),
    );
  }

  onMedicineSubmit(Medicine item) {
    setState(() {
      medicine = item;
      _quantityController.text = medicine.defaultQuantity.toString();
      drugCost = getDrugCost();
      totalCost = getTotalCost();
    });
  }
}
