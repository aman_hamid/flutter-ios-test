import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/analytics.dart';
export 'package:pocketpills/utils/analytics_event_constant.dart';

class BaseModel extends ChangeNotifier {
  ViewState _state = ViewState.Idle;

  ViewState get state => _state;
  String errorMessage = getInternetMessageError();
  String noInternetConnection = getNoInternetMessageError();

  Analytics analyticsEvents = locator<Analytics>();

  void setState(ViewState viewState) {
    _state = viewState;
    notifyListeners();
  }

  void showSnackBar(var context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  Future<ConnectivityResult> checkInternet() async {
    ConnectivityResult result = await Connectivity().checkConnectivity();
    return result;
  }

  Future<bool> isInternetConnected() async {
    ConnectivityResult connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      return false;
    } else {
      return true;
    }
  }
}
