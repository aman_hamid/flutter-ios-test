// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employer_suggestion_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmployerSuggestionResponse _$EmployerSuggestionResponseFromJson(
    Map<String, dynamic> json) {
  return EmployerSuggestionResponse(
    (json['suggestedEmployers'] as List)
        ?.map((e) => e == null
            ? null
            : SuggestedEmployers.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['alreadyMappedEmployers'] as int,
    json['suggestionsValidCount'] as int,
  );
}

Map<String, dynamic> _$EmployerSuggestionResponseToJson(
        EmployerSuggestionResponse instance) =>
    <String, dynamic>{
      'suggestedEmployers': instance.suggestedEmployers,
      'alreadyMappedEmployers': instance.alreadyMappedEmployers,
      'suggestionsValidCount': instance.suggestionsValidCount,
    };
