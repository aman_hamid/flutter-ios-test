import 'package:flutter/material.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:url_launcher/url_launcher.dart';

class CallDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Container(
          child: Row(
            children: <Widget>[
              Icon(Icons.call),
              SizedBox(
                  width: PPUIHelper.HorizontalSpaceMedium),
              Text("Call PocketPills?"),
            ],
          )),
      content:
      Text("Press Call to talk to your care team"),
      actions: <Widget>[
        // usually buttons at the bottom of the dialog
        FlatButton(
          child: Text("CANCEL"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text("CALL"),
          onPressed: () {
            launch("tel://" +
                ViewConstants.PHARMACY_PHONE.toString());
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
  static Function show(BuildContext context){
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return CallDialog();
        },
      );
  }
}
