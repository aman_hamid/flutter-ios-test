import 'dart:ui';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/payment_card.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_payment_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';
import 'package:pocketpills/ui/shared/dialogs/payment_zero_copay_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/vitamins/vitamins_bottom_sheet.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/empty_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/profile/payment_detail_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ProfilePaymentView extends StatefulWidget {
  final Function onSuccess;
  final Function onBack;
  final bool noPadding;
  final int position;
  final BaseStepperSource source;

  ProfilePaymentView({Key key, this.onBack, this.noPadding = false, this.onSuccess, this.source, this.position = 0}) : super(key: key);

  static const routeName = 'profilepayment';

  @override
  State<StatefulWidget> createState() {
    return ProfilePaymentState();
  }
}

class ProfilePaymentState extends BaseState<ProfilePaymentView> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: widget.position > 0
            ? widget.onBack
            : () async {
                Navigator.pop(context);
                return true;
              },
        child: MultiProvider(
          providers: [ChangeNotifierProvider<ProfilePaymentModel>(create: (_) => ProfilePaymentModel())],
          child: Consumer<ProfilePaymentModel>(
            builder: (BuildContext context, ProfilePaymentModel profilePaymentModel, Widget child) {
              return FutureBuilder(
                future: myFutureMethodOverall(profilePaymentModel, context),
                // ignore: missing_return
                builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (profilePaymentModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                    return NoInternetScreen(
                      onClickRetry: profilePaymentModel.clearAsyncMemorizer,
                    );
                  }

                  if (snapshot.hasData == true && profilePaymentModel.connectivityResult != ConnectivityResult.none) {
                    return afterFutureResolved(context, profilePaymentModel, profilePaymentModel.cards);
                  } else if (snapshot.hasError && profilePaymentModel.connectivityResult != ConnectivityResult.none) {
                    Crashlytics.instance.log(snapshot.hasError.toString());
                    return ErrorScreen();
                  }

                  if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                    return LoadingScreen();
                  }
                },
              );
            },
          ),
        ));
  }

  Future myFutureMethodOverall(ProfilePaymentModel profilePaymentModel, BuildContext context) async {
    Future<List<PaymentCard>> future1 = profilePaymentModel.fetchPaymentData(Provider.of<DashboardModel>(context).selectedPatientId); // will take 1 sec
    Future<Map<String, dynamic>> future2 = profilePaymentModel.getLocalization(["payment", "order-checkout"]); // will take 3 secs
    return await Future.wait([future1, future2]);
  }

  Widget afterFutureResolved(BuildContext context, ProfilePaymentModel profilePaymentModel, List<PaymentCard> cards) {
    return Scaffold(
        floatingActionButton: ((widget.source == null || widget.source != BaseStepperSource.ORDER_STEPPER) || cards.length > 0)
            ? Stack(children: <Widget>[
                Align(
                  alignment: Alignment.bottomCenter,
                  child: FloatingActionButton.extended(
                      elevation: 10.0,
                      isExtended: true,
                      icon: Icon(Icons.add_circle),
                      label: Text(
                        LocalizationUtils.getSingleValueString("payment", "payment.button.add-new").toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () async {
                        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_add_a_card);
                        final result = await Navigator.pushNamed(
                          context,
                          PaymentDetailWidget.routeName,
                        );
                        if (result != null && result == true) refreshCards(profilePaymentModel);
                      }),
                ),
                profilePaymentModel.state != ViewState.Busy
                    ? Container()
                    : Container(
                        color: Colors.white30,
                      )
              ])
            : SizedBox(
                height: 0.0,
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: Stack(children: <Widget>[
          Padding(
            padding: widget.noPadding == true ? EdgeInsets.all(0) : EdgeInsets.only(top: MEDIUM_XXX),
            child: Container(child: getCards(context, profilePaymentModel, cards)),
          ),
          profilePaymentModel.state != ViewState.Busy
              ? Container()
              : Container(color: Colors.white30, child: Center(child: SizedBox(height: 32, width: 32, child: CircularProgressIndicator())))
        ]),
        bottomNavigationBar: getButtons(profilePaymentModel));
  }

  Widget getButtons(ProfilePaymentModel profilePaymentModel) {
    //return Container();
    return widget.onSuccess == null
        ? null
        : (widget.source == BaseStepperSource.VITAMINS_SCREEN || widget.source == BaseStepperSource.VITAMINS_SUBSCRIPTION_SCREEN)
            ? Builder(
                builder: (BuildContext context) => VitaminsBottomSheet(
                  onBack: () {
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_payment_back);
                    if (widget.onBack != null) widget.onBack();
                  },
                  onContiune: () {
                    updateShoppingCart(profilePaymentModel);
                  },
                  stepNumber: 1,
                ),
              )
            : Builder(
                builder: (BuildContext context) {
                  List<Widget> rowChildren = [
                    SecondaryButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.button.back").toUpperCase(),
                      onPressed: widget.onBack,
                    ),
                    SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
                    PrimaryButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.button.continue").toUpperCase(),
                      onPressed: widget.onSuccess,
                    )
                  ];
                  if (profilePaymentModel.cards == null || profilePaymentModel.cards.length == 0) {
                    rowChildren = [
                      SecondaryButton(
                        text: LocalizationUtils.getSingleValueString("common", "common.button.back").toUpperCase(),
                        onPressed: widget.onBack,
                      ),
                    ];
                  }
                  return Builder(
                      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                              child: Container(
                            color: Colors.white,
                            child: Row(mainAxisSize: MainAxisSize.min, children: rowChildren),
                          )));
                },
              );
  }

  Widget getCards(BuildContext context, ProfilePaymentModel profilePaymentModel, List<PaymentCard> cards) {
    if (cards.length == 0) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          EmptyScreen(
              imageLink: "graphics/empty_credit_card.png",
              title: (widget.source == BaseStepperSource.ORDER_STEPPER)
                  ? LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-payment")
                  : LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-add-payment"),
              description: (widget.source == BaseStepperSource.ORDER_STEPPER)
                  ? LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-payment")
                  : LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-add-payment"),
              showTransfer: false),
          (widget.source == BaseStepperSource.ORDER_STEPPER)
              ? Padding(
                  padding: const EdgeInsets.all(MEDIUM_XXX),
                  child: Row(
                    children: <Widget>[
                      SecondaryButton(
                          text: LocalizationUtils.getSingleValueString("payment", "payment.button.zero-copay"),
                          onPressed: () {
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_zero_copay);
                            PaymentZeroCopayDialog.show(profilePaymentModel, context, onSuccess);
                          }),
                      SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
                      PrimaryButton(
                        text: LocalizationUtils.getSingleValueString("payment", "payment.button.add-new"),
                        onPressed: () async {
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_add_a_card);
                          final result = await Navigator.pushNamed(
                            context,
                            PaymentDetailWidget.routeName,
                          );
                          if (result != null && result == true) refreshCards(profilePaymentModel);
                        },
                      )
                    ],
                  ),
                )
              : SizedBox(
                  height: 0.0,
                ),
        ],
      );
    }
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        Padding(
          child: PPTexts.getMainViewHeading(
            LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-payment"),
          ),
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: SMALL_XX),
        ),
        Padding(
          child: PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-payment"), isBold: false),
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: SMALL_XX),
        ),
        ListView.builder(
            primary: false,
            shrinkWrap: true,
            itemCount: cards.length,
            itemBuilder: (BuildContext ctxt, int index) {
              PaymentCard card = cards[index];
              return PPCard(
                  margin: EdgeInsets.only(bottom: SMALL_XXX, left: MEDIUM_XXX, right: MEDIUM_XXX, top: SMALL_XXX),
                  padding: EdgeInsets.all(0.0),
                  child: getCardView(card, profilePaymentModel));
            }),
      ],
    ));
  }

  Widget getCardView(PaymentCard card, ProfilePaymentModel profilePaymentModel) {
    if (card.paymentMode == PaymentMode.MY_HSA) {
      return Container(
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Radio(
                  value: card.id,
                  groupValue: profilePaymentModel.selectedCardId,
                  onChanged: (int cardId) async {
                    bool connectivityResult = await profilePaymentModel.isInternetConnected();
                    if (connectivityResult == false) {
                      onFail(context, errMessage: profilePaymentModel.noInternetConnection);
                      return;
                    }
                    bool res = await profilePaymentModel.setPatientCardDefault(cardId);
                    if (res == true) profilePaymentModel.setSelectedCard(cardId);
                  },
                ),
                Expanded(
                  child: PPTexts.getTertiaryHeading(LocalizationUtils.getSingleValueString("payment", "payment.label.health-spending-account"), isBold: true),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: MEDIUM_XXX),
                  child: Image.asset(Utils.cardTypeToImage(card.paymentMode), width: 38),
                ),
              ],
            ),
            PPDivider()
          ],
        ),
      );
    } else {
      return Container(
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Radio(
                  value: card.id,
                  groupValue: profilePaymentModel.selectedCardId,
                  onChanged: (int cardId) async {
                    bool connectivityResult = await profilePaymentModel.isInternetConnected();
                    if (connectivityResult == false) {
                      onFail(context, errMessage: profilePaymentModel.noInternetConnection);
                      return;
                    }
                    bool res = await profilePaymentModel.setPatientCardDefault(cardId);
                    if (res == true) profilePaymentModel.setSelectedCard(cardId);
                  },
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: MEDIUM_XX),
                    child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                      profilePaymentModel.selectedCardId == card.id
                          ? PPTexts.getTertiaryHeading("**** **** **** " + card.maskedNumber,
                              isBold: true,
                              child: Padding(
                                  padding: EdgeInsets.only(right: MEDIUM_XXX), child: PPChip(label: LocalizationUtils.getSingleValueString("common", "common.label.default"))))
                          : PPTexts.getTertiaryHeading("**** **** **** " + card.maskedNumber, isBold: true),
                      SizedBox(
                        height: SMALL_X,
                      ),
                      PPTexts.getDescription(card.expiry)
                    ]),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(LARGE, 0, REGULAR_XXX, 0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  card.paymentMode == PaymentMode.MY_HSA
                      ? PPContainer.emptyContainer()
                      : TransparentButton(
                          text: LocalizationUtils.getSingleValueString("common", "common.button.delete").toUpperCase(),
                          onPressed: () {
                            deletePaymentCard(card.id, profilePaymentModel);
                          },
                        ),
                  card.paymentMode == PaymentMode.MY_HSA
                      ? Padding(
                          padding: const EdgeInsets.only(bottom: MEDIUM_XXX),
                          child: Image.asset(Utils.cardTypeToImage(card.paymentMode), width: 38),
                        )
                      : Image.asset(Utils.cardTypeToImage(card.cardType), width: LARGE_XX),
                ],
              ),
            ),
          ],
        ),
      );
    }
  }

  deletePaymentCard(int id, ProfilePaymentModel profilePaymentModel) async {
    bool connectivityResult = await profilePaymentModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: profilePaymentModel.noInternetConnection);
      return;
    }
    bool result = await profilePaymentModel.deletePatientCard(id);
    if (result == true) {
      refreshCards(profilePaymentModel);
    }
  }

  refreshCards(ProfilePaymentModel profilePaymentModel) {
    profilePaymentModel.clearPaymentData();
  }

  void updateShoppingCart(ProfilePaymentModel profilePaymentModel) async {
    bool connectivityResult = await profilePaymentModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: profilePaymentModel.noInternetConnection);
      return;
    }
    if (profilePaymentModel.selectedCardId != null && profilePaymentModel.selectedCardId != -1) {
      profilePaymentModel.setState(ViewState.Busy);
      bool success = await Provider.of<VitaminsCatalogModel>(context).updateShoppingCartPaymentCard(profilePaymentModel.selectedCardId);
      bool completeCheckout = await Provider.of<VitaminsCatalogModel>(context).completeShoppingCartCheckOut();
      profilePaymentModel.setState(ViewState.Idle);
      if (success != null && completeCheckout != null && success == true && completeCheckout == true) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_complete);
        if (widget.onSuccess != null) widget.onSuccess();
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_complete_error);
        onFail(context, errMessage: profilePaymentModel.errorMessage);
      }
    } else {
      onFail(context, errMessage: LocalizationUtils.getSingleValueString("payment", "payment.label.card-selection-error"));
    }
  }

  onSuccess() {
    widget.onSuccess();
  }
}
