import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';

class PPInputDecor {
  static InputDecoration getDecoration(
      {String labelText,
      String hintText,
      bool collapsed = false,
      String helperText,
      Widget suffixIcon,
      filled = false,
      EdgeInsets contentPadding,
      Widget prefixIcon}) {
    TextStyle errorStyle = TextStyle(height: 0.5);
    TextStyle helperStyle = TextStyle(height: 0.5);
    if (collapsed) {
      errorStyle = TextStyle(height: 0.0);
      helperStyle = TextStyle(height: 0.0);
    }
    if (contentPadding == null)
      contentPadding = EdgeInsets.fromLTRB(12, 15, 12, 15);
    return InputDecoration(
        contentPadding: contentPadding,
        labelText: labelText,
        alignLabelWithHint: true,
        helperText: helperText,
        helperStyle: helperStyle,
        errorStyle: errorStyle,
        labelStyle:
            TextStyle(fontWeight: FontWeight.w500, height: 0.5, fontSize: 14.0),
        hintStyle: TextStyle(fontWeight: FontWeight.normal),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: BorderSide(color: brandColor),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: BorderSide(color: tertiaryColor),
        ),
        suffixIcon: suffixIcon,
        prefixIcon: prefixIcon,
        filled: filled,
        fillColor: Colors.white,
        hintText: hintText);
  }
}
