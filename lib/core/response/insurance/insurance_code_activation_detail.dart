import 'package:json_annotation/json_annotation.dart';

part 'insurance_code_activation_detail.g.dart';

@JsonSerializable()
class InsuranceCodeActivationDetail{

  bool updated;
  bool insertedPatient;
  bool createdUser;
  bool addedInsurance;
  bool addedAddress;
  bool addedDependents;

  InsuranceCodeActivationDetail(this.updated, this.insertedPatient, this.createdUser,
      this.addedInsurance, this.addedAddress, this.addedDependents);

  factory InsuranceCodeActivationDetail.fromJson(Map<String, dynamic> json) => _$InsuranceCodeActivationDetailFromJson(json);

  Map<String, dynamic> toJson() => _$InsuranceCodeActivationDetailToJson(this);

}
