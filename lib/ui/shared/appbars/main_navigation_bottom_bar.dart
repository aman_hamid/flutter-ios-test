import 'package:flutter/material.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/medications_model.dart';
import 'package:pocketpills/core/viewmodels/order_model.dart';
import 'package:pocketpills/core/viewmodels/prescription_model.dart';
import 'package:pocketpills/ui/custom_bottom_navigation_bar.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class MainNavigationBottomBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainNavigationBottomBarState();
  }
}

class MainNavigationBottomBarState extends State<MainNavigationBottomBar> {
  @override
  Widget build(BuildContext context) {
    return Consumer<DashboardModel>(
      builder: (BuildContext context, DashboardModel dashboardModel, Widget child) {
        return Container(
          decoration: new BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 2,
                spreadRadius: 0,
                offset: Offset(
                  0, // horizontal,
                  -2.0, // vertical,
                ),
              )
            ],
          ),
          child: CustomBottomNavigationBar(
            elevation: 0,
            type: BottomNavigationBarType.fixed,
            //type: const List<BottomNavigationBarType>,
            selectedIconTheme: const IconThemeData(color: null),
            unselectedIconTheme: const IconThemeData(color: null),
            items: [
              BottomNavigationBarItem(
                icon: Image.asset('graphics/icons/home.png', width: PPUIHelper.ICONSizeXSmall, height: PPUIHelper.ICONSizeXSmall),
                //activeIcon: ImageIcon(AssetImage('graphics/icons/home-active.png')),
                activeIcon: Image.asset('graphics/icons/home-active.png', width: PPUIHelper.ICONSizeXSmall, height: PPUIHelper.ICONSizeXSmall),
                title: Text(LocalizationUtils.getSingleValueString("common", "common.navbar.home"), style: TextStyle(height: 1.2, fontSize: 12.0)),
              ),
              BottomNavigationBarItem(
                icon: Image.asset('graphics/icons/med-vit.png', width: PPUIHelper.ICONSizeXSmall, height: PPUIHelper.ICONSizeXSmall),
                activeIcon: Image.asset('graphics/icons/med-vit-active.png', width: PPUIHelper.ICONSizeXSmall, height: PPUIHelper.ICONSizeXSmall),
                title: Text(LocalizationUtils.getSingleValueString("common", "common.navbar.meds"), style: TextStyle(height: 1.2, fontSize: 12.0)),
              ),
              BottomNavigationBarItem(
                icon: Image.asset('graphics/icons/prescription.png', width: PPUIHelper.ICONSizeXSmall, height: PPUIHelper.ICONSizeXSmall),
                activeIcon: Image.asset('graphics/icons/prescription-active.png', width: PPUIHelper.ICONSizeXSmall, height: PPUIHelper.ICONSizeXSmall),
                title: Text(LocalizationUtils.getSingleValueString("common", "common.navbar.prescription"), style: TextStyle(height: 1.2, fontSize: 12.0)),
              ),
              BottomNavigationBarItem(
                icon: Image.asset('graphics/icons/order.png', width: PPUIHelper.ICONSizeXSmall, height: PPUIHelper.ICONSizeXSmall),
                activeIcon: Image.asset('graphics/icons/order-active.png', width: PPUIHelper.ICONSizeXSmall, height: PPUIHelper.ICONSizeXSmall),
                title: Text(LocalizationUtils.getSingleValueString("common", "common.navbar.order"), style: TextStyle(height: 1.2, fontSize: 12.0)),
              ),
            ],
            //selectedItemColor: Colors.transparent,
            unselectedItemColor: primaryColor,
            iconSize: 24.0,
            showSelectedLabels: true,
            showUnselectedLabels: true,
            currentIndex: dashboardModel.dashboardIndex,
            backgroundColor: Colors.white,
            selectedFontSize: PPUIHelper.FontSizeSmall,
            unselectedLabelStyle: TextStyle(fontWeight: FontWeight.bold, color: primaryColor),
            selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold, color: primaryColor),
            onTap: (int index) {
              if (index == 0)
                Provider.of<HomeModel>(context).clearData();
              else if (index == 1)
                Provider.of<MedicationsModel>(context).clearData();
              else if (index == 2)
                Provider.of<PrescriptionModel>(context).clearData();
              else if (index == 3) Provider.of<OrderModel>(context).clearData();
              dashboardModel.dashboardIndex = index;
            },
          ),
        );
      },
    );
  }
}
