import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/login/login_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/forgotpassword_view.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/ui/views/start/start_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/route/CustomRoute.dart';
import 'package:provider/provider.dart';

class LoginWidget extends StatefulWidget {
  static const routeName = 'login';
  final String snackbarMessage;
  final String phone;
  final BaseStepperSource source;

  LoginWidget({Key key, this.snackbarMessage, this.phone, this.source = BaseStepperSource.UNKNOWN_SCREEN}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends BaseState<LoginWidget> with SingleTickerProviderStateMixin {
  final DataStoreService dataStore = locator<DataStoreService>();

  static const passwordValidationError = 'Enter your password to login';

  //final String loginHeading = "Welcome Back!";
  //final String loginDescription = "Login to your PocketPills account";

  String phState = '';
  String prevMobileNumber = "";

  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _phoneNumberFnode = FocusNode();
  final FocusNode _passwordFnode = FocusNode();
  final _formKey = GlobalKey<FormState>();

  bool phAutoValidate = false;
  bool showSnackBarMessage = false;
  bool phoneNumberField = false, passwordField = false;

  @override
  void initState() {
    super.initState();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.open_login);
    _phoneNumberController.addListener(phoneNumberListener);
    _passwordController.addListener(passwordFieldListener);
    _phoneNumberController.text = widget.phone;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.phone != null && widget.phone != "") {
      FocusScope.of(context).requestFocus(_passwordFnode);
    } else {
      FocusScope.of(context).requestFocus(_phoneNumberFnode);
    }
  }

  phoneNumberListener() {
    if (phoneNumberField == false) {
      phoneNumberField = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.phone_entered);
    }
  }

  checkPhoneNumber(BuildContext context) async {
    if (_phoneNumberController.text.length == 10 && prevMobileNumber.length != _phoneNumberController.text.length) {
      prevMobileNumber = _phoneNumberController.text;
      PhoneVerification checkPhone = await Provider.of<SignUpModel>(context).verifyPhone(prevMobileNumber);
      if (checkPhone.redirect != "LOGIN") {
        setState(() {
          phState = checkPhone.redirect;
          phAutoValidate = true;
        });
      }
    } else if (_phoneNumberController.text.length < 10)
      setState(() {
        phState = "";
      });
    prevMobileNumber = _phoneNumberController.text;
  }

  passwordFieldListener() {
    if (passwordField == false) {
      passwordField = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.password_entered);
    }
  }

  @override
  void dispose() {
    _phoneNumberController.removeListener(phoneNumberListener);
    _passwordController.removeListener(passwordFieldListener);
    _passwordFnode.dispose();
    _phoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider<LoginModel>(create: (_) => LoginModel()), ChangeNotifierProvider<SignUpModel>(create: (_) => SignUpModel())],
      child: Consumer2<LoginModel, SignUpModel>(builder: (BuildContext context, LoginModel loginModel, SignUpModel signUpModel, Widget child) {
        return FutureBuilder(
            future: loginModel.getLocalization(["login", "forgot"]),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return Scaffold(
                    body: GestureDetector(
                  onTap: () {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                  },
                  child: Builder(
                    builder: (BuildContext context) {
                      return SafeArea(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
                          child: Builder(
                            builder: (BuildContext context) {
                              if (showSnackBarMessage == false && widget.snackbarMessage != null) {
                                Future.delayed(const Duration(milliseconds: 100), () {
                                  setState(() {
                                    showSnackBarMessage = true;
                                  });
                                  this.showOnSnackBar(context, successMessage: widget.snackbarMessage);
                                });
                              }
                              return SingleChildScrollView(
                                child: Form(
                                  key: _formKey,
                                  child: Column(
                                    children: <Widget>[
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(
                                              height: LARGE_XX,
                                            ),
                                            Image.asset(
                                              getLogoImage(),
                                              height: LARGE_XX,
                                            ),
                                            SizedBox(
                                              height: LARGE_XX,
                                            ),
                                            Text(
                                              LocalizationUtils.getSingleValueString("login", "login.all.title"),
                                              style: REGULAR_XXX_PRIMARY_BOLD,
                                            ),
                                            SizedBox(
                                              height: SMALL_X,
                                            ),
                                            Text(
                                              LocalizationUtils.getSingleValueString("login", "login.all.description"),
                                              style: MEDIUM_XXX_SECONDARY,
                                            ),
                                            SizedBox(
                                              height: LARGE,
                                            ),
                                          ],
                                        ),
                                      ),
                                      getUsernameField(),
                                      PPUIHelper.verticalSpaceMedium(),
                                      getPasswordField(context, loginModel, signUpModel),
                                      PPUIHelper.verticalSpaceLarge(),
                                      (loginModel.state == ViewState.Busy || signUpModel.state == ViewState.Busy)
                                          ? ViewConstants.progressIndicator
                                          : getLoginButton(context, loginModel, signUpModel),
                                      PPUIHelper.verticalSpaceLarge(),
                                      Center(
                                          child: TransparentButton(
                                              text: LocalizationUtils.getSingleValueString("login", "login.all.forgot-password"),
                                              onPressed: () {
                                                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_forget_password);
                                                Navigator.pushReplacement(
                                                    context, CustomRoute(builder: (BuildContext context) => ForgotPasswdWidget(phoneNumber: _phoneNumberController.text)));
                                              })),
                                      PPUIHelper.verticalSpaceXLarge(),
                                      Center(
                                        child: TransparentButton(
                                            text: LocalizationUtils.getSingleValueString("login", "login.fields.new-member"),
                                            onPressed: () {
                                              analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_signup_login_view);
                                              Navigator.of(context).pushNamedAndRemoveUntil(StartView.routeName, (Route<dynamic> route) => false);
                                            }),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      );
                    },
                  ),
                ));
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  TextFormField getUsernameField() {
    return PPFormFields.getNumericFormField(
        focusNode: _phoneNumberFnode,
        textInputAction: TextInputAction.next,
        keyboardType: TextInputType.phone,
        maxLength: 10,
        minLength: 10,
        labelText: LocalizationUtils.getSingleValueString("app-landing", "app-landing.labels.phone"),
        errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
        controller: _phoneNumberController,
        onTextChanged: (value) {
          checkPhoneNumber(context);
        },
        onFieldSubmitted: (value) {
          FocusScope.of(context).requestFocus(_passwordFnode);
        },
        validator: (value) {
          if (value.isEmpty) return LocalizationUtils.getSingleValueString("common", "common.label.required") + "*";
          String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
          if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error");
          if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("login", "login.fields.cell-error2");
          if (phState != "") return getPhoneValidation()[phState];
          return null;
        });
  }

  Widget getPasswordField(context, loginModel, signUpModel) {
    return PPFormFields.getPasswordFormField(
        key: Key("login_password"),
        focusNode: _passwordFnode,
        controller: _passwordController,
        textInputAction: TextInputAction.done,
        hintText: "", //LocalizationUtils.getSingleValueString("login", "login.fields.password-hint"),
        helperText: LocalizationUtils.getSingleValueString("login", "login.fields.password-info"),
        minLength: 8,
        onErrorStr: LocalizationUtils.getSingleValueString("login", "login.fields.password-hint"),
        labelText: LocalizationUtils.getSingleValueString("login", "login.fields.password-label"),
        onFieldSubmitted: (value) {
          onLoginPressed(context, loginModel, signUpModel);
        },
        validator: (value) {
          if (value.isEmpty) return LocalizationUtils.getSingleValueString("login", "login.fields.password-info");
          if (value.length < 8) return LocalizationUtils.getSingleValueString("login", "login.fields.password-error-text");
          return null;
        });
  }

  getLoginButton(context, loginModel, signUpModel) {
    return PrimaryButton(
        fullWidth: true,
        isSmall: true,
        text: LocalizationUtils.getSingleValueString("login", "login.all.login-securely"),
        iconData: Icons.arrow_forward,
        onPressed: () {
          onLoginPressed(context, loginModel, signUpModel);
        });
  }

  void onLoginPressed(context, LoginModel loginModel, SignUpModel signUpModel) async {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }

    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_login);
    if (_formKey.currentState.validate()) {
      var success = await loginModel.login(_phoneNumberController.text, _passwordController.text);
      if (success == true) {
        signUpModel.handleLoginSuccess(context);
        bool succesGetUserInfo = await signUpModel.getUserInfo(listenable: true);
        if (succesGetUserInfo == true) {
          signUpModel.getSignUpFlow(context);
        } else {
          onFail(context, errMessage: signUpModel.errorMessage);
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.failed_login);
        onFail(context, errMessage: loginModel.errorMessage);
      }
    } else {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.failed_login_validation);
      if (phState != "")
        onFail(context, content: onPhoneVerificationError());
      else
        onFail(context);
    }
  }

  Widget onPhoneVerificationError() {
    Widget textWidget = Text(LocalizationUtils.getSingleValueString("login", "login.fields.cellphone-error") + "  ");
    Widget linkWidget = InkWell(
      child: Text(
        LocalizationUtils.getSingleValueString("login", "login.fields.signup"),
        textAlign: TextAlign.left,
        style: TextStyle(color: linkColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.2),
      ),
      onTap: () {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_signup_login_view);
        Navigator.pushNamedAndRemoveUntil(context, SplashView.routeName, (Route<dynamic> route) => false);
      },
    );
    if (phState == "FORGOT") {
      linkWidget = InkWell(
        child: Text(
          LocalizationUtils.getSingleValueString("login", "login.all.forgot-password"),
          textAlign: TextAlign.left,
          style: TextStyle(color: linkColor, fontWeight: FontWeight.bold, fontSize: 16.0, height: 1.2),
        ),
        onTap: () {
          // Can't pass the route name in pushReplacement
          Navigator.pushReplacement(context, CustomRoute(builder: (BuildContext context) => ForgotPasswdWidget(phoneNumber: _phoneNumberController.text)));
        },
      );
      textWidget = Text(LocalizationUtils.getSingleValueString("forgot", "forgot.all.password-not-set") + "  ");
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[textWidget, linkWidget],
    );
  }
}
