// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'localization_update_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocalizationUpdateResponse _$LocalizationUpdateResponseFromJson(
    Map<String, dynamic> json) {
  return LocalizationUpdateResponse(
    json['moduleKey'] as String,
    json['updatedDateTimeInUTC'] as String,
  );
}

Map<String, dynamic> _$LocalizationUpdateResponseToJson(
        LocalizationUpdateResponse instance) =>
    <String, dynamic>{
      'moduleKey': instance.moduleKey,
      'updatedDateTimeInUTC': instance.updatedDateTimeInUTC,
    };
