import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/transfer/subdivisions_name.dart';
part 'subdivisions.g.dart';

@JsonSerializable()
class Subdivisions {
  SubdivisionsName names;

  Subdivisions(this.names);

  factory Subdivisions.fromJson(Map<String, dynamic> json) => _$SubdivisionsFromJson(json);
}
