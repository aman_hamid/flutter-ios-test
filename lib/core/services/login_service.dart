import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:pocketpills/core/enums/signup_stepper_state_enums.dart';
import 'package:pocketpills/core/models/signup_dto.dart';
import 'package:pocketpills/core/request/device_body.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/login_response.dart';
import 'package:pocketpills/core/response/signup_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/core/apis/http_apis.dart';

class LoginService {
  final HttpApi _api = locator<HttpApi>();
  static final DataStoreService dataStore = locator<DataStoreService>();
  static final Analytics analyticsEvents = locator<Analytics>();

  final Base64Codec base64 = Base64Codec();

  Future<Response> login(String phoneNumber, String password) async {
    DeviceBody loginRequest = DeviceBody();
    var stringToBeEncoded = phoneNumber + ":" + password;
    var encodedString =
        "Basic " + base64.encode(utf8.encode(stringToBeEncoded));
    await dataStore.saveAuthorizationToken(encodedString);
    await loginRequest.init();
    var response =
        await _api.postLogin(encodedString, RequestWrapper(body: loginRequest));
    await loginUser(response);
    return response;
  }

  //Response should have a valid LoginResponse to login user
  Future<bool> loginUser(Response response) async {
    if (response != null && response.data['details'] != null) {
      BaseResponse<LoginResponse> loginResponse =
          BaseResponse<LoginResponse>.fromJson(response.data);
      await dataStore.writeInt(
          DataStoreService.USERID, loginResponse.response.userId);

      if (loginResponse.response.userPatientList.length != null &&
          loginResponse.response.userPatientList.length > 0) {
        await dataStore.writeInt(DataStoreService.PATIENTID,
            loginResponse.response.userPatientList[0].patientId);
        try {
          await dataStore.writeString(DataStoreService.FIRSTNAME,
              loginResponse.response.userPatientList[0].patient.firstName);
          await dataStore.writeString(DataStoreService.EMAIL,
              loginResponse.response.userPatientList[0].patient.email);
          await dataStore.writeString(DataStoreService.LASTNAME,
              loginResponse.response.userPatientList[0].patient.lastName);
          await dataStore.writeString(
              DataStoreService.LANGUAGE, ViewConstants.languageId);
          await dataStore.writeString(DataStoreService.DATE_OF_BIRTH,
              loginResponse.response.userPatientList[0].patient.birthDate);
          await dataStore.writeString(
              DataStoreService.ZIP,
              loginResponse.response.userPatientList[0].patient.countryCode
                  .toString());
          await dataStore.writeString(DataStoreService.GENDER,
              loginResponse.response.userPatientList[0].patient.gender);
        } catch (ex) {
          Crashlytics.instance.log(ex.toString());
        }
      }
      try {
        await dataStore
            .saveAuthorizationToken(response.headers['Authorization'][0]);
      } catch (ex) {
        Crashlytics.instance.log(ex.toString());
      }
      try {
        await getAuthCookie(response);
      } catch (ex) {
        Crashlytics.instance.log(ex.toString());
      }
      await dataStore.markSelectedPatientAsCaregiver(true);
      await dataStore.writeBoolean(DataStoreService.IS_LOGGED_IN, true);

      analyticsEvents.setUserIdentifiers();
      return true;
    }
    return false;
  }

  Future<bool> registerUser(Response response) async {
    if (response != null) {
      BaseResponse<SignupResponse> res =
          BaseResponse<SignupResponse>.fromJson(response.data);
      await dataStore.writeInt(DataStoreService.USERID, res.response.userId);
      await dataStore.writeInt(
          DataStoreService.PATIENTID, res.response.patientId);
      print('AMANPATIENTID:${res.response.patientId}');
      SignupDto dto = res.response.signupDto;
      await dataStore.writeString(DataStoreService.FIRSTNAME, dto.firstName);
      await dataStore.writeString(DataStoreService.EMAIL, dto.email);
      await dataStore.writeString(DataStoreService.LASTNAME, dto.lastName);
      await dataStore.writeString(
          DataStoreService.LANGUAGE,
          dto.locale == ViewConstants.languageEnglish
              ? ViewConstants.languageId
              : ViewConstants.languageIdFr);
      await dataStore.writeString(DataStoreService.DATE_OF_BIRTH,
          dto.birthDate != null ? dto.birthDate : null);
      await dataStore.writeString(DataStoreService.GENDER, dto.gender);
      await dataStore.writeString(DataStoreService.PHONE,
          dto.phone != null ? dto.phone.toString() : null);
      await dataStore.writeBoolean(
          DataStoreService.HAS_PASSWORD, dto.hasPassword);
      await dataStore.writeBoolean(DataStoreService.VERIFIED, dto.verified);
      print("AMANEMAILVERIFIED:${dto.isMailVerified}");
      await dataStore.writeBoolean(
          DataStoreService.MAIL_VERIFIED, dto.isMailVerified);

      await dataStore.writeString(DataStoreService.PROVINCE, dto.province);

      /* try {
>>>>>>> 72c02d25b9afc7437e11ceb5ac5245433dad9ade
        await dataStore
            .saveAuthorizationToken(response.headers['Authorization'][0]);
      } catch (ex) {
        Crashlytics.instance.log(ex.toString());
      }*/

      if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == false) {
        try {
          await dataStore
              .saveAuthorizationToken(response.headers['Authorization'][0]);
        } catch (ex) {
          Crashlytics.instance.log(ex.toString());
        }
      }

      try {
        await getAuthCookie(response);
      } catch (ex) {
        Crashlytics.instance.log(ex.toString());
      }
      await dataStore.writeBoolean(DataStoreService.IS_LOGGED_IN, true);

      if (res.response.prescription != null) {
        await dataStore.writeBoolean(
            DataStoreService.COMPLETE_TRANSFER_MODULE, true);
        if ((res.response.prescription.pharmacyAddress == null ||
                res.response.prescription.pharmacyName == null ||
                res.response.prescription.pharmacyPhone == null) &&
            res.response.prescription.prescriptionState == "PENDING") {
          await dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, true);
          await dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, false);
          GlobalVariable().prescriptionId =
              res.response.prescription.omsPrescriptionId;
          GlobalVariable().userFlow = PrescriptionTypeConstant.SIGNUP;
        } else {
          await dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, true);
          await dataStore.writeBoolean(
              DataStoreService.TRANSFER_SKIPPED, false);
        }
      } else {
        await dataStore.writeBoolean(
            DataStoreService.COMPLETE_TRANSFER_MODULE, false);
      }

      if (dto.firstName == null ||
          dto.lastName == null ||
          dto.birthDate == null) {
        await dataStore.writeBoolean(
            DataStoreService.COMPLETE_ABOUT_YOU, false);
      } else {
        await dataStore.writeBoolean(DataStoreService.COMPLETE_ABOUT_YOU, true);
      }
      if (dto.province == null || dto.gender == null || dto.email == null) {
        await dataStore.writeBoolean(
            DataStoreService.COMPLETE_ALMOST_DONE, false);
      } else {
        await dataStore.writeBoolean(
            DataStoreService.HAS_DAILY_MEDICATION, dto.hasDailyMedication);
        await dataStore.writeBoolean(
            DataStoreService.IS_CARE_GIVER, dto.isCaregiver);
        await dataStore.writeBoolean(
            DataStoreService.COMPLETE_ALMOST_DONE, true);
      }

      if (dto.password == null ||
          (dto.verified == false && dto.isMailVerified == false)) {
        await dataStore.writeBoolean(
            DataStoreService.COMPLETE_OTP_PASSWORD, false);
      } else {
        await dataStore.writeBoolean(
            DataStoreService.COMPLETE_OTP_PASSWORD, true);
      }

      analyticsEvents.setUserIdentifiers();
      return true;
    } else {
      return false;
    }
  }

  Future<void> getAuthCookie(Response response) async {
    if (response == null ||
        response.headers == null ||
        response.headers['set-cookie'] == null) return null;
    for (int i = 0; i < response.headers['set-cookie'].length; i++) {
      if (response.headers['set-cookie'][i].contains("cookieAuthorization")) {
        return await dataStore.saveAuthorizationCookie(
            response.headers['set-cookie'][i].split(';')[0].split('=')[1]);
      }
    }
  }

  bool isUserLogin() {
    return dataStore.readBoolean(DataStoreService.IS_LOGGED_IN);
  }

  int checkOutSignUpFlow() {
    bool aboutYouDetails =
        dataStore.readBoolean(DataStoreService.COMPLETE_ABOUT_YOU);
    bool transferDetails =
        dataStore.readBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE);
    bool telehalthCard = dataStore
        .readBoolean(DataStoreService.COMPLETE_TELEHEALTH_HELTHCARD_UPLOAD);
    bool almostDone =
        dataStore.readBoolean(DataStoreService.COMPLETE_ALMOST_DONE);
    bool otpPassword =
        dataStore.readBoolean(DataStoreService.COMPLETE_OTP_PASSWORD);
    if (aboutYouDetails == false) {
      return SignupStepperStateEnums.SIGN_UP_ABOUT_YOU;
    } else if (transferDetails == false) {
      return SignupStepperStateEnums.SIGN_UP_TRANSFER;
    } else if (telehalthCard == true) {
      return SignupStepperStateEnums.SIGN_UP_TELEHEALTH_CARD;
    } else if (almostDone == false) {
      return SignupStepperStateEnums.SIGN_UP_ALMOST_DONE;
    } else if (otpPassword == false) {
      return SignupStepperStateEnums.SIGN_UP_OTP_PASSWORD;
    }
  }
}
