import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'address.g.dart';

@JsonSerializable()
class Address extends BaseRequest {
  String nickname;
  String province;
  String postalCode;
  String streetAddress;
  String streetAddressLineTwo;
  String city;
  @JsonKey(includeIfNull: false)
  String country;
  @JsonKey(includeIfNull: false)
  bool disabled;
  @JsonKey(includeIfNull: false)
  int id;
  bool isDefault;
  @JsonKey(includeIfNull: false)
  int patientId;

  Address(
      {this.province,
      this.city,
      this.country,
      this.nickname,
      this.postalCode,
      this.streetAddress,
      this.streetAddressLineTwo,
      this.disabled,
      this.id,
      this.patientId,
      this.isDefault});

  factory Address.fromJson(Map<String, dynamic> json) => _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);
}
