// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_signup_preference_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthSignUpPreferenceRequest _$TelehealthSignUpPreferenceRequestFromJson(
    Map<String, dynamic> json) {
  return TelehealthSignUpPreferenceRequest(
    type: json['type'] as String,
    prescriptionComment: json['prescriptionComment'] as String,
    prescriptionFilledByExternalPharmacy:
        json['prescriptionFilledByExternalPharmacy'] as bool,
    prescriptionState: json['prescriptionState'] as String,
    prescriptionRequestReason: json['prescriptionRequestReason'] as String,
    prescriptionRequestCategory: json['prescriptionRequestCategory'] as String,
    telehealthRequestedMedications:
        json['telehealthRequestedMedications'] as String,
    prescriptionMedicalConditions:
        json['prescriptionMedicalConditions'] as String,
    appointmentTime: PPDateUtils.fromStr(json['appointmentTime'] as String),
  );
}

Map<String, dynamic> _$TelehealthSignUpPreferenceRequestToJson(
        TelehealthSignUpPreferenceRequest instance) =>
    <String, dynamic>{
      'type': instance.type,
      'prescriptionComment': instance.prescriptionComment,
      'prescriptionFilledByExternalPharmacy':
          instance.prescriptionFilledByExternalPharmacy,
      'prescriptionState': instance.prescriptionState,
      'prescriptionRequestReason': instance.prescriptionRequestReason,
      'prescriptionRequestCategory': instance.prescriptionRequestCategory,
      'telehealthRequestedMedications': instance.telehealthRequestedMedications,
      'prescriptionMedicalConditions': instance.prescriptionMedicalConditions,
      'appointmentTime': PPDateUtils.toStr(instance.appointmentTime),
    };
