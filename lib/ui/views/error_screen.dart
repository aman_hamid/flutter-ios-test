import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ErrorScreen extends StatelessWidget {
  final Function onRetry;
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  final AsyncMemoizer<bool> _memoizer = AsyncMemoizer();

  ErrorScreen({Key key, this.onRetry}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: checkSession(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.data != null && snapshot.data == true) {
            return getErrorWidget(context, onRetry, true);
          } else if (snapshot.data == false) {
            return getErrorWidget(context, onRetry, false);
          }
          return LoadingScreen();
        });
  }

  Future<bool> checkSession() async {
    return this._memoizer.runOnce(() async {
      Response success = await _api.checkSession();
      if (success.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    });
  }

  Future<void> logout(BuildContext context) async {
    bool success = await dataStore.logOut();
    if (success) {
      Navigator.pushNamedAndRemoveUntil(context, SplashView.routeName, (Route<dynamic> route) => false);
    }
  }

  Widget getErrorWidget(BuildContext context, Function onRetry, bool isSession) {
    return Scaffold(
      body: Container(
        child: Center(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "graphics/error_image.png",
                      width: MediaQuery.of(context).size.width / 2,
                    ),
                  ],
                ),
                PPUIHelper.verticalSpaceMedium(),
                PPTexts.getMainViewHeading(getErrorTitle(), textAlign: TextAlign.center, mainAxisAlignment: MainAxisAlignment.center),
                PPTexts.getSecondaryHeading(getErrorDescription(), isBold: false, textAlign: TextAlign.center, mainAxisAlignment: MainAxisAlignment.center),
                PPUIHelper.verticalSpaceLarge(),
                PrimaryButton(
                  fullWidth: true,
                  text: getButtonText().toUpperCase(),
                  onPressed: isSession == false
                      ? () {
                          logout(context);
                        }
                      : onRetry != null
                          ? onRetry
                          : () {
                              Provider.of<DashboardModel>(context).clearUserPatientList();
                              Navigator.pushNamedAndRemoveUntil(context, DashboardWidget.routeName, (Route<dynamic> route) => false);
                            },
                )
              ],
            ),
          ),
        )),
      ),
    );
  }
}
