import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/singup_stepper_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_success_details_view.dart';
import 'package:pocketpills/ui/views/signup/signup_user_contact_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class SignUpEmailDetailsWidget extends StatefulWidget {
  static const routeName = 'signupEmailDetails';

  final BaseStepperSource source;

  SignUpEmailDetailsWidget({Key key, this.source}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignUpEmailDetailsState();
  }
}

class SignUpEmailDetailsState extends BaseState<SignUpEmailDetailsWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();

  final _passwordDetailsformKey = GlobalKey<FormState>();

  bool showReferral = false;
  bool autovalidate = false;

  static bool gender = false, password = false, email = false, referralCode = false;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _invitationCodeController = TextEditingController();

  FocusNode _emailFnode;

  static bool selfMedication = false, manageMedication = false, provincial = false;

  @override
  void initState() {
    super.initState();
    _emailController.addListener(emailListener);
    _invitationCodeController.addListener(referralListener);
    _emailFnode = FocusNode();
  }

  @override
  void dispose() {
    _emailController.removeListener(emailListener);
    _invitationCodeController.removeListener(referralListener);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _emailController.text = dataStore.readString(DataStoreService.EMAIL);
    String referralCode = dataStore.readString(DataStoreService.REFERRAL_CODE);
    String invitationCode = dataStore.readString(DataStoreService.INVITATION_CODE);
    (referralCode != null || invitationCode != null) ? showReferral = true : showReferral = false;
    _invitationCodeController.text = referralCode != null
        ? referralCode
        : invitationCode != null
            ? invitationCode
            : "";
  }

  emailListener() {
    if (email == false) {
      email = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_email_entered);
    }
  }

  passwordListener() {
    if (password == false) {
      password = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_newpassword_entered);
    }
  }

  referralListener() {
    if (referralCode == false) {
      referralCode = true;
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_referralcode_entered);
    }
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.source == BaseStepperSource.NEW_USER
          ? () async {
              Navigator.pop(context);
              return true;
            }
          : () => _onBackPressed(context),
      child: ChangeNotifierProvider(
        create: (_) => SignUpAboutYouModel(),
        child: Consumer<SignUpAboutYouModel>(builder: (BuildContext context, SignUpAboutYouModel model, Widget child) {
          return FutureBuilder(
              future: model.getLocalization(["signup"]),
              builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getMainView(model);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
          getMainView(model);
        }),
      ),
    );
  }

  Widget getMainView(SignUpAboutYouModel model) {
    return BaseScaffold(
      appBar: widget.source == BaseStepperSource.NEW_USER
          ? SignUpStepperAppBar(
              appBar: AppBar(),
              step: "5/5",
            )
          : null,
      bottomNavigationBar: Builder(
          builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                child: model.state == ViewState.Busy
                    ? ViewConstants.progressIndicator
                    : PrimaryButton(
                        text: LocalizationUtils.getSingleValueString("signup", "signup.almostdone.next"),
                        onPressed: () {
                          onNextClick(context, model);
                        },
                        fullWidth: true),
              )),
      body: GestureDetector(
        onTap: () {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: SafeArea(
          child: Builder(
            builder: (BuildContext context) {
              return SingleChildScrollView(child: getPasswordDetailsView(model));
            },
          ),
        ),
      ),
    );
  }

  Widget getPasswordDetailsView(SignUpAboutYouModel model) {
    return Form(
      key: _passwordDetailsformKey,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, top: REGULAR_XXX),
                  child: Column(
                    children: <Widget>[
                      PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("signup", "signup.almostdone.title-employer")),
                      SizedBox(height: SMALL_X),
                      PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("signup", "signup.almostdone.description-employer"), isBold: false),
                      SizedBox(height: REGULAR_XXX),
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.only(
                    left: MEDIUM_XXX,
                    right: MEDIUM_XXX,
                  ),
                  child: Column(children: <Widget>[
                    SizedBox(height: MEDIUM_XXX),
                    PPFormFields.getTextField(
                        keyboardType: TextInputType.emailAddress,
                        focusNode: _emailFnode,
                        controller: _emailController,
                        textInputAction: TextInputAction.done,
                        onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
                        decoration: PPInputDecor.getDecoration(
                            labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.email-label"),
                            hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-email")),
                        onFieldSubmitted: (value) {
                          onNextClick(context, model);
                        }),
                    SizedBox(height: MEDIUM_XXX),
                    Container(
                      decoration: BoxDecoration(color: Color(0xfff7f7f7), borderRadius: BorderRadius.circular(4.0)),
                      child: Column(
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      showReferral == false ? showReferral = true : showReferral = false;
                                    });
                                  },
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        LocalizationUtils.getSingleValueString("signup", "signup.fields.have-referral-code"),
                                        style: TextStyle(fontWeight: FontWeight.w600),
                                      ),
                                      new Expanded(
                                          child: Align(
                                        alignment: Alignment.topRight,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              showReferral == false ? showReferral = true : showReferral = false;
                                            });
                                          },
                                          child: Image.asset(showReferral == false ? 'graphics/icons/ic_round_arrow_down.png' : 'graphics/icons/ic_round_arrow_up.png',
                                              width: 24.0, height: 24.0),
                                        ),
                                      )),
                                    ],
                                  ))),
                          getReferralCodeView(),
                        ],
                      ),
                    ),
                  ])),
            ],
          ),
        ],
      ),
    );
  }

  Widget getReferralCodeView() {
    if (showReferral == true) {
      return AnimatedContainer(
        duration: Duration(seconds: 2),
        child: Column(
          children: <Widget>[
            PPDivider(),
            PPUIHelper.verticalSpaceLarge(),
            Padding(
                padding: EdgeInsets.all(12.0),
                child: PPFormFields.getTextField(
                    controller: _invitationCodeController,
                    decoration: PPInputDecor.getDecoration(
                        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.referral-label"),
                        hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-referral-code")),
                    validator: (value) {
                      return null;
                    }))
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  onNextClick(context, SignUpAboutYouModel signUpAboutYouModel) async {
    var connectivityResult = await signUpAboutYouModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: signUpAboutYouModel.noInternetConnection);
      return;
    }
    if (_passwordDetailsformKey.currentState.validate() && StringUtils.isEmail(_emailController.text)) {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      var success = await signUpAboutYouModel.updateAboutYou(email: _emailController.text, referral: _invitationCodeController.text == "" ? null : _invitationCodeController.text);
      if (success) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_signupdetails_verification);
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_congratulations);
        if (dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED) == true) {
          Navigator.of(context)
              .pushNamedAndRemoveUntil(SignupUserContactWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(SignUpSuccessDetailsWidget.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_signupdetails_verification_failed);
        onFail(context, errMessage: signUpAboutYouModel.errorMessage);
      }
    } else if (StringUtils.isEmail(_emailController.text) == false) {
      onFail(context, errMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.valid-email"));
    }
  }
}
