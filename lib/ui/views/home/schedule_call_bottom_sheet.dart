import 'package:flutter/material.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ScheduleCallBottomSheet extends StatefulWidget {
  final BuildContext context;
  final bool scheduleCall;

  ScheduleCallBottomSheet({this.context, this.scheduleCall});

  _ScheduleCallBottomSheetState createState() => _ScheduleCallBottomSheetState();
}

class _ScheduleCallBottomSheetState extends BaseState<ScheduleCallBottomSheet> with SingleTickerProviderStateMixin {
  Widget build(BuildContext bc) {
    return MultiProvider(
        providers: [ChangeNotifierProvider<UserContactModel>(create: (_) => UserContactModel())],
        child: Consumer<UserContactModel>(builder: (BuildContext context, UserContactModel userContactModel, Widget child) {
          return Wrap(
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: MEDIUM_X, right: MEDIUM_X, left: MEDIUM_X),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                widget.scheduleCall == true
                                    ? LocalizationUtils.getSingleValueString("modal", "modal.schedule.title")
                                    : LocalizationUtils.getSingleValueString("modal", "modal.schedule.reschedule-call"),
                                style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
                              ),
                              SizedBox(
                                height: SMALL,
                              ),
                              Text(
                                LocalizationUtils.getSingleValueString("modal", "modal.schedule.time-call-description"),
                                style: MEDIUM_XX_SECONDARY,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM_XXX),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("modals", "modals.timings.day")),
                          PPUIHelper.verticalSpaceSmall(),
                          PPFormFields.getDropDown(
                            value: userContactModel.day,
                            onChanged: (String newValue) async {
                              userContactModel.setDay(newValue);
                            },
                            items: getDayMap()
                                .map((String key, String value) {
                                  return MapEntry(
                                      key,
                                      DropdownMenuItem<String>(
                                        value: key,
                                        child: Text(value),
                                      ));
                                })
                                .values
                                .toList(),
                          ),
                          PPUIHelper.verticalSpaceMedium(),
                          PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("modals", "modals.timings.time")),
                          PPUIHelper.verticalSpaceSmall(),
                          PPFormFields.getDropDown(
                            value: userContactModel.time,
                            onChanged: (String newValue) async {
                              userContactModel.setTime(newValue);
                            },
                            items: getTimeMap()
                                .map((String key, String value) {
                                  return MapEntry(
                                      key,
                                      DropdownMenuItem<String>(
                                        value: key,
                                        child: Text(value),
                                      ));
                                })
                                .values
                                .toList(),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: MEDIUM_XXX,
                            ),
                            child: PPDivider(),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(MEDIUM_XXX),
                            child: Row(
                              children: <Widget>[
                                SecondaryButton(
                                  text: LocalizationUtils.getSingleValueString("modal", "modal.all.close"),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                SizedBox(
                                  width: MEDIUM_XXX,
                                ),
                                PrimaryButton(
                                  text: widget.scheduleCall == true
                                      ? LocalizationUtils.getSingleValueString("modal", "modal.schedule.submit")
                                      : LocalizationUtils.getSingleValueString("modal", "modal.schedule.reschedule"),
                                  onPressed: () async {
                                    await userContactModel.updateContactInfo();
                                    Navigator.pop(context);
                                  },
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          );
        }));
  }
}
