import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';

class Avatar extends StatelessWidget {
  final String displayImage;
  final bool displayBorder;
  final double width;
  final double height;

  Avatar({
    @required this.displayImage,
    this.displayBorder = true,
    this.width = 50,
    this.height = 50,
  });

  @override
  Widget build(BuildContext context) {
    Widget statusIndicator;
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: displayBorder
                ? Border.all(
                    color: violetColor,
                    width: 3,
                  )
                : Border(),
          ),
          padding: EdgeInsets.only(left: 4, right: 4),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Image.network(
              displayImage,
              width: width,
              height: height,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ],
    );
  }
}
