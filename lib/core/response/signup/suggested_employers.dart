
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/signup/invitation_code_data_set.dart';
part 'suggested_employers.g.dart';

@JsonSerializable()
class SuggestedEmployers{

  int id;
  bool disabled;
  String employerName;
  String employerDisplayName;
  List<InvitationCodeDataSet> invitationCodeDataSet;

  SuggestedEmployers(this.id, this.disabled, this.employerName, this.invitationCodeDataSet, this.employerDisplayName);

  factory SuggestedEmployers.fromJson(Map<String, dynamic> json) =>
      _$SuggestedEmployersFromJson(json);

}