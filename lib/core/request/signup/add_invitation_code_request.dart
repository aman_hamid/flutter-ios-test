import 'package:pocketpills/core/request/base_request.dart';

class AddInvitationCodeRequest extends BaseRequest {
  String invitationCode;

  AddInvitationCodeRequest({
    this.invitationCode,
  });

  Map<String, dynamic> toJson() => <String, dynamic>{
        'invitationCode': this.invitationCode,
      };
}
