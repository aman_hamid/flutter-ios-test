import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

part 'medicine_response.g.dart';

@JsonSerializable()
class MedicineResponse {
  @JsonKey(name: 'success')
  bool status;

  @JsonKey(name: 'userMessage')
  String errMessage;

  @JsonKey(name: 'message')
  String apiMessage;

  @JsonKey(name: 'medicine')
  Medicine medicine;

  MedicineResponse({this.status, this.errMessage, this.apiMessage, this.medicine});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory MedicineResponse.fromJson(Map<String, dynamic> json) => _$MedicineResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MedicineResponseToJson(this);
}
