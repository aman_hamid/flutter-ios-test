import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:pocketpills/app_config.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/logos.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

const userAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';

class ChatWidget extends StatefulWidget {
  static const routeName = '/chat';

  @override
  State<StatefulWidget> createState() {
    return ChatState();
  }
}

class ChatState extends State<ChatWidget> {
  String webviewUrl = locator<AppConfig>().webviewBaseUrl + "/chat";
  final String logoutUrl = 'logout';

  final DataStoreService dataStore = locator<DataStoreService>();
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  bool cookieSet = false;

  @override
  void initState() {
    super.initState();
    String authCookie = dataStore.getAuthorizationCookie();
    String userId = dataStore.getUserId().toString();
    String patientId = dataStore.getPatientId().toString();
    webviewUrl = webviewUrl +
        "?cookieAuthorization=$authCookie&userId=$userId&patientId=$patientId";
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
        url: webviewUrl,
        //resizeToAvoidBottomInset: Platform.isIOS ? false : true,
        appBar: AppBar(
          title: Text(LocalizationUtils.getSingleValueString(
              "common", "common.label.chat-with-us")),
        ),
        withJavascript: true,
        withZoom: true,
        withLocalStorage: true,
        clearCookies: false,
        hidden: true,
        initialChild: Center(
            child: Image.asset(
          Logos.ppLogoPath,
          width: 100,
          height: 100,
        )),
        userAgent: userAgent,
        afterWebLoadComplete: () {});
  }
}
