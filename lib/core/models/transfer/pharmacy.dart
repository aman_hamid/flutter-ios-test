import 'package:json_annotation/json_annotation.dart';
part 'pharmacy.g.dart';

@JsonSerializable()
class Pharmacy {
  String pharmacyName;
  String pharmacyAddress;
  String pharmacyPlaceId;

  Pharmacy({this.pharmacyName, this.pharmacyPlaceId, this.pharmacyAddress});

  factory Pharmacy.fromJson(Map<String, dynamic> json) => _$PharmacyFromJson(json);
}
