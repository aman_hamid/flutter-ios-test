import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';

class MedicineDetailViewModel extends BaseModel {
  String _dropDownValue = "0";
  bool _coverDispeningFee = false;
  Medicine _medicine;
  TextEditingController _quantityController = new TextEditingController();
  num _totalCost;
  num _drugCost;

  String get dropDownValue => _dropDownValue;

  num getDrugCost() {
    num quantity = 0;
    if (_quantityController.text.length > 0) {
      quantity = int.parse(_quantityController.text);
    } else
      return 0;
    num cost = medicine.getDrugCost(quantity: quantity);
    return cost;
  }

  num getTotalCost() {
    int quantity = 0;
    if (_quantityController.text.length > 0) {
      quantity = int.parse(_quantityController.text);
    } else
      return 0;
    num cost = medicine.getTotalCost(
        coverDispensingFee: coverDispeningFee, quantity: quantity, insuranceCover: int.parse(dropDownValue));
    return cost;
  }

  void onQuantityChange() {
    drugCost = getDrugCost();
    totalCost = getTotalCost();
    notifyListeners();
  }

  void onInsuranceCoverageChange() {
    totalCost = getTotalCost();
    notifyListeners();
  }

  set dropDownValue(String value) {
    _dropDownValue = value;
    notifyListeners();
  }

  bool get coverDispeningFee => _coverDispeningFee;

  set coverDispeningFee(bool value) {
    _coverDispeningFee = value;
    notifyListeners();
  }

  Medicine get medicine => _medicine;

  set medicine(Medicine value) {
    _medicine = value;
    getDrugCost();
    getTotalCost();
    quantityController.text = value.defaultQuantity.toString();
    notifyListeners();
  }

  num get totalCost => _totalCost;

  set totalCost(num value) {
    _totalCost = value;
    notifyListeners();
  }

  num get drugCost => _drugCost;

  set drugCost(num value) {
    _drugCost = value;
    notifyListeners();
  }

  TextEditingController get quantityController => _quantityController;

  set quantityController(TextEditingController value) {
    _quantityController = value;
    notifyListeners();
  }
}
