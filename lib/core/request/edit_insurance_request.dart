import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'edit_insurance_request.g.dart';

@JsonSerializable()
class EditInsuranceRequest extends BaseRequest {
  String insuranceType;
  bool isInsuranceFront;
  String patientId;

  EditInsuranceRequest({this.insuranceType, this.patientId, this.isInsuranceFront});

  factory EditInsuranceRequest.fromJson(Map<String, dynamic> json) => _$EditInsuranceRequestFromJson(json);
  Map<String, dynamic> toJson() => _$EditInsuranceRequestToJson(this);
}
