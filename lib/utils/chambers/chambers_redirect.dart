import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';

import '../../locator.dart';

class ChambersRedirect {
  static String routeName() {
    final DataStoreService dataStore = locator<DataStoreService>();

    if (dataStore.readString(DataStoreService.FIRSTNAME) != null &&
        dataStore.readString(DataStoreService.LASTNAME) != null &&
        dataStore.readString(DataStoreService.DATE_OF_BIRTH) != null &&
        //dataStore.readString(DataStoreService.PHONE) != null &&
        dataStore.readString(DataStoreService.PROVINCE) != null &&
        dataStore.readString(DataStoreService.GENDER) != null &&
        (dataStore.readBoolean(DataStoreService.MAIL_VERIFIED) ||
            dataStore.readBoolean(DataStoreService.VERIFIED))) {
      print('AMANREDIRECTTO:${DashboardWidget.routeName}');
      return DashboardWidget.routeName;
    } else if (dataStore.readString(DataStoreService.FIRSTNAME) == null ||
        dataStore.readString(DataStoreService.LASTNAME) == null ||
        dataStore.readString(DataStoreService.DATE_OF_BIRTH) == null) {
      print('AMANREDIRECTTO:${SignupWidget.routeName}');
      return SignupWidget.routeName;
    } else if ((dataStore.readString(DataStoreService.FIRSTNAME) != null &&
            dataStore.readString(DataStoreService.LASTNAME) != null &&
            dataStore.readString(DataStoreService.DATE_OF_BIRTH) != null) ||
        !dataStore.readBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE)) {
      print('AMANREDIRECTTO:${TransferWidget.routeName}');
      return TransferWidget.routeName;
    } else if (dataStore.readString(DataStoreService.PROVINCE) == null ||
        dataStore.readString(DataStoreService.GENDER) == null ||
        dataStore.readString(DataStoreService.EMAIL) == null) {
      print('AMANREDIRECTTO:${SignUpAlmostDoneWidget.routeName}');
      return SignUpAlmostDoneWidget.routeName;
    }
  }
}
