/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.pocketpills;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.pocketpills.staging";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "staging";
  public static final int VERSION_CODE = 181;
  public static final String VERSION_NAME = "4.4.53";
  // Fields from product flavor: staging
  public static final String ADMIN_KEY = "v3tuP?kn}x&%fWgRbp@K(+XRfl!VJ%JMEYLJ*>0j8)sr-R!%}U(=yFcerE%y(aK";
  public static final String BASE_SERVER = "http://qa.pocketpills.com:9090";
}
