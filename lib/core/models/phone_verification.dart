import 'package:json_annotation/json_annotation.dart';

part 'phone_verification.g.dart';

@JsonSerializable()

class PhoneVerification{
  String userMessage;
  String redirect;
  int userId;
  PhoneVerification({this.userMessage,this.redirect,this.userId});

  factory PhoneVerification.fromJson(Map<String, dynamic> json) => _$PhoneVerificationFromJson(json);

  Map<String, dynamic> toJson() => _$PhoneVerificationToJson(this);

}