// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_success_field.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionSuccessField _$TransactionSuccessFieldFromJson(
    Map<String, dynamic> json) {
  return TransactionSuccessField(
    json['cardType'] as String,
    json['title'] as String,
    json['image'] as String,
    json['description'] as String,
    json['action'] as String,
  );
}

Map<String, dynamic> _$TransactionSuccessFieldToJson(
        TransactionSuccessField instance) =>
    <String, dynamic>{
      'cardType': instance.cardType,
      'title': instance.title,
      'image': instance.image,
      'description': instance.description,
      'action': instance.action,
    };
