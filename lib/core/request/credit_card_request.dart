import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'credit_card_request.g.dart';

@JsonSerializable()
class CreditCardRequest extends BaseRequest {
  bool isDefault;
  String patientId;
  String postalCode;
  String token;

  CreditCardRequest({this.isDefault, this.patientId, this.postalCode, this.token});

  factory CreditCardRequest.fromJson(Map<String, dynamic> json) => _$CreditCardRequestFromJson(json);
  Map<String, dynamic> toJson() => _$CreditCardRequestToJson(this);
}
