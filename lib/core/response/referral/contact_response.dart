import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/referral/item_response.dart';
import 'package:pocketpills/core/response/referral/postal_address_response.dart';
part 'contact_response.g.dart';

@JsonSerializable()
class ContactResponse {
  String identifier, displayName, givenName, middleName, prefix, suffix, familyName, company, jobTitle;
  String androidAccountTypeRaw, androidAccountName;
  String androidAccountType;
  List<ItemResponse> emails = [];
  List<ItemResponse> phones = [];
  List<PostalAddressResponse> postalAddresses = [];
  String birthday;

  ContactResponse(
      this.identifier,
      this.displayName,
      this.givenName,
      this.middleName,
      this.prefix,
      this.suffix,
      this.familyName,
      this.company,
      this.jobTitle,
      this.androidAccountTypeRaw,
      this.androidAccountName,
      this.androidAccountType,
//      this.emails,
//      this.phones,
//      this.postalAddresses,
      this.birthday);

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
        'identifier': this.identifier,
        'displayName': this.displayName,
        'givenName': this.givenName,
        'middleName': this.middleName,
        'familyName': this.familyName,
        'prefix': this.prefix,
        'suffix': this.suffix,
        'company': this.company,
        'jobTitle': this.jobTitle,
        'androidAccountTypeRaw': this.androidAccountTypeRaw,
        'androidAccountType': this.androidAccountType,
        'androidAccountName': this.androidAccountName,
        'emails': this.emails,
        'phones': this.phones,
        'postalAddresses': this.postalAddresses,
        'birthday': this.birthday,
      };

  factory ContactResponse.fromJson(Map<dynamic, dynamic> json) => _$ContactResponseFromJson(json);
}
