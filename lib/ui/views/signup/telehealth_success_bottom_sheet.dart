import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

import 'package:pocketpills/ui/shared/constants/view_constants.dart';

class SuccessSheet extends StatefulWidget {
  BuildContext context;
  String openFrom = "";

  SuccessSheet(this.context, this.openFrom);

  _SuccessSheetState createState() => _SuccessSheetState();
}

class _SuccessSheetState extends BaseState<SuccessSheet>
    with SingleTickerProviderStateMixin {
  SignUpTransferModel model;
  BuildContext context;

  _SuccessSheetState();

  @override
  void initState() {
    super.initState();
    model = SignUpTransferModel();
    context = widget.context;
    print("openfrom" + widget.openFrom);
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext buildContext) {
    return FutureBuilder(
        future: myFutureMethodOverall(model),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            if (widget.openFrom == "SIGNUP_TELEHEALTH") {
              analyticsEvents.sendAnalyticsEvent("consultation_signup_success");
            }
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(SignUpTransferModel model) async {
    Future<Map<String, dynamic>> future1 =
        model.getLocalization(["signup", "modal"]);
    return await Future.wait([future1]);
  }

  Widget getMainView() {
    return Container(
      height: languageFromHeight(),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            decoration: new BoxDecoration(
                color: Colors.white, //new Color.fromRGBO(255, 0, 0, 0.0),
                borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(10.0),
                    topRight: const Radius.circular(10.0))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: widget.openFrom == "SIGNUP_TELEHEALTH" ? 170.0 : 130,
                  alignment: Alignment.bottomCenter,
                  decoration: new BoxDecoration(
                      color:
                          lightBlueColor, //new Color.fromRGBO(255, 0, 0, 0.0),
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(10.0),
                          topRight: const Radius.circular(10.0))),
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: MEDIUM_XX, top: LARGE_XXX, left: 10, right: 10),
                    child: Column(
                      children: [
                        widget.openFrom == "SIGNUP_TELEHEALTH"
                            ? Text(
                                LocalizationUtils.getSingleValueString(
                                    "modal", "modal.signup.booked"),
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: primaryColor,
                                    fontWeight: FontWeight.bold),
                              )
                            : Text(
                                LocalizationUtils.getSingleValueString("modal",
                                        "modal.signup.congratulations") +
                                    " " +
                                    dataStore
                                        .readString(DataStoreService.FIRSTNAME),
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: primaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                        /*          dataStore.writeString(DataStoreService.DOCTOR_NAME, selectedItem.doctorName);
                dataStore.writeString(DataStoreService.APPOINTMENT_TIME, selectedItem.displayDate);*/
                        SizedBox(height: 10.0),
                        widget.openFrom == "SIGNUP_TELEHEALTH"
                            ? Text(
                                LocalizationUtils.getSingleValueString("modal",
                                        "modal.signup.telehealth-subtitle2")
                                    .replaceAll(
                                        "{{doctorName}}",
                                        dataStore.readString(
                                            DataStoreService.DOCTOR_NAME))
                                    .replaceAll(
                                        "{{slotDate}}",
                                        dataStore.readString(
                                            DataStoreService.APPOINTMENT_DATE))
                                    .replaceAll(
                                        "{{timeSlot}}",
                                        dataStore.readString(
                                            DataStoreService.APPOINTMENT_TIME)),
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: primaryColor,
                                    fontWeight: FontWeight.normal),
                                textAlign: TextAlign.center,
                              )
                            : Text(
                                LocalizationUtils.getSingleValueString(
                                    "modal", "modal.signup.subtitle"),
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: primaryColor,
                                    fontWeight: FontWeight.normal),
                              )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: subText(),
                ),
                SizedBox(height: 20.0),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: PrimaryButton(
                    fullWidth: true,
                    text: LocalizationUtils.getSingleValueString(
                            "modal", "modal.signup.complete-profile")
                        .toUpperCase(),
                    onPressed: () {
                      getNextRoute();
                    },
                  ),
                ),
                SizedBox(height: 10.0),
              ],
            ),
          ),
          Align(
            alignment: languageFromAlignment(),
            child: CircleAvatar(
                radius: 40,
                backgroundColor: lightBlueColor,
                child: CircleAvatar(
                  radius: 36,
                  backgroundColor: brandColor,
                  backgroundImage: CachedNetworkImageProvider(
                    "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg",
                  ),
                )),
          ),
        ],
      ),
    );
  }

  double languageFromHeight() {
    if (getSelectedLanguage() == ViewConstants.languageIdEn) {
      return 350;
    } else {
      if (getSelectedLanguage() == ViewConstants.languageIdFr &&
          widget.openFrom == "SIGNUP_PRESCRIPTION") {
        return 330;
      } else {
        return 350;
      }
    }
  }

  Alignment languageFromAlignment() {
    if (getSelectedLanguage() == ViewConstants.languageIdEn &&
        widget.openFrom == "SIGNUP_TELEHEALTH") {
      return Alignment(0.0, -.90);
    } else if (getSelectedLanguage() == ViewConstants.languageIdEn &&
        widget.openFrom != "DASHBOARD_TELEHEALTH") {
      return Alignment(0.0, -.75);
    } else if (getSelectedLanguage() == ViewConstants.languageIdFr &&
        widget.openFrom == "SIGNUP_TELEHEALTH") {
      return Alignment(0.0, -1.15);
    } else if (getSelectedLanguage() == ViewConstants.languageIdFr &&
        widget.openFrom != "SIGNUP_TELEHEALTH") {
      return Alignment(0.0, -1.15);
    } else if (getSelectedLanguage() == ViewConstants.languageIdFr &&
        widget.openFrom != "SIGNUP_PRESCRIPTION") {
      return Alignment(0.0, -1.15);
    } else if (getSelectedLanguage() == ViewConstants.languageIdEn &&
        widget.openFrom != "SIGNUP_PRESCRIPTION") {
      return Alignment(0.0, -.85);
    }
  }

  Widget subText() {
    if (widget.openFrom == "SIGNUP_TELEHEALTH") {
      return Text(
        LocalizationUtils.getSingleValueString(
            "modal", "modal.signup.telehealth-details"),
        style: TextStyle(
            fontSize: 14.0, color: primaryColor, fontWeight: FontWeight.normal),
      );
    } else if (widget.openFrom == "DASHBOARD_TELEHEALTH") {
      return Text(
        LocalizationUtils.getSingleValueString(
            "modal", "modal.signup.doctor-call"),
        style: TextStyle(
            fontSize: 14.0, color: primaryColor, fontWeight: FontWeight.normal),
      );
    } else if (widget.openFrom == "SIGNUP_PRESCRIPTION") {
      return Text(
        LocalizationUtils.getSingleValueString("modal", "modal.signup.details"),
        style: TextStyle(
            fontSize: 14.0, color: primaryColor, fontWeight: FontWeight.normal),
      );
    } else {
      return Text("");
    }
  }

  openVideoPlayer() {
    FlutterYoutube.playYoutubeVideoByUrl(
      apiKey: ApplicationConstant.YOU_TUBE_APIKEY,
      videoUrl: "https://www.youtube.com/watch?v=" + "JyIeDivW1Jo",
      autoPlay: true,
      fullScreen: false,
    );
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);

  Widget bottomSheetItems(IconData icons, String text, String subtext) {
    return Container(
      height: 60.0,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.all(MEDIUM_X),
        child: Row(
          children: [
            Icon(
              icons,
              color: darkBlueColor2,
            ),
            SizedBox(width: 15.0),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  text,
                  style: TextStyle(
                      fontSize: 15.4,
                      color: darkBlueColor2,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(height: 1.0),
                Text(
                  subtext,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontSize: 12.0,
                      color: darkBlueColor2,
                      fontWeight: FontWeight.w300),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  getNextRoute() {
    // await userContactModel.putContact();
    if (dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) == true) {
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);

      Navigator.pushNamed(context, OrderStepper.routeName,
          arguments: BaseStepperArguments(startStep: 1, from: 'telehealth'));
    } else if (dataStore.readBoolean(DataStoreService.DASHBOARD_TELEHEALTH) ==
        true) {
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
      Navigator.pop(context);
    } else if (dataStore.readBoolean(DataStoreService.SIGNUP_PRESCRIPTION) ==
        true) {
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.SIGNUP_PRESCRIPTION, false);
      Navigator.pushNamed(context, OrderStepper.routeName,
          arguments: BaseStepperArguments(startStep: 0, from: 'telehealth'));
    } else {
      Navigator.pop(context);
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
    }
  }
}
