// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shipment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Shipment _$ShipmentFromJson(Map<String, dynamic> json) {
  return Shipment(
    status: json['status'] as String,
    id: json['id'] as int,
    updatedDateTime: json['updatedDateTime'] as String,
    modifiedBy: json['modifiedBy'] as String,
    dueDate: json['dueDate'] as String,
    createdDateTime: json['createdDateTime'] as String,
    createdBy: json['createdBy'] as String,
    attemptedDeliveryDate: json['attemptedDeliveryDate'] as String,
    deliveryDate: json['deliveryDate'] as String,
    deliveryInstructions: json['deliveryInstructions'] as String,
    expectedDeliveryDate: json['expectedDeliveryDate'] as String,
    isTrackingAvailable: json['isTrackingAvailable'] as bool,
    items: (json['items'] as List)
        ?.map((e) =>
            e == null ? null : ShipmentItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    labelSize: json['labelSize'] as String,
    order: json['order'] as String,
    packageSize: json['packageSize'] as String,
    provider: json['provider'] as String,
    providerShipmentId: json['providerShipmentId'] as String,
    returnDate: json['returnDate'] as String,
    service: json['service'] as String,
    shipmentDate: json['shipmentDate'] as String,
    shipmentNo: json['shipmentNo'] as String,
    weight: json['weight'] as num,
  );
}

Map<String, dynamic> _$ShipmentToJson(Shipment instance) => <String, dynamic>{
      'dueDate': instance.dueDate,
      'id': instance.id,
      'shipmentNo': instance.shipmentNo,
      'createdDateTime': instance.createdDateTime,
      'updatedDateTime': instance.updatedDateTime,
      'order': instance.order,
      'provider': instance.provider,
      'status': instance.status,
      'providerShipmentId': instance.providerShipmentId,
      'items': instance.items,
      'weight': instance.weight,
      'deliveryInstructions': instance.deliveryInstructions,
      'packageSize': instance.packageSize,
      'service': instance.service,
      'labelSize': instance.labelSize,
      'expectedDeliveryDate': instance.expectedDeliveryDate,
      'shipmentDate': instance.shipmentDate,
      'deliveryDate': instance.deliveryDate,
      'attemptedDeliveryDate': instance.attemptedDeliveryDate,
      'returnDate': instance.returnDate,
      'createdBy': instance.createdBy,
      'modifiedBy': instance.modifiedBy,
      'isTrackingAvailable': instance.isTrackingAvailable,
    };
