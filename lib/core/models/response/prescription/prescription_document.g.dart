// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prescription_document.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrescriptionDocument _$PrescriptionDocumentFromJson(Map<String, dynamic> json) {
  return PrescriptionDocument(
    json['documentPath'] as String,
    json['thumbnailPath'] as String,
    json['documentType'] as String,
  );
}

Map<String, dynamic> _$PrescriptionDocumentToJson(
        PrescriptionDocument instance) =>
    <String, dynamic>{
      'documentPath': instance.documentPath,
      'thumbnailPath': instance.thumbnailPath,
      'documentType': instance.documentType,
    };
