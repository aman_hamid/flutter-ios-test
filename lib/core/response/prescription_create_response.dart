import 'dart:core';

import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';

part 'prescription_create_response.g.dart';

@JsonSerializable()
class PrescriptionCreateResponse {
  int omsPrescriptionId;
  SuccessDetails successDetails;

  PrescriptionCreateResponse(this.omsPrescriptionId, this.successDetails);

  factory PrescriptionCreateResponse.fromJson(Map<String, dynamic> json) => _$PrescriptionCreateResponseFromJson(json);
}
