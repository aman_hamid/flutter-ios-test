import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/vitamins/coupon.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/ui/views/vitamins/vitamin_filter_arguments.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_widget.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:provider/provider.dart';

class VitaminsCouponView extends BaseStatelessWidget {
  final BaseStepperSource source;
  final EdgeInsets padding;

  VitaminsCouponView(
      {@required this.source = BaseStepperSource.UNKNOWN_SCREEN, this.padding = const EdgeInsets.all(0.0)});

  @override
  Widget build(BuildContext context) {
    return Consumer<VitaminsCatalogModel>(
        builder: (BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Widget child) {
      return FutureBuilder(
          future: vitaminsCatalogModel.getCouponCodeList(),
          builder: (BuildContext context, AsyncSnapshot<List<Coupon>> snapshot) {
            if (snapshot.hasData && snapshot.data != null && snapshot.data.length > 0) {
              return InkWell(
                onTap: source == BaseStepperSource.NEW_USER
                    ? () {
                        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_own_pack);
                        Provider.of<VitaminsCatalogModel>(context).clearVitaminList();
                        Navigator.pushNamed(context, VitaminsCatalogWidget.routeName,
                            arguments:
                                VitaminFilterArguments(source: BaseStepperSource.UNKNOWN_SCREEN, filterArgu: ""));
                      }
                    : null,
                child: Padding(
                  padding: padding,
                  child: Container(
                    decoration: BoxDecoration(
                      color: quartiaryColor,
                      borderRadius: BorderRadius.circular(SMALL_XX),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: MEDIUM_XXX, top: MEDIUM, bottom: MEDIUM),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  snapshot.data[0].title,
                                  style: MEDIUM_XX_PINK_BOLD,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  height: SMALL,
                                ),
                                Text(
                                  "You will " + changeCouponCodeDescription(snapshot.data[0].description),
                                  style: MEDIUM_X_SECONDARY,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                )
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: MEDIUM_XXX),
                          child: Image.asset(
                            'graphics/referral/gift.png',
                            height: REGULAR_XXX,
                            width: REGULAR_XXX,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            } else if (snapshot.hasError && snapshot.error != null) {
              return PPContainer.emptyContainer();
            }
            return PPContainer.emptyContainer();
          });
    });
  }

  String changeCouponCodeDescription(String input) {
    if (input == null || input.isEmpty) return input;
    String description = "";
    List<String> list = input.split(".");

    if (list.length > 0) {
      for (int i = 0; i < list.length; i++) {
        i == 0 ? description += list[i].toString().toLowerCase() : description += ". " + list[i];
      }
    }
    return description;
  }
}
