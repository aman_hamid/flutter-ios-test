import 'dart:convert';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/response/pillreminder/month_wise_medication_response.dart';
import 'package:pocketpills/core/response/pillreminder/pill_reminder_medications_response.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/views/pillreminder/component/month_picker_dialog.dart';

class PillReminderMedicationModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<PillReminderMedicationResponse> _memoizer = AsyncMemoizer();

  Future<PillReminderMedicationResponse> _futureMedicationResponse;
  PillReminderMedicationResponse dayWiseMedications;

  ConnectivityResult connectivityResult;

  DateTime _dateTime;

  PillReminderDayModel() {
    dateTime = DateTime.now();
  }

  Future<PillReminderMedicationResponse> fetchPillReminderDayData() async {
    _futureMedicationResponse = this._memoizer.runOnce(() async {
      return await this.getPillReminderMedicationData();
    });
    return _futureMedicationResponse;
  }

  Future<PillReminderMedicationResponse> getPillReminderMedicationData() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getPillReminderMedicationView();
    if (response != null) {
      BaseResponse<PillReminderMedicationResponse> res =
          BaseResponse<PillReminderMedicationResponse>.fromJson(response.data);

      return res.response;
    } else {
      return null;
    }
  }

  String getFormattedDateEEEEddMMMM() {
    return DateFormat('EEEE, dd MMMM').format(dateTime);
  }

  String getFormattedTime(String time) {
    return DateFormat.jm().format(DateFormat("hh:mm:ss").parse(time));
  }

  DateTime get dateTime => _dateTime;

  set dateTime(DateTime value) {
    _dateTime = value;
  }
}
