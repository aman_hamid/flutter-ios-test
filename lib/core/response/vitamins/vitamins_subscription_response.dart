import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/vitamins/shopping_cart.dart';
part 'vitamins_subscription_response.g.dart';

@JsonSerializable()
class VitaminsSubscriptionResponse {
  String nextFillDate;
  ShoppingCart shoppingCart;
  bool subscriptionActive;

  VitaminsSubscriptionResponse(this.nextFillDate, this.shoppingCart, this.subscriptionActive);

  factory VitaminsSubscriptionResponse.fromJson(Map<String, dynamic> json) =>
      _$VitaminsSubscriptionResponseFromJson(json);
}
