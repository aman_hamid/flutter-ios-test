// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'telehealth_province.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TelehealthProvinceItem _$TelehealthProvinceItemFromJson(
    Map<String, dynamic> json) {
  return TelehealthProvinceItem(
    json['value'] as String,
    json['label'] as String,
  );
}

Map<String, dynamic> _$TelehealthProvinceItemToJson(
        TelehealthProvinceItem instance) =>
    <String, dynamic>{
      'value': instance.value,
      'label': instance.label,
    };
