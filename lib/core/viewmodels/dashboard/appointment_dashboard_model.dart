import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_request_update.dart';
import 'package:pocketpills/core/response/appointment_slot_list_response.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/oms_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/telehealth/appointment_first_responce.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_create_response.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_appointment_details_response.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class AppointmentDashboardModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  SuccessDetails successDetails;
  AsyncMemoizer<AppointmentFirstResponse> _prescriptionDetailMemoizer = AsyncMemoizer();
  Future<AppointmentFirstResponse> _futurePrescription;
  ConnectivityResult connectivityResult;
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();
  List<AppointmentTime> appointmentsList = [];
  Future<AppointmentFirstResponse> fetchPrescriptionById(int prescriptionId) async {
    _futurePrescription = this._prescriptionDetailMemoizer.runOnce(() async {
      return await this.getPrescriptionsById(prescriptionId);
    });
    return _futurePrescription;
  }

  clearAsyncMemoizer() {
    _prescriptionDetailMemoizer = AsyncMemoizer();
    contentMemoizer = AsyncMemoizer();
    notifyListeners();
  }

  Future<bool> sendPreferences(TelehealthRequestUpdate telehealthRequest,int prescriptionId ) async {

    setState(ViewState.Busy);
    Response response =
    await _api.updateTelehealthAppointment(prescriptionId, telehealthRequest);
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<TelehealthCreateResponse> baseResponse =
      BaseResponse<TelehealthCreateResponse>.fromJson(response.data);
      if (!baseResponse.status) {
        errorMessage = baseResponse.getErrorMessage();
        return false;
      }

      notifyListeners();
      return true;
    } else {
      return false;
    }

  }

  Future<AppointmentFirstResponse> getPrescriptionsById(int prescriptionId) async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    Response response = await _api.getPrescriptionsByIdAppointment(prescriptionId);
    if (response != null) {
      OMSResponse<AppointmentFirstResponse> omsResponse = OMSResponse<AppointmentFirstResponse>.fromJson(response.data);
      if (!omsResponse.status) {
        errorMessage = omsResponse.getErrorMessage();
        return null;
      }
      print(omsResponse.response);
      return omsResponse.response;
    } else {
      return null;
    }
  }

  Future<List<AppointmentTime>> getAppointmentDate() async {
    Response response = await _api.getAppointmentSlots();
    if (response != null) {
      OMSResponse<AppointmentSlotListResponse> res = OMSResponse<AppointmentSlotListResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      appointmentsList = res.response.items;
      return res.response.items;
    } else {
      return null;
    }
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
