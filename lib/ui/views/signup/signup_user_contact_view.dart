import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/signup/sign_up_stepper_model.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/custom_stepper.dart';
import 'package:pocketpills/ui/shared/appbars/singup_stepper_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_widget.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class SignupUserContactWidget extends StatefulWidget {
  static const routeName = 'Signupusercontact';

  BaseStepperSource source;

  SignupUserContactWidget({this.source = BaseStepperSource.UNKNOWN_SCREEN});

  @override
  State<StatefulWidget> createState() {
    return SignupUserContactState();
  }
}

class SignupUserContactState extends BaseState<SignupUserContactWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();

  bool transferSkipped = false;
  String getStartedWithoutPhoneNumber;

  @override
  void initState() {
    super.initState();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_user_contact_view);
    this.transferSkipped = dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED);
  }

  bool isDoubleLine = false;

  @override
  Widget build(BuildContext context) {
    MediaQuery.of(context).size.width <= 400 ? isDoubleLine = true : isDoubleLine = false;
    return MultiProvider(
      providers: [ChangeNotifierProvider<UserContactModel>(create: (_) => UserContactModel())],
      child: Consumer2<SignupStepperModel, UserContactModel>(
        builder: (BuildContext context, SignupStepperModel stepperModel, UserContactModel userContactModel, Widget child) {
          return FutureBuilder(
              future: myFutureMethodOverall(userContactModel),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getMainView(context, stepperModel, userContactModel);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        },
      ),
    );
  }

  Future myFutureMethodOverall(UserContactModel userContactModel) async {
    Future<Map<String, dynamic>> future1 = userContactModel.getLocalization(["signup", "stepper", "modals"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(BuildContext context, SignupStepperModel stepperModel, UserContactModel userContactModel) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      return _afterFutureResolved(context, userContactModel, stepperModel);
    } else {
      return Scaffold(
          body: CustomStepper(
        controlsBuilder: (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
          return PPContainer.emptyContainer();
        },
        type: CustomStepperType.horizontal,
        currentStep: stepperModel.currentStep,
        onStepContinue: stepperModel.currentStep < 3 ? () => stepperModel.incrCurrentStep() : null,
        onStepCancel: stepperModel.currentStep > 0 ? () => stepperModel.decrCurrentStep() : null,
        steps: <CustomStep>[
          CustomStep(
            title: Padding(
              padding: const EdgeInsets.only(left: 4.0),
              child: Text(
                LocalizationUtils.getSingleValueString("stepper", "stepper.labels.personal"),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14),
              ),
            ),
            content: SizedBox(height: MediaQuery.of(context).size.height, child: Container()),
            isActive: stepperModel.currentStep >= 0,
            state: stepperModel.currentStep == 0 ? CustomStepState.editing : CustomStepState.complete,
          ),
          CustomStep(
            title: new Text(
              LocalizationUtils.getSingleValueString("stepper", "stepper.labels.pharmacy"),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14),
            ),
            content: SizedBox(height: MediaQuery.of(context).size.height, child: Container()),
            isActive: stepperModel.currentStep >= 1,
            state: stepperModel.currentStep == 1 ? CustomStepState.editing : (stepperModel.currentStep >= 1 ? CustomStepState.complete : CustomStepState.disabled),
          ),
          CustomStep(
            title: new Text(
              LocalizationUtils.getSingleValueString("stepper", "stepper.labels.almostdone"),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14),
            ),
            content: SizedBox(height: MediaQuery.of(context).size.height, child: _afterFutureResolved(context, userContactModel, stepperModel)),
            isActive: stepperModel.currentStep >= 2,
            state: stepperModel.currentStep == 2 ? CustomStepState.editing : (stepperModel.currentStep >= 2 ? CustomStepState.complete : CustomStepState.disabled),
          ),
          CustomStep(
            title: Padding(
              padding: const EdgeInsets.only(right: 4.0),
              child: Text(
                LocalizationUtils.getSingleValueString("stepper", "stepper.labels.free_vitamins"),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14),
              ),
            ),
            subtitle: Text(
              LocalizationUtils.getSingleValueString("stepper", "stepper.labels.20-off"),
              textAlign: TextAlign.right,
              style: TextStyle(fontSize: 10),
            ),
            icon: Icon(
              Icons.card_giftcard,
              color: stepperModel.currentStep >= 3 ? brandColor : secondaryColor,
            ),
            content: SizedBox(height: MediaQuery.of(context).size.height, child: Container()),
            isActive: stepperModel.currentStep >= 3,
            state: stepperModel.currentStep >= 3 ? CustomStepState.complete : CustomStepState.disabled,
          ),
        ],
      ));
    }
  }

  Widget _afterFutureResolved(BuildContext context, UserContactModel userContactModel, SignupStepperModel stepperModel) {
    return Scaffold(
      appBar: widget.source == BaseStepperSource.NEW_USER
          ? SignUpStepperAppBar(
              appBar: AppBar(),
              step: null,
            )
          : null,
      bottomNavigationBar: Builder(
          builder: (BuildContext context) => userContactModel.state == ViewState.Busy
              ? ViewConstants.progressIndicator
              : PPBottomBars.getButtonedBottomBar(
                  child: PrimaryButton(
                  text: LocalizationUtils.getSingleValueString("signup", "signup.schedulecall.finish"),
                  fullWidth: true,
                  onPressed: () async {
                    bool result = await userContactModel.updateContactInfo();
                    if (result == true) {
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil(VitaminsWidget.routeName, (Route<dynamic> route) => false, arguments: SourceArguments(source: BaseStepperSource.NEW_USER));
                    } else {
                      onFail(context, errMessage: userContactModel.errorMessage);
                    }
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_user_contact_done);
                  },
                ))),
      body: Builder(
        builder: (BuildContext context) => SafeArea(
            child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: Container(
                  child: Column(
                children: <Widget>[
                  SizedBox(
                    height: MEDIUM_XXX,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.only(top: SMALL_XXX),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              LocalizationUtils.getSingleValueString("signup", "signup.schedulecall.title"),
                              style: REGULAR_XXX_PRIMARY_BOLD,
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  PPUIHelper.verticalSpaceSmall(),
                  PPTexts.getSecondaryHeading(
                      transferSkipped == true
                          ? LocalizationUtils.getSingleValueString("signup", "signup.schedulecall.description")
                          : LocalizationUtils.getSingleValueString("signup", "signup.schedulecall.description2"),
                      isBold: false),
                  PPUIHelper.verticalSpaceLarge(),
                  PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("signup", "signup.schedulecall.label")),
                  PPUIHelper.verticalSpaceMedium(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("modals", "modals.timings.day")),
                      PPUIHelper.verticalSpaceSmall(),
                      PPFormFields.getDropDown(
                        value: userContactModel.day,
                        onChanged: (String newValue) {
                          userContactModel.setDay(newValue);
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.user_contact_day_change);
                        },
                        items: getDayMap()
                            .map((String key, String value) {
                              return MapEntry(
                                  key,
                                  DropdownMenuItem<String>(
                                    value: key,
                                    child: Text(value),
                                  ));
                            })
                            .values
                            .toList(),
                      ),
                      PPUIHelper.verticalSpaceMedium(),
                      PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("modals", "modals.timings.time")),
                      PPUIHelper.verticalSpaceSmall(),
                      PPFormFields.getDropDown(
                        value: userContactModel.time,
                        onChanged: (String newValue) {
                          userContactModel.setTime(newValue);
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.user_contact_time_change);
                        },
                        items: getTimeMap()
                            .map((String key, String value) {
                              return MapEntry(
                                  key,
                                  DropdownMenuItem<String>(
                                    value: key,
                                    child: Text(value),
                                  ));
                            })
                            .values
                            .toList(),
                      ),
                    ],
                  ),
                ],
              )),
            ),
          ],
        )),
      ),
    );
  }
}
