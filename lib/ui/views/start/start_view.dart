import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/enums/signup_stepper_state_enums.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/core/response/signup_phonenumber_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/login/login_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/formfields/pp_radiogroup.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/login_arguments.dart';
import 'package:pocketpills/ui/views/login/login_view.dart';
import 'package:pocketpills/ui/views/login/loginverification_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/verification_arguments.dart';
import 'package:pocketpills/ui/views/start/pp_carousel.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/chambers/chambers_redirect.dart';
import 'package:pocketpills/utils/custom_drop_down.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/navigation_service.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:url_launcher/url_launcher.dart';

class StartView extends StatefulWidget {
  static const routeName = 'start';

  String deepLinkRouteName = null;
  int carouselIndex;
  bool chambersFlow;

  StartView(
      {this.deepLinkRouteName = null,
      this.carouselIndex = 0,
      this.chambersFlow = false});

  @override
  State<StatefulWidget> createState() {
    return StartViewState();
  }
}

class StartViewState extends BaseState<StartView> {
  final DataStoreService dataStore = locator<DataStoreService>();
  final SmsAutoFill _autoFill = SmsAutoFill();

  final TextEditingController _phoneNumberController = TextEditingController();
  final FocusNode _phoneNumberFocusNode = FocusNode();

  bool phAutoValidate = false;
  bool _hintShown = false;

  String getStartedWithoutPhoneNumber;
  String languageDropDownValue;

  int selectedRadio;

  @override
  initState() {
    super.initState();
    this.initDynamicLinks();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.carousel1);
    if (widget.deepLinkRouteName != null) {
      print("+++++++" + widget.deepLinkRouteName);
    }
    getStartedWithoutPhoneNumber =
        dataStore.readString(DataStoreService.GET_STARTED_WITHOUT_PHONE_NUMBER);
    if (dataStore.readBoolean(DataStoreService.IS_LOGGED_IN)) {
      analyticsEvents.setUserIdentifiers();
    }
    _phoneNumberFocusNode.addListener(() async {
      if (!_hintShown) {
        _hintShown = true;
        await _askPhoneHint();
      }
      await SmsAutoFill().listenForCode;
    });
    _phoneNumberController.addListener(shiftLoginFocus);
    selectedRadio = 0;
  }

  Future<void> _askPhoneHint() async {
    String hint = await _autoFill.hint;
    _phoneNumberController.value =
        TextEditingValue(text: StringUtils.getFormattedPhoneNumber(hint) ?? '');
  }

  @override
  void dispose() {
    _phoneNumberController.removeListener(shiftLoginFocus);
    super.dispose();
  }

  shiftLoginFocus() async {
    if (_phoneNumberController.text.length ==
        SignUpModuleConstant.PHONE_NUMBER_LENGTH) {
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.verify_phone_entered);
      setState(() {
        phAutoValidate = false;
      });
    }
  }

  void initDynamicLinks() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;

    if (deepLink != null) {
      //Navigator.pushNamed(context, deepLink.path);
    }

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;

      if (deepLink != null) {
        print("deep link $deepLink" + deepLink.path + " " + deepLink.scheme);
        // Navigator.pushNamed(context, deepLink.path);
      }
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
      Crashlytics.instance.log(e.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginModel>(create: (_) => LoginModel())
      ],
      child: Consumer<LoginModel>(
          builder: (BuildContext context, LoginModel loginModel, Widget child) {
        return FutureBuilder(
            future: loginModel.getLocalization(["app-landing", "common"]),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainFlow(loginModel);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  TextFormField getUsernameField(BuildContext contextm, LoginModel model) {
    return PPFormFields.getNumericFormField(
        maxLength: 10,
        keyboardType: TextInputType.phone,
        minLength: 10,
        textInputAction: TextInputAction.done,
        controller: _phoneNumberController,
        focusNode: _phoneNumberFocusNode,
        labelText: LocalizationUtils.getSingleValueString(
            "app-landing", "app-landing.labels.cell-phone"),
        errorText: LocalizationUtils.getSingleValueString(
                "common", "common.label.required") +
            "*",
        onFieldSubmitted: (value) {
          onClickProcedSecurely(context, model);
        },
        validator: (value) {
          if (value.isEmpty)
            return LocalizationUtils.getSingleValueString(
                    "common", "common.label.required") +
                "*";
          String formattedNumber = value
              .replaceAll("\(", "")
              .replaceAll("\)", "")
              .replaceAll(" ", "")
              .replaceAll("-", "")
              .replaceAll("+", "");
          if (formattedNumber.length < 10)
            return LocalizationUtils.getSingleValueString(
                "app-landing", "app-landing.labels.cell-error1");
          if (formattedNumber.length > 10)
            return LocalizationUtils.getSingleValueString(
                "app-landing", "app-landing.labels.cell-error2");
          return null;
        });
  }

  Widget getMainFlow(LoginModel loginModel) {
    return BaseScaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: whiteColor,
        elevation: 1,
        brightness: Platform.isIOS == true ? Brightness.light : null,
        iconTheme: IconThemeData(color: Colors.white //change your color here
            ),
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              getLogoImage(),
              width: MediaQuery.of(context).size.width * 0.45,
            ),
          ],
        ),
        leading: null,
        actions: [
          _getLanguagePreference(),
          SizedBox(
            width: 5.0,
          )
        ],
      ),
      body: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
                child: SizedBox(
                    height: MediaQuery.of(context).size.height * 0.65,
                    child: PPCarousel(carouselIndex: widget.carouselIndex))),
            Align(
              alignment: Alignment.bottomCenter,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: MEDIUM, right: MEDIUM, top: MEDIUM_XXX),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(MEDIUM_X),
                            topLeft: Radius.circular(MEDIUM_X)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            blurRadius: REGULAR,
                          ),
                        ],
                        color: Colors.white),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: MEDIUM_X,
                          right: MEDIUM_XXX,
                          left: MEDIUM_XXX,
                          bottom: MEDIUM_XXX),
                      child: getStartedView(loginModel, context),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getLanguagePreference() {
    return CustomDropdownButtonHideUnderline(
      child: CustomDropdownButton(
        value: languageDropDownValue == null
            ? getSelectedLanguage() == "en"
                ? "en"
                : "fr"
            : languageDropDownValue,
        onChanged: (String newValue) {
          setState(() {
            languageDropDownValue = newValue;
            changeLanguage(context, newValue);
          });
        },
        selectedItemBuilder: (BuildContext context) {
          return ViewConstants.languageMap
              .map((String key, String value) {
                return MapEntry(
                  key,
                  CustomDropdownMenuItem<String>(
                    value: key,
                    child: Text(
                      key.toUpperCase(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                );
              })
              .values
              .toList();
        },
        items: ViewConstants.languageMap
            .map((String key, String value) {
              return MapEntry(
                key,
                CustomDropdownMenuItem<String>(
                  value: key,
                  child: Container(
                    width: 280,
                    child: Text(
                      value,
                      style: new TextStyle(fontSize: 15.0),
                    ),
                  ),
                ),
              );
            })
            .values
            .toList(),
      ),
    );
  }

  Widget getStartedView(LoginModel loginModel, BuildContext context) {
    return getPhoneNumberView(loginModel, context);
  }

  Widget getPhoneNumberView(LoginModel loginModel, BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: SMALL_X,
        ),
        PPTexts.getSecondaryHeading(
          LocalizationUtils.getSingleValueString(
              "app-landing", "app-landing.labels.phone"),
        ),
        SizedBox(
          height: MEDIUM,
        ),
        Builder(builder: (context) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              getUsernameField(context, loginModel),
              SizedBox(
                height: SMALL,
              ),
              loginModel.state == ViewState.Busy
                  ? ViewConstants.progressIndicator
                  : PrimaryButton(
                      fullWidth: true,
                      disabled: true,
                      butttonColor: brandColor,
                      text: LocalizationUtils.getSingleValueString(
                          "app-landing", "app-landing.button.button"),
                      iconData: Icons.arrow_forward,
                      onPressed: () {
                        onClickProcedSecurely(context, loginModel);
                      },
                    ),
              SizedBox(
                height: SMALL_XX,
              ),
              getTnCAndPrivacyPolicyView()
            ],
          );
        })
      ],
    );
  }

  Widget getTnCAndPrivacyPolicyView() {
    TapGestureRecognizer _onTapTandC = TapGestureRecognizer()
      ..onTap = () async {
        if (await canLaunch(getTermsAndCondtions())) {
          await launch(getTermsAndCondtions());
        }
      };
    TapGestureRecognizer _onTapPrivacyPolicy = TapGestureRecognizer()
      ..onTap = () async {
        if (await canLaunch(getPrivacyPolicy())) {
          await launch(getPrivacyPolicy());
        }
      };

    List<String> list = LocalizationUtils.getSingleValueString(
            "app-landing", "app-landing.labels.terms-conditions")
        .split("-");
    String first = list[0];
    String second = list[1];
    String third = list[2];
    String fourth = list[3];

    return RichText(
      text: TextSpan(
        text: first + ' ',
        style: MEDIUM_X_SECONDARY,
        children: <TextSpan>[
          TextSpan(
              text: second + ' ',
              style: MEDIUM_X_SECONDARY_BOLD_MEDIUM_UNDER_LINE,
              recognizer: _onTapTandC),
          TextSpan(text: third + ' ', style: MEDIUM_X_SECONDARY),
          TextSpan(
              text: fourth,
              style: MEDIUM_X_SECONDARY_BOLD_MEDIUM_UNDER_LINE,
              recognizer: _onTapPrivacyPolicy),
        ],
      ),
    );
  }

  onClickProcedSecurely(BuildContext context, LoginModel loginModel) async {
    var connectivityResult = await loginModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: loginModel.noInternetConnection);
      return;
    }
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_proceed);
    if (_phoneNumberController.text.length == 10) {
      phAutoValidate = false;

      if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true &&
          null == dataStore.readString(DataStoreService.PHONE)) {
        dataStore.writeString(
            DataStoreService.PHONE, _phoneNumberController.text);
        await HttpApiUtils().chambersActivate(
            dataStore.readString(DataStoreService.GROUP),
            dataStore.readString(DataStoreService.TOKEN),
            _phoneNumberController.text);

        if (ChambersRedirect.routeName().contains('signup')) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              ChambersRedirect.routeName(), (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(
                  source: BaseStepperSource.NEW_USER, chambersFlow: true));
        }
        if (ChambersRedirect.routeName().contains('transfer')) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              ChambersRedirect.routeName(), (Route<dynamic> route) => false,
              arguments: TransferArguments(
                source: BaseStepperSource.NEW_USER,
                chambersFlow: true,
              ));
        }
        if (ChambersRedirect.routeName().contains('signUpAlmostDoneWidget')) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              ChambersRedirect.routeName(), (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(
                  source: BaseStepperSource.NEW_USER, chambersFlow: true));
        }
        return;
      }

      PhoneVerification checkPhone =
          await loginModel.verifyPhone(_phoneNumberController.text);
      if (checkPhone != null) {
        if (checkPhone.redirect == PhoneNumberStateConstant.LOGIN) {
          locator<NavigationService>().pushNamed(LoginWidget.routeName,
              argument: LoginArguments(phone: _phoneNumberController.text),
              viewPushEvent: AnalyticsEventConstant.landing_login,
              viewPopEvent: AnalyticsEventConstant.carousel1);
        } else if (checkPhone.redirect == PhoneNumberStateConstant.SIGNUP) {
          moveInSignUpFlow(loginModel);
        } else if (checkPhone.redirect == PhoneNumberStateConstant.FORGOT) {
          var success = await loginModel.getOtp(_phoneNumberController.text);
          if (success == true) {
            locator<NavigationService>()
                .pushNamed(LoginVerificationWidget.routeName,
                    argument: VerificationArguments(
                      phoneNo: int.parse(_phoneNumberController.text),
                    ),
                    viewPushEvent: AnalyticsEventConstant.landing_forgot,
                    viewPopEvent: AnalyticsEventConstant.carousel1);
          } else
            onFail(context, errMessage: loginModel.errorMessage);
        }
      } else {
        onFail(context, errMessage: ApplicationConstant.apiError);
      }
    } else if (_phoneNumberController.text.length < 10) {
      showOnSnackBar(context,
          successMessage: LocalizationUtils.getSingleValueString(
              "app-landing", "app-landing.labels.phone-info"));
    } else {
      showOnSnackBar(context,
          successMessage: LocalizationUtils.getSingleValueString(
              "app-landing", "app-landing.labels.phone-error"));
    }
  }

  moveInSignUpFlow(LoginModel loginModel) async {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.landing_signup);
    SignupPhoneNumberResponse signupResponse =
        await loginModel.phoneNumberSignup(_phoneNumberController.text);
    if (signupResponse != null && signupResponse.userId != null) {
      locator<NavigationService>().pushNamed(
        SignupWidget.routeName,
        argument: SignupStepperArguments(
            position: SignupStepperStateEnums.SIGN_UP_ABOUT_YOU),
      );
    } else
      onFail(context, errMessage: loginModel.errorMessage);
  }
}
