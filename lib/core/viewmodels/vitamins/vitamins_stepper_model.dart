import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class VitaminsStepperModel extends BaseModel {
  int currentStep = 0;
  BaseStepperSource source;
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  ConnectivityResult connectivityResult;
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();

  void initialize(BaseStepperSource tmpSource, {int startStep = 0}) {
    currentStep = startStep;
    this.source = tmpSource;
  }

  setCurrentStep({int step = 0}) {
    currentStep = step;
    notifyListeners();
  }

  getCurrentStep() {
    if (currentStep != null) return currentStep;
    currentStep = 0;
    return 0;
  }

  resetWithoutNotify() {
    currentStep = 3;
    currentStep = 0;
  }

  incrCurrentStep() {
    currentStep++;
    notifyListeners();
  }

  decrCurrentStep() {
    currentStep--;
    notifyListeners();
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
