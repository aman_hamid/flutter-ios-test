import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/medication.dart';
import 'package:pocketpills/core/response/pillreminder/medication_details.dart';
import 'package:pocketpills/core/response/pillreminder/pill_reminder_medications_response.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/medications_model.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_medication_model.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_calendar_view.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class PillReminderMedicationsView extends StatefulWidget {
  static const routeName = 'pillReminderMedicationsView';

  @override
  State<StatefulWidget> createState() {
    return PillReminderMedicationsViewState();
  }
}

class PillReminderMedicationsViewState extends BaseState<PillReminderMedicationsView> {
  bool bottomSheetShown = false;

  @override
  void initState() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.medications);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => PillReminderMedicationModel(),
        child: Consumer<PillReminderMedicationModel>(
          builder: (BuildContext context, PillReminderMedicationModel medicationModel, Widget child) {
            return Scaffold(
              appBar: InnerAppBar(
                titleText: "All Reminders",
                appBar: AppBar(),
              ),
              body: FutureBuilder(
                future: medicationModel.fetchPillReminderDayData(),
                builder: (BuildContext context, AsyncSnapshot<PillReminderMedicationResponse> snapshot) {
                  if (medicationModel.connectivityResult == ConnectivityResult.none &&
                      snapshot.connectionState == ConnectionState.done) {
                    return NoInternetScreen(
                      onClickRetry: () {},
                    );
                  }

                  if (snapshot.hasData && medicationModel.connectivityResult != ConnectivityResult.none) {
                    return _afterFutureBuild(context, snapshot.data, medicationModel);
                  } else if (snapshot.hasError && medicationModel.connectivityResult != ConnectivityResult.none) {
                    Crashlytics.instance.log(snapshot.hasError.toString());
                    return ErrorScreen();
                  }

                  if (snapshot.connectionState == ConnectionState.active ||
                      snapshot.connectionState == ConnectionState.waiting) {
                    return LoadingScreen();
                  }
                  return PPContainer.emptyContainer();
                },
              ),
            );
          },
        ));
  }

  Widget _afterFutureBuild(BuildContext context, PillReminderMedicationResponse medicationResponse,
      PillReminderMedicationModel medicationModel) {
    return getMedicationsChildren(medicationModel, medicationResponse.pocketPackDetails);
  }

  Widget getMedicationsChildren(
      PillReminderMedicationModel medicationModel, List<PocketPackDetails> pocketPackDetails) {
    List<Widget> medicationsChildren = [];
    if (pocketPackDetails == null) {
      return PPContainer.emptyContainer();
    }

    pocketPackDetails.forEach((val) => generateSectionFor(medicationModel, val, medicationsChildren));
    medicationsChildren.add(PPUIHelper.verticalSpace(SMALL_XXX));

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Column(children: medicationsChildren),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: REGULAR_XXX),
            child: Row(
              children: <Widget>[
                SecondaryButton(
                  text: "View your progress",
                  fullWidth: false,
                  onPressed: () {
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.pill_pack_calender_view);
                    Navigator.pushNamed(context, PillReminderCalendarView.routeName,
                        arguments: PillReminderCalendarView());
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  generateSectionFor(PillReminderMedicationModel medicationModel, PocketPackDetails pocketPackDetails,
      List<Widget> medicationsChildren) {
    medicationsChildren.add(Column(
      children: <Widget>[
        PPTexts.getSectionHeader(
            child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              PPUIHelper.verticalSpaceSmall(),
              Text(
                pocketPackDetails.partOfDay,
                style: MEDIUM_XXX_PRIMARY_BOLD,
              ),
              Text(
                StringUtils.getFormattedTimeHHMMNN(pocketPackDetails.time),
                style: MEDIUM_XX_SECONDARY,
              ),
              PPUIHelper.verticalSpaceSmall()
            ],
          ),
        )),
        PPDivider()
      ],
    ));
    pocketPackDetails.medicationDetails
        .forEach((medication) => addCheckboxForMedication(medicationModel, medicationsChildren, medication));
  }

  void addCheckboxForMedication(PillReminderMedicationModel medicationModel, List<Widget> medicationsChildren,
      MedicationDetails medicationDetails) {
    return medicationsChildren.add(Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: MEDIUM_XXX, top: MEDIUM_X, bottom: MEDIUM_X),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  medicationDetails.type,
                  style: MEDIUM_X_SECONDARY,
                ),
                Text(
                  medicationDetails.medication,
                  style: MEDIUM_XXX_PRIMARY_BOLD,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                Text(
                  medicationDetails.applicableForAllDays == true
                      ? "All Days"
                      : medicationDetails.applicableDaysOfWeek.toString(),
                  style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                )
              ],
            ),
          ),
          PPDivider()
        ],
      ),
    ));
  }

  Future paintBottomBar(MedicationsModel medicationsModel, bool value, Medication medication) async {
    setState(() {
      if (value == true) {
        medicationsModel.markMedicationSelected(medication.id);
      } else {
        medicationsModel.unmarkMedicationSelected(medication.id);
      }
    });
  }
}
