abstract class BaseRequest {
  //Override this method in child classes
  Map<String, dynamic> toJson();
}
