// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refill_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RefillRequest _$RefillRequestFromJson(Map<String, dynamic> json) {
  return RefillRequest(
    comment: json['comment'] as String,
    medications: (json['medications'] as List)
        ?.map((e) => e == null
            ? null
            : MedicationRequest.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$RefillRequestToJson(RefillRequest instance) =>
    <String, dynamic>{
      'comment': instance.comment,
      'medications': instance.medications?.map((e) => e?.toJson())?.toList(),
    };
