import 'dart:async';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/about_you_request.dart';
import 'package:pocketpills/core/request/device_body.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/login_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/response/signedin_verify_response.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:sms_autofill/sms_autofill.dart';

class SignUpAboutYouModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  final LoginService loginService = locator<LoginService>();
  ConnectivityResult connectivityResult;
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();

  SignUpAboutYouModel() {
    startTimer();
  }

  Timer timer;
  int start = 30;
  bool stopTimer = false;

  int get getStartTime => start;

  void setStartTime(int value) {
    this.start = value;
  }

  Future<bool> updateAboutYou(
      {String province, bool selfMedication, bool isCaregiver, bool pocketPacks, String password, String gender, String email, String referral, String phone}) async {
    setState(ViewState.Busy);
    AboutYouRequest abtRequest = AboutYouRequest(
        province: province != null ? province.toUpperCase() : province,
        hasDailyMedication: selfMedication,
        isCaregiver: isCaregiver,
        pocketPacks: pocketPacks,
        gender: gender,
        password: password,
        email: email,
        invitationCode: referral,
        referralCode: referral,
        phone: phone);
    var response = await _api.updateUserInfo(RequestWrapper(body: abtRequest));
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      errorMessage = res.apiMessage;
      return await loginService.registerUser(response);
    } else {
      return false;
    }
  }

  Future<bool> getVerifyRegistrationFlow(String phoneNo, String otps) async {
    Response response = await getVerifyRegistration(phoneNo, otps, true);
    OldBaseResponse res = OldBaseResponse.fromJson(response.data);
    if (!res.status) {
      errorMessage = res.getErrorMessage();
      return false;
    }
    return await handleRegistrationResponse(response, phoneNo);
  }

  Future<Response> getVerifyRegistration(String phoneNo, String otps, bool isRegistration) async {
    setState(ViewState.Busy);
    DeviceBody deviceBody = DeviceBody();
    int userid = dataStore.getUserId();
    await deviceBody.init();
    Response response = await _api.getVerifyRegistrationFlow(RequestWrapper(body: deviceBody), phoneNo, otps, isRegistration.toString(), userid);
    setState(ViewState.Idle);
    return response;
  }

  Future<bool> handleRegistrationResponse(response, String phoneNo) async {
    return await loginService.registerUser(response);
  }

  Future<SignedinVerifyResponse> handleSignedInResponse(response, String phoneNo) async {
    BaseResponse<SignedinVerifyResponse> res = BaseResponse<SignedinVerifyResponse>.fromJson(response.data);
    if (!res.status) {
      errorMessage = res.getErrorMessage();
      return null;
    }
    await dataStore.writeString(DataStoreService.SET_PASSWORD_TOKEN, res.response.setPasswordToken);
    await dataStore.saveResetToken(res.response.resetKey);
    await dataStore.writeString(DataStoreService.PHONE, phoneNo);
    return res.response;
  }

  Future<bool> getOtp(String phoneNo) async {
    setState(ViewState.Busy);
    await SmsAutoFill().listenForCode;
    var response = await _api.getOtp(phoneNo);
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<LoginResponse> res = BaseResponse<LoginResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  Future<bool> resendOtp(String phoneNo, String mode, bool isRegistrationProcess) async {
    setState(ViewState.Busy);
    var response = await _api.resendOtp(phoneNo, mode, isRegistrationProcess);
    setState(ViewState.Idle);

    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      errorMessage = res.apiMessage;
      return true;
    } else {
      return false;
    }
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    timer = Timer.periodic(oneSec, (Timer timer) {
      if (start < 1 || stopTimer == true) {
        timer.cancel();
      } else {
        start--;
      }
    });
  }

  updateUI() {
    notifyListeners();
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
