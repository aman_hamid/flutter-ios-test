import 'package:pocketpills/core/request/base_request.dart';

class LocalizationRequest extends BaseRequest {
  String combinedIdentifierKey;
  LocalizationRequest({this.combinedIdentifierKey});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'combinedIdentifierKey': this.combinedIdentifierKey + ".*.*"
      };
}
