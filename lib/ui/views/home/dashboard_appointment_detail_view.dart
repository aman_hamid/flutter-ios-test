import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/response/telehealth/appointment_first_responce.dart';
import 'package:pocketpills/core/viewmodels/dashboard/prescription_dashboard_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/prescriptions/prescription_detail_view.dart';
import 'package:pocketpills/ui/views/home/reschedule_time_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/documents_type.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/models/response/prescription/prescription_medication.dart';
import 'package:pocketpills/core/utils/date_utils.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/prescription_model.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/views/imageupload/image_viewer.dart';
import 'package:pocketpills/ui/views/pdf_viewer.dart';
import 'package:pocketpills/core/viewmodels/dashboard/appointment_dashboard_model.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_appointment_details_response.dart';

class DashboardAppointmentDetailsView extends StatefulWidget {
  static const routeName = 'dashboardAppointmentDetailsView';
  final int prescriptionId;

  DashboardAppointmentDetailsView({this.prescriptionId});

  @override
  State<StatefulWidget> createState() {
    return DashboardAppointmentDetailsViewState();
  }
}

class DashboardAppointmentDetailsViewState
    extends BaseState<DashboardAppointmentDetailsView> {
  ScrollController _scrollController;
  List<bool> selectedIndexes = [];
  bool itemSelected = false;
  AppointmentTime selectedItem;
  @override
  void initState() {
    super.initState();
    _scrollController = new ScrollController(
      // NEW
      initialScrollOffset: 0.0, // NEW
      keepScrollOffset: true, // NEW
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AppointmentDashboardModel>(
              create: (_) => AppointmentDashboardModel())
        ],
        child: Consumer<AppointmentDashboardModel>(
          builder: (BuildContext context,
              AppointmentDashboardModel appointmentDashboardModel,
              Widget child) {
            return FutureBuilder(
                future: appointmentDashboardModel
                    .getLocalization(["consultation", "common"]),
                builder: (BuildContext context,
                    AsyncSnapshot<Map<String, dynamic>> snapshot) {
                  if (snapshot.hasData != null && snapshot.data != null) {
                    analyticsEvents.sendAnalyticsEvent("appointment_details_open");
                    return getMainView(appointmentDashboardModel);
                  } else if (snapshot.hasError) {
                    return ErrorScreen();
                  } else {
                    return LoadingScreen();
                  }
                });
          },
        ));
  }

  Widget getMainView(AppointmentDashboardModel appointmentDashboardModel) {
    return Scaffold(
      appBar: InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString(
            "consultation", "consultation.appointment.appointment-title"),
        appBar: AppBar(),
      ),
      body: FutureBuilder(
        future: appointmentDashboardModel
            .fetchPrescriptionById(widget.prescriptionId),
        builder: (BuildContext context,
            AsyncSnapshot<AppointmentFirstResponse> snapshot) {
          if (appointmentDashboardModel.connectivityResult ==
                  ConnectivityResult.none &&
              snapshot.connectionState == ConnectionState.done) {
            return NoInternetScreen(
              onClickRetry: appointmentDashboardModel.clearAsyncMemoizer,
            );
          }

          if (snapshot.hasData &&
              appointmentDashboardModel.connectivityResult !=
                  ConnectivityResult.none) {
            return getPrescriptionDetailView(
                snapshot.data, appointmentDashboardModel);
          } else if (snapshot.hasError &&
              appointmentDashboardModel.connectivityResult !=
                  ConnectivityResult.none) {
            Crashlytics.instance.log(snapshot.hasError.toString());
            return ErrorScreen();
          }

          if (snapshot.connectionState == ConnectionState.active ||
              snapshot.connectionState == ConnectionState.waiting) {
            return LoadingScreen();
          }
          return PPContainer.emptyContainer();
        },
      ),
    );
  }



  Widget getPrescriptionDetailView(AppointmentFirstResponse appointment,
      AppointmentDashboardModel model) {
    return Container(
      child: Builder(
        builder: (BuildContext context) {
          List<Widget> prescriptionDetailsChildren = [];
          prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
          /*  prescriptionDetailsChildren.add(Padding(
            padding: EdgeInsets.symmetric(
                horizontal: PPUIHelper.HorizontalSpaceMedium),
            child: appointment.prescriptionDTO.telehealthRequestedMedications != null
                ? getPrescriptionCardHeading(appointment)
                : Container(),
          ));*/

          prescriptionDetailsChildren.add(Padding(
            padding: EdgeInsets.symmetric(
                horizontal: PPUIHelper.HorizontalSpaceMedium),
            child: appointment.prescriptionDTO.prescriptionMedicalConditions != null
                ? PPTexts.getSecondaryHeading(appointment.prescriptionDTO.prescriptionMedicalConditions)
                : Container(),
          ));
          prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceSmall());
          prescriptionDetailsChildren.add(Padding(
            padding: EdgeInsets.symmetric(
                horizontal: PPUIHelper.HorizontalSpaceMedium),
            //child: PPTexts.getSecondaryHeading("Category:" + appointment.prescriptionRequestCategory, isBold: false),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(LocalizationUtils.getSingleValueString("consultation",
                    "consultation.appointment.doctor") +
                    ": ",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: secondaryColor,
                        fontSize: PPUIHelper.FontSizeMedium)),
                Text(appointment.prescriptionDTO.doctor.doctorNameWithDr,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: secondaryColor,
                        fontSize: PPUIHelper.FontSizeMedium))
              ],
            ),
          ));
          prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceSmall());
          prescriptionDetailsChildren.add(Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                      LocalizationUtils.getSingleValueString("consultation",
                          "consultation.appointment.clinic") +
                          ": ",
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: secondaryColor,
                          fontSize: PPUIHelper.FontSizeMedium)),
                  Text(appointment.prescriptionDTO.telehealthClinic,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: secondaryColor,
                          fontSize: PPUIHelper.FontSizeMedium))
                ],
              )));
          prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
          prescriptionDetailsChildren.add(PPTexts.getSectionHeader(
              text: LocalizationUtils.getSingleValueString("consultation",
                  "consultation.appointment.date")));
          prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceSmall());

          prescriptionDetailsChildren.add(Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Column(children: [
                      PPTexts.getSecondaryHeading(appointment.slotDate,
                          isBold: true),
                      PPUIHelper.verticalSpaceSmall(),
                      PPTexts.getSecondaryHeading(appointment.timeSlot,
                          isBold: false, color: Colors.black, fontSize: 16)
                    ]),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        PPUIHelper.verticalSpaceMedium(),
                        Container(
                          child: PPTexts.getSecondaryHeading("",
                              isBold: true,child:InkWell(
                                onTap: () async {
                                  showCallAndChatBottomSheet(
                                      context,
                                      model,
                                      appointment.prescriptionDTO.prescriptionFilledByExternalPharmacy,
                                      appointment.prescriptionDTO.prescriptionMedicalConditions,
                                      appointment.prescriptionDTO.prescriptionComment,
                                      appointment.prescriptionDTO.appointmentTime.toString(),
                                      appointment.prescriptionDTO.telehealthRequestedMedications,
                                      widget.prescriptionId
                                  );
                                },
                                child: Text(
                                    LocalizationUtils.getSingleValueString("common",
                                        "common.button.reschedule"),
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: brandColor,
                                        fontSize: PPUIHelper.FontSizeMedium)),
                              ) ),
                          alignment: Alignment.center,
                        ),
                        //Center Column contents horizontally,
                      ],
                    ),
                  )

                ],
              )),);

          if (appointment.prescriptionDTO.prescriptionComment != null) {
            prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceSmall());
            prescriptionDetailsChildren.add(PPTexts.getSectionHeader(
                text: LocalizationUtils.getSingleValueString("consultation",
                    "consultation.appointment.description")));
            prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceSmall());
            prescriptionDetailsChildren.add(Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: PPTexts.getSecondaryHeading(
                  appointment.prescriptionDTO.prescriptionComment,
                  isBold: false,color: Colors.black,fontSize: 16),
            ));
          }
          prescriptionDetailsChildren.add(PPUIHelper.verticalSpaceMedium());
          if (appointment.prescriptionDTO.telehealthRequestedMedications != null &&
              appointment.prescriptionDTO.telehealthRequestedMedications.isNotEmpty) {
            List telehealthRequestedMedications =
            appointment.prescriptionDTO.telehealthRequestedMedications.split(',');
            prescriptionDetailsChildren.add(PPTexts.getSectionHeader(
                text: LocalizationUtils.getSingleValueString("consultation",
                    "consultation.appointment.current-medications")));
            prescriptionDetailsChildren
                .add(getMedications(telehealthRequestedMedications));
          }
          return SingleChildScrollView(
            child: Column(children: prescriptionDetailsChildren),
          );
        },
      ),
    );
  }

  static Widget getPrescriptionCardHeading(
      TelehealthAppointmentResponse appointment) {
    String reason = ViewConstants.getPrescriptionStatus(
        appointment.prescriptionRequestReason);
    reason = '${reason[0].toUpperCase()}${reason.substring(1).toLowerCase()}';
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[Expanded(child: PPTexts.getHeading(reason))],
    );
  }

  showCallAndChatBottomSheet(
      BuildContext context,
      AppointmentDashboardModel models,
      bool prescriptionFilledByExternalPharmacy,
      String prescriptionMedicalConditions,
      String prescriptionComment,
      String appointmentTime,
      String telehealthRequestedMedications,
      int prescriptionId) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return RescheduleTimeSheet(
              context: bottomSheetContext,
              model: models,
              prescriptionFilledByExternalPharmacy:
                  prescriptionFilledByExternalPharmacy,
              prescriptionMedicalConditions: prescriptionMedicalConditions,
              prescriptionComment: prescriptionComment,
              appointmentTime: appointmentTime,
              telehealthRequestedMedications: telehealthRequestedMedications,
              prescriptionId: prescriptionId);
        });
  }

  Widget getMedications(List<String> telehealthRequestedMedications) {
    List<Widget> medicationsChildren = [];
    if (telehealthRequestedMedications.length > 0) {
      medicationsChildren.add(PPUIHelper.verticalSpaceMedium());
      for (int i = 0; i < telehealthRequestedMedications.length; i++) {
        List<Widget> medicinesColumn = [];
        medicinesColumn.add(PPUIHelper.verticalSpaceSmall());
        medicinesColumn.add(PPTexts.getSecondaryHeading(
            telehealthRequestedMedications[i],
            isBold: true));
        medicinesColumn.add(PPUIHelper.verticalSpaceSmall());
        medicinesColumn.add(PPUIHelper.verticalSpaceSmall());
        if (i != telehealthRequestedMedications.length - 1)
          medicinesColumn.add(Divider());

        medicationsChildren.add(Padding(
          padding: EdgeInsets.symmetric(
              horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Container(
            child: Column(children: medicinesColumn),
          ),
        ));
      }
    }
    return Container(child: Column(children: medicationsChildren));
  }

  showBottomSheet(BuildContext context) {
    showModalBottomSheet<Null>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Consumer<AppointmentDashboardModel>(
          builder: (BuildContext context,
              AppointmentDashboardModel dashboardModel, Widget child) {
            for (int i = 0; i < dashboardModel.appointmentsList.length; i++) {
              selectedIndexes.add(false);
            }
            return SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              PPTexts.getHeading("SWITCH ACCOUNT"),
                              PPTexts.getDescription(
                                  "Switch account or add new")
                            ],
                          ),
                        ),
                        SecondaryButton(
                          text: "ADD MEMBER",
                          onPressed: () {},
                        )
                      ],
                    ),
                  ),
                  PPDivider(),
                  ConstrainedBox(
                    constraints: new BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height / 2,
                    ),
                    child: appointmentDate(context, dashboardModel,
                        dashboardModel.appointmentsList),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  Widget appointmentDate(BuildContext context, AppointmentDashboardModel model,
      List<AppointmentTime> appointmentTime) {
    return new GridView.builder(
        shrinkWrap: true,
        primary: false,
        controller: _scrollController,
        scrollDirection: Axis.vertical,
        itemCount: appointmentTime.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 2.3),
        itemBuilder: (BuildContext context, int index) {
          AppointmentTime appointmentTimeItem = appointmentTime[index];
          return Container(
            decoration: new BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(40, 0, 0, 0),
                  blurRadius: 3,
                  spreadRadius: 0,
                  offset: Offset(
                    0, // horizontal,
                    1.0, // vertical,
                  ),
                )
              ],
              border: Border.all(color: Color(0xFFE3DBFF), width: 1),
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(5.0),
              color: selectedIndexes[index] == true
                  ? brandColor
                  : appointmentTimeItem.available == false
                      ? bgDateColor
                      : Colors.white,
            ),
            margin: EdgeInsets.all(SMALL_XXX),
            child: Card(
              color: Colors.white,
              shape: RoundedRectangleBorder(),
              margin: EdgeInsets.all(1.0),
              elevation: 0,
              child: InkWell(
                borderRadius: BorderRadius.circular(4.0),
                child: Container(
                  padding: EdgeInsets.all(0.0),
                  child: Container(
                    color: selectedIndexes[index] == true
                        ? brandColor
                        : appointmentTimeItem.available == false
                            ? bgDateColor
                            : Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: MEDIUM_X, right: MEDIUM_X, top: MEDIUM),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    appointmentTimeItem.displayDate,
                                    style: selectedIndexes[index] == true
                                        ? MEDIUM_XX_WHITE_BOLD
                                        : MEDIUM_XX_PRIMARY_BOLD,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                  SizedBox(
                                    height: SMALL_XX,
                                  ),
                                  Text(
                                    appointmentTimeItem.status,
                                    style: selectedIndexes[index] == true
                                        ? MEDIUM_XX_WHITE_BOLD
                                        : appointmentTimeItem.available == true
                                            ? MEDIUM_XX_GREEN_BOLD_MEDIUM
                                            : MEDIUM_XX_RED_BOLD_MEDIUM,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                onTap: () {
                  if (appointmentTimeItem.available == true) {
                    setState(() {
                      //model.updateAppointment(appointmentTimeItem.id);
                      if (selectedIndexes[index] != true) {
                        for (int i = 0; i < appointmentTime.length; i++) {
                          selectedIndexes[i] = false;
                        }
                        itemSelected = true;
                        selectedItem = appointmentTimeItem;
                        selectedIndexes[index] = true;
                      } else {
                        itemSelected = false;
                        selectedIndexes[index] = false;
                      }
                    });
                  }
                },
              ),
            ),
          );
        });
  }

}
