import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/payment_card.dart';

part 'order_payment.g.dart';

@JsonSerializable()
class OrderPayment{

  int id;
  String transactionId;
  num amount;
  String paymentMode;
  String status;
  PaymentCard paymentDetails;
  int customerPaymentModeId;
  String paymentDate;


  OrderPayment({
    this.id, this.transactionId, this.amount, this.paymentMode, this.status, this.customerPaymentModeId,this.paymentDate,this.paymentDetails
  });

  factory OrderPayment.fromJson(Map<String, dynamic> json) => _$OrderPaymentFromJson(json);

  Map<String, dynamic> toJson() => _$OrderPaymentToJson(this);
}