// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'old_base_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OldBaseResponse _$OldBaseResponseFromJson(Map<String, dynamic> json) {
  return OldBaseResponse(
    status: json['success'] as bool,
    errMessage: json['userMessage'] as String,
    apiMessage: json['message'] as String,
  );
}

Map<String, dynamic> _$OldBaseResponseToJson(OldBaseResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
    };
