import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';

part 'month_wise_medication_response.g.dart';

@JsonSerializable()
class MonthWiseMedicationResponse {
  Map<String, DayWiseMedications> dayWiseMedications;

  MonthWiseMedicationResponse(this.dayWiseMedications);

  factory MonthWiseMedicationResponse.fromJson(Map<String, dynamic> json) =>
      _$MonthWiseMedicationResponseFromJson(json);
}
