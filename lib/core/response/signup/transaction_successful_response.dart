import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';

part 'transaction_successful_response.g.dart';

@JsonSerializable()
class TransactionSuccessfulResponse extends BaseResponse {
  String prescriptionType;
  String prescriptionPreference;
  String deliveryBy;
  bool dueDate;
  SuccessDetails successDetails;

  TransactionSuccessfulResponse(this.successDetails);

  factory TransactionSuccessfulResponse.fromJson(Map<String, dynamic> json) =>
      _$TransactionSuccessfulResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionSuccessfulResponseToJson(this);
}
