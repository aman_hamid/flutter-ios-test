import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/pillreminder/pill_reminder_day_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_day_wise_text_status.dart';
import 'package:pocketpills/ui/views/pillreminder/pill_reminder_status_update_button.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/screen_util.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class PillReminderDayPackView extends StatefulWidget {
  @override
  _PillReminderDayPackViewState createState() => _PillReminderDayPackViewState();
}

class _PillReminderDayPackViewState extends State<PillReminderDayPackView> {
  @override
  Widget build(BuildContext context) {
    return Consumer<PillReminderDayModel>(builder: (BuildContext context, PillReminderDayModel pillReminderDayModel, Widget child) {
      return FutureBuilder(
        future: myFutureMethodOverall(pillReminderDayModel),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData && snapshot.data[0] != null) {
            return afterFutureBuild(context, pillReminderDayModel.dayWiseMedications, pillReminderDayModel);
          }

          return PPContainer.emptyContainer();
        },
      );
    });
  }

  Future myFutureMethodOverall(PillReminderDayModel model) async {
    Future<DayWiseMedications> future1 = model.fetchPillReminderDayData();
    Future<Map<String, dynamic>> future2 = model.getLocalization(["app-dashboard"]);
    return await Future.wait([future1, future2]);
  }

  Widget afterFutureBuild(BuildContext context, DayWiseMedications dayWiseMedications, PillReminderDayModel pillReminderDayModel) {
    if (dayWiseMedications != null && dayWiseMedications.pocketPackDetails != null && dayWiseMedications.pocketPackDetails.length > 0) {
      return SafeArea(
        child: Scaffold(
            backgroundColor: Colors.black.withOpacity(0.7),
            body: InkWell(
              onTap: () {
                Provider.of<DashboardModel>(context).applicationStart = true;
              },
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SingleChildScrollView(
                      child: Container(
                          child: Align(
                        alignment: Alignment.bottomCenter,
                        // Center is a layout widget. It takes a single child and positions it
                        // in the middle of the parent.
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(SMALL_XXX)),
                            color: whiteColor,
                          ),
                          child: FractionallySizedBox(
                            widthFactor: 0.9,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(SMALL_XXX), topRight: Radius.circular(SMALL_XXX)),
                                    color: whiteColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: MEDIUM_XXX, left: MEDIUM_XXX, bottom: MEDIUM_XXX),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          dayWiseMedications.title ?? "",
                                          style: REGULAR_XXX_PRIMARY_BOLD,
                                        ),
                                        Text(
                                          StringUtils.getFormattedDateEEEEddMMMM(dayWiseMedications.date),
                                          style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                IntrinsicHeight(
                                  child: Container(
                                    color: Colors.white,
                                    padding: EdgeInsets.only(top: SMALL_XXX),
                                    child: FractionallySizedBox(
                                        widthFactor: 1.12,
                                        heightFactor: 1,
                                        child: Container(width: double.infinity, child: getPillReminderTimes(dayWiseMedications.pocketPackDetails, pillReminderDayModel))),
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  height: MEDIUM_XXX,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(SMALL_XXX), bottomRight: Radius.circular(SMALL_XXX)),
                                    color: whiteColor,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SizedBox(
                            height: LARGE,
                          ),
                          InkWell(
                            onTap: () {
                              Provider.of<DashboardModel>(context).applicationStart = true;
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(LARGE_XX)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: SMALL_XXX,
                                  ),
                                ],
                                color: whiteColor,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(MEDIUM_XXX),
                                child: Icon(
                                  Icons.home,
                                  size: 32,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MEDIUM_X,
                          ),
                          Text(
                            LocalizationUtils.getSingleValueString("common", "common.navbar.home"),
                            style: MEDIUM_XXX_WHITE_BOLD,
                          ),
                          SizedBox(
                            height: 32,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getPillReminderTimes(List<PocketPackDetails> pocketPackDetails, PillReminderDayModel pillReminderDayModel) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        color: Colors.transparent,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.fromLTRB(LARGE_X, 0.0, LARGE_X, MEDIUM),
          child: IntrinsicHeight(
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: pocketPackDetails.length == 0
                    ? <Widget>[PPContainer.emptyContainer()]
                    : pocketPackDetails
                        .map(
                          (pocketPackDetail) => Container(
                            constraints: BoxConstraints(maxWidth: pocketPackDetails.length == 1 ? ScreenUtil.screenWidthDp - 64 : ScreenUtil.screenWidthDp * .75),
                            child: PPCard(
                                onTap: () async {},
                                cardColor: whiteColor,
                                margin: EdgeInsets.only(left: SMALL_XXX, right: SMALL_XXX, top: SMALL_X),
                                padding: EdgeInsets.all(0.0),
                                child: Container(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(topRight: Radius.circular(SMALL_X), topLeft: Radius.circular(SMALL_X)),
                                        color: headerBgColor,
                                      ),
                                      padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: MEDIUM_X),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                getDayLanguage(pocketPackDetail.partOfDay),
                                                style: MEDIUM_XXX_PRIMARY_BOLD,
                                              ),
                                              Text(
                                                StringUtils.getFormattedTimeHHMMNN(pocketPackDetail.time),
                                                style: MEDIUM_X_SECONDARY_BOLD_MEDIUM,
                                              )
                                            ],
                                          ),
                                          PillReminderDayWiseTextStatus(pocketPackDetail),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(vertical: MEDIUM_X, horizontal: MEDIUM_XXX),
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: pocketPackDetail.medicationDetails.map((v) {
                                              return Padding(
                                                padding: const EdgeInsets.symmetric(vertical: SMALL_X),
                                                child: Text(
                                                  Utils.removeDecimalZeroFormat(v.quantity) + " x " + v.medication,
                                                  style: MEDIUM_XX_PRIMARY,
                                                ),
                                              );
                                            }).toList()),
                                      ),
                                    ),
                                    PillReminderStatusUpdateButton(
                                      updateMissedStatus: () {
                                        updateMissedStatus(pillReminderDayModel, pocketPackDetail);
                                      },
                                      updateTakenStatus: () {
                                        updateTakenStatus(pillReminderDayModel, pocketPackDetail);
                                      },
                                      pocketPackDetails: pocketPackDetail,
                                    )
                                  ],
                                ))),
                          ),
                        )
                        .toList()),
          ),
        ),
      ),
    );
  }

  void updateMissedStatus(PillReminderDayModel pillReminderDayModel, PocketPackDetails pocketPackDetail) async {
    await pillReminderDayModel.updatePillReminderStatus(PillStatusType.MISSED, pocketPackDetail.pocketPackIds);
  }

  void updateTakenStatus(PillReminderDayModel pillReminderDayModel, PocketPackDetails pocketPackDetail) async {
    await pillReminderDayModel.updatePillReminderStatus(PillStatusType.TAKEN, pocketPackDetail.pocketPackIds);
  }
}

getDayLanguage(String partOfDay) {
  switch (partOfDay) {
    case "MORNING":
      return LocalizationUtils.getSingleValueString("modal", "modal.timeslot.morning").toUpperCase();
      break;
    case "AFTERNOON":
      return LocalizationUtils.getSingleValueString("modal", "modal.timeslot.afternoon").toUpperCase();
      break;
    case "EVENING":
      return LocalizationUtils.getSingleValueString("modal", "modal.timeslot.evening").toUpperCase();
      break;
    case "BEDTIME":
      return LocalizationUtils.getSingleValueString("modal", "modal.timeslot.bedtime").toUpperCase();
      break;
    default:
      return partOfDay;
      break;
  }
}
