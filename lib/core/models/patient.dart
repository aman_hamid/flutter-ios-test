import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'patient.g.dart';

@JsonSerializable()
class Patient extends BaseRequest {
  int id;
  bool disabled;
  String firstName;
  String lastName;
  String birthDate;
  int countryCode;
  String phn;
  String emailVerificationToken;
  String province;
  List insuranceDetailsSet;
  bool hasDailyMedication;
  bool hasViewedAboutYouUser;
  bool hasViewedAboutYouPatient;
  bool hasViewedAlmostDone;
  bool hasViewedUserRegistrationSummary;
  String maritalStatus;
  String invitationCode;
  String pharmacyCode;
  String gender;
  String employerDisplayName;
  int phone;
  String email;
  bool isMailVerified;

  Patient(
      {this.id,
      this.disabled,
      this.firstName,
      this.lastName,
      this.birthDate,
      this.countryCode,
      this.phn,
      this.emailVerificationToken,
      this.province,
      this.insuranceDetailsSet,
      this.hasDailyMedication,
      this.hasViewedAboutYouUser,
      this.hasViewedAboutYouPatient,
      this.hasViewedAlmostDone,
      this.hasViewedUserRegistrationSummary,
      this.maritalStatus,
      this.pharmacyCode,
      this.gender,
      this.phone,
      this.email,
      this.isMailVerified,
      this.employerDisplayName});

  factory Patient.fromJson(Map<String, dynamic> json) => _$PatientFromJson(json);

  Map<String, dynamic> toJson() => _$PatientToJson(this);

  String getFullName() {
    if (firstName == null && lastName == null)
      return "User";
    else if (lastName == null)
      return firstName;
    else
      return firstName + " " + lastName;
  }
}
