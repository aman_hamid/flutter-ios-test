// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medicine_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicineRequest _$MedicineRequestFromJson(Map<String, dynamic> json) {
  return MedicineRequest(
    quantity: json['quantity'] as num,
    drugName: json['drugName'] as String,
    din: json['din'] as String,
    userSigCode: json['userSigCode'] as String,
  );
}

Map<String, dynamic> _$MedicineRequestToJson(MedicineRequest instance) =>
    <String, dynamic>{
      'din': instance.din,
      'drugName': instance.drugName,
      'quantity': instance.quantity,
      'userSigCode': instance.userSigCode,
    };
