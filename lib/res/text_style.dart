//import 'package:flutter/material.dart';
//import 'package:pocketpills/ui/shared/constants/colors.dart';
//
//import 'package:pocketpills/ui/style/size_style.dart';
//import 'package:pocketpills/utils/size_config.dart';
//
//// Medium
// TextStyle MEDIUM_WHITE = TextStyle(color: whiteColor, fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM));
//
// TextStyle MEDIUM_SECONDARY =
//    TextStyle(color: secondaryColor, fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM));
//
////Medium_X
// TextStyle MEDIUM_X_SECONDARY =
//    TextStyle(color: secondaryColor, fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM_X), height: 1.4);
//
// TextStyle MEDIUM_X_PINK =
//TextStyle(color: pinkColor, fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM_X), height: 1.4);
//
// TextStyle MEDIUM_X_SECONDARY_BOLD = TextStyle(
//    color: secondaryColor, fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM_X), fontWeight: FontWeight.bold);
//
// TextStyle MEDIUM_X_SECONDARY_BOLD_MEDIUM = TextStyle(
//    color: secondaryColor,
//    fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM_X),
//    height: 1.4,
//    fontWeight: FontWeight.w500);
//
// TextStyle MEDIUM_X_WHITE_BOLD = TextStyle(
//    color: whiteColor, fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM_X), fontWeight: FontWeight.bold);
//
////Medium_XX
// TextStyle MEDIUM_XX_PRIMARY_BOLD = TextStyle(
//    color: primaryColor, fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XX), fontWeight: FontWeight.bold);
//
// TextStyle MEDIUM_XX_PRIMARY_MEDIUM_BOLD = TextStyle(
//    color: primaryColor, fontSize:  ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XX), fontWeight: FontWeight.w500);
//
// TextStyle MEDIUM_XX_PURPLE_BOLD = TextStyle(
//    color: brandColor, fontSize:  ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XX), fontWeight: FontWeight.bold);
//
// TextStyle MEDIUM_XX_RED_BOLD = TextStyle(
//    color: errorColor, fontSize:  ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XX), fontWeight: FontWeight.bold);
//
// TextStyle MEDIUM_XX_SECONDARY =
//    TextStyle(color: secondaryColor, fontSize:  ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XX), height: 1.4);
//
// TextStyle MEDIUM_XX_SECONDARY_BOLD_MEDIUM = TextStyle(
//    color: secondaryColor,
//    fontSize:  ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XX),
//    height: 1.4,
//    fontWeight: FontWeight.w500);
//
// TextStyle MEDIUM_XX_LINK = TextStyle(
//    color: linkColor,
//    fontSize:  ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XX),
//    height: 1.4,
//    fontWeight: FontWeight.w500);
//
////Medium_XXX
// TextStyle MEDIUM_XXX_PRIMARY_BOLD = TextStyle(
//  color: primaryColor,
//  fontSize:  ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XXX),
//  fontWeight: FontWeight.bold,
//);
//
// TextStyle MEDIUM_XXX_WHITE_BOLD = TextStyle(
//  color: whiteColor,
//  fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XXX),
//  fontWeight: FontWeight.bold,
//);
//
// TextStyle MEDIUM_XXX_WHITE = TextStyle(
//  color: whiteColor,
//  fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XXX),
//);
//
// TextStyle MEDIUM_XXX_SECONDARY =
//    TextStyle(color: secondaryColor, fontSize: ScreenUtil(allowFontScaling: true).setSp(MEDIUM_XXX), height: 1.4);
//
////Regular_XXX
// TextStyle REGULAR_XXX_PRIMARY_BOLD = TextStyle(
//    color: primaryColor, fontSize: ScreenUtil(allowFontScaling: true).setSp(REGULAR_XXX), fontWeight: FontWeight.bold);

import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';

// Medium
const TextStyle MEDIUM_WHITE = TextStyle(color: whiteColor, fontSize: MEDIUM);

const TextStyle MEDIUM_SECONDARY = TextStyle(color: secondaryColor, fontSize: MEDIUM);

//Medium_X
const TextStyle MEDIUM_X_SECONDARY = TextStyle(color: secondaryColor, fontSize: MEDIUM_X, height: 1.4);

const TextStyle MEDIUM_X_PRIMARY = TextStyle(color: primaryColor, fontSize: MEDIUM_X, height: 1.4);

const TextStyle MEDIUM_X_PRIMARY_BOLD = TextStyle(color: primaryColor, fontSize: MEDIUM_X, height: 1.4, fontWeight: FontWeight.bold);

const TextStyle MEDIUM_X_PRIMARY_MEDIUM_BOLD = TextStyle(color: primaryColor, fontSize: MEDIUM_X, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_X_MAROON_MEDIUM_BOLD = TextStyle(color: maroonColor, fontSize: MEDIUM_X, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_X_PINK = TextStyle(color: pinkColor, fontSize: MEDIUM_X, height: 1.4);

const TextStyle MEDIUM_X_SECONDARY_BOLD = TextStyle(color: secondaryColor, fontSize: MEDIUM_X, fontWeight: FontWeight.bold);

const TextStyle MEDIUM_X_SECONDARY_BOLD_MEDIUM = TextStyle(color: secondaryColor, fontSize: MEDIUM_X, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_X_SECONDARY_BOLD_MEDIUM_UNDER_LINE = TextStyle(
  color: secondaryColor,
  fontSize: MEDIUM_X,
  height: 1.4,
  fontWeight: FontWeight.w500,
  decoration: TextDecoration.underline,
);

const TextStyle MEDIUM_X_LINK_MEDIUM_BOLD = TextStyle(color: linkColor, fontSize: MEDIUM_X, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_X_LINK = TextStyle(color: linkColor, fontSize: MEDIUM_X, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_X_GREEN_BOLD = TextStyle(color: successColor, fontSize: MEDIUM_X, height: 1.4, fontWeight: FontWeight.bold);

const TextStyle MEDIUM_X_TERTIARY_BOLD_MEDIUM = TextStyle(color: tertiaryColor, fontSize: MEDIUM_X, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_X_WHITE_BOLD = TextStyle(color: whiteColor, fontSize: MEDIUM_X, fontWeight: FontWeight.bold);

//Medium_XX
const TextStyle MEDIUM_XX_PRIMARY_BOLD = TextStyle(color: primaryColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.bold);

const TextStyle MEDIUM_XX_PRIMARY = TextStyle(color: primaryColor, fontSize: MEDIUM_XX, height: 1.4);

const TextStyle MEDIUM_XX_PRIMARY_MEDIUM_BOLD = TextStyle(color: primaryColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XX_PINK_BOLD = TextStyle(color: pinkColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.bold);

const TextStyle MEDIUM_XX_PURPLE_BOLD = TextStyle(color: brandColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.bold);

const TextStyle MEDIUM_XX_RED_BOLD = TextStyle(color: errorColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.bold);

const TextStyle MEDIUM_XX_RED_BOLD_MEDIUM = TextStyle(color: errorColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XX_WHITE_BOLD = TextStyle(color: whiteColor, fontSize: MEDIUM_XX, fontWeight: FontWeight.bold);

const TextStyle MEDIUM_XX_WHITE_RGB_OP_60 = TextStyle(color: Color.fromRGBO(255, 255, 255, 60), fontSize: MEDIUM_XX, height: 1.4);

const TextStyle MEDIUM_XX_SECONDARY = TextStyle(color: secondaryColor, fontSize: MEDIUM_XX, height: 1.4);

const TextStyle MEDIUM_XX_SECONDARY_BOLD_MEDIUM = TextStyle(color: secondaryColor, fontSize: MEDIUM_XX, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XX_GREEN_BOLD_MEDIUM = TextStyle(color: successColor, fontSize: MEDIUM_XX, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XX_WHITE_BOLD_MEDIUM = TextStyle(color: whiteColor, fontSize: MEDIUM_XX, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XX_LIGHT_BLUE_BOLD_MEDIUM = TextStyle(color: lightBlue, fontSize: MEDIUM_XX, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XX_LINK = TextStyle(color: linkColor, fontSize: MEDIUM_XX, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XXX_PRIMARY_BOLD = TextStyle(color: primaryColor, fontSize: MEDIUM_XXX, fontWeight: FontWeight.bold, height: 1.4);

const TextStyle MEDIUM_XXX_DARK_BLUE_BOLD = TextStyle(color: darkBlue, fontSize: MEDIUM_XXX, fontWeight: FontWeight.bold, height: 1.4);

const TextStyle MEDIUM_XXX_BLACK_BOLD = TextStyle(
  color: blackColor,
  fontSize: MEDIUM_XXX,
  fontWeight: FontWeight.bold,
);

const TextStyle MEDIUM_XXX_PRIMARY_BOLD_MEDIUM = TextStyle(color: primaryColor, fontSize: MEDIUM_XXX, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XXX_WHITE_BOLD = TextStyle(
  color: whiteColor,
  fontSize: MEDIUM_XXX,
  fontWeight: FontWeight.bold,
);

const TextStyle MEDIUM_XXX_PINK_BOLD = TextStyle(color: pinkColor, fontSize: MEDIUM_XXX, fontWeight: FontWeight.bold);

const TextStyle MEDIUM_XXX_WHITE_MEDUIM_BOLD = TextStyle(
  color: whiteColor,
  fontSize: MEDIUM_XXX,
  fontWeight: FontWeight.w500,
);

const TextStyle MEDIUM_XXX_WHITE = TextStyle(
  color: whiteColor,
  fontSize: MEDIUM_XX,
);

const TextStyle MEDIUM_XX_WHITE_OPACITY_MEDIUM_BOLD = TextStyle(color: whiteOpacity, fontSize: MEDIUM_XX, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XXX_SECONDARY = TextStyle(color: secondaryColor, fontSize: MEDIUM_XXX, height: 1.4);

const TextStyle MEDIUM_XXX_SECONDARY_BOLD_MEDIUM = TextStyle(color: secondaryColor, fontSize: MEDIUM_XXX, height: 1.4, fontWeight: FontWeight.w500);

const TextStyle MEDIUM_XXX_LINK_BOLD_MEDIUM = TextStyle(color: linkColor, fontSize: MEDIUM_XXX, height: 1.4, fontWeight: FontWeight.w500);

//Regular_XXX
const TextStyle REGULAR_XXX_PRIMARY_BOLD = TextStyle(color: primaryColor, fontSize: REGULAR_XXX, fontWeight: FontWeight.bold);

const TextStyle REGULAR_XXX_DARK_BLUE_BOLD = TextStyle(color: darkBlue, fontSize: REGULAR_XXX, fontWeight: FontWeight.bold);
