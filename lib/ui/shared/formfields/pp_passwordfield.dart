import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PPPasswordField extends StatefulWidget {
   const PPPasswordField({
     this.autovalidate=false,
     this.focusNode,
    this.fieldKey,
    this.hintText,
    this.labelText,
    this.helperText,
    this.onSaved,
    this.validator,
    this.onFieldSubmitted,
    this.controller,
    this.decoration,
     this.inputAction
  });

  final TextEditingController controller;
  final FocusNode focusNode;
  final bool autovalidate;
  final Key fieldKey;
  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  final InputDecoration decoration;
  final TextInputAction inputAction;


  @override
  _PasswordFieldState createState() => new _PasswordFieldState();
}

class _PasswordFieldState extends State<PPPasswordField> {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return new TextFormField(
      key: widget.fieldKey,
      autovalidate: widget.autovalidate,
      controller: widget.controller,
      style: TextStyle(
          fontSize: PPUIHelper.FontSizeLarge,
          fontWeight: FontWeight.bold,
          color: primaryColor
      ),
      focusNode: widget.focusNode,
      obscureText: _obscureText,
      onSaved: widget.onSaved,
      validator: widget.validator,
      textInputAction: widget.inputAction,
      onFieldSubmitted: widget.onFieldSubmitted,
      decoration: PPInputDecor.getDecoration(
        hintText: widget.hintText,
        labelText: widget.labelText,
        helperText: widget.helperText,
        suffixIcon:  GestureDetector(
          onTap: () {
            setState(() {
              _obscureText = !_obscureText;
            });
          },
          child: Icon(_obscureText ? Icons.visibility : Icons.visibility_off),
        ),
      ),
    );
  }
}