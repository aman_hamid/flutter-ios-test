// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CouponListResponse _$CouponListResponseFromJson(Map<String, dynamic> json) {
  return CouponListResponse(
    status: json['success'] as bool,
    errMessage: json['userMessage'] as String,
    apiMessage: json['message'] as String,
    coupons: (json['details'] as List)
        ?.map((e) =>
            e == null ? null : Coupon.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CouponListResponseToJson(CouponListResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'details': instance.coupons,
    };
