// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_info_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInfoResponse _$UserInfoResponseFromJson(Map<String, dynamic> json) {
  return UserInfoResponse(
    json['userId'] as int,
    json['prescription'] == null
        ? null
        : TransferPrescription.fromJson(
            json['prescription'] as Map<String, dynamic>),
    json['signupDto'] == null
        ? null
        : SignupDto.fromJson(json['signupDto'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UserInfoResponseToJson(UserInfoResponse instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'prescription': instance.prescription,
      'signupDto': instance.signupDto,
    };
