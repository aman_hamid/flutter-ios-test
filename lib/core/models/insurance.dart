import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'insurance.g.dart';

@JsonSerializable()
class Insurance extends BaseRequest {
  int id;
  bool disabled;
  int patientId;
  String primaryInsuranceBackImage;
  String primaryInsuranceBackImageOriginal;
  String primaryInsuranceFrontImage;
  String primaryInsuranceFrontImageOriginal;
  String provincialInsuranceBackImage;
  String provincialInsuranceBackImageOriginal;
  String provincialInsuranceFrontImage;
  String provincialInsuranceFrontImageOriginal;
  String secondaryInsuranceBackImage;
  String secondaryInsuranceBackImageOriginal;
  String secondaryInsuranceFrontImage;
  String secondaryInsuranceFrontImageOriginal;
  String tertiaryInsuranceFrontImage;
  String tertiaryInsuranceFrontImageOriginal;
  String tertiaryInsuranceBackImage;
  String tertiaryInsuranceBackImageOriginal;
  String quaternaryInsuranceFrontImage;
  String quaternaryInsuranceFrontImageOriginal;
  String quaternaryInsuranceBackImage;
  String quaternaryInsuranceBackImageOriginal;

  Insurance(
      {this.id,
      this.patientId,
      this.disabled,
      this.primaryInsuranceBackImage,
      this.primaryInsuranceBackImageOriginal,
      this.primaryInsuranceFrontImage,
      this.primaryInsuranceFrontImageOriginal,
      this.provincialInsuranceBackImage,
      this.provincialInsuranceBackImageOriginal,
      this.provincialInsuranceFrontImage,
      this.provincialInsuranceFrontImageOriginal,
      this.secondaryInsuranceBackImage,
      this.secondaryInsuranceBackImageOriginal,
      this.secondaryInsuranceFrontImage,
      this.secondaryInsuranceFrontImageOriginal,
      this.tertiaryInsuranceFrontImage,
      this.tertiaryInsuranceFrontImageOriginal,
      this.tertiaryInsuranceBackImage,
      this.tertiaryInsuranceBackImageOriginal,
      this.quaternaryInsuranceFrontImage,
      this.quaternaryInsuranceFrontImageOriginal,
      this.quaternaryInsuranceBackImage,
      this.quaternaryInsuranceBackImageOriginal});

  factory Insurance.fromJson(Map<String, dynamic> json) => _$InsuranceFromJson(json);

  Map<String, dynamic> toJson() => _$InsuranceToJson(this);
}
