// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vitamins_subscription_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VitaminsSubscriptionResponse _$VitaminsSubscriptionResponseFromJson(
    Map<String, dynamic> json) {
  return VitaminsSubscriptionResponse(
    json['nextFillDate'] as String,
    json['shoppingCart'] == null
        ? null
        : ShoppingCart.fromJson(json['shoppingCart'] as Map<String, dynamic>),
    json['subscriptionActive'] as bool,
  );
}

Map<String, dynamic> _$VitaminsSubscriptionResponseToJson(
        VitaminsSubscriptionResponse instance) =>
    <String, dynamic>{
      'nextFillDate': instance.nextFillDate,
      'shoppingCart': instance.shoppingCart,
      'subscriptionActive': instance.subscriptionActive,
    };
