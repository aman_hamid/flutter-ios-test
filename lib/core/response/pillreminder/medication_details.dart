import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'medication_details.g.dart';

@JsonSerializable()
class MedicationDetails {
  List<String> applicableDaysOfWeek;
  bool applicableForAllDays;
  String medication;
  num quantity;
  String quantityUnit;
  String type;

  MedicationDetails(this.applicableDaysOfWeek, this.applicableForAllDays, this.medication, this.quantity,
      this.quantityUnit, this.type);

  factory MedicationDetails.fromJson(Map<String, dynamic> json) => _$MedicationDetailsFromJson(json);
}
