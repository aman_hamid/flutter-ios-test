import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/res/colors.dart';

part 'color_schema.g.dart';

@JsonSerializable()
class ColorSchema {
  String name;

  ColorSchema({this.name});

  Color getCardColor() {
    switch (name) {
      case "color-scheme-a":
        return lightPinkColor;
        break;
      case "color-scheme-b":
        return darkBlueColor;
        break;
      case "color-scheme-c":
        return peachColor;
        break;
      case "color-scheme-d":
        return violetColor;
        break;
      default:
        return lightPinkColor;
        break;
    }
  }

  Color getTextColor() {
    switch (name) {
      case "color-scheme-a":
      case "color-scheme-c":
      case "color-scheme-d":
        return darkPrimaryColor;
        break;
      case "color-scheme-b":
        return whiteColor;
        break;
      default:
        return darkPrimaryColor;
        break;
    }
  }

  factory ColorSchema.fromJson(Map<String, dynamic> json) => _$ColorSchemaFromJson(json);

  Map<String, dynamic> toJson() => _$ColorSchemaToJson(this);
}
