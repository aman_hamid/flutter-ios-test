import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class TelehealthArguments {
  String from;
  SignUpTransferModel modelSignUp;
  BaseStepperSource source;
  UserPatient userPatient;
  TelehealthArguments(
      {this.modelSignUp, this.source, this.userPatient, this.from});
}
