import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BaseScaffold extends StatefulWidget {
  final Widget appBar;
  final Widget body;
  final Widget bottomNavigationBar;

  BaseScaffold({this.appBar, @required this.body, this.bottomNavigationBar});

  @override
  _BaseScaffoldState createState() => _BaseScaffoldState();
}

class _BaseScaffoldState extends State<BaseScaffold> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: widget.bottomNavigationBar != null ? widget.bottomNavigationBar : null,
      appBar: widget.appBar != null ? widget.appBar : null,
      body: GestureDetector(
          onTap: () {
            SystemChannels.textInput.invokeMethod('TextInput.hide');
          },
          child: widget.body),
    );
  }
}
