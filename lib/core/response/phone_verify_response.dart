import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

part 'phone_verify_response.g.dart';

@JsonSerializable()
class PhoneVerifyResponse {
  @JsonKey(name: 'success')
  bool status;

  @JsonKey(name: 'userMessage')
  String errMessage;

  @JsonKey(name: 'message')
  String apiMessage;

  @JsonKey(name: 'details')
  PhoneVerification response;

  PhoneVerifyResponse({this.status, this.errMessage, this.apiMessage, this.response});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory PhoneVerifyResponse.fromJson(Map<String, dynamic> json) => _$PhoneVerifyResponseFromJson(json);
}
