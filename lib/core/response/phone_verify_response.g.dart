// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_verify_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhoneVerifyResponse _$PhoneVerifyResponseFromJson(Map<String, dynamic> json) {
  return PhoneVerifyResponse(
    status: json['success'] as bool,
    errMessage: json['userMessage'] as String,
    apiMessage: json['message'] as String,
    response: json['details'] == null
        ? null
        : PhoneVerification.fromJson(json['details'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PhoneVerifyResponseToJson(
        PhoneVerifyResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'details': instance.response,
    };
