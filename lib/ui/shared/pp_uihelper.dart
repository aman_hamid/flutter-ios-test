import 'package:flutter/material.dart';

/// Contains useful functions to reduce boilerplate code
class PPUIHelper {
  // Vertical spacing constants. Adjust to your liking.
  static const double VerticalSpaceXSmall = 4.0;
  static const double VerticalSpaceSmall = 6.0;
  static const double VerticalSpaceXMedium = 8.0;
  static const double VerticalSpaceMedium = 16.0;
  static const double VerticalSpaceLarge = 24.0;
  static const double VerticalSpaceXLarge = 36.0;

  //Height
  static const double mediumHeight = 48.0;
  static const double largeHeight = 56.0;
  static const double smallHeight = 36.0;

  // Vertical spacing constants. Adjust to your liking.
  static const double HorizontalSpacexSmall = 8.0;
  static const double HorizontalSpaceSmall = 12.0;
  static const double HorizontalSpaceMedium = 16.0;
  static const double HorizontalSpaceLarge = 24.0;
  static const double HorizontalSpaceXLarge = 36.0;

  //FONTSIZES
  static const double ICONSizeXSmall= 28.0;
  static const double FontSizeXSmall= 8.0;
  static const double FontSizeSmall= 12.0;
  static const double FontSizeMedium= 14.0;
  static const double FontSizeLarge= 16.0;
  static const double FontSizeXLarge= 20.0;
  static const double FontSizeXXLarge= 24.0;
//  static const double FontSizeXXLarge= 30.0;
  //FontWeight

  //FONTCOLOR

  /// Returns a vertical space with height set to [VerticalSpaceSmall]
  static Widget verticalSpaceXSmall() {
    return verticalSpace(VerticalSpaceXSmall);
  }

  /// Returns a vertical space with height set to [VerticalSpaceSmall]
  static Widget verticalSpaceSmall() {
    return verticalSpace(VerticalSpaceSmall);
  }

  /// Returns a vertical space with height set to [VerticalSpaceMedium]
  static Widget verticalSpaceMedium() {
    return verticalSpace(VerticalSpaceMedium);
  }

  static Widget verticalSpaceXMedium() {
    return verticalSpace(VerticalSpaceXMedium);
  }

  /// Returns a vertical space with height set to [VerticalSpaceLarge]
  static Widget verticalSpaceLarge() {
    return verticalSpace(VerticalSpaceLarge);
  }

  static Widget verticalSpaceXLarge() {
    return verticalSpace(VerticalSpaceXLarge);
  }

  /// Returns a vertical space equal to the [height] supplied
  static Widget verticalSpace(double height) {
    return Container(height: height);
  }

  /// Returns a vertical space with height set to [HorizontalSpaceSmall]
  static Widget horizontalSpaceSmall() {
    return horizontalSpace(HorizontalSpaceSmall);
  }

  /// Returns a vertical space with height set to [HorizontalSpaceMedium]
  static Widget horizontalSpaceMedium() {
    return horizontalSpace(HorizontalSpaceMedium);
  }

  /// Returns a vertical space with height set to [HorizontalSpaceLarge]
  static Widget horizontalSpaceLarge() {
    return horizontalSpace(HorizontalSpaceLarge);
  }

  /// Returns a vertical space equal to the [width] supplied
  static Widget horizontalSpace(double width) {
    return Container(width: width);
  }
}
