import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:pocketpills/app_config.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/constants/logos.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class WebviewHomeView extends StatefulWidget {
  static const routeName = '/';

  final String sourceUrl;

  WebviewHomeView({this.sourceUrl});

  @override
  State<StatefulWidget> createState() {
    return WebViewState1();
  }
}

class WebViewState1 extends State<WebviewHomeView> {
  static final webviewUrl = locator<AppConfig>().webviewBaseUrl;
  final String initUrl = '$webviewUrl';
  final String logoutUrl = 'logout';

  final DataStoreService dataStore = locator<DataStoreService>();
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  bool cookieSet = false;

  StreamSubscription<String> _onTokenGet;

//  Future<bool> _onBack(){
//    return Future.value(false);
//  }

  @override
  void initState() {
    super.initState();
//    _onTokenGet = flutterWebviewPlugin.onTokenGet.listen((String token) {
//      if (mounted) {
//        setState(() {
//          print("push token recenview$token");
//        });
//      }
//    });
    flutterWebviewPlugin.onUrlChanged.listen((_) async {
      var getUrl =
          "function getCurrentUrl() {return window.location + '    <<>>><<<>>>>  ' + document.cookie;} getCurrentUrl();";
      var currentUrl = await flutterWebviewPlugin.evalJavascript(getUrl);
      print(currentUrl);
      if (currentUrl.contains(logoutUrl) &&
          dataStore.readBoolean(DataStoreService.IS_LOGGED_IN)) {
        print('logging out');
        flutterWebviewPlugin.close();
        flutterWebviewPlugin.dispose();
        await dataStore.logoutUser();
        Navigator.pushReplacementNamed(context, 'start');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            LocalizationUtils.getSingleValueString(
                "common", "common.label.share-experience"),
          ),
        ),
        body: WillPopScope(
          onWillPop: () {
            Navigator.pop(context);
          },
          child: WebviewScaffold(
            url: widget.sourceUrl,
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(0.1), // here the desired height
              child: AppBar(
                title: const Text(''),
              ),
            ),
            withZoom: true,
            withLocalStorage: true,
            hidden: true,
            initialChild: Center(
                child: Image.asset(
              Logos.ppLogoPath,
              width: 100,
              height: 100,
            )),
            afterWebLoadComplete: () {
              flutterWebviewPlugin.onStateChanged.listen((viewState) async {
                /*
            if (viewState.type == WebViewState.finishLoad && !cookieSet) {
              flutterWebviewPlugin.cleanCookies();
              String authCookie = dataStore.getAuthorizationCookie();
              String userId = dataStore.getUserId().toString();
              String patientId = dataStore.getPatientId().toString();
              cookieSet = true;
              print(authCookie + userId + patientId);
              flutterWebviewPlugin.evalJavascript(
                  "document.cookie = 'cookieAuthorization=$authCookie'");
              flutterWebviewPlugin
                  .evalJavascript("document.cookie = 'userId=$userId'");
              flutterWebviewPlugin
                  .evalJavascript("document.cookie = 'patientId=$patientId'");
              flutterWebviewPlugin
                  .evalJavascript("document.location='$webviewUrl'");
              print("Done setting cookies");

            }  */
              });
            },
          ),
        ));
  }
}
