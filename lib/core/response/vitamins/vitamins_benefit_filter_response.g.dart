// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vitamins_benefit_filter_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VitaminsBenefitFilterResponse _$VitaminsBenefitFilterResponseFromJson(
    Map<String, dynamic> json) {
  return VitaminsBenefitFilterResponse(
    (json['itemGroupFilterList'] as List)
        ?.map((e) => e == null
            ? null
            : ItemGroupFilterList.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$VitaminsBenefitFilterResponseToJson(
        VitaminsBenefitFilterResponse instance) =>
    <String, dynamic>{
      'itemGroupFilterList': instance.itemGroupFilterList,
    };
