import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/vitamins/vitamins_subscription_response.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_subscription_model.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/chat_view.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/call_dialog.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/vitamins/vitamins_coupon_view.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_catalog_filter_widget.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_subscription_widget.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../error_screen.dart';

class VitaminsWidget extends StatefulWidget {
  static const routeName = 'vitaminsWidget';

  final BaseStepperSource source;

  VitaminsWidget({Key key, this.source = BaseStepperSource.UNKNOWN_SCREEN});

  @override
  _VitaminsWidgetState createState() => _VitaminsWidgetState();
}

class _VitaminsWidgetState extends BaseState<VitaminsWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<VitaminsSubscriptionModel>(
      builder: (BuildContext context, VitaminsSubscriptionModel vitaminsSubscriptionModel, Widget child) {
        return FutureBuilder(
            future: vitaminsSubscriptionModel.getLocalization(["vitamins"]),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(vitaminsSubscriptionModel);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      },
    );
  }

  Widget getMainView(VitaminsSubscriptionModel vitaminsSubscriptionModel) {
    return BaseScaffold(
        appBar: InnerAppBar(
          titleText: LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.appbar-title"),
          appBar: AppBar(),
          backButtonIcon: widget.source == BaseStepperSource.NEW_USER
              ? Icon(
                  Icons.home,
                  color: whiteColor,
                )
              : Icon(
                  Icons.arrow_back,
                  color: whiteColor,
                ),
          leadingBackButton: () {
            Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
          },
        ),
        body: FutureBuilder(
            future: vitaminsSubscriptionModel.fetchVitaminsSubscription(),
            // ignore: missing_return
            builder: (BuildContext context, AsyncSnapshot<VitaminsSubscriptionResponse> snapshot) {
              if (vitaminsSubscriptionModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                return NoInternetScreen(
                  onClickRetry: vitaminsSubscriptionModel.clearVitaminList,
                );
              }
              if (snapshot.hasData && vitaminsSubscriptionModel.connectivityResult != ConnectivityResult.none) {
                return Container(
                  child: Padding(
                    padding: const EdgeInsets.all(MEDIUM_XXX),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: <Widget>[
                      Text(
                        getStringHeader(1) + ",",
                        style: TextStyle(fontSize: LARGE_X, letterSpacing: Platform.isIOS ? 1.6 : 0, fontWeight: FontWeight.w800, height: 1.4, color: primaryColor),
                      ),
                      SizedBox(
                        height: SMALL_X,
                      ),
                      Text(
                        getStringHeader(2),
                        style: TextStyle(fontSize: LARGE_X, fontWeight: FontWeight.w400, height: 1.4, color: primaryColor),
                      ),
                      SizedBox(
                        height: REGULAR_XXX,
                      ),
                      (widget.source == BaseStepperSource.NEW_USER)
                          ? VitaminsCouponView(
                              source: BaseStepperSource.NEW_USER,
                              padding: EdgeInsets.all(0),
                            )
                          : PPContainer.emptyContainer(),
                      SizedBox(
                        height: MEDIUM_XXX,
                      ),
                      IntrinsicHeight(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Expanded(
                              child: Stack(
                                children: <Widget>[
                                  PPCard(
                                    onTap: () {
                                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_with_expert);
                                      showTalkToAnExpertBottomSheet();
                                    },
                                    padding: EdgeInsets.only(right: MEDIUM_XXX, top: REGULAR_XXX, left: MEDIUM_XXX, bottom: MEDIUM_XXX),
                                    margin: EdgeInsets.only(right: SMALL_XXX, top: MEDIUM_X),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.card-title")),
                                        SizedBox(
                                          height: MEDIUM,
                                        ),
                                        Text(
                                          LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.card-subtitle"),
                                          style: TextStyle(color: secondaryColor, fontSize: MEDIUM_XX, height: snapshot.data.subscriptionActive == false ? 1.4 : 1.6),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: MEDIUM_XXX),
                                    child: Image.asset(
                                      'graphics/icons/vitamin_pack.png',
                                      height: REGULAR_XXX,
                                      width: REGULAR_XXX,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            snapshot.data.subscriptionActive == false
                                ? Expanded(
                                    child: Stack(
                                      children: <Widget>[
                                        PPCard(
                                          onTap: () {
                                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_own_pack);
                                            Provider.of<VitaminsCatalogModel>(context).clearVitaminList();
                                            (widget.source == BaseStepperSource.NEW_USER)
                                                ? Navigator.pushNamed(context, VitaminsCatalogFilterWidget.routeName,
                                                    arguments: BaseStepperArguments(source: BaseStepperSource.NEW_USER))
                                                : Navigator.pushNamed(context, VitaminsCatalogFilterWidget.routeName,
                                                    arguments: BaseStepperArguments(source: BaseStepperSource.VITAMINS_SCREEN));
                                          },
                                          margin: EdgeInsets.only(left: SMALL_XXX, top: MEDIUM_X),
                                          padding: EdgeInsets.only(right: MEDIUM_XXX, top: REGULAR_XXX, left: MEDIUM_XXX, bottom: MEDIUM_XXX),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              PPTexts.getSecondaryHeading(getGenderTextHead(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient))),
                                              SizedBox(
                                                height: MEDIUM,
                                              ),
                                              Text(
                                                getGenderTextDescription(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient)),
                                                style: MEDIUM_XX_SECONDARY,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: REGULAR_XXX),
                                          child: Image.asset(
                                            'graphics/icons/contact_phone.png',
                                            height: REGULAR_XXX,
                                            width: REGULAR_XXX,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                : Expanded(
                                    child: Stack(
                                      children: <Widget>[
                                        PPCard(
                                          onTap: () {
                                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_manage_subscription);
                                            vitaminsSubscriptionModel.clearVitaminList();
                                            Navigator.pushNamed(context, VitaminsSubcriptionWidget.routeName,
                                                arguments: BaseStepperArguments(source: BaseStepperSource.VITAMINS_SUBSCRIPTION_SCREEN));
                                          },
                                          margin: EdgeInsets.only(left: SMALL_XXX, top: MEDIUM_X),
                                          padding: EdgeInsets.only(right: MEDIUM_XXX, top: REGULAR_XXX, left: MEDIUM_XXX, bottom: MEDIUM_XXX),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              PPTexts.getSecondaryHeading(
                                                  getGenderTextSubscription(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient))),
                                              SizedBox(
                                                height: MEDIUM,
                                              ),
                                              RichText(
                                                text: TextSpan(
                                                  text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.subscription.next-delivery") + ':\n',
                                                  style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                      text: StringUtils.getSubscriptionDate(vitaminsSubscriptionModel.vitaminsSubscriptionResponse.nextFillDate) != null
                                                          ? StringUtils.getSubscriptionDate(vitaminsSubscriptionModel.vitaminsSubscriptionResponse.nextFillDate)
                                                          : "",
                                                      style: MEDIUM_XX_SECONDARY,
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: MEDIUM_XXX),
                                          child: Icon(
                                            Icons.card_giftcard,
                                            color: brandColor,
                                            size: REGULAR_XXX,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: MEDIUM_XXX,
                      ),
                      SizedBox(
                        height: LARGE_X,
                      ),
                      (widget.source == BaseStepperSource.NEW_USER)
                          ? Row(children: <Widget>[
                              SecondaryButton(
                                text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.skip"),
                                onPressed: () {
                                  Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
                                },
                              ),
                            ])
                          : Container()
                    ]),
                  ),
                );
              } else if (snapshot.hasError && vitaminsSubscriptionModel.connectivityResult != ConnectivityResult.none) {
                Crashlytics.instance.log(snapshot.hasError.toString());
                return ErrorScreen();
              }

              if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                return LoadingScreen();
              }
            }));
  }

  showTalkToAnExpertBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 150,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(MEDIUM_X),
                  child: PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("modal", "modal.transferhelp.title"), textAlign: TextAlign.start),
                ),
                Divider(
                  color: secondaryColor,
                  height: 1,
                ),
                SizedBox(
                  height: REGULAR_XXX,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: MEDIUM_XXX, right: SMALL_XXX),
                        child: RaisedButton.icon(
                          color: Colors.white,
                          disabledTextColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: tertiaryColor),
                            borderRadius: BorderRadius.circular(SMALL_XX),
                          ),
                          icon: Icon(
                            Icons.textsms,
                            color: secondaryColor,
                          ),
                          label: Text(LocalizationUtils.getSingleValueString("modal", "modal.transferhelp.chat"), style: MEDIUM_XXX_SECONDARY),
                          onPressed: () {
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_with_expert_chat);
                            Navigator.pushNamed(context, ChatWidget.routeName);
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: SMALL_XXX, right: MEDIUM_XXX),
                        child: RaisedButton.icon(
                          color: Colors.white,
                          disabledTextColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: tertiaryColor),
                            borderRadius: BorderRadius.circular(SMALL_XX),
                          ),
                          icon: Icon(
                            Icons.call,
                            color: secondaryColor,
                          ),
                          label: Text(LocalizationUtils.getSingleValueString("common", "common.all.call"), style: MEDIUM_XXX_SECONDARY),
                          onPressed: () {
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_with_expert_call);
                            launch("tel://" + ViewConstants.PHARMACY_PHONE.toString());
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: REGULAR_XXX,
                ),
              ],
            ),
          );
        });
  }

  String getStringHeader(int i) {
    var stringData = LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.title");
    if (i == 1) {
      return stringData.split(",").first.replaceAll(" ", "\n");
    } else {
      return stringData.split(",").last.replaceAll(" <br><span class='font-light'>", "").replaceAll("</span>", "");
    }
  }

  String getGenderTextHead(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.pack-title-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.pack-title-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.pack-title-OTHER");
      default:
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.pack-title");
    }
  }

  String getGenderTextDescription(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.pack-subtitle-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.pack-subtitle-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.pack-subtitle-OTHER");
      default:
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.pack-subtitle");
    }
  }

  String getGenderTextSubscription(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.subscription-title-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.subscription-title-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.subscription-title-OTHER");
      default:
        return LocalizationUtils.getSingleValueString("vitamins", "vitamins.landing.subscription-title");
    }
  }
}
