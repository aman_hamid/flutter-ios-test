import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/payment_card.dart';

part 'transaction.g.dart';

@JsonSerializable()
class Transaction {
  int id;
  String orderId;
  String meta;
  PaymentCard creditCard;
  bool paymentSuccessTransaction;
  bool refundSuccessTransaction;
  bool success;
  num transactionAmount;
  String transactionType;

  Transaction(
      {this.id,
      this.orderId,
      this.creditCard,
      this.meta,
      this.paymentSuccessTransaction,
      this.refundSuccessTransaction,
      this.success,
      this.transactionAmount,
      this.transactionType});

  factory Transaction.fromJson(Map<String, dynamic> json) => _$TransactionFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionToJson(this);
}
