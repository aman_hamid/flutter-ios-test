import 'package:json_annotation/json_annotation.dart';
part 'transfer_prescription.g.dart';

@JsonSerializable()
class TransferPrescription {
  String pharmacyName;
  String pharmacyAddress;
  int pharmacyPhone;
  String prescriptionState;
  int omsPrescriptionId;

  TransferPrescription(
      this.prescriptionState, this.pharmacyName, this.pharmacyAddress, this.pharmacyPhone, this.omsPrescriptionId);

  factory TransferPrescription.fromJson(Map<String, dynamic> json) => _$TransferPrescriptionFromJson(json);
}
