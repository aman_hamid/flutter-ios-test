abstract class AnalyticsEvents {
  Future<void> sendAnalyticsEvent(String eventName, [Map<String, dynamic> bundle]);

  Future<void> setAnalyticsEventIdentify();
}
