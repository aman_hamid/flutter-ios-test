import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class TimeOptions extends StatefulWidget {
  final Medicine medicine;
  final Function(String) showSnackBar;

  TimeOptions({Key key, @required this.medicine, this.showSnackBar}) : super(key: key);

  @override
  _TimeOptionsState createState() => _TimeOptionsState();
}

class _TimeOptionsState extends BaseState<TimeOptions> {
  Medicine medicine;
  List<bool> tileStates = [false, false, false, false];
  //modals
  List<String> dayTimes;

  @override
  void initState() {
    super.initState();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_edit_dosage);
    medicine = widget.medicine;
    VitaminsCatalogModel modal = VitaminsCatalogModel();
    getLanguageData(modal);
    dayTimes = [
      LocalizationUtils.getSingleValueString("modal", "modal.timeslot.morning"),
      LocalizationUtils.getSingleValueString("modal", "modal.timeslot.afternoon"),
      LocalizationUtils.getSingleValueString("modal", "modal.timeslot.evening"),
      LocalizationUtils.getSingleValueString("modal", "modal.timeslot.bedtime")
    ];
    for (int i = 0; i < tileStates.length; i++) {
      if (medicine.modifyuserSigCode[i].toString() == "1") {
        tileStates[i] = true;
      } else {
        tileStates[i] = false;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<VitaminsCatalogModel>(builder: (BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Widget child) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Padding(
                      padding: const EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            medicine.name,
                            maxLines: 2,
                            style: MEDIUM_XXX_PRIMARY_BOLD,
                          ),
                          Text(
                            LocalizationUtils.getSingleValueString("copay", "copay.copay-request.medicine-slot").replaceAll("{{medicine}}", Utils.intToString(medicine.dosage)),
                            style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                          ),
                        ],
                      )),
                ),
                FlatButton(
                    onPressed: () async {
                      bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
                      if (connectivityResult == false) {
                        widget.showSnackBar(vitaminsCatalogModel.noInternetConnection);
                        Navigator.pop(context);
                        return;
                      }
                      vitaminsCatalogModel.updateMedicineToShoppingCart(medicine, medicine.userSigCode, medicine.dosage);
                      Navigator.pop(context);
                    },
                    child: Text(
                      LocalizationUtils.getSingleValueString("modal", "modal.button.set-default").toUpperCase(),
                      style: MEDIUM_XX_LINK,
                    ))
              ],
            ),
          ),
          PPDivider(),
          getDosageTiles(context, vitaminsCatalogModel, medicine),
          PPDivider(),
          PPUIHelper.verticalSpaceMedium(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SecondaryButton(
                  text: LocalizationUtils.getSingleValueString("common", "common.button.close").toUpperCase(),
                  isExpanded: true,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                PPUIHelper.horizontalSpaceSmall(),
                PrimaryButton(
                  text: LocalizationUtils.getSingleValueString("common", "common.button.save").toUpperCase(),
                  isExpanded: true,
                  onPressed: selectedLength() < 1
                      ? null
                      : () async {
                          bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
                          if (connectivityResult == false) {
                            widget.showSnackBar(vitaminsCatalogModel.noInternetConnection);
                            Navigator.pop(context);
                            return;
                          }
                          vitaminsCatalogModel.updateDosageTime(medicine, tileStates);
                          Navigator.pop(context);
                          widget.showSnackBar(LocalizationUtils.getSingleValueString("copay", "copay.copay-request.cart-update-message"));
                        },
                )
              ],
            ),
          ),
          PPUIHelper.verticalSpaceMedium(),
        ],
      );
    });
  }

  int selectedLength() {
    int count = 0;
    for (int i = 0; i < tileStates.length; i++) {
      if (tileStates[i] == true) count++;
    }
    return count;
  }

  setTileState(Medicine medicine) {
    tileStates = [];
    for (int i = 0; i < medicine.modifyuserSigCode.length; i++) {
      medicine.modifyuserSigCode[i] == 0 ? tileStates.add(true) : tileStates.add(false);
    }
  }

  Widget getDosageTiles(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Medicine medicine) {
    List<Widget> tiles = [];
    for (int i = 0; i < dayTimes.length; i++) {
      String dayTime = dayTimes.elementAt(i);
      tiles.add(CheckboxListTile(
        controlAffinity: ListTileControlAffinity.leading,
        title: PPTexts.getHeading(dayTime),
        value: tileStates[i],
        onChanged: (bool value) {
          setState(() {
            tileStates[i] = value;
          });
        },
      ));
    }
    return Container(
        child: Column(
      children: tiles,
    ));
  }
}

void getLanguageData(VitaminsCatalogModel model) {
  FutureBuilder(
      future: myFutureMethodOverall(model),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData != null && snapshot.data != null) {
          return null;
        } else if (snapshot.hasError) {
          return ErrorScreen();
        } else {
          return LoadingScreen();
        }
      });
}

Future myFutureMethodOverall(VitaminsCatalogModel model) async {
  Future<Map<String, dynamic>> future1 = model.getLocalization(["modal", "common", "copay"]);
  return await Future.wait([future1]);
}
