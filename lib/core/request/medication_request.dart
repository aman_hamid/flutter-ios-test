import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
part 'medication_request.g.dart';

@JsonSerializable(explicitToJson: true)
class MedicationRequest extends BaseRequest {
  int medicationId;
  num quantity;
  String din;
  String plan;
  String drugName;
  final int variantId;

  MedicationRequest({this.medicationId, this.quantity, this.din, this.plan, this.drugName, this.variantId});

  factory MedicationRequest.fromJson(Map<String, dynamic> json) => _$MedicationRequestFromJson(json);
  Map<String, dynamic> toJson() => _$MedicationRequestToJson(this);
}
