// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContactResponse _$ContactResponseFromJson(Map<String, dynamic> json) {
  return ContactResponse(
    json['identifier'] as String,
    json['displayName'] as String,
    json['givenName'] as String,
    json['middleName'] as String,
    json['prefix'] as String,
    json['suffix'] as String,
    json['familyName'] as String,
    json['company'] as String,
    json['jobTitle'] as String,
    json['androidAccountTypeRaw'] as String,
    json['androidAccountName'] as String,
    json['androidAccountType'] as String,
    json['birthday'] as String,
  )
    ..emails = (json['emails'] as List)
        ?.map((e) =>
            e == null ? null : ItemResponse.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..phones = (json['phones'] as List)
        ?.map((e) =>
            e == null ? null : ItemResponse.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..postalAddresses = (json['postalAddresses'] as List)
        ?.map((e) => e == null
            ? null
            : PostalAddressResponse.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ContactResponseToJson(ContactResponse instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'displayName': instance.displayName,
      'givenName': instance.givenName,
      'middleName': instance.middleName,
      'prefix': instance.prefix,
      'suffix': instance.suffix,
      'familyName': instance.familyName,
      'company': instance.company,
      'jobTitle': instance.jobTitle,
      'androidAccountTypeRaw': instance.androidAccountTypeRaw,
      'androidAccountName': instance.androidAccountName,
      'androidAccountType': instance.androidAccountType,
      'emails': instance.emails,
      'phones': instance.phones,
      'postalAddresses': instance.postalAddresses,
      'birthday': instance.birthday,
    };
