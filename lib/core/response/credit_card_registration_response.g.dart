// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credit_card_registration_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreditCardRegistrationResponse _$CreditCardRegistrationResponseFromJson(
    Map<String, dynamic> json) {
  return CreditCardRegistrationResponse(
    errMessage: json['userMessage'] as String,
    status: json['success'] as bool,
    apiMessage: json['message'] as String,
    html: json['html'] as String,
  );
}

Map<String, dynamic> _$CreditCardRegistrationResponseToJson(
        CreditCardRegistrationResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'html': instance.html,
    };
