// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invitation_code_data_set.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InvitationCodeDataSet _$InvitationCodeDataSetFromJson(
    Map<String, dynamic> json) {
  return InvitationCodeDataSet(
    json['id'] as int,
    json['disabled'] as bool,
    json['invitationCode'] as String,
    json['employerDataId'] as int,
  );
}

Map<String, dynamic> _$InvitationCodeDataSetToJson(
        InvitationCodeDataSet instance) =>
    <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'invitationCode': instance.invitationCode,
      'employerDataId': instance.employerDataId,
    };
