import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'signup_transfer_upload_image_request.g.dart';

@JsonSerializable()
class SignupTransferUploadImageRequest extends BaseRequest {
  String documentPath;
  String documentType;
  String resizedDocumentPath;
  int pageNumber;

  SignupTransferUploadImageRequest(
      {this.documentPath, this.documentType, this.resizedDocumentPath, this.pageNumber});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'documentPath': this.documentPath,
        'documentType': this.documentType,
        'resizedDocumentPath': this.resizedDocumentPath,
        'pageNumber': this.pageNumber,
      };
}
