import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/signup_stepper_state_enums.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/signup/verification_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/secondary_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/sign_up_stepper.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/verification_state.dart';
import 'package:pocketpills/ui/shared/pin_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class SignUpVerificationWidget extends StatefulWidget {
  static const routeName = 'signupverification';
  final int phoneNo;
  final BaseStepperSource source;

  const SignUpVerificationWidget({Key key, @required this.phoneNo, this.source = BaseStepperSource.UNKNOWN_SCREEN}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignupVerificationState();
  }
}

class SignupVerificationState extends VerificationState<SignUpVerificationWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();
  final String verificationDescription = 'We need to verify your phone number, please enter the 4 digit verification code sent to your phone number.';

  final TextEditingController _phoneNumberController = TextEditingController();

  final FocusNode _phoneNumberFocusNode = FocusNode();
  int phoneNumber;

  @override
  void initState() {
    super.initState();
    phoneNumber = widget.phoneNo;
  }

  _displayDialog(BuildContext context, VerificationModel model) async {
    return showDialog(
        context: context,
        builder: (bc) {
          return AlertDialog(
            title: Text(LocalizationUtils.getSingleValueString("modal", "modal.editphone.title")),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: MEDIUM_XXX,
                  ),
                  PPFormFields.getNumericFormField(
                      maxLength: 10,
                      keyboardType: TextInputType.phone,
                      minLength: 10,
                      textInputAction: TextInputAction.next,
                      controller: _phoneNumberController,
                      focusNode: _phoneNumberFocusNode,
                      onFieldSubmitted: (value) {},
                      validator: (value) {
                        if (value.isEmpty) return LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-phone");
                        String formattedNumber = value.replaceAll("\(", "").replaceAll("\)", "").replaceAll(" ", "").replaceAll("-", "").replaceAll("+", "");
                        if (formattedNumber.length < 10) return LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error1");
                        if (formattedNumber.length > 10) return LocalizationUtils.getSingleValueString("signup", "signup.fields.cell-error2");
                        return null;
                      }),
                  Text(
                    LocalizationUtils.getSingleValueString("signup", "signup.otp.dialoge-description"),
                    style: MEDIUM_XX_SECONDARY,
                  )
                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text(LocalizationUtils.getSingleValueString("modal", "modal.all.update")),
                onPressed: () async {
                  if (_phoneNumberController.text != null && _phoneNumberController.text != "") {
                    bool result = await model.updateAboutYou(phone: _phoneNumberController.text);
                    if (result == true) {
                      onResendPressed(context, model);
                      setState(() {
                        phoneNumber = int.parse(_phoneNumberController.text);
                      });
                    } else {
                      onFail(context, errMessage: model.errorMessage);
                    }
                    Navigator.of(context).pop();
                  }
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<VerificationModel>(
      builder: (context, VerificationModel model, child) {
        innerModel = model;
        return FutureBuilder(
            future: myFutureMethodOverall(model),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      },
    );
  }

  Future myFutureMethodOverall(VerificationModel verificationModel) async {
    Future<Map<String, dynamic>> future1 = verificationModel.getLocalization(["forgot", "signup"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(VerificationModel model) {
    return Scaffold(
      appBar: SecondaryAppBar(),
      bottomNavigationBar: Builder(
        builder: (BuildContext context) => super.getBottomWidget(context),
      ),
      body: Builder(
        builder: (BuildContext context) {
          innerContext = context;
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            child: Builder(
              builder: (BuildContext context) {
                return GestureDetector(
                  onTap: () {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                  },
                  child: SingleChildScrollView(
                    child: Form(
                      key: formKey,
                      child: Column(
                        children: <Widget>[
                          PPUIHelper.verticalSpaceLarge(),
                          PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("signup", "signup.otp.title-with-password")),
                          //PPUIHelper.verticalSpaceSmall(),
                          PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("signup", "signup.otp.verification-description-signup"), isBold: false),
                          PPUIHelper.verticalSpaceSmall(),
                          PPTexts.getDescription(
                            LocalizationUtils.getSingleValueString("signup", "signup.otp.sent-to") + "  " + phoneNumber.toString(),
                            isBold: true,
                            color: secondaryColor,
                            isExpanded: false,
                            lineHeight: 1,
                            child: TransparentButton(
                              text: LocalizationUtils.getSingleValueString("signup", "signup.transfer.selected-edit"),
                              bgColor: Colors.white,
                              onPressed: () {
                                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.edit_phone_click);
                                if (widget.source == BaseStepperSource.ABOUT_YOU_VERIFICATION_LATER_SCREEN) {
                                  _displayDialog(context, model);
                                } else
                                  Navigator.pop(context);
                              },
                            ),
                          ),
                          PPUIHelper.verticalSpaceMedium(),
                          PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("signup", "signup.fields.otp-label"), isBold: true),
                          PPUIHelper.verticalSpaceSmall(),
                          PinView(
                            count: 4, // count of the fields, excluding dashes
                            autoFocusFirstField: false,
                            submit: (String otpVal) {
                              submitOtp(otpVal);
                            },
                            // describes the dash positions (not indexes)
                          ),
                          PPUIHelper.verticalSpaceMedium(),
                          model.state == ViewState.Busy ? ViewConstants.progressIndicator : Row(),
                          PPUIHelper.verticalSpaceLarge()
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }

  VoidCallback onResendPressed(BuildContext context, VerificationModel model) {
    return () async {
      await model.resendOtp(widget.phoneNo.toString(), 'sms', true);
      start = 30;
      this.startTimer();
    };
  }

  VoidCallback onGetCallPressed(BuildContext context, VerificationModel model) {
    return () async {
      await model.resendOtp(widget.phoneNo.toString(), 'call', true);
      start = 30;
      this.startTimer();
    };
  }

  @override
  onPressed(BuildContext context, VerificationModel verficationModel, String otp) async {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    var connectivityResult = await verficationModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: verficationModel.noInternetConnection);
      return;
    }

    if (otp != null) {
      var success = await verficationModel.getVerifyRegistrationFlow(widget.phoneNo.toString(), otp, dataStore.getUserId());
      if (success) {
        stopTimer = true;
        try {
          Provider.of<DashboardModel>(context).clearUserPatientList();
        } catch (ex) {
          Crashlytics.instance.log(ex.toString());
        }
        Provider.of<SignUpModel>(context).handleSignupSuccess(context);
        if (widget.source == BaseStepperSource.ABOUT_YOU_SCREEN) {
          dataStore.writeBoolean(DataStoreService.VERIFIED, true);
          Navigator.of(context).pushNamedAndRemoveUntil(SignupStepper.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(position: SignupStepperStateEnums.SIGN_UP_ALMOST_DONE));
        } else if (widget.source == BaseStepperSource.ABOUT_YOU_VERIFICATION_LATER_SCREEN) {
          dataStore.writeBoolean(DataStoreService.VERIFIED, true);
          Navigator.pop(context, true);
        } else {
          Navigator.of(context).pushNamedAndRemoveUntil(SignupStepper.routeName, (Route<dynamic> route) => false,
              arguments: SignupStepperArguments(position: SignupStepperStateEnums.SIGN_UP_ABOUT_YOU));
        }
      } else
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_otp_verification_failed);
      onFail(context, errMessage: verficationModel.errorMessage);
    }
  }
}
