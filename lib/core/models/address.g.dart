// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) {
  return Address(
    province: json['province'] as String,
    city: json['city'] as String,
    country: json['country'] as String,
    nickname: json['nickname'] as String,
    postalCode: json['postalCode'] as String,
    streetAddress: json['streetAddress'] as String,
    streetAddressLineTwo: json['streetAddressLineTwo'] as String,
    disabled: json['disabled'] as bool,
    id: json['id'] as int,
    patientId: json['patientId'] as int,
    isDefault: json['isDefault'] as bool,
  );
}

Map<String, dynamic> _$AddressToJson(Address instance) {
  final val = <String, dynamic>{
    'nickname': instance.nickname,
    'province': instance.province,
    'postalCode': instance.postalCode,
    'streetAddress': instance.streetAddress,
    'streetAddressLineTwo': instance.streetAddressLineTwo,
    'city': instance.city,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('country', instance.country);
  writeNotNull('disabled', instance.disabled);
  writeNotNull('id', instance.id);
  val['isDefault'] = instance.isDefault;
  writeNotNull('patientId', instance.patientId);
  return val;
}
