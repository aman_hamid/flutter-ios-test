import 'package:permission_handler/permission_handler.dart';

class PermissionUtils {
  Future<bool> requestPermission(PermissionGroup permissionGroup) async {
    PermissionStatus _permissionStatus = PermissionStatus.unknown;

    final List<PermissionGroup> permissions = <PermissionGroup>[permissionGroup];
    final Map<PermissionGroup, PermissionStatus> permissionRequestResult =
        await PermissionHandler().requestPermissions(permissions);

    _permissionStatus = permissionRequestResult[permissionGroup];

    if (_permissionStatus == PermissionStatus.granted) {
      return true;
    } else {
      return false;
    }
  }

  Future<PermissionStatus> requestPermissionWithStatus(PermissionGroup permissionGroup) async {
    PermissionStatus _permissionStatus = PermissionStatus.unknown;

    final List<PermissionGroup> permissions = <PermissionGroup>[permissionGroup];
    final Map<PermissionGroup, PermissionStatus> permissionRequestResult =
        await PermissionHandler().requestPermissions(permissions);

    _permissionStatus = permissionRequestResult[permissionGroup];

    return _permissionStatus;
  }
}
