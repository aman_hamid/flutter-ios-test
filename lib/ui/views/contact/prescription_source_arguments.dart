import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class PrescriptionSourceArguments {
  final BaseStepperSource source;
  final SuccessDetails successDetails;

  PrescriptionSourceArguments({this.source, this.successDetails});
}
