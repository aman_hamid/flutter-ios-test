// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DashboardItem _$DashboardItemFromJson(Map<String, dynamic> json) {
  return DashboardItem(
    json['title'] as String,
    json['description'] as String,
    json['action'] as String,
    json['cardType'] as String,
    json['icon'] as String,
    json['tagName'] as String,
    json['image'] as String,
    json['itemId'] as String,
    json['id'] as int,
    json['trackActionComplete'] as bool,
    json['colorSchema'] == null
        ? null
        : ColorSchema.fromJson(json['colorSchema'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DashboardItemToJson(DashboardItem instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'action': instance.action,
      'cardType': instance.cardType,
      'icon': instance.icon,
      'tagName': instance.tagName,
      'image': instance.image,
      'itemId': instance.itemId,
      'id': instance.id,
      'trackActionComplete': instance.trackActionComplete,
      'colorSchema': instance.colorSchema,
    };
