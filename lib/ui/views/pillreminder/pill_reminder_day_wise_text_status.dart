import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class PillReminderDayWiseTextStatus extends StatelessWidget {
  final PocketPackDetails pocketPackDetail;

  PillReminderDayWiseTextStatus(this.pocketPackDetail);

  @override
  Widget build(BuildContext context) {
    switch (pocketPackDetail.getPillStatusType()) {
      case PillStatusType.TAKEN:
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(REGULAR_XX)),
            color: whiteColor,
          ),
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: SMALL_XX),
              child: Text(
                LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.taken"),
                style: MEDIUM_X_GREEN_BOLD,
              )),
        );
        break;
      case PillStatusType.MISSED:
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(REGULAR_XX)),
            color: whiteColor,
          ),
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: MEDIUM_XXX, vertical: SMALL_XX),
              child: Text(
                LocalizationUtils.getSingleValueString("app-dashboard", "app-dashboard.label.missed"),
                style: MEDIUM_X_SECONDARY_BOLD,
              )),
        );
        break;
      case PillStatusType.FUTURE:
      case PillStatusType.NONE:
        return PPContainer.emptyContainer();
        break;
    }
  }
}
