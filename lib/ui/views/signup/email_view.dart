import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_email_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class EmailWidget extends StatefulWidget {
  static const routeName = 'email';

  final Function onSuccess;
  final Function onBack;

  EmailWidget({Key key, this.onSuccess, this.onBack});

  @override
  State<StatefulWidget> createState() {
    return EmailState();
  }
}

class EmailState extends BaseState<EmailWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();
  static const emailHeading = "You're Almost Done";
  static const emailDescription =
      "We send important updates regarding your order status via email. Please enter your email address and click the link sent to you for verification";

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _invitationCodeController = TextEditingController();
  bool autovalidate = false;
  bool showReferral = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => SignUpEmailModel(),
      child: Consumer<SignUpEmailModel>(builder: (BuildContext context, SignUpEmailModel model, Widget child) {
        return FutureBuilder(
            future: model.getLocalization(["signup"]),
            builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Widget getMainView(SignUpEmailModel model) {
    return Scaffold(
      bottomNavigationBar: Builder(
          builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                child: model.state == ViewState.Busy
                    ? ViewConstants.progressIndicator
                    : PrimaryButton(
                        fullWidth: true,
                        text: LocalizationUtils.getSingleValueString("common", "common.button.next"),
                        onPressed: onEmailClick(context, model),
                      ),
              )),
      body: Builder(
        builder: (BuildContext context) => SafeArea(
          child: Container(
            child: Builder(
              builder: (BuildContext context) {
                return GestureDetector(
                    onTap: () {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                    },
                    child: SingleChildScrollView(
                      padding: EdgeInsets.all(16),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            PPUIHelper.verticalSpaceLarge(),
                            PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("signup", "signup.fields.you-are-almostdone")),
                            //PPUIHelper.verticalSpaceSmall(),
                            PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("signup", "signup.almostdone.email-description"), isBold: false),
                            PPUIHelper.verticalSpaceLarge(),
                            PPFormFields.getTextField(
                              autovalidate: autovalidate,
                              keyboardType: TextInputType.emailAddress,
                              controller: _emailController,
                              decoration: PPInputDecor.getDecoration(
                                  labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.email-label"),
                                  hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-email")),
                            ),
                            PPUIHelper.verticalSpaceLarge(),
                            Container(
                              decoration: BoxDecoration(color: Color(0xfff7f7f7), borderRadius: BorderRadius.circular(4.0)),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.all(12.0),
                                      child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              showReferral == false ? showReferral = true : showReferral = false;
                                            });
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                LocalizationUtils.getSingleValueString("signup", "signup.fields.have-referral-code"),
                                                style: TextStyle(fontWeight: FontWeight.w600),
                                              ),
                                              new Expanded(
                                                  child: Align(
                                                alignment: Alignment.topRight,
                                                child: InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      showReferral == false ? showReferral = true : showReferral = false;
                                                    });
                                                  },
                                                  child: Image.asset(showReferral == false ? 'graphics/icons/ic_round_arrow_down.png' : 'graphics/icons/ic_round_arrow_up.png',
                                                      width: 24.0, height: 24.0),
                                                ),
                                              )),
                                            ],
                                          ))),
                                  getReferralCodeView()
                                ],
                              ),
                            ),
                            PPUIHelper.verticalSpaceLarge(),
                          ],
                        ),
                      ),
                    ));
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget getReferralCodeView() {
    if (showReferral == true) {
      return AnimatedContainer(
        duration: Duration(seconds: 2),
        child: Column(
          children: <Widget>[
            PPDivider(),
            PPUIHelper.verticalSpaceLarge(),
            Padding(
                padding: EdgeInsets.all(12.0),
                child: PPFormFields.getTextField(
                    controller: _invitationCodeController,
                    decoration: PPInputDecor.getDecoration(
                        labelText: LocalizationUtils.getSingleValueString("signup", "signup.fields.referral-optional"),
                        hintText: LocalizationUtils.getSingleValueString("signup", "signup.fields.enter-referral-code")),
                    validator: (value) {
                      return null;
                    }))
          ],
        ),
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }

  onEmailClick(BuildContext context, SignUpEmailModel model) {
    return () async {
      if (_formKey.currentState.validate()) {
        if (_invitationCodeController.text != null && _invitationCodeController.text.length > 0) {
          var inviteRes = await model.updateInvitationCode(_invitationCodeController.text);
          if (inviteRes == true) {
            showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("signup", "signup.fields.referral-code-applied"));
            Future.delayed(const Duration(milliseconds: 1500), () {
              afterHandlingInvitationCode(context, model);
            });
          } else
            onFail(context, errMessage: model.errorMessage);
        } else
          afterHandlingInvitationCode(context, model);
      } else {
        setState(() {
          autovalidate = true;
        });
      }
    };
  }

  afterHandlingInvitationCode(BuildContext context, SignUpEmailModel model) async {
    var success = await model.updateEmail(_emailController.text);
    if (success) {
      if (widget.onSuccess != null) widget.onSuccess();
    } else
      onFail(context, errMessage: model.errorMessage);
  }
}
