import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pocketpills/core/response/pillreminder/day_wise_medications.dart';
import 'package:pocketpills/core/response/pillreminder/pill_status_enum.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/res/colors.dart';

const int _DefaultDashes = 0;
const Color _DefaultColor = brandColor;
const double _DefaultGapSize = 6;
const double _DefaultStrokeWidth = 1;

class DashedCircle extends StatelessWidget {
  final double gapSize;
  final double strokeWidth;
  final Widget child;
  final bool dateSelected;
  final DayWiseMedications dayWiseMedications;

  DashedCircle(
      {this.child,
      this.gapSize = _DefaultGapSize,
      this.strokeWidth = _DefaultStrokeWidth,
      this.dateSelected = false,
      this.dayWiseMedications});

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: DashedCirclePainter(
          gapSize: gapSize,
          strokeWidth: strokeWidth,
          dateSelected: dateSelected,
          dayWiseMedications: dayWiseMedications,
          dashes: dayWiseMedications.pocketPackDetails != null ? dayWiseMedications.pocketPackDetails.length : 0),
      child: child,
    );
  }
}

class DashedCirclePainter extends CustomPainter {
  final double gapSize;
  final int dashes;
  final double strokeWidth;
  final bool dateSelected;
  Color color = _DefaultColor;
  final DayWiseMedications dayWiseMedications;

  DashedCirclePainter(
      {this.dashes = _DefaultDashes,
      this.gapSize = _DefaultGapSize,
      this.strokeWidth = _DefaultStrokeWidth,
      this.dateSelected = false,
      this.dayWiseMedications});

  Color getDashColor(PocketPackDetails pocketPackDetail) {
    if (dateSelected) {
      switch (pocketPackDetail.getPillStatusType()) {
        case PillStatusType.FUTURE:
          return whiteColor.withOpacity(0.7);
          break;
        case PillStatusType.TAKEN:
          return whiteColor;
          break;
        case PillStatusType.MISSED:
          return whiteColor.withOpacity(0.5);
          break;
        case PillStatusType.NONE:
          return whiteColor.withOpacity(0.7);
          break;
        default:
          return whiteColor.withOpacity(0.7);
          break;
      }
    } else {
      switch (pocketPackDetail.getPillStatusType()) {
        case PillStatusType.FUTURE:
          return greyColor;
          break;
        case PillStatusType.TAKEN:
          return brandColor;
          break;
        case PillStatusType.MISSED:
          return greyColor;
          break;
        case PillStatusType.NONE:
          return greyColor;
          break;
        default:
          return greyColor;
          break;
      }
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    final double gap = pi / 180 * gapSize;
    final double singleAngle = (pi * 2) / dashes;

    for (int i = 0; i < dashes; i++) {
      final Paint paint = Paint()
        ..color = getDashColor(dayWiseMedications.pocketPackDetails[i])
        ..strokeWidth = _DefaultStrokeWidth
        ..strokeCap = StrokeCap.round
        ..style = PaintingStyle.stroke;

      canvas.drawArc(Offset.zero & size, gap + singleAngle * i, singleAngle - gap * 2, false, paint);
    }
  }

  @override
  bool shouldRepaint(DashedCirclePainter oldDelegate) {
    return dashes != oldDelegate.dashes || color != oldDelegate.color;
  }
}
