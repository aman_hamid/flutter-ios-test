import 'package:pocketpills/core/request/base_request.dart';

class UpdatePillReminderStatusRequest extends BaseRequest {
  String status;
  int day;
  int month;
  int year;
  List<int> pocketPackIds;

  UpdatePillReminderStatusRequest({this.status, this.day, this.month, this.year, this.pocketPackIds});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'status': this.status,
        'day': this.day,
        'month': this.month,
        'year': this.year,
        'pocketPackIds': this.pocketPackIds,
      };
}
