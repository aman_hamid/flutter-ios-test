// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_email_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmailUpdateRequest _$EmailUpdateRequestFromJson(Map<String, dynamic> json) {
  return EmailUpdateRequest(
    email: json['email'] as String,
  );
}

Map<String, dynamic> _$EmailUpdateRequestToJson(EmailUpdateRequest instance) =>
    <String, dynamic>{
      'email': instance.email,
    };
