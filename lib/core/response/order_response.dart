import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/address.dart';
import 'package:pocketpills/core/models/order_item.dart';
import 'package:pocketpills/core/models/order_payment.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/models/shipment.dart';
import 'package:pocketpills/core/models/transaction.dart';
import 'package:pocketpills/core/models/user.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

import 'order_document.dart';

part 'order_response.g.dart';

@JsonSerializable()
class OrderResponse {
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime dueDate;
  int id;
  String orderNo;
  int patientId;
  Patient patient;
  User caregiver;
  int caregiverId;
  int pharmacyId;
  String pharmacyCode;
  Address address;
  String status;
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime orderedDateTime;
  List<OrderItem> orderItems;
  List<Shipment> shipments;
  String notes;
  List<Transaction> transactions;
  OrderPayment orderPayment;

  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime createdDateTime;
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime updatedDateTime;
  String createdBy;
  String modifiedBy;
  String consultationBy;
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime consultationOn;
  String consultationStatus;

  OrderDocument orderReceiptDocument;

  OrderResponse(
      {this.orderPayment,
      this.status,
      this.patientId,
      this.patient,
      this.pharmacyCode,
      this.address,
      this.caregiver,
      this.caregiverId,
      this.consultationBy,
      this.consultationOn,
      this.consultationStatus,
      this.createdBy,
      this.createdDateTime,
      this.dueDate,
      this.id,
      this.modifiedBy,
      this.notes,
      this.orderedDateTime,
      this.orderItems,
      this.orderNo,
      this.pharmacyId,
      this.shipments,
      this.transactions,
      this.updatedDateTime,
      this.orderReceiptDocument});

  factory OrderResponse.fromJson(Map<String, dynamic> json) =>
      _$OrderResponseFromJson(json);

  Map<String, dynamic> toJson() => _$OrderResponseToJson(this);
}
