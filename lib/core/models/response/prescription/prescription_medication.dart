import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'prescription_medication.g.dart';

@JsonSerializable()
class PrescriptionMedication {
  @JsonKey(name: "rxId")
  int medicationId;
  @JsonKey(name: "drug")
  String medicationName;
  num remainingQty;
  num filledQty;
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime validTill;
  String doctorFirstName;
  String doctorLastName;

  factory PrescriptionMedication.fromJson(Map<String, dynamic> json) =>
      _$PrescriptionMedicationFromJson(json);

  Map<String, dynamic> toJson() => _$PrescriptionMedicationToJson(this);

  PrescriptionMedication(
      this.medicationId,
      this.medicationName,
      this.remainingQty,
      this.filledQty,
      this.validTill,
      this.doctorFirstName,
      this.doctorLastName);
}
