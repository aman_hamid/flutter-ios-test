import 'dart:core';

import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';

part 'telehealth_create_response.g.dart';

@JsonSerializable()
class TelehealthCreateResponse {
  int omsPrescriptionId;
  SuccessDetails successDetails;

  TelehealthCreateResponse(this.omsPrescriptionId, this.successDetails);

  factory TelehealthCreateResponse.fromJson(Map<String, dynamic> json) =>
      _$TelehealthCreateResponseFromJson(json);
}
