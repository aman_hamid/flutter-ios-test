// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signedin_verify_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignedinVerifyResponse _$SignedinVerifyResponseFromJson(
    Map<String, dynamic> json) {
  return SignedinVerifyResponse(
    resetKey: json['resetKey'] as String,
    isPasswordSet: json['isPasswordSet'] as bool,
    setPasswordToken: json['setPasswordToken'] as String,
  );
}

Map<String, dynamic> _$SignedinVerifyResponseToJson(
        SignedinVerifyResponse instance) =>
    <String, dynamic>{
      'resetKey': instance.resetKey,
      'isPasswordSet': instance.isPasswordSet,
      'setPasswordToken': instance.setPasswordToken,
    };
