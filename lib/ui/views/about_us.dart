import 'dart:io';
import 'package:flutter/material.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:package_info/package_info.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:launch_review/launch_review.dart';

class AboutUsWidget extends StatefulWidget {
  static const routeName = 'aboutus';

  @override
  _AboutUsWidgetState createState() => _AboutUsWidgetState();
}

class _AboutUsWidgetState extends BaseState<AboutUsWidget> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  static const routeName = 'aboutus';

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  void initState() {
    super.initState();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_about_us);
    _initPackageInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LocalizationUtils.getSingleValueString("common", "common.label.about-us")),
        brightness: Platform.isIOS == true ? Brightness.light : null,
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: Builder(
            builder: (BuildContext context) {
              return SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    PPUIHelper.verticalSpaceLarge(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          getLogoImage(),
                          height: LARGE_XX,
                        ),
                      ],
                    ),
                    PPUIHelper.verticalSpaceMedium(),
                    PPUIHelper.verticalSpaceSmall(),
                    PPTexts.getDescription(LocalizationUtils.getSingleValueString("common", "common.label.about-pocketpills")),
                    PPUIHelper.verticalSpaceMedium(),
                    TransparentButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.label.version") + ": " + _packageInfo.version,
                      onPressed: () {},
                    ),
                    PPUIHelper.verticalSpaceSmall(),
                    //PPTexts.getDescription("Reach out to us"),
                    PPUIHelper.verticalSpaceSmall(),
                    TransparentButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.label.privacy-policy") + " ",
                      onPressed: () {
                        launch(ViewConstants.PRIVACY_POLICY_URL);
                      },
                    ),
                    TransparentButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.label.terms-conditions") + " ",
                      onPressed: () {
                        launch(ViewConstants.TOC_URL);
                      },
                    ),
                    TransparentButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.label.visit-website") + " ",
                      onPressed: () {
                        launch(ViewConstants.WEBSITE_URL);
                      },
                    ),
                    TransparentButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.label.rate-playstore") + " ",
                      onPressed: () {
                        LaunchReview.launch();
                      },
                    ),
                    TransparentButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.label.contact-pocketpills") + " ",
                      onPressed: () {
                        launch(ViewConstants.SUPPORT_EMAIL_ID);
                      },
                    ),
                    PPTexts.getDescription("\u00a9 " + LocalizationUtils.getSingleValueString("common", "common.label.copyright"), textAlign: TextAlign.center),
                    PPUIHelper.verticalSpaceMedium(),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
