// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'free_medicine_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FreeMedicineResponse _$FreeMedicineResponseFromJson(Map<String, dynamic> json) {
  return FreeMedicineResponse(
    status: json['success'] as bool,
    errMessage: json['userMessage'] as String,
    apiMessage: json['message'] as String,
    medicines: (json['details'] as List)
        ?.map((e) =>
            e == null ? null : Medicine.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$FreeMedicineResponseToJson(
        FreeMedicineResponse instance) =>
    <String, dynamic>{
      'success': instance.status,
      'userMessage': instance.errMessage,
      'message': instance.apiMessage,
      'details': instance.medicines,
    };
