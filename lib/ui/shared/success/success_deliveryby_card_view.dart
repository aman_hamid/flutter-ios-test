import 'package:flutter/material.dart';
import 'package:pocketpills/core/response/signup/transaction_success_field.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';

class SuccessDeliveryByCardView extends BaseStatelessWidget {
  final UserContactModel userContactModel;
  final TransactionSuccessField field;

  SuccessDeliveryByCardView(this.field, this.userContactModel);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: MEDIUM_X),
      child: Container(
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                color: bghighlight2,
                width: MEDIUM,
              ),
              SizedBox(
                width: MEDIUM_X,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: SMALL_XXX,
                    ),
                    Text(
                      field.title ?? "",
                      style: MEDIUM_XXX_PRIMARY_BOLD,
                    ),
                    SizedBox(
                      height: SMALL,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: MEDIUM_XXX),
                      child: Text(
                        field.description ?? "",
                        style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 4,
                      ),
                    ),
                    SizedBox(
                      height: MEDIUM_X,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 36,
                      child: PPFormFields.getDropDown(
                        fullWidth: true,
                        value: userContactModel.deliveryBy,
                        onChanged: (String newValue) async {
                          userContactModel.setDeliveryTime(newValue);
                        },
                        items: ViewConstants.deliveryTimeMap
                            .map((String key, String value) {
                              return MapEntry(
                                  key,
                                  DropdownMenuItem<String>(
                                    value: key,
                                    child: Text(value),
                                  ));
                            })
                            .values
                            .toList(),
                      ),
                    ),
                    SizedBox(
                      height: SMALL_XXX,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
