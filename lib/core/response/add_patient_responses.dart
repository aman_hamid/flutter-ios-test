import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/user_patient.dart';
part 'add_patient_responses.g.dart';


@JsonSerializable()
class AddPatientResponse{
   @JsonKey(name:'patientId')
   int patientId;

   @JsonKey(name : 'patientList')
   List<UserPatient> patientList;

   AddPatientResponse(this.patientId,this.patientList);

   factory AddPatientResponse.fromJson(Map<String, dynamic> json) =>
       _$AddPatientResponseFromJson(json);

}