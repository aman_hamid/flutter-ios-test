import 'package:json_annotation/json_annotation.dart';
part 'coupon_code.g.dart';

@JsonSerializable()
class CouponCode {
  int id;
  bool disabled;
  String userSuppliedCouponCode;
  bool isApplied;
  String reason;
  bool applied;

  CouponCode(this.id, this.disabled, this.userSuppliedCouponCode, this.isApplied, this.reason, this.applied);

  factory CouponCode.fromJson(Map<String, dynamic> json) => _$CouponCodeFromJson(json);
}
