import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class SourceArguments {
  final BaseStepperSource source;
  SourceArguments({this.source});
}
