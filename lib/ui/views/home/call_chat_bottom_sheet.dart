import 'package:flutter/material.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/chat_view.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/call_dialog.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/utils/analytics_event_constant.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:url_launcher/url_launcher.dart';

class CallChatBottomSheet extends StatefulWidget {
  BuildContext context;

  CallChatBottomSheet({this.context});

  _CallChatBottomSheetState createState() => _CallChatBottomSheetState();
}

class _CallChatBottomSheetState extends BaseState<CallChatBottomSheet> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext bc) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(MEDIUM_X),
            child: Text(
              LocalizationUtils.getSingleValueString("modal", "modal.customercare.title"),
              style: MEDIUM_XXX_PRIMARY_BOLD,
            ),
          ),
          Divider(
            color: secondaryColor,
            height: 1,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: SMALL_XXX),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                FlatButton.icon(
                  icon: Icon(
                    Icons.call,
                  ),
                  //`Icon` to display
                  label: Padding(
                    padding: const EdgeInsets.only(bottom: SMALL_XXX),
                    child: Text(
                      LocalizationUtils.getSingleValueString("common", "common.customercare.call") + " 1-833-HELLO-RX ",
                      style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
                    ),
                  ),
                  //`Text` to display
                  onPressed: () {
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_with_expert_call);
                    launch("tel://" + ViewConstants.PHARMACY_PHONE.toString());
                    Navigator.of(widget.context).pop();
                  },
                ),
                FlatButton.icon(
                  icon: Icon(
                    Icons.message,
                  ),
                  //`Icon` to display
                  label: Padding(
                    padding: const EdgeInsets.only(bottom: SMALL_XXX),
                    child: Text(
                      LocalizationUtils.getSingleValueString("common", "common.customercare.chat"),
                      style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
                    ),
                  ),
                  //`Text` to display
                  onPressed: () {
                    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_vitamins_with_expert_chat);
                    Navigator.pushNamed(context, ChatWidget.routeName);
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
