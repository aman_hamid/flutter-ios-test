import 'package:enum_to_string/enum_to_string.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';

part 'transaction_success_field.g.dart';

@JsonSerializable()
class TransactionSuccessField {
  String cardType;
  String title;
  String image;
  String description;
  String action;

  TransactionSuccessField(this.cardType, this.title, this.image, this.description, this.action);

  TransactionSuccessCardType getCardType() {
    return EnumToString.fromString(TransactionSuccessCardType.values, cardType);
  }

  TransactionSuccessActionType getActionType() {
    return EnumToString.fromString(TransactionSuccessActionType.values, action);
  }

  factory TransactionSuccessField.fromJson(Map<String, dynamic> json) => _$TransactionSuccessFieldFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionSuccessFieldToJson(this);
}
