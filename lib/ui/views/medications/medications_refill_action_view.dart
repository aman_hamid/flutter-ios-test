import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/response/medication.dart';
import 'package:pocketpills/core/utils/utils.dart';
import 'package:pocketpills/core/viewmodels/medications_model.dart';
import 'package:pocketpills/core/viewmodels/order_model.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_input_list_tile.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class MedicationsRefillAction extends BaseStatelessWidget {
  static const routeName = 'medicationsrefillaction';

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [],
      child: Consumer<MedicationsModel>(
        builder: (BuildContext context, MedicationsModel medicationsModel, Widget child) {
          List<Medication> medications = medicationsModel.getMarkedMedications();
          return FutureBuilder(
              future: myFutureMethodOverall(medicationsModel),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getMainView(medicationsModel, medications);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        },
      ),
    );
  }

  Future myFutureMethodOverall(MedicationsModel model) async {
    Future<Map<String, dynamic>> future1 = model.getLocalization(["common", "medications", "refill"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(MedicationsModel medicationsModel, List<Medication> medications) {
    return Scaffold(
      bottomNavigationBar: Builder(
        builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
          child: medicationsModel.state == ViewState.Busy
              ? ViewConstants.progressIndicator
              : Row(
                  children: [
                    SecondaryButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.button.back"),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
                    PrimaryButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.button.refill"),
                      onPressed: onRefillClick(context, medicationsModel),
                    ),
                  ],
                ),
        ),
      ),
      appBar: InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString("refill", "refill.labels.medications"),
        appBar: AppBar(),
      ),
      body: Builder(
        builder: (BuildContext context) => Container(
          child: Builder(
            builder: (BuildContext context) {
              List<Widget> medicationRefillActionChildren = [];
              medicationRefillActionChildren.add(
                PPTexts.getSectionHeader(
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(LocalizationUtils.getSingleValueString("refill", "refill.labels.medications"), style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor)),
                        Text(LocalizationUtils.getSingleValueString("refill", "refill.labels.quantity"), style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor))
                      ],
                    ),
                  ),
                ),
              );
              medicationRefillActionChildren.add(PPUIHelper.verticalSpaceMedium());
              medications.forEach((medication) => medicationRefillActionChildren.add(getMedicationCard(medicationsModel, medication)));
              if (medicationsModel.isOrderedQuantityMore()) {
                medicationRefillActionChildren.add(PPTexts.getSectionHeader(
                    addPadding: true,
                    child: Container(
                        padding: EdgeInsets.symmetric(vertical: PPUIHelper.VerticalSpaceSmall),
                        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                          Icon(Icons.error, size: 16, color: warningColor),
                          SizedBox(
                            width: PPUIHelper.HorizontalSpaceSmall,
                          ),
                          Expanded(
                            flex: 5,
                            child: Column(
                              children: <Widget>[
                                PPTexts.getHeading(LocalizationUtils.getSingleValueString("refill", "refill.all.error-more"), justText: true, textOverflow: TextOverflow.ellipsis),
                                PPTexts.getDescription(LocalizationUtils.getSingleValueString("refill", "refill.all.warning-more"))
                              ],
                            ),
                          ),
                        ]))));
              }
              return SingleChildScrollView(
                child: Column(children: medicationRefillActionChildren),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget getMedicationCard(MedicationsModel medicationsModel, Medication medication) {
    TextEditingController textEditingController = medicationsModel.getControllerForMedication(medication.id, medication);
    Widget card = new Container(
      child: PPInputListTile.getInputListTile(PPTexts.getHeading(medication.drug), getQuantityLeftWidget(medicationsModel, medication), textEditingController),
    );
    return card;
  }

  getQuantityLeftWidget(MedicationsModel medicationsModel, Medication medication) {
    try {
      if (medication.dgType != 'O' && double.parse(medicationsModel.getSelectedQuantityForMedication(medication.id, medication)) > medication.quantityLeft) {
        return PPTexts.getDescription(
            LocalizationUtils.getSingleValueString("refill", "refill.labels.quantity-left") + ': ' + Utils.removeDecimalZeroFormat(medication.quantityLeft),
            color: warningColor,
            isChildLeading: true,
            child: Icon(Icons.error, size: 16, color: warningColor));
      }
      return PPTexts.getDescription(medication.dgType == 'O'
          ? LocalizationUtils.getSingleValueString("refill", "refill.labels.unlimited") + ' ' + LocalizationUtils.getSingleValueString("refill", "refill.labels.quantity")
          : LocalizationUtils.getSingleValueString("refill", "refill.labels.quantity-left") + ': ' + Utils.removeDecimalZeroFormat(medication.quantityLeft));
    } catch (e) {
      print(e);
      if (medication.quantityLeft != null)
        return PPTexts.getDescription(medication.dgType == 'O'
            ? LocalizationUtils.getSingleValueString("refill", "refill.labels.unlimited") + ' ' + LocalizationUtils.getSingleValueString("refill", "refill.labels.quantity")
            : LocalizationUtils.getSingleValueString("refill", "refill.labels.quantity-left") + ': ' + Utils.removeDecimalZeroFormat(medication.quantityLeft));
      else
        return PPTexts.getDescription(
            LocalizationUtils.getSingleValueString("refill", "refill.labels.unlimited") + ' ' + LocalizationUtils.getSingleValueString("refill", "refill.labels.quantity"));
    }
  }

  onRefillClick(BuildContext context, MedicationsModel medicationsModel) {
    return () async {
      bool result = await medicationsModel.refillMedications();
      if (result == true) {
        Provider.of<OrderModel>(context).clearData();
        Navigator.of(context).pushNamedAndRemoveUntil(UserContactWidget.routeName, (Route<dynamic> route) => false,
            arguments: PrescriptionSourceArguments(source: BaseStepperSource.REFILL_SCREEN, successDetails: medicationsModel.successDetails));
      } else
        onFail(context, errMessage: medicationsModel.errorMessage);
    };
  }
}
