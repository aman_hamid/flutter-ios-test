import 'package:pocketpills/core/request/base_request.dart';

class PPDistinctEntityRequest extends BaseRequest {
  String utmMedium;
  String utmSource;
  String utmCampaign;
  String utmContent;
  String utmTerm;
  String idfa;
  String advertisingId;
  String gclid;
  String mixpanelDistinctId;

  PPDistinctEntityRequest(
      {this.utmMedium,
      this.utmSource,
      this.utmCampaign,
      this.utmContent,
      this.utmTerm,
      this.idfa,
      this.advertisingId,
      this.gclid,
      this.mixpanelDistinctId});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'utmMedium': this.utmMedium,
        'utmSource': this.utmSource,
        'utmCampaign': this.utmCampaign,
        'utmContent': this.utmContent,
        'utmTerm': this.utmTerm,
        'idfa': this.idfa,
        'advertisingId': this.advertisingId,
        'gclid': this.gclid,
        'mixpanelDistinctId': this.mixpanelDistinctId,
      };
}
