import 'package:json_annotation/json_annotation.dart';
part 'coupon.g.dart';

@JsonSerializable()
class Coupon {
  String mappedCouponCode;
  String title;
  String description;
  bool active;

  Coupon(this.mappedCouponCode, this.title, this.description, this.active);

  factory Coupon.fromJson(Map<String, dynamic> json) => _$CouponFromJson(json);
}
