import 'package:pocketpills/core/models/medicine.dart';

class CopayRequestArguments {
  Medicine medicine;
  int quantity;
  CopayRequestArguments({this.medicine, this.quantity});
}
