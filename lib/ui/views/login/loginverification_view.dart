import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/response/signedin_verify_response.dart';
import 'package:pocketpills/core/viewmodels/signup/verification_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/login/resetpassword_view.dart';
import 'package:pocketpills/ui/views/login/setpassword_view.dart';
import 'package:pocketpills/ui/views/signup/verification_state.dart';
import 'package:pocketpills/ui/shared/pin_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class LoginVerificationWidget extends StatefulWidget {
  static const routeName = 'loginverification';
  final int phoneNo;

  LoginVerificationWidget({
    Key key,
    @required this.phoneNo,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginVerificationState();
  }
}

class LoginVerificationState extends VerificationState<LoginVerificationWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<VerificationModel>(
      builder: (context, VerificationModel model, child) {
        innerModel = model;
        return FutureBuilder(
            future: myFutureMethodOverall(model),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      },
    );
  }

  Future myFutureMethodOverall(VerificationModel verificationModel) async {
    Future<Map<String, dynamic>> future1 = verificationModel.getLocalization(["login", "forgot"]); // will take 3 secs
    return await Future.wait([future1]);
  }

  Widget getMainView(VerificationModel model) {
    return Scaffold(
      bottomNavigationBar: Builder(
        builder: (BuildContext context) => super.getBottomWidget(context),
      ),
      body: Builder(
        builder: (BuildContext context) {
          innerContext = context;
          return SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: Builder(
                builder: (BuildContext context) {
                  return GestureDetector(
                    onTap: () {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                    },
                    child: SingleChildScrollView(
                      child: Form(
                        key: formKey,
                        child: Column(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: LARGE_XX,
                                ),
                                Image.asset(
                                  getLogoImage(),
                                  height: LARGE_XX,
                                ),
                                SizedBox(
                                  height: LARGE_XX,
                                ),
                                Text(
                                  LocalizationUtils.getSingleValueString("forgot", "forgot.all.otp-title"),
                                  style: REGULAR_XXX_PRIMARY_BOLD,
                                ),
                                SizedBox(
                                  height: SMALL_XX,
                                ),
                                Text(
                                  LocalizationUtils.getSingleValueString("forgot", "forgot.all.otp-description").replaceAll("{{phone}}", widget.phoneNo.toString()),
                                  style: MEDIUM_XXX_SECONDARY,
                                ),
                                SizedBox(
                                  height: LARGE,
                                ),
                                Text(
                                  LocalizationUtils.getSingleValueString("forgot", "forgot.all.otp-title"),
                                  style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                                ),
                              ],
                            ),
                            PPUIHelper.verticalSpaceSmall(),
                            PinView(
                              count: 4, // count of the fields, excluding dashes
                              autoFocusFirstField: false,
                              submit: (String otpVal) {
                                submitOtp(otpVal);
                              },
                              // describes the dash positions (not indexes)
                            ),
                            PPUIHelper.verticalSpaceMedium(),
                            model.state == ViewState.Busy ? ViewConstants.progressIndicator : Row(),
                            PPUIHelper.verticalSpaceLarge(),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  VoidCallback onResendPressed(BuildContext context, model) {
    return () async {
      await model.resendOtp(widget.phoneNo.toString(), 'sms', false);
      start = 30;
      this.startTimer();
    };
  }

  @override
  VoidCallback onGetCallPressed(BuildContext context, model) {
    return () async {
      await model.resendOtp(widget.phoneNo.toString(), 'call', false);
      start = 30;
      this.startTimer();
    };
  }

  @override
  onPressed(BuildContext context, VerificationModel verificationModel, String otp) async {
    var connectivityResult = await verificationModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: verificationModel.noInternetConnection);
      return;
    }
    if (otp != null) {
      SignedinVerifyResponse response = await verificationModel.getVerifyLoginFlow(widget.phoneNo.toString(), otp);
      if (response == null)
        onFail(context, errMessage: verificationModel.errorMessage);
      else if (response.isPasswordSet == true) {
        stopTimer = true;
        Navigator.pushReplacementNamed(context, ResetPasswordWidget.routeName);
      } else if (response.isPasswordSet == false) {
        stopTimer = true;
        Navigator.pushReplacementNamed(context, SetPasswordWidget.routeName);
      }
    }
  }
}
