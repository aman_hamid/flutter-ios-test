import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'signup_phone_number_request.g.dart';

@JsonSerializable()

class SignupPhoneNumberRequest extends BaseRequest{
  @JsonKey(name: "phone")
  String phone;

  SignupPhoneNumberRequest({this.phone});

  factory SignupPhoneNumberRequest.fromJson(Map<String, dynamic> json) => _$SignupPhoneNumberRequestFromJson(json);

  Map<String, dynamic> toJson() => _$SignupPhoneNumberRequestToJson(this);

}