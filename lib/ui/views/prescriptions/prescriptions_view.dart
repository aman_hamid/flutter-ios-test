import 'dart:io';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/utils/date_utils.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/prescription_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/empty_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/prescriptions/prescription_detail_view.dart';
import 'package:pocketpills/ui/views/home/reschedule_time_bottom_sheet.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class PrescriptionsWidget extends StatefulWidget {
  static const routeName = 'prescriptions';

  @override
  State<StatefulWidget> createState() {
    return PrescriptionsState();
  }
}

class PrescriptionsState extends BaseState<PrescriptionsWidget> {
  @override
  void initState() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_prescriptions);
    super.initState();
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: MultiProvider(
        providers: [],
        child: Consumer<PrescriptionModel>(
          builder: (BuildContext context, PrescriptionModel prescriptionModel, Widget child) {
            return FutureBuilder(
              future: myFutureMethodOverall(prescriptionModel, context),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                return Scaffold(
                  body: Builder(
                    builder: (BuildContext context) => Container(
                      //margin: const EdgeInsets.fromLTRB(0.0, 24.0, 0.0, 0),
                      child: Builder(builder: (BuildContext context) {
                        if (snapshot.hasData) {
                          return getPrescriptionCards(prescriptionModel);
                        } else if (snapshot.hasError) {
                          Crashlytics.instance.log(snapshot.hasError.toString());
                          return ErrorScreen();
                        }
                        return LoadingScreen();
                      }),
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }

  Future myFutureMethodOverall(PrescriptionModel prescriptionModel, BuildContext context) async {
    Future<List<Prescription>> future1 = prescriptionModel.fetchPrescriptions(Provider.of<DashboardModel>(context).selectedPatientId);
    Future<Map<String, dynamic>> future2 = prescriptionModel.getLocalization(["prescriptions"]);
    return await Future.wait([future1, future2]);
  }

  Widget getPrescriptionCards(PrescriptionModel prescriptionModel) {
    List<Widget> ret = [];
    if (prescriptionModel.prescriptionList.length == 0) {
      return EmptyScreen(
        title: LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.all.empty"),
        description: getDescriptionText(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient)),
        showTransfer: true,
      );
    } else
      for (int i = 0; i < prescriptionModel.prescriptionList.length; i++) {
        if (i == 0) ret.add(PPUIHelper.verticalSpaceLarge());
        ret.add(getPrescriptionCard(prescriptionModel, prescriptionModel.prescriptionList.elementAt(i), i));
        ret.add(PPUIHelper.verticalSpaceMedium());
      }
    return SingleChildScrollView(
      child: Column(
        children: ret,
      ),
    );
  }

  Widget getPrescriptionCard(PrescriptionModel prescriptionModel, Prescription prescription, int index) {
    return PPCard(
      child: Column(
        children: getPrescriptionViewCardBasedOnTransferData(prescription),
      ),
      onTap: () {
        prescriptionModel.setPrescriptionIndex(index);
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_prescriptions_details);
        Navigator.pushNamed(context, PrescriptionDetailWidget.routeName);
      },
    );
  }

  List<Widget> getPrescriptionViewCardBasedOnTransferData(Prescription prescription) {
    if (prescription.exPharmacyName != null && prescription.exPharmacyName.isNotEmpty) {
      return <Widget>[
        getPrescriptionCardBody(prescription),
        Divider(),
        PPTexts.getDescription(LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.labels.transferred") + " " + prescription.exPharmacyName)
        //PPUIHelper.verticalSpaceMedium(),
      ];
    } else if (prescription.medications != null &&
        prescription.medications.length > 0 &&
        prescription.medications.elementAt(0).doctorFirstName != null &&
        prescription.medications.elementAt(0).doctorLastName != null) {
      return <Widget>[
        getPrescriptionCardBody(prescription),
        Divider(),
        PPTexts.getDescription(LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.labels.prescribed") +
            ": " +
            prescription.medications.elementAt(0).doctorFirstName +
            " " +
            prescription.medications.elementAt(0).doctorLastName)

        //PPUIHelper.verticalSpaceMedium(),
      ];
    } else {
      return <Widget>[
        getPrescriptionCardBody(prescription),
        //PPUIHelper.verticalSpaceMedium(),
      ];
    }
  }

  Widget getPrescriptionCardBody(Prescription prescription) {
    String status = ViewConstants.getPrescriptionStatus(prescription.status);
    status = '${status[0].toUpperCase()}${status.substring(1).toLowerCase()}';
    String type = ViewConstants.getPrescriptionType(prescription.type);
    //type = '${type[0].toUpperCase()}${type.substring(1).toLowerCase()}';
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Column(
            children: <Widget>[
              PPTexts.getHeading(type),
              PPTexts.getDescription(PPDateUtils.displayDate(prescription.createDateTime), isBold: true),
              PPTexts.getDescription(LocalizationUtils.getSingleValueString("common", "common.label.prescription-id") + ": " + prescription.id.toString()),
            ],
          ),
        ),
        PPChip(
          label: status,
          color: ViewConstants.getColorForPrescriptionStatus(prescription.status),
        )
      ],
    );
  }

  static Widget getPrescriptionCardHeading(Prescription prescription) {
    String status = ViewConstants.getPrescriptionStatus(prescription.status);
    status = '${status[0].toUpperCase()}${status.substring(1).toLowerCase()}';
    String type = ViewConstants.getPrescriptionType(prescription.type);
    //type = '${type[0].toUpperCase()}${type.substring(1).toLowerCase()}';
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[Expanded(child: PPTexts.getHeading(type))],
    );
  }

  String getDescriptionText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.all.description-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.all.description-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.all.description-other");
      default:
        return LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.all.description");
    }
  }
}
