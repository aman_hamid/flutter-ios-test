import 'package:flutter/material.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/appbars/singup_stepper_appbar.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/success_view.dart';
import 'package:pocketpills/ui/views/vitamins/vitamins_widget.dart';

class SignUpSuccessDetailsWidget extends StatefulWidget {
  static const routeName = 'signupSuccessDetails';

  final BaseStepperSource source;

  SignUpSuccessDetailsWidget({Key key, this.source}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignUpSuccessDetailsState();
  }
}

class SignUpSuccessDetailsState extends BaseState<SignUpSuccessDetailsWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();
  bool transferSkipped = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    transferSkipped = dataStore.readBoolean(DataStoreService.TRANSFER_SKIPPED);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: transferSkipped == false
          ? SignUpStepperAppBar(
              appBar: AppBar(),
              step: null,
            )
          : null,
      body: getSuccessView(),
    );
  }

  Widget getSuccessView() {
    if (transferSkipped == true) {
      return VitaminsWidget(
        // Already have a App bar
        source: BaseStepperSource.NEW_USER,
      );
    } else {
      return SignUpSuccessWidget(
        source: BaseStepperSource.NEW_USER,
      );
    }
  }
}
