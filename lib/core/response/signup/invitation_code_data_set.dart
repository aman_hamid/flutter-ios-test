import 'package:json_annotation/json_annotation.dart';

part 'invitation_code_data_set.g.dart';

@JsonSerializable()
class InvitationCodeDataSet {
  int id;
  bool disabled;
  String invitationCode;
  int employerDataId;

  InvitationCodeDataSet(this.id, this.disabled, this.invitationCode, this.employerDataId);

  factory InvitationCodeDataSet.fromJson(Map<String, dynamic> json) =>
      _$InvitationCodeDataSetFromJson(json);
}
