import 'package:json_annotation/json_annotation.dart';

import 'insurance_code_activation_detail.dart';

part 'insurance_code_activation_details_response.g.dart';

@JsonSerializable()
class InsuranceCodeActivationDetailsResponse{

  InsuranceCodeActivationDetail insuranceCodeActivationDetails;//insuranceCodeActivationDetails

  InsuranceCodeActivationDetailsResponse(this.insuranceCodeActivationDetails);

  factory InsuranceCodeActivationDetailsResponse.fromJson(Map<String, dynamic> json) => _$InsuranceCodeActivationDetailsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$InsuranceCodeActivationDetailsResponseToJson(this);

}
