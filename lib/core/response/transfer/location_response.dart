import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/transfer/location.dart';
import 'package:pocketpills/core/models/transfer/subdivisions.dart';
part 'location_response.g.dart';

@JsonSerializable()
class LocationResponse {
  Location location;
  List<Subdivisions> subdivisions;

  LocationResponse(this.location, this.subdivisions);

  factory LocationResponse.fromJson(Map<String, dynamic> json) => _$LocationResponseFromJson(json);
}
