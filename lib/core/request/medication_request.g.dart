// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medication_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicationRequest _$MedicationRequestFromJson(Map<String, dynamic> json) {
  return MedicationRequest(
    medicationId: json['medicationId'] as int,
    quantity: json['quantity'] as num,
    din: json['din'] as String,
    plan: json['plan'] as String,
    drugName: json['drugName'] as String,
    variantId: json['variantId'] as int,
  );
}

Map<String, dynamic> _$MedicationRequestToJson(MedicationRequest instance) =>
    <String, dynamic>{
      'medicationId': instance.medicationId,
      'quantity': instance.quantity,
      'din': instance.din,
      'plan': instance.plan,
      'drugName': instance.drugName,
      'variantId': instance.variantId,
    };
