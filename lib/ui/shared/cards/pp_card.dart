import 'package:flutter/material.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';

class PPCard extends StatelessWidget {
  final Function onTap;
  final Widget child;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final Color cardColor;
  final RoundedRectangleBorder roundedRectangleBorder;
  PPCard(
      {this.onTap, this.child, this.padding, this.margin, this.roundedRectangleBorder, this.cardColor = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(40, 0, 0, 0),
            blurRadius: 3,
            spreadRadius: 0,
            offset: Offset(
              0, // horizontal,
              1.0, // vertical,
            ),
          )
        ],
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(4.0),
        color: cardColor,
      ),
      margin: this.margin == null ? EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium) : this.margin,
      child: Card(
        color: cardColor,
        shape: roundedRectangleBorder,
        margin: EdgeInsets.all(0.0),
        elevation: 0,
        child: InkWell(
          borderRadius: BorderRadius.circular(4.0),
          child: Container(
            padding: this.padding != null
                ? this.padding
                : EdgeInsets.symmetric(
                    horizontal: PPUIHelper.HorizontalSpaceMedium,
                    vertical: PPUIHelper.VerticalSpaceMedium,
                  ),
            child: child != null ? child : Container(),
          ),
          onTap: onTap != null ? onTap : () {},
        ),
      ),
    );
  }
}
