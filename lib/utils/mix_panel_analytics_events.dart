import 'dart:typed_data';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:pocketpills/app_config.dart';
import 'dart:async';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/analytics_event.dart';
import 'package:native_mixpanel/native_mixpanel.dart';

class MixPanelAnalyticsEvents implements AnalyticsEvents {
  Mixpanel _mixpanel;
  final DataStoreService dataStore = locator<DataStoreService>();
  static final token = locator<AppConfig>().mixPanelToken;

  MixPanelAnalyticsEvents() {
    _mixpanel = Mixpanel(
      shouldLogEvents: true,
      isOptedOut: false,
    );
    _mixpanel.initialize(token);
    openNotifications(); // PocketPills staging
  }

  @override
  Future<void> sendAnalyticsEvent(String eventName, [Map<String, dynamic> bundle]) async {
    if (bundle == null) bundle = Map();
    await _mixpanel.track(eventName, bundle);
  }

  @override
  Future<void> setAnalyticsEventIdentify() async {
    try {
      Map<String, String> map = new Map();
      map["\$email"] = dataStore.readCheckedString(DataStoreService.EMAIL);
      map["\$first_name"] = dataStore.readCheckedString(DataStoreService.FIRSTNAME);
      map["\$last_name"] = dataStore.readCheckedString(DataStoreService.LASTNAME);
      map["\$phone"] = dataStore.readCheckedString(DataStoreService.PHONE);
      map["\$date_of_birth"] = dataStore.readCheckedString(DataStoreService.DATE_OF_BIRTH);
      map["\$gender"] = dataStore.readCheckedString(DataStoreService.GENDER);
      map["\$user_id"] = dataStore.getUserId() != null ? dataStore.getUserId().toString() : "";
      map["\$patient_id"] = dataStore.getPatientId() != null ? dataStore.getPatientId().toString() : "";
      map.addAll(getUtmParamsMap());
      await _mixpanel.setPeopleProperties(map);
    } catch (ex) {
      Crashlytics.instance.log(ex.toString());
    }
  }

  @override
  Future<void> setMixpanelPeoplePeoperty(Map peopertiesMap) async {
    try {
      Map<String, String> map = new Map();
      map.addAll(peopertiesMap);
      map.addAll(getUtmParamsMap());
      await _mixpanel.setPeopleProperties(map);
    } catch (ex) {
      Crashlytics.instance.log(ex.toString());
    }
  }

  Map getUtmParamsMap() {
    Map<String, String> map = new Map();
    dataStore.readString(DataStoreService.UTM_CAMPAIGN) != null
        ? map["utm_campaign [last touch]"] = dataStore.readString(DataStoreService.UTM_CAMPAIGN)
        // ignore: unnecessary_statements
        : null;
    dataStore.readString(DataStoreService.UTM_CONTENT) != null
        ? map["utm_content"] = dataStore.readString(DataStoreService.UTM_CONTENT)
        // ignore: unnecessary_statements
        : null;
    dataStore.readString(DataStoreService.UTM_TERM) != null
        ? map["utm_term"] = dataStore.readString(DataStoreService.UTM_TERM)
        // ignore: unnecessary_statements
        : null;
    dataStore.readString(DataStoreService.UTM_MEDIUM) != null
        ? map["utm_medium"] = dataStore.readString(DataStoreService.UTM_MEDIUM)
        // ignore: unnecessary_statements
        : null;
    dataStore.readString(DataStoreService.UTM_CAMPAIGN) != null
        ? map["utm_campaign"] = dataStore.readString(DataStoreService.UTM_CAMPAIGN)
        // ignore: unnecessary_statements
        : null;
    dataStore.readString(DataStoreService.UTM_SOURCE) != null
        ? map["utm_source [last touch]"] = dataStore.readString(DataStoreService.UTM_SOURCE)
        // ignore: unnecessary_statements
        : null;
    dataStore.readString(DataStoreService.UTM_MEDIUM) != null
        ? map["utm_medium [last touch]"] = dataStore.readString(DataStoreService.UTM_MEDIUM)
        // ignore: unnecessary_statements
        : null;

    return map;
  }

  Future<void> setMixpanelIdentifier() async {
    if (dataStore.getUserId() != null) {
      await _mixpanel.initialize(token);
      await _mixpanel.identify(dataStore.getUserId().toString());
    }
  }

  Future<dynamic> flush() async {
    await _mixpanel.flush();
  }

  Future<dynamic> reset() async {
    await _mixpanel.initialize(token);
    await _mixpanel.reset();
  }

  void openNotifications() async {
    try {
      dynamic deviceToken = await _mixpanel.getDeviceToken();
      String _ = convertRawDeviceToken(deviceToken);
    } catch (ex) {
      Crashlytics.instance.log(ex.toString());
    }
  }

  Future<dynamic> getMixpanelDistinctID() async {
    return await _mixpanel.getDistinctId();
  }

  String convertRawDeviceToken(Uint8List deviceToken) {
    String s = '';
    for (int byte in deviceToken) {
      String byteString = byte.toRadixString(16);
      if (byteString.length == 1) byteString = '0' + byteString;
      s += byteString;
    }
    return s;
  }
}
