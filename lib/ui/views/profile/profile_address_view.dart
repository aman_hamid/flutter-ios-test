import 'dart:ui';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/address.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_address_model.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/cards/pp_card.dart';
import 'package:pocketpills/ui/shared/chips/pp_chip.dart';
import 'package:pocketpills/ui/shared/dialogs/address_delete_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/shared/vitamins/vitamins_bottom_sheet.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/empty_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/profile/address_detail_arguments.dart';
import 'package:pocketpills/ui/views/profile/address_detail_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ProfileAddressView extends StatefulWidget {
  final Function onSuccess;
  final Function onBack;
  final BaseStepperSource source;
  final int position;

  ProfileAddressView({Key key, this.onBack, this.onSuccess, this.source, this.position = 0}) : super(key: key);

  static const routeName = 'profileaddress';

  @override
  State<StatefulWidget> createState() {
    return ProfileAddressState();
  }
}

class ProfileAddressState extends BaseState<ProfileAddressView> {
  Future<List<Address>> addressList;
  int selectedAddressId;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: widget.position > 0
            ? widget.onBack
            : () async {
                Navigator.pop(context);
                return true;
              },
        child: ChangeNotifierProvider(
            create: (_) => ProfileAddressModel(),
            child: Consumer<ProfileAddressModel>(builder: (BuildContext context, ProfileAddressModel profileAddressModel, Widget child) {
              return FutureBuilder(
                future: myFutureMethodOverall(profileAddressModel, context),
                builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (profileAddressModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                    return NoInternetScreen(
                      onClickRetry: profileAddressModel.clearAsyncMemorizer,
                    );
                  }
                  if (snapshot.hasData && profileAddressModel.connectivityResult != ConnectivityResult.none) {
                    return afterFutureResolved(context, profileAddressModel, profileAddressModel.addresses);
                  } else if (snapshot.hasError && profileAddressModel.connectivityResult != ConnectivityResult.none) {
                    Crashlytics.instance.log(snapshot.hasError.toString());
                    return ErrorScreen();
                  }

                  if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                    return LoadingScreen();
                  }

                  if (snapshot.hasData) {
                    return afterFutureResolved(context, profileAddressModel, profileAddressModel.addresses);
                  } else if (snapshot.hasError) {
                    Crashlytics.instance.log(snapshot.hasError.toString());
                    return ErrorScreen();
                  }
                  return LoadingScreen();
                },
              );
            })));
  }

  Future myFutureMethodOverall(ProfileAddressModel profileAddressModel, BuildContext context) async {
    Future<List<Address>> future1 = profileAddressModel.fetchAddressData(Provider.of<DashboardModel>(context).selectedPatientId); // will take 1 sec
    Future<Map<String, dynamic>> future2 = profileAddressModel.getLocalization(["address", "order-checkout"]); // will take 3 secs
    return await Future.wait([future1, future2]);
  }

  Widget afterFutureResolved(BuildContext context, ProfileAddressModel profileAddressModel, List<Address> addresses) {
    if (addresses != null) {
      return Scaffold(
          key: _scaffoldKey,
          floatingActionButton: Stack(children: <Widget>[
            profileAddressModel.singleAddress == false
                ? Align(
                    alignment: Alignment.bottomCenter,
                    child: FloatingActionButton.extended(
                        elevation: 10.0,
                        isExtended: true,
                        icon: Icon(Icons.add_circle),
                        label: Text(
                          LocalizationUtils.getSingleValueString("address", "address.labels.addnew").toUpperCase(),
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () async {
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_add_an_address);
                          final result = await Navigator.pushNamed(
                            context,
                            AddressDetailWidget.routeName,
                            arguments: AddressDetailArguments(
                              address: null,
                            ),
                          );
                          if (result != null && result == true) profileAddressModel.updateAddressList();
                        }),
                  )
                : PPContainer.emptyContainer(),
            profileAddressModel.state != ViewState.Busy
                ? Container()
                : Container(
                    color: Colors.white30,
                  )
          ]),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          body: Stack(children: <Widget>[
            getAddresView(context, profileAddressModel, addresses),
            profileAddressModel.state != ViewState.Busy
                ? Container()
                : Container(color: Colors.white30, child: Center(child: SizedBox(height: LARGE_X, width: LARGE_X, child: CircularProgressIndicator())))
          ]),
          bottomNavigationBar: getButtons(profileAddressModel));
    } else {
      return PPContainer.emptyContainer();
    }
  }

  Widget getAddresView(BuildContext context, ProfileAddressModel profileAddressModel, List<Address> addresses) {
    BaseStepperSource from = getStepperFrom();
    if (addresses != null && addresses.length == 0 && profileAddressModel.addressSuggestions?.length == 0) {
      return AddressDetailWidget(
        address: null,
        updateList: () {
          profileAddressModel.updateAddressList();
        },
        onSuccess: widget.onSuccess,
        onBack: widget.onBack,
        source: widget.source == null ? BaseStepperSource.EDIT_PROFILE : BaseStepperSource.ORDER_STEPPER,
        from: from,
      );
    }
    if (addresses != null && addresses.length == 1 && addresses[0].nickname.contains("TBC")) {
      return AddressDetailWidget(
        address: addresses[0],
        updateList: () {
          profileAddressModel.updateAddressList();
        },
        onSuccess: widget.onSuccess,
        onBack: widget.onBack,
        source: widget.source == null ? BaseStepperSource.EDIT_PROFILE : BaseStepperSource.ORDER_STEPPER,
        from: from,
      );
    }

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          getAddresses(context, profileAddressModel, addresses),
          getAddressSuggestionsView(
            context,
            profileAddressModel,
          )
        ],
      ),
    );
  }

  Widget getButtons(ProfileAddressModel profileAddressModel) {
    return widget.onSuccess == null
        ? null
        : (widget.source == BaseStepperSource.VITAMINS_SCREEN || widget.source == BaseStepperSource.VITAMINS_SUBSCRIPTION_SCREEN)
            ? profileAddressModel.addresses.length == 0
                ? PPContainer.emptyContainer()
                : Builder(
                    builder: (BuildContext context) => VitaminsBottomSheet(
                      onBack: () {
                        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_address_back);
                        if (widget.onBack != null) widget.onBack();
                      },
                      onContiune: () {
                        updateShoppingCart(profileAddressModel);
                      },
                      stepNumber: 0,
                    ),
                  )
            : Builder(
                builder: (BuildContext context) {
                  List<Widget> rowChildren = [
                    SecondaryButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.button.back"),
                      onPressed: widget.onBack,
                    ),
                    SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
                    PrimaryButton(
                      text: LocalizationUtils.getSingleValueString("common", "common.button.continue").toUpperCase(),
                      onPressed: widget.onSuccess,
                    )
                  ];
                  if (profileAddressModel.addresses == null || profileAddressModel.addresses.length == 0) {
                    rowChildren = [
                      SecondaryButton(
                        text: LocalizationUtils.getSingleValueString("common", "common.button.back"),
                        onPressed: widget.onBack,
                      ),
                    ];
                  }
                  return profileAddressModel.singleAddress == true
                      ? PPContainer.emptyContainer()
                      : Builder(
                          builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                                  child: Container(
                                color: Colors.white,
                                child: Row(mainAxisSize: MainAxisSize.min, children: rowChildren),
                              )));
                },
              );
  }

  Widget getAddressSuggestionsView(BuildContext context, ProfileAddressModel profileAddressModel) {
    return (profileAddressModel.addressSuggestions != null && profileAddressModel.addressSuggestions.length > 0)
        ? Column(
            children: <Widget>[
              PPTexts.getSectionHeader(
                  child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    PPUIHelper.verticalSpaceSmall(),
                    PPTexts.getHeading(
                      LocalizationUtils.getSingleValueString("address", "address.label.other-address"),
                    ),
                    PPUIHelper.verticalSpaceSmall()
                  ],
                ),
              )),
              Container(
                height: MediaQuery.of(context).size.width,
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: SMALL_XXX, vertical: SMALL_XXX),
                  itemCount: profileAddressModel.addressSuggestions.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    Address address = profileAddressModel.addressSuggestions[index];
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.37,
                            decoration: new BoxDecoration(
                              border: Border.all(color: borderColor, width: 1),
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(4.0),
                              color: whiteColor,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ConstrainedBox(
                                    constraints: new BoxConstraints(
                                      minHeight: 140.0,
                                      minWidth: MediaQuery.of(context).size.width * 0.37,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: MEDIUM_X, vertical: MEDIUM_XX),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          RichText(
                                            maxLines: 5,
                                            overflow: TextOverflow.ellipsis,
                                            text: TextSpan(
                                              text: address.nickname != null ? address.nickname + "\n" : "",
                                              style: MEDIUM_XXX_PRIMARY_BOLD,
                                              children: <TextSpan>[
                                                getAddressWidgetTextElement(address.streetAddress),
                                                getAddressWidgetTextElement(address.streetAddressLineTwo),
                                                getAddressWidgetTextElement(address.city),
                                                getAddressWidgetTextElement(address.province + " - "),
                                                getAddressWidgetTextElement(address.postalCode),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),
                                Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Column(
                                    children: <Widget>[
                                      PPDivider(),
                                      Container(
                                        width: MediaQuery.of(context).size.width * 0.37,
                                        child: FlatButton(
                                          onPressed: () async {
                                            bool connectivityResult = await profileAddressModel.isInternetConnected();
                                            if (connectivityResult == false) {
                                              onFail(context, errMessage: profileAddressModel.noInternetConnection);
                                              return;
                                            }
                                            bool result = await profileAddressModel.addPatientAddress(
                                                name: address.nickname,
                                                sal1: address.streetAddress,
                                                sal2: address.streetAddressLineTwo == "" ? null : address.streetAddressLineTwo,
                                                city: address.city,
                                                province: address.province,
                                                isDefault: false,
                                                postalCode: address.postalCode.toUpperCase());
                                            if (result == true) {
                                              showOnSnackBar(context, successMessage: profileAddressModel.errorMessage);
                                            } else {
                                              onFail(context, errMessage: profileAddressModel.errorMessage);
                                            }
                                          },
                                          child: Text(
                                            (profileAddressModel.addresses != null && profileAddressModel.addresses.length > 0)
                                                ? LocalizationUtils.getSingleValueString("address", "address.button.copy")
                                                : LocalizationUtils.getSingleValueString("address", "address.button.button-add"),
                                            style: MEDIUM_XX_PURPLE_BOLD,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              )
            ],
          )
        : SizedBox(
            height: 0.0,
          );
  }

  Widget getAddresses(BuildContext context, ProfileAddressModel profileAddressModel, List<Address> addresses) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: REGULAR,
        ),
        Padding(
          child: PPTexts.getMainViewHeading(
            LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.title-address"),
          ),
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: SMALL_XX),
        ),
        Padding(
          child: PPTexts.getSecondaryHeading(getDescriptionText(PatientUtils.getForGender(Provider.of<DashboardModel>(context).selectedPatient)), isBold: false),
          padding: EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium, vertical: SMALL_XX),
        ),
        SizedBox(
          height: REGULAR,
        ),
        ListView.builder(
            primary: false,
            shrinkWrap: true,
            itemCount: addresses.length,
            itemBuilder: (BuildContext ctxt, int index) {
              Address address = addresses[index];
              return PPCard(
                padding: EdgeInsets.all(0.0),
                margin: EdgeInsets.only(bottom: MEDIUM_XXX, left: MEDIUM_XXX, right: MEDIUM_XXX),
                child: Container(
                  constraints: BoxConstraints(maxWidth: double.infinity),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Radio(
                            value: address.id,
                            groupValue: profileAddressModel.selectedAddressId,
                            onChanged: (int addressId) async {
                              bool connectivityResult = await profileAddressModel.isInternetConnected();
                              if (connectivityResult == false) {
                                onFail(context, errMessage: profileAddressModel.noInternetConnection);
                                return;
                              }
                              setDefaultAddress(profileAddressModel, addressId);
                            },
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: MEDIUM_X),
                              child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                address.nickname != null
                                    ? profileAddressModel.selectedAddressId == address.id
                                        ? Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                address.nickname,
                                                style: MEDIUM_XXX_PRIMARY_BOLD,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(right: MEDIUM_XXX),
                                                child: PPChip(label: LocalizationUtils.getSingleValueString("address", "address.label.default")),
                                              ),
                                            ],
                                          )
                                        : Padding(
                                            padding: EdgeInsets.symmetric(vertical: 0.0),
                                            child: Text(
                                              address.nickname,
                                              style: MEDIUM_XXX_PRIMARY_BOLD,
                                            ))
                                    : SizedBox(
                                        width: 0.0,
                                      ),
                                getAddressWidgetElement(address.streetAddress),
                                getAddressWidgetElement(address.streetAddressLineTwo),
                                getAddressWidgetElement(address.city),
                                Row(
                                  children: <Widget>[
                                    getAddressWidgetElement(address.province + " - "),
                                    getAddressWidgetElement(address.postalCode),
                                  ],
                                )
                              ]),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: LARGE_X),
                        child: Row(
                          children: <Widget>[
                            TransparentButton(
                              text: LocalizationUtils.getSingleValueString("address", "address.button.delete").toUpperCase(),
                              onPressed: AddressDeleteDialog.show(profileAddressModel, context, address.id,
                                  showSnackBar: (value) => {(value != null && value != "") ? showSnackBar(value) : null}),
                            ),
                            TransparentButton(
                                text: LocalizationUtils.getSingleValueString("address", "address.button.edit").toUpperCase(),
                                onPressed: () async {
                                  final result = await Navigator.pushNamed(
                                    context,
                                    AddressDetailWidget.routeName,
                                    arguments: AddressDetailArguments(
                                      address: address,
                                    ),
                                  );
                                }),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
        SizedBox(
          height: (profileAddressModel.addressSuggestions != null && profileAddressModel.addressSuggestions.length > 0) ? REGULAR_XXX : LARGE_XXX,
        ),
      ],
    );
  }

  setDefaultAddress(ProfileAddressModel profileAddressModel, int addressId) async {
    bool connectivityResult = await profileAddressModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: profileAddressModel.noInternetConnection);
      return;
    }
    bool res = await profileAddressModel.setPatientAddressDefault(addressId);
    if (res == true) profileAddressModel.setSelectedAddress(addressId);
  }

  showSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      duration: Duration(seconds: 3),
    ));
  }

  Widget getAddressWidgetElement(String element) {
    return element != null && element != ""
        ? Text(
            element,
            style: MEDIUM_XX_SECONDARY,
          )
        : SizedBox(
            width: 0.0,
          );
  }

  TextSpan getAddressWidgetTextElement(String element) {
    return element != null && element != ""
        ? TextSpan(
            text: element + " ",
            style: prefix0.TextStyle(
              fontSize: MEDIUM_XX,
              color: secondaryColor,
              height: 1.4,
              fontWeight: FontWeight.w400,
            ),
          )
        : TextSpan(
            text: "",
            style: MEDIUM_XX_SECONDARY,
          );
  }

  refreshAddresses(ProfileAddressModel profileAddressModel) async {
    await new Future.delayed(new Duration(seconds: 1), () {
      profileAddressModel.clearAddressData();
    });
  }

  void updateShoppingCart(ProfileAddressModel profileAddressModel) async {
    bool connectivityResult = await profileAddressModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: profileAddressModel.noInternetConnection);
      return;
    }
    if (profileAddressModel.selectedAddressId != null && profileAddressModel.selectedAddressId != -1) {
      profileAddressModel.setState(ViewState.Busy);
      bool success = await Provider.of<VitaminsCatalogModel>(context).updateShoppingCartAddress(profileAddressModel.selectedAddressId);
      profileAddressModel.setState(ViewState.Idle);
      if (success) {
        analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_review_cart_payment);
        if (widget.onSuccess != null) widget.onSuccess();
      } else {
        onFail(context, errMessage: profileAddressModel.errorMessage);
      }
    } else {
      onFail(context, errMessage: LocalizationUtils.getSingleValueString("address", "address.labels.address-empty-info"));
    }
  }

  String getDescriptionText(String pronounForGender) {
    switch (pronounForGender) {
      case "MALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-add-address-MALE");
      case "FEMALE":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-add-address-FEMALE");
      case "OTHER":
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-add-address-other");
      default:
        return LocalizationUtils.getSingleValueString("order-checkout", "order-checkout.main.description-add-address");
    }
  }

  BaseStepperSource getStepperFrom() {
    if (widget.source == null) {
      return null;
    } else {
      switch (widget.source) {
        case BaseStepperSource.VITAMINS_SCREEN:
          return BaseStepperSource.VITAMINS_SCREEN;
          break;
        case BaseStepperSource.VITAMINS_SUBSCRIPTION_SCREEN:
          return BaseStepperSource.VITAMINS_SUBSCRIPTION_SCREEN;
          break;
        default:
          return null;
          break;
      }
    }
  }
}
