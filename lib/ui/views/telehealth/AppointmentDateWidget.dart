import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/telehealthAppointmentDetails.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/tele_health_appointment_time.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/core/viewmodels/telehealth/preference_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/appbar_icons.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/signup/teleheath_arguments.dart';
import 'package:pocketpills/ui/views/telehealth/telehealth_preference.dart';
import 'package:pocketpills/utils/analytics.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import '../../../locator.dart';
import '../error_screen.dart';

class AppointmentDateWidget extends StatefulWidget {
  static const routeName = 'appointmentDateList';
  String from;
  SignUpTransferModel modelSignUp;
  BaseStepperSource source;
  UserPatient userPatient;
  AppointmentDateWidget(
      {this.from = "telehealth",
      this.modelSignUp,
      this.source,
      this.userPatient});
  @override
  _AppointmentDateWidgetState createState() => _AppointmentDateWidgetState();
}

class _AppointmentDateWidgetState extends State<AppointmentDateWidget> {
  ScrollController _scrollController;
  List<bool> selectedIndexes = [];
  bool itemSelected = false;
  AppointmentTime selectedItem;
  final Analytics analyticsEvents = locator<Analytics>();
  final DataStoreService dataStore = locator<DataStoreService>();
  bool eventTime = false;
  @override
  void initState() {
    super.initState();
    _scrollController = new ScrollController(
      // NEW
      initialScrollOffset: 0.0, // NEW
      keepScrollOffset: true, // NEW
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.from == "telehealth"
          ? widget.source == BaseStepperSource.NEW_USER
          ? () async {
        Navigator.pop(context);
        return true;
      } : () => _onBackPressed(context)
          : () async {
        Navigator.of(context).pushNamedAndRemoveUntil(
            DashboardWidget.routeName, (Route<dynamic> route) => false);
        return true;
      },
      child: ChangeNotifierProvider.value(
        value: PreferenceModel(),
        child: Consumer<PreferenceModel>(builder:
            (BuildContext context, PreferenceModel model, Widget child) {
          String name = StringUtils.capitalize(PatientUtils.getYouOrNameTitle(
              Provider.of<DashboardModel>(context).selectedPatient));
          name = '${name[0].toUpperCase()}${name.substring(1)}';
          return FutureBuilder(
              future: myFutureMethodOverall(model),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                Map<String, String> map = new Map();
                if (snapshot.hasData != null && snapshot.data != null) {
                  List<AppointmentTime> appointmentTime = List();
                  appointmentTime.addAll(snapshot.data[1]);
                  for (int i = 0; i < appointmentTime.length; i++) {
                    map['slot'+appointmentTime[i].index.toString()] = appointmentTime[i].available.toString();
                    selectedIndexes.add(false);
                  }
                  if(eventTime == false) {
                    eventTime = true;
                    widget.from == "telehealth"
                        ?analyticsEvents.sendAnalyticsEvent("au_appointment",map):analyticsEvents.sendAnalyticsEvent("appointment",map);
                  }
                  return appointmentTimeLoad(context, model, snapshot.data[1],name);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        }),
      ),
    );
  }

  Future myFutureMethodOverall(PreferenceModel model) async {
    Future<Map<String, dynamic>> future1 =
        model.getLocalization(["signup", "consultation","common"]);
    Future<List<AppointmentTime>> future2 = model.getAppointmentDate();
    return await Future.wait([future1,future2]);
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  Widget appointmentTimeLoad(BuildContext context, PreferenceModel model,
      List<AppointmentTime> appointmentTime, String name) {
    return BaseScaffold(
      appBar: innerAppbar(widget.from, name),
      bottomNavigationBar: Builder(
        builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
          child: model.state == ViewState.Busy
              ? ViewConstants.progressIndicator
              : Row(
                  children: <Widget>[
                    PrimaryButton(
                      text: LocalizationUtils.getSingleValueString("common",
                          "common.button.confirm"),
                      onPressed: () {
                        if(selectedItem!=null) {
                          Map<String, String> map = new Map();
                          map["daysDiffer"] = selectedItem.appointmentDate.toString();
                          map["slotSelected"] = selectedItem.index.toString();
                          widget.from == "telehealth"
                              ?analyticsEvents.sendAnalyticsEvent("au_appointment_submit",map):analyticsEvents.sendAnalyticsEvent("appointment_submit",map);
                          AppointmentDetails.AppointmentSelectedItem = selectedItem;
                          dataStore.writeString(DataStoreService.DOCTOR_NAME, selectedItem.doctorName);
                          dataStore.writeString(DataStoreService.APPOINTMENT_TIME, selectedItem.timeSlot);
                          dataStore.writeString(DataStoreService.APPOINTMENT_DATE, selectedItem.displayDate);
                          if(widget.from == "telehealth") {
                            Navigator.pushNamed(context, TelehealthPreference.routeName,
                                arguments: TelehealthArguments(
                                  modelSignUp: widget.modelSignUp,
                                  source: widget.source,
                                  userPatient: widget.userPatient,
                                  from: "telehealth",
                                ));
                          } else {
                            Navigator.pushNamed(context, TelehealthPreference.routeName,
                                arguments: TelehealthArguments(
                                  modelSignUp: widget.modelSignUp,
                                  source: widget.source,
                                  userPatient: widget.userPatient,
                                  from: "",
                                ));
                          }

                        } else if(selectedItem == null) {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text(LocalizationUtils.getSingleValueString("signup",
                                "signup.appointment.required"),),
                          ));
                        }
                      },
                    ),
                  ],
                ),
        ),
      ),
      body: Builder(
        builder: (BuildContext context) => Container(
          child: Builder(
            builder: (BuildContext context) {
              return Stack(children: <Widget>[
                Container(
                  color: whiteColor,
                  child: Padding(
                    padding: itemSelected
                        ? const EdgeInsets.only(bottom: 100.0,left: MEDIUM,right: MEDIUM,top: MEDIUM)
                        : const EdgeInsets.all(MEDIUM),
                    child: ListView(
                      children: <Widget>[
                        SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
                        Center(
                          child: Avatar(
                              displayImage:
                                  "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
                        ),
                        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: PPUIHelper.HorizontalSpaceMedium),
                          child: Text(
                            LocalizationUtils.getSingleValueString("signup",
                                "signup.appointment.book"),
                            style: TextStyle(
                              color: lightBlue,
                              fontWeight: FontWeight.w500,
                              fontSize: 16.0,
                              height: 1.5,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
                        appointmentDate(context, model, appointmentTime),
                        // Padding(
                        //   padding: const EdgeInsets.all(SMALL_XXX),
                        //   child: vitaminNotFoundCard(),
                        // ),
                      ],
                    ),
                  ),
                ),
                if (itemSelected) doctorView(),
                model.state != ViewState.Busy
                    ? Container()
                    : Container(
                        color: Colors.white30,
                        child: Center(
                            child: SizedBox(
                                height: LARGE_X,
                                width: LARGE_X,
                                child: CircularProgressIndicator())))
              ]);
            },
          ),
        ),
      ),
    );
  }

  Widget innerAppbar(String from, String name) {
    if (from == "telehealth") {
      return AppBar(
        backgroundColor: Colors.white,
        title: Image.asset('graphics/logo-horizontal-dark.png',
            width: MediaQuery.of(context).size.width * 0.45),
        centerTitle: true,
      );
    } else {
      return InnerAppBar(
          titleText: name,
          appBar: AppBar(),
          leadingBackButton: () {
            Navigator.of(context).pushNamedAndRemoveUntil(
                DashboardWidget.routeName, (Route<dynamic> route) => false);
          });
    }
  }

  Widget appointmentDate(BuildContext context, PreferenceModel model,
      List<AppointmentTime> appointmentTime) {
    return new GridView.builder(
        shrinkWrap: true,
        primary: false,
        controller: _scrollController,
        scrollDirection: Axis.vertical,
        itemCount: appointmentTime.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 2.1),
        itemBuilder: (BuildContext context, int index) {
          AppointmentTime appointmentTimeItem = appointmentTime[index];
          return Container(
            decoration: new BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(40, 0, 0, 0),
                  blurRadius: 3,
                  spreadRadius: 0,
                  offset: Offset(
                    0, // horizontal,
                    1.0, // vertical,
                  ),
                )
              ],
              border: Border.all(color: Color(0xFFE3DBFF), width: 1),
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(5.0),
              color: selectedIndexes[index] == true
                  ? brandColor
                  : appointmentTimeItem.available == false
                      ? bgDateColor
                      : Colors.white,
            ),
            margin: EdgeInsets.all(SMALL_XXX),
            child: Card(
              color: Colors.white,
              shape: RoundedRectangleBorder(),
              margin: EdgeInsets.all(1.0),
              elevation: 0,
              child: InkWell(
                borderRadius: BorderRadius.circular(4.0),
                child: Container(
                  padding: EdgeInsets.all(0.0),
                  child: Container(
                    color: selectedIndexes[index] == true
                        ? brandColor
                        : appointmentTimeItem.available == false
                            ? bgDateColor
                            : Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: MEDIUM_X, right: MEDIUM_X, top: MEDIUM),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    appointmentTimeItem.displayDate,
                                    style: selectedIndexes[index] == true
                                        ? MEDIUM_XX_WHITE_BOLD
                                        : MEDIUM_XX_PRIMARY_BOLD,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                  ),
                                  SizedBox(
                                    height: SMALL_XX,
                                  ),
                                  Text(
                                    appointmentTimeItem.status,
                                    style: selectedIndexes[index] == true
                                        ? MEDIUM_XX_WHITE_BOLD
                                        : appointmentTimeItem.available ==
                                                true
                                            ? MEDIUM_XX_GREEN_BOLD_MEDIUM
                                            : MEDIUM_XX_RED_BOLD_MEDIUM,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                onTap: () {
                  if (appointmentTimeItem.available == true) {
                    setState(() {
                      //model.updateAppointment(appointmentTimeItem.id);
                      Map<String, String> map = new Map();
                      if (selectedIndexes[index] != true) {
                        for (int i = 0; i < appointmentTime.length; i++) {
                          selectedIndexes[i] = false;
                        }
                        itemSelected = true;
                        selectedItem = appointmentTimeItem;
                        selectedIndexes[index] = true;
                      } else {
                        itemSelected = false;
                        selectedItem = null;
                        selectedIndexes[index] = false;
                      }
                    });
                  }
                },
              ),
            ),
          );
        });
  }

  Widget doctorView() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 100,
          decoration: BoxDecoration(
            color: bgDateColor,
            boxShadow: [
              BoxShadow(
                color: quartiaryColor,
                offset: Offset(0.1, 0.0), //(x,y)
                blurRadius: 6.0,
              ),
            ],
          ),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                selectedItem.clinicName,
                style: MEDIUM_X_PRIMARY,
              ),

              PPUIHelper.verticalSpaceXSmall(),
              Text(
                selectedItem.doctorName,
                style: MEDIUM_XXX_BLACK_BOLD,
              ),
              SizedBox(
                height: MEDIUM,
              ),
              Row(
                children: [
                  Text(
                    selectedItem.displayDate,
                    style: MEDIUM_XX_PRIMARY,
                  ),
                  Text(
                    ", ",
                    style: MEDIUM_XX_PRIMARY,
                  ),
                  Text(
                    selectedItem.timeSlot,
                    style: MEDIUM_XX_PRIMARY,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
