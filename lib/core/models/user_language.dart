import 'package:pocketpills/core/request/base_request.dart';

class LanguageRequest extends BaseRequest {
  String locale;

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = Map();
    map['locale'] = locale;
    return map;
  }
}
