import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/res/colors.dart';

class SignUpStepperAppBar extends StatefulWidget implements PreferredSizeWidget {
  final AppBar appBar;
  final String step;

  SignUpStepperAppBar({Key key, this.appBar, this.step}) : super(key: key);

  @override
  _SignUpStepperAppBarState createState() => _SignUpStepperAppBarState();

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}

class _SignUpStepperAppBarState extends State<SignUpStepperAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: whiteColor,
      elevation: 1,
      brightness: Platform.isIOS == true ? Brightness.light : null,
      iconTheme: IconThemeData(color: Colors.white60 //change your color here
          ),
      centerTitle: false,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Image.asset(
            'graphics/logo-horizontal-dark.png',
            width: MediaQuery.of(context).size.width * 0.45,
          ),
          widget.step != null
              ? RichText(
                  text: TextSpan(
                    text: 'Step ',
                    style: MEDIUM_XXX_SECONDARY,
                    children: <TextSpan>[
                      TextSpan(
                        text: widget.step,
                        style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM,
                      ),
                    ],
                  ),
                )
              : Container(),
        ],
      ),
      leading: null,
    );
  }
}
