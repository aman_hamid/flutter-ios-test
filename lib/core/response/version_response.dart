import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

part 'version_response.g.dart';

@JsonSerializable()
class VersionResponse {
  @JsonKey(name: 'success')
  bool status;

  @JsonKey(name: 'userMessage')
  String errMessage;

  @JsonKey(name: 'message')
  String apiMessage;

  bool shouldUpgrade;

  bool skippable;

  VersionResponse({this.status, this.errMessage, this.apiMessage, this.shouldUpgrade, this.skippable});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory VersionResponse.fromJson(Map<String, dynamic> json) => _$VersionResponseFromJson(json);
}
