import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/request/device_body.dart';

part 'setpassword_request.g.dart';

@JsonSerializable(explicitToJson: true)

class SetPasswordRequest extends BaseRequest{

  String password;
  String token;
  String dob;
  DeviceBody loginBody;

  SetPasswordRequest({this.password, this.token, this.dob, this.loginBody});

  Map<String, dynamic> toJson() => _$SetPasswordRequestToJson(this);

}