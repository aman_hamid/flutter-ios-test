// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chambers_signup_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChambersSignupRequest _$ChambersSignupRequestFromJson(
    Map<String, dynamic> json) {
  return ChambersSignupRequest(
    employerLandingPageRef: json['employerLandingPageRef'] as String,
    userIdentifier: json['userIdentifier'] as String,
  );
}

Map<String, dynamic> _$ChambersSignupRequestToJson(
        ChambersSignupRequest instance) =>
    <String, dynamic>{
      'employerLandingPageRef': instance.employerLandingPageRef,
      'userIdentifier': instance.userIdentifier,
    };
