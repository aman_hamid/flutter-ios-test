// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'success_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuccessDetails _$SuccessDetailsFromJson(Map<String, dynamic> json) {
  return SuccessDetails(
    json['title'] as String,
    json['description'] as String,
    json['image'] as String,
    json['helpText'] as String,
    (json['fields'] as List)
        ?.map((e) => e == null
            ? null
            : TransactionSuccessField.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$SuccessDetailsToJson(SuccessDetails instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'image': instance.image,
      'helpText': instance.helpText,
      'fields': instance.fields,
    };
