import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';

part 'medication_refill_response.g.dart';

@JsonSerializable()
class MedicationRefillResponse extends BaseResponse {
  int omsOrderId;
  SuccessDetails successDetails;

  MedicationRefillResponse(this.omsOrderId, this.successDetails);

  factory MedicationRefillResponse.fromJson(Map<String, dynamic> json) => _$MedicationRefillResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MedicationRefillResponseToJson(this);
}
