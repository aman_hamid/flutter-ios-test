import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

part 'tele_health_appointment_time.g.dart';

@JsonSerializable()
class AppointmentTime {
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime actualAppointmentTime;
  String appointmentDate;
  bool available;
  String clinicName;
  String displayDate;
  String doctorName;
  int index;
  String status;
  String timeSlot;

  AppointmentTime(
      this.actualAppointmentTime,
      this.appointmentDate,
      this.available,
      this.clinicName,
      this.displayDate,
      this.doctorName,
      this.index,
      this.timeSlot,
      );

  factory AppointmentTime.fromJson(Map<String, dynamic> json) => _$AppointmentTimeFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentTimeToJson(this);
}
