import 'dart:io';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/transfer/pharmacy.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/signup/add_province_request.dart';
import 'package:pocketpills/core/request/signup_transfer_prescription_request.dart';
import 'package:pocketpills/core/request/transfer_prescription_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/response/prescription/success_details.dart';
import 'package:pocketpills/core/response/prescription_create_response.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province.dart';
import 'package:pocketpills/core/response/telehealth/telehealth_province_responce.dart';
import 'package:pocketpills/core/services/login_service.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/core/viewmodels/success/global_variable.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/core/request/telehealth_province_request.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:uuid/uuid.dart';
import 'package:pocketpills/core/request/telehealth/telehealth_signup_preference_request.dart';

class SignUpTransferModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final dataStore = locator<DataStoreService>();
  final LoginService loginService = locator<LoginService>();

  final places = new GoogleMapsPlaces(apiKey: ApplicationConstant.kGoogleApiKey);

  List<Pharmacy> pharmacyPredictions = List();
  List<TelehealthProvinceItem> provinceArrayList = List();
  Future<List<TelehealthProvinceItem>> provinceArrayListMemo;
  File frontImage;
  File backImage;
  List<String> popularPharmacies = ["Shoppers drug mart","Costco","Rexall","Walmart Pharmacy"];


  bool _pharmacySubmitted = false;
  bool searchNearBy = false;

  String _pharmacyName;
  String _pharmacyPhoneNumber;
  String _pharmacyAddress;
  String _province;
  String prevPharmacyName = "";
  Location location;
  BaseStepperSource source;
  SuccessDetails successDetails;
  int provinceValue = -1;
  String provinceClick = "";
  List<Component> components = [Component('country', 'ca')];
  ConnectivityResult connectivityResult;

  AsyncMemoizer<void> _geoIpMemoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();
  AsyncMemoizer<List<TelehealthProvinceItem>> provinceMemoizer = AsyncMemoizer();
  HttpApiUtils httpApiUtils = HttpApiUtils();

  bool isDialogDisplayed;
  String quebecCheck = '';

  SignUpTransferModel({BaseStepperSource source = BaseStepperSource.UNKNOWN_SCREEN, this.searchNearBy, this.isDialogDisplayed = false}) {
    this.source = source;
    getIpAddress();
    getProvinceName();
  }

  getIpAddress() async {
    try {
      location = await HttpApiUtils().getLocationUsingIp();
      _province = await HttpApiUtils().getSubdivisionName();
    } catch (ex) {
      Crashlytics.instance.log(ex.toString());
    }
    searchNearBy == true ? await getPlaceSuggestionsList() : null;
  }

  void updateDialogDisplayed(bool val) {
    isDialogDisplayed = val;
    notifyListeners();
  }

  void getProvinceName() async {
    return this._geoIpMemoizer.runOnce(() async {
      if (httpApiUtils != null) {
        try {
          String subdivisionName = await httpApiUtils.getSubdivisionName();
          quebecCheck = subdivisionName.toUpperCase();
        } catch (ex) {
          Crashlytics.instance.log(ex.toString());
        }
      }
    });
  }

  Future<List<TelehealthProvinceItem>> fetchPrescriptProvinceList() async {
    this.provinceMemoizer = AsyncMemoizer();
    provinceArrayListMemo = this.provinceMemoizer.runOnce(() async {
      return await this.getProvinceList();
    });
    return provinceArrayListMemo;
  }

  Future<List<TelehealthProvinceItem>> getProvinceList() async {
    Response response = await _api.searchProvinceList();
    if (response != null) {
      BaseResponse<ProvinceResponse> res = BaseResponse<ProvinceResponse>.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      provinceArrayList = res.response.items;
      return res.response.items;
    } else {
      return null;
    }
  }

  Future<PlaceDetails> getPlaceDetails(String placeId) async {
    PlacesDetailsResponse response = await places.getDetailsByPlaceId(placeId);
    return response.result;
  }

  Future<void> getPlaceSuggestionsList() async {
    try {
      if (location != null) {
        PlacesSearchResponse response =
            await places.searchNearbyWithRankBy(location, "distance", type: 'pharmacy', keyword: 'pharmacy', name: 'pharmacy', language: getSelectedLanguage());
        await handlePharmacyNearByResult(response);

        if (pharmacyPredictions != null && pharmacyPredictions.length > 0) {
          if (source == BaseStepperSource.NEW_USER) {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_nearby_shown);
          } else {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.transfer_nearby_shown);
          }
        }
      }

      notifyListeners();
    } catch (e) {
      Crashlytics.instance.log(e.toString());
    }
  }

  getPlaceSuggestions(String searchKeyword) async {
    if (prevPharmacyName != searchKeyword) {
      prevPharmacyName = searchKeyword;
      if (pharmacySubmitted) {
        pharmacyPredictions.clear();
      } else {
        try {
          if (location != null) {
            PlacesSearchResponse response = await places.searchNearbyWithRankBy(location, "distance", name: searchKeyword, type: 'pharmacy', language: getSelectedLanguage());
            await handlePharmacyNearByResult(response);
          } else {
            PlacesAutocompleteResponse response = await places.autocomplete(searchKeyword, components: components, types: ['establishment']);
            await handleAutoCompleteResult(response);
          }

          notifyListeners();
        } catch (e) {}
      }
    }
  }

  Future<void> handlePharmacyNearByResult(PlacesSearchResponse response) {
    if (response != null && response.results != null && response.results.length > 0) {
      pharmacyPredictions.clear();
      response.results.forEach((value) {
        Pharmacy pharmacy = Pharmacy(pharmacyName: value.name, pharmacyAddress: value.vicinity, pharmacyPlaceId: value.placeId);
        pharmacyPredictions.add(pharmacy);
      });
    }
    notifyListeners();
  }

  Future<void> handleAutoCompleteResult(PlacesAutocompleteResponse response) {
    if (response != null && response != null) {
      pharmacyPredictions.clear();
      response.predictions.forEach((value) {
        Pharmacy pharmacy = Pharmacy(pharmacyName: value.structuredFormatting.mainText, pharmacyAddress: value.structuredFormatting.secondaryText, pharmacyPlaceId: value.placeId);
        pharmacyPredictions.add(pharmacy);
      });
    }
    notifyListeners();
  }

  Future<bool> uploadPrescription(UserPatient userPatient, String placeId, String pharmacyName, String pharmacyAddress, int pharmacyPhone, bool isTransferAll, String comments,
      bool isSignUpFlow, String prescriptionState, String province, String medicineName, num quantity) async {
    setState(ViewState.Busy);

    TransferPrescriptionRequest transferRequest = TransferPrescriptionRequest(
        pharmacyName: pharmacyName,
        pharmacyAddress: pharmacyAddress,
        placeId: placeId,
        pharmacyPhone: pharmacyPhone,
        isTransferAll: isTransferAll,
        comment: comments,
        prescriptionState: prescriptionState != null ? prescriptionState : 'FILED',
        prescriptionType: PrescriptionTypeConstant.TRANSFER,
        quantity: quantity,
        medicationName: medicineName);

    var response;

    if (isSignUpFlow == true) {
      SignupTransferPrescriptionRequest signupTransferRequest = SignupTransferPrescriptionRequest(
          pharmacyName: pharmacyName,
          pharmacyAddress: pharmacyAddress,
          placeId: placeId,
          pharmacyPhone: pharmacyPhone,
          prescriptionComment: comments,
          isTransferAll: isTransferAll,
          prescriptionState: prescriptionState != null ? prescriptionState : 'FILED',
          type: 'TRANSFER');

      if (province != null && province != "") {
        bool successUpdateProvince = await updateProvince(province);
        if (successUpdateProvince == true) {
          return await handleNewUserTransferRefill(signupTransferRequest);
        } else {
          Crashlytics.instance.log("Province not updated in transfer refill flow");
          return false;
        }
      } else {
        return await handleNewUserTransferRefill(signupTransferRequest);
      }
    } else {
      int prescriptionId = await _getPrescriptionId();
      response = await _api.updatePrescription(prescriptionId, transferRequest);
      setState(ViewState.Idle);
      if (response != null) {
        BaseResponse<PrescriptionCreateResponse> baseResponse = BaseResponse<PrescriptionCreateResponse>.fromJson(response.data);
        if (!baseResponse.status) {
          errorMessage = baseResponse.getErrorMessage();
          return false;
        }
        this.successDetails = baseResponse.response.successDetails;
        GlobalVariable().prescriptionId = baseResponse.response.omsPrescriptionId;
        GlobalVariable().userFlow = PrescriptionTypeConstant.TRANSFER;
        return true;
      } else {
        return false;
      }
    }
  }

  Future<bool> uploadPrescriptionSignUp() async {
    // int prescriptionId = await _getPrescriptionId();
    SignupTransferPrescriptionRequest requestSignUp = SignupTransferPrescriptionRequest(
        type: PrescriptionTypeConstant.UPLOAD, pharmacyName: null, pharmacyAddress: null, pharmacyPhone: null, isTransferAll: false, prescriptionState: "PENDING");
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: requestSignUp));
    if (response == null) {
      return false;
    } else {
      print(response.toString());
    }
    setState(ViewState.Idle);
    return true;
  }

  Future<bool> sendPreferencesSignUp(TelehealthSignUpPreferenceRequest telehealthRequest) async {
    // int prescriptionId = await _getPrescriptionId();

    setState(ViewState.Busy);
    var response = await _api.uploadPrescriptionSignUp(RequestWrapper(body: telehealthRequest));
    setState(ViewState.Idle);
    if (response != null) {
    } else {
      return false;
    }
    setState(ViewState.Idle);
    return true;
  }

  Future<bool> handleNewUserTransferRefill(SignupTransferPrescriptionRequest signupTransferRequest) async {
    bool success = await newUserTransferRefill(signupTransferRequest);
    if (success == true) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateProvince(String province) async {
    AddProvinceRequest addProvinceRequest = AddProvinceRequest(province: province.toUpperCase());
    setState(ViewState.Busy);
    var response = await _api.updateUserInfo(RequestWrapper(body: addProvinceRequest));
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return await loginService.registerUser(response);
    } else {
      return false;
    }
  }

  Future<bool> telehealthProvince(String province) async {
    TelehealthProvinceRequest addProvinceRequest = TelehealthProvinceRequest(province: province.toUpperCase(), signupFlow: "TELEHEALTH");
    setState(ViewState.Busy);
    var response = await _api.telehealthProvinceAdd(RequestWrapper(body: addProvinceRequest));
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      return await loginService.registerUser(response);
    } else {
      return false;
    }
  }

  Future<bool> newUserTransferRefill(SignupTransferPrescriptionRequest signupTransferRequest) async {
    Response response = await _api.addSignUpPrescription(RequestWrapper(body: signupTransferRequest));
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse oldBaseResponse = OldBaseResponse.fromJson(response.data);
      if (!oldBaseResponse.status) {
        errorMessage = oldBaseResponse.getErrorMessage();
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  Future<int> _getPrescriptionId() async {
    var response = await _api.getPrescriptionId(PrescriptionTypeConstant.TRANSFER);
    if (response != null && response.data != null && response.data['details'] != null && response.data['details']['prescriptionId'] != null)
      return response.data['details']['prescriptionId'];
    return null;
  }

  String generateRandomRequestId() {
    var uuid = new Uuid();
    return uuid.v4();
  }

  clearPharmacyInfo() {
    pharmacyAddress = "";
    pharmacyPhoneNumber = "";
    pharmacyName = "";
    _province = "";
    updateDialogDisplayed(false);
    notifyListeners();
  }

  set pharmacyName(String value) {
    _pharmacyName = value;
    notifyListeners();
  }

  String get province => _province;

  set province(String value) {
    _province = value;
  }

  String get pharmacyName => _pharmacyName;

  String get pharmacyPhoneNumber => _pharmacyPhoneNumber;

  String get pharmacyAddress => _pharmacyAddress;

  set pharmacyPhoneNumber(String value) {
    _pharmacyPhoneNumber = value;
    notifyListeners();
  }

  set pharmacyAddress(String value) {
    _pharmacyAddress = value;
    notifyListeners();
  }

  bool checkSignupFlow(BaseStepperSource source) {
    return source == BaseStepperSource.NEW_USER;
  }

  bool get pharmacySubmitted => _pharmacySubmitted;

  set pharmacySubmitted(bool value) {
    _pharmacySubmitted = value;
    notifyListeners();
  }

  void setProvinceValue(int value) {
    provinceValue = value;
    notifyListeners();
  }

  void setProvincePop(String value) {
    provinceClick = value;
    notifyListeners();
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
