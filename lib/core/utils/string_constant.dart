import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ApplicationConstant {
  static const String PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=com.pocketpills";
  static const String APPLE_STORE_URL = "https://apps.apple.com/ca/app/pocketpills/id1367442074";
  static const String REVIEW_GOOGLE_FORM_URL = "https://docs.google.com/forms/d/e/1FAIpQLScvL5jy7Lv4hXEqA8XrG72rZ70hzG0vqMdQ4PB6L2y856Dq5A/viewform";

  static const String YOU_TUBE_APIKEY = "AIzaSyBTfcd2OLzkuk_p9ytyvgdnf6nTORAvkrA";

  static const String COUNTRY_CODE = "+1";
  static const String INVITATION_CODE_FAIL = "We could not find your details in our system, please upload your insurance and we will take care of the rest.";

  static const apiError = "Something went wrong.Please try again later";

  static const kGoogleApiKey = "AIzaSyD_kmhoDKK-Ee53SlNUH6TrG0MIsZIJkSE";

  static const fax_number = "1-888-329-6979";
  static const care_number = "1-855-950-7225";
}

//Sign Up
class SignUpModuleConstant {
  static const int PHONE_NUMBER_LENGTH = 10;

  static const String signUpHeading = "A Bit About You";
  static const String signUpDescription = "We want to know a bit about you so we can provide you with the best care possible.";
  static const String errMessage = 'Invalid username/password.';
  static const String blankFieldError = 'Required*';
  static const String incorrectValueError = 'Provide a valid value';
  static const String dateError = 'Invalid Date';
  static const String tncError = 'Please accept terms of use';
  static const int daysInTwentyFiveYear = 9130;
  static const int daysInSixtyFiveYear = 23740;
  static const int daysInHundredYear = 36524;

  static const String pocketpills_t_and_C = "https://pocketpills.com/terms-of-use";
  static const String pocketpills_privacy_policy = "https://www.pocketpills.com/privacy-policy";
}

String getPrivacyPolicy() {
  if (getSelectedLanguage() == ViewConstants.languageIdEn) {
    return "https://www.pocketpills.com/privacy-policy";
  } else {
    return "https://www.pocketpills.com/fr/privacy-policy";
  }
}

String getTermsAndCondtions() {
  if (getSelectedLanguage() == ViewConstants.languageIdEn) {
    return "https://pocketpills.com/terms-of-use";
  } else {
    return "https://www.pocketpills.com/fr/terms-of-use";
  }
}

//Referral
class ReferralModuleConstant {
  static const String referralSharedMessageTitle = "Refer & Earn on PocketPills";
}

class PhoneNumberStateConstant {
  static String LOGIN = "LOGIN";
  static String SIGNUP = "SIGNUP";
  static String FORGOT = "FORGOT";
}

class PaymentMode {
  static const String MASTERCARD = "MASTERCARD";
  static const String DISCOVER = "DISCOVER";
  static const String JCB = "JCB";
  static const String AMEX = "AMEX";
  static const String MAESTRO = "MAESTRO";
  static const String MY_HSA = "MY_HSA";
}

class PrescriptionTypeConstant {
  static String UPLOAD = "UPLOAD";
  static String TRANSFER = "TRANSFER";
  static String SIGNUP = "SIGNUP";
  static String REFILL = "REFILL";
  static String TELEHEALTH = "TELEHEALTH";
}
