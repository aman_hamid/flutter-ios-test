import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/signup_email_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class SignUpEmailModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();
  ConnectivityResult connectivityResult;
  AsyncMemoizer<Map<String, dynamic>> contentMemoizer = AsyncMemoizer();

  Future<bool> updateEmail(String email) async {
    setState(ViewState.Busy);
    String userId = dataStore.getUserId().toString();
    SignUpEmailRequest emailRequest = SignUpEmailRequest(email: email);
    var response = await _api.updateUserAlmostDone(RequestWrapper(body: emailRequest), userId);
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      dataStore.writeString(DataStoreService.EMAIL, email);
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateInvitationCode(String invitationCode) async {
    setState(ViewState.Busy);
    var response = await _api.activateInvitationCode(invitationCode);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (res.errMessage == ApplicationConstant.INVITATION_CODE_FAIL) {
        setState(ViewState.Busy);
        var response = await _api.postReferralCode(invitationCode);
        setState(ViewState.Idle);
        if (response != null) {
          OldBaseResponse res = OldBaseResponse.fromJson(response.data);
          if (!res.status) {
            errorMessage = res.getErrorMessage();
            return false;
          } else {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.referral_code_applied);
            return true;
          }
        } else {
          return false;
        }
      } else {
        errorMessage = res.getErrorMessage();
        setState(ViewState.Idle);
        return true;
      }
    } else {
      setState(ViewState.Idle);
      return false;
    }
  }

  Future<Map<String, dynamic>> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String> keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic> localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = List<LocalizationRequest>();
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
