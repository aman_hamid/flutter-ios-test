import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class LoginArguments {
  final String snackbarMessage;
  final String phone;
  final BaseStepperSource source;
  LoginArguments({this.snackbarMessage, this.phone, this.source});
}
