import 'dart:core';
import 'package:json_annotation/json_annotation.dart';
part 'order_document.g.dart';

@JsonSerializable()
class OrderDocument {
  String documentPath;
  String thumbnailPath;
  String documentType;

  factory OrderDocument.fromJson(Map<String, dynamic> json) =>
      _$OrderDocumentFromJson(json);

  Map<String, dynamic> toJson() => _$OrderDocumentToJson(this);

  OrderDocument(this.documentPath, this.thumbnailPath, this.documentType);
}
