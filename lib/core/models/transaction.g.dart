// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Transaction _$TransactionFromJson(Map<String, dynamic> json) {
  return Transaction(
    id: json['id'] as int,
    orderId: json['orderId'] as String,
    creditCard: json['creditCard'] == null
        ? null
        : PaymentCard.fromJson(json['creditCard'] as Map<String, dynamic>),
    meta: json['meta'] as String,
    paymentSuccessTransaction: json['paymentSuccessTransaction'] as bool,
    refundSuccessTransaction: json['refundSuccessTransaction'] as bool,
    success: json['success'] as bool,
    transactionAmount: json['transactionAmount'] as num,
    transactionType: json['transactionType'] as String,
  );
}

Map<String, dynamic> _$TransactionToJson(Transaction instance) =>
    <String, dynamic>{
      'id': instance.id,
      'orderId': instance.orderId,
      'meta': instance.meta,
      'creditCard': instance.creditCard,
      'paymentSuccessTransaction': instance.paymentSuccessTransaction,
      'refundSuccessTransaction': instance.refundSuccessTransaction,
      'success': instance.success,
      'transactionAmount': instance.transactionAmount,
      'transactionType': instance.transactionType,
    };
