import 'package:pocketpills/core/models/address.dart';

class AddressDetailArguments {
  final Address address;

  AddressDetailArguments({this.address});
}
