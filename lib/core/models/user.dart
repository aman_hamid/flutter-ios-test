import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'user.g.dart';

@JsonSerializable()

class User extends BaseRequest{
  int phone;
  String email;
  int countryCode;
  String firstName;
  String lastName;
  String gender;
  bool isMailVerified;
  int birthDate;
  String locale;
  String province;
  bool isCaregiver;
  bool hasDailyMedication;
  String passwordRequestToken;
  Map<String,dynamic> userContact;
  int id;
  bool disabled;
  bool verified;
  bool hasPassword;

  User({this.phone, this.email, this.countryCode, this.firstName,this.lastName, this.gender, this.isMailVerified, this.birthDate, this.locale, this.province, this.isCaregiver, this.hasDailyMedication, this.passwordRequestToken, this.userContact, this.id, this.disabled, this.verified, this.hasPassword});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

}