import 'package:flutter/material.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/success/base_success_screen.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';
import 'package:pocketpills/utils/analytics.dart';

class OrderStepperSuccessScreen extends StatelessWidget {
  static const String routeName = '0rderStepperSuccessScreen';
  String from;
  OrderStepperSuccessScreen({this.from});
  final Analytics analyticsEvents = locator<Analytics>();

  @override
  Widget build(BuildContext context) {
    if (null != from && from != 'telehealth') {
      analyticsEvents.sendAnalyticsEvent("consultation_intermediate_success");
    }
    return BaseSuccessScreen(
      source: BaseStepperSource.ORDER_STEPPER,
      transactionSuccessEnum: TransactionSuccessEnum.ORDER_CHECKOUT_SUCCESS,
    );
  }
}
