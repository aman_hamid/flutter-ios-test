import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';

part 'update_shopping_cart_request.g.dart';

@JsonSerializable(explicitToJson: true)
class UpdateShoppingCartRequest extends BaseRequest {
  int addressId;
  int ccId;
  String coupon;
  bool removeCoupon;
  String comment;

  UpdateShoppingCartRequest({this.addressId, this.ccId, this.coupon, this.removeCoupon, this.comment});

  factory UpdateShoppingCartRequest.fromJson(Map<String, dynamic> json) => _$UpdateShoppingCartRequestFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateShoppingCartRequestToJson(this);
}
