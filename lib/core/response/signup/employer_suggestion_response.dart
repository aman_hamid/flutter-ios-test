import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/signup/suggested_employers.dart';

part 'employer_suggestion_response.g.dart';

@JsonSerializable()
class EmployerSuggestionResponse{

  List<SuggestedEmployers> suggestedEmployers;
  int alreadyMappedEmployers;
  int suggestionsValidCount;

  EmployerSuggestionResponse(this.suggestedEmployers, this.alreadyMappedEmployers,
      this.suggestionsValidCount);

  factory EmployerSuggestionResponse.fromJson(Map<String, dynamic> json) =>
      _$EmployerSuggestionResponseFromJson(json);
}