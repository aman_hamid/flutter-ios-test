import 'package:connectivity/connectivity.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/response/insurance/insurance_code_activation_details_response.dart';
import 'package:pocketpills/core/response/signup/suggested_employers.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/utils/chambers/chambers_redirect.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';

class SignupWidget extends StatefulWidget {
  static const routeName = 'signup';

  final Function onSuccess;
  final Function onBack;
  final Function(String) onShowSnackBar;
  final BaseStepperSource source;
  bool chambersFlow;

  SignupWidget(
      {Key key,
      this.onBack,
      this.onSuccess,
      this.onShowSnackBar,
      this.source = BaseStepperSource.UNKNOWN_SCREEN,
      this.chambersFlow})
      : super(key: key);

  @override
  SignupState createState() => SignupState();
}

class SignupState extends BaseState<SignupWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();

  String prevMobileNumber = "";
  String _mmValue;
  String _ddValue;
  String _yyyyValue;
  String _invitationCode;

  final TextEditingController _firstnameController = TextEditingController();
  final TextEditingController _lastnameController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();
  final TextEditingController _monthController = TextEditingController();
  final TextEditingController _yearController = TextEditingController();

  final FocusNode _firstNameFnode = FocusNode();
  final FocusNode _lastNameFnode = FocusNode();
  final FocusNode _dateFnode = FocusNode();
  final FocusNode _monthFnode = FocusNode();
  final FocusNode _yearFnode = FocusNode();

  static bool firstName = false,
      lastName = false,
      month = false,
      day = false,
      year = false,
      password = false,
      email = false,
      referralCode = false;

  final _formKey = GlobalKey<FormState>();
  List<bool> tileStates = [false, false, false, false];

  bool autovalidate = false;
  bool phAutoValidate = false;
  bool showDateError = false;
  bool isTnCAccepted = true;
  bool isInOnboardingFlow = false;
  bool isInOnboardingFlowSuccess = false;
  static bool gender = false;

  int selectedRadio = -1;

  @override
  void initState() {
    super.initState();

    _monthController.addListener(monthControllerListener);
    _monthController.selection = TextSelection.fromPosition(
        TextPosition(offset: _monthController.text.length));
    _firstnameController.addListener(firstNameListener);
    _firstnameController.selection = TextSelection.fromPosition(TextPosition(
        offset: _firstnameController.text.length > 0
            ? _firstnameController.text.length
            : 0));
    _lastnameController.addListener(lastNameListener);
    _lastnameController.selection = TextSelection.fromPosition(TextPosition(
        offset: _lastnameController.text.length > 0
            ? _lastnameController.text.length
            : 0));
    _dateController.addListener(dateControllerListener);
    _dateController.selection = TextSelection.fromPosition(
        TextPosition(offset: _dateController.text.length));
    _yearController.addListener(yearControllerListener);
    _yearController.selection = TextSelection.fromPosition(
        TextPosition(offset: _yearController.text.length));
  }

  @override
  void dispose() {
    _monthController.removeListener(monthControllerListener);
    _firstnameController.removeListener(firstNameListener);
    _lastnameController.removeListener(lastNameListener);
    _dateController.removeListener(dateControllerListener);
    _yearController.removeListener(yearControllerListener);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _firstnameController.text =
        dataStore.readString(DataStoreService.FIRSTNAME);
    _lastnameController.text = dataStore.readString(DataStoreService.LASTNAME);
    String dob = dataStore.readString(DataStoreService.DATE_OF_BIRTH);
    String invitationCode =
        dataStore.readString(DataStoreService.INVITATION_CODE);
    invitationCode != null ? _invitationCode = invitationCode : "";
    List<String> dobString = (dob != null) ? dob.split("-") : [];
    if (dobString.length == 3) {
      _yearController.text = dobString[0];
      _monthController.text = dobString[1];
      _dateController.text = dobString[2];
    }
//    genderRadioGroup.initialValue = dataStore.readString(DataStoreService.GENDER);
  }

  firstNameListener() {
    if (firstName == false) {
      firstName = true;
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.au_firstname_entered);
    }
  }

  lastNameListener() {
    if (lastName == false) {
      lastName = true;
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.au_lastname_entered);
    }
  }

  void monthControllerListener() {
    if (_monthController.text.length == 2 &&
        _mmValue != _monthController.text) {
      _mmValue = _monthController.text;
      String validationError = validateMonth(_mmValue, showErr: true);
      if (validationError != null) {
        setDateErrorState(true);
      } else
        FocusScope.of(context).requestFocus(_dateFnode);
      if (evaluateDate()) setDateErrorState(false);
    }
    if (month == false) {
      month = true;
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.au_dob_month_entered);
    }
    _mmValue = _monthController.text;
  }

  void dateControllerListener() {
    if (_dateController.text.length == 2 && _ddValue != _dateController.text) {
      _ddValue = _dateController.text;
      String validationError = validateDate(_ddValue, showErr: true);
      if (validationError != null) {
        setDateErrorState(true);
      } else
        FocusScope.of(context).requestFocus(_yearFnode);
      if (evaluateDate()) setDateErrorState(false);
    }
    if (day == false) {
      day = true;
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.au_dob_date_entered);
    }
    _ddValue = _dateController.text;
  }

  void yearControllerListener() {
    if (_yearController.text.length == 4 &&
        _yyyyValue != _yearController.text) {
      _yyyyValue = _yearController.text;
      String validationError = validateYear(_yyyyValue, showErr: true);
      if (validationError != null) {
        setDateErrorState(true);
      } else if (evaluateDate()) setDateErrorState(false);
    }
    if (year == false) {
      year = true;
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.au_dob_year_entered);
    }
    _yyyyValue = _yearController.text;
  }

  bool evaluateDate() {
    if (validateMonth(_mmValue, showErr: true) == null &&
        validateDate(_ddValue, showErr: true) == null &&
        validateYear(_yyyyValue, showErr: true) == null)
      return true;
    else
      return false;
  }

  Widget getStartedFlow(SignUpModel model) {
    return Padding(
        padding: EdgeInsets.only(
          left: MEDIUM_XXX,
          right: MEDIUM_XXX,
          top: REGULAR_XXX,
        ),
        child: Column(
          children: <Widget>[
            // PPTexts.getMainViewHeading(SignUpModuleConstant.signUpHeading),
            Center(
              child: Avatar(
                  displayImage:
                      "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
            ),
            SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: PPUIHelper.HorizontalSpaceMedium),
              child: Text(
                LocalizationUtils.getSingleValueString(
                    "signup", "signup.about.description"),
                style: TextStyle(
                  color: lightBlue,
                  fontWeight: FontWeight.w500,
                  fontSize: 16.0,
                  height: 1.5,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            // SizedBox(height: PPUIHelper.VerticalSpaceXLarge),
            // PPTexts.getSecondaryHeading(SignUpModuleConstant.signUpDescription, isBold: false),
            SizedBox(height: REGULAR_XXX),
            //getNameField(),
            getCombinedNameField(),
            SizedBox(height: MEDIUM_X),
            getDOB(model),
            SizedBox(height: MEDIUM_X),
//            getGenderUI(),
            SizedBox(height: LARGE_X),
          ],
        ));
  }

  Widget getCurrentWidget(SignUpModel model) {
    if (isInOnboardingFlowSuccess == false && isInOnboardingFlow == false) {
      return getStartedFlow(model);
    } else if (isInOnboardingFlowSuccess == false) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
              left: MEDIUM_XXX,
              right: MEDIUM_XXX,
              top: REGULAR_XXX,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  LocalizationUtils.getSingleValueString(
                      "signup", "signup.employer.title"),
                  style: REGULAR_XXX_PRIMARY_BOLD,
                ),
                SizedBox(
                  height: SMALL_XX,
                ),
                Text(
                  LocalizationUtils.getSingleValueString(
                      "signup", "signup.employer.description"),
                  style: MEDIUM_XX_SECONDARY,
                ),
              ],
            ),
          ),
          SizedBox(
            height: SMALL_XX,
          ),
          getDosageTiles(model)
        ],
      );
    } else {
      List<ListTile> benefitsList = [];
      InsuranceCodeActivationDetailsResponse response =
          model.insuranceCodeActivationDetailsResponse;
      if (response.insuranceCodeActivationDetails.addedDependents != null &&
          response.insuranceCodeActivationDetails.addedDependents == true) {
        benefitsList.add(ListTile(
          leading: Icon(
            Icons.check_circle_outline,
            color: successColor,
          ),
          title: Text(
            LocalizationUtils.getSingleValueString(
                "signup", "signup.employer-success.members-details-employer"),
            style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
          ),
        ));
      }

      benefitsList.add(ListTile(
        leading: Icon(
          Icons.check_circle_outline,
          color: successColor,
        ),
        title: Text(
          LocalizationUtils.getSingleValueString(
              "signup", "signup.employer-success.insurance-details"),
          style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
        ),
      ));

      if (response.insuranceCodeActivationDetails.addedAddress != null &&
          response.insuranceCodeActivationDetails.addedAddress == true) {
        benefitsList.add(ListTile(
          leading: Icon(
            Icons.check_circle_outline,
            color: successColor,
          ),
          title: Text(
            LocalizationUtils.getSingleValueString(
                "signup", "signup.employer-success.address-details"),
            style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
          ),
        ));
      }

      benefitsList.add(ListTile(
        leading: Icon(
          Icons.check_circle_outline,
          color: successColor,
        ),
        title: Text(
          LocalizationUtils.getSingleValueString(
              "signup", "signup.employer-success.coverage-details"),
          style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
        ),
      ));

      return Container(
        margin:
            EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              height: MEDIUM_XXX,
            ),
            Image.asset(
              "graphics/benefits_found.png",
              height: MediaQuery.of(context).size.height * 0.20,
            ),
            PPUIHelper.verticalSpaceMedium(),
            Text(
              LocalizationUtils.getSingleValueString(
                  "signup", "signup.employer.info-found"),
              style: REGULAR_XXX_PRIMARY_BOLD,
            ),
            SizedBox(
              height: SMALL_XX,
            ),
            Text(
              LocalizationUtils.getSingleValueString(
                  "signup", "signup.employer-success.description-employer"),
              style: MEDIUM_XXX_SECONDARY,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: MEDIUM_XXX,
            ),
            Column(
              children: benefitsList,
            )
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) =>
          SignUpModel(httpApiUtils: Provider.of<HttpApiUtils>(context)),
      child: Consumer<SignUpModel>(
          builder: (BuildContext context, SignUpModel model, Widget child) {
        return FutureBuilder(
            future: myFutureMethodOverall(model, context),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(model);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Future myFutureMethodOverall(SignUpModel model, BuildContext context) async {
    Future<Map<String, dynamic>> future1 = model.getLocalization([
      "signup",
      "forgot",
      "common",
      "modal",
      "modals",
      "copay"
    ]); // will take 3 secs
    return await Future.wait([future1]);
  }

  Widget getMainView(SignUpModel model) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Image.asset('graphics/logo-horizontal-dark.png',
            width: MediaQuery.of(context).size.width * 0.45),
        centerTitle: true,
      ),
      body: GestureDetector(
          onTap: () {
            SystemChannels.textInput.invokeMethod('TextInput.hide');
          },
          child: Column(
            children: <Widget>[
              Expanded(
                child: Stack(children: <Widget>[
                  ListView(
                    children: <Widget>[
                      Form(key: _formKey, child: getCurrentWidget(model)),
                      SizedBox(
                        height: LARGE_XX,
                      )
                    ],
                  ),
                  model.provinceAgeCheck
                      ? Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(MEDIUM_X),
                                    topLeft: Radius.circular(MEDIUM_X)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: REGULAR,
                                  ),
                                ],
                                color: Colors.white),
                            child: Padding(
                              padding: const EdgeInsets.only(right: SMALL_XXX),
                              child: Row(
                                children: <Widget>[
                                  Image.asset(
                                    "graphics/onterio.png",
                                    height: LARGE_XX,
                                  ),
                                  Expanded(
                                    child: Text(
                                      model.discountText,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: MEDIUM_X_SECONDARY,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      : PPContainer.emptyContainer()
                ]),
              ),
              Builder(
                  builder: (BuildContext context) =>
                      PPBottomBars.getButtonedBottomBar(
                        child: model.state == ViewState.Busy
                            ? ViewConstants.progressIndicator
                            : getSignUpButton(context, model),
                      ))
            ],
          )),
    );
  }

  Widget getDOB(SignUpModel model) {
    return Column(
      children: <Widget>[
        PPTexts.getFormLabel(LocalizationUtils.getSingleValueString(
            "signup", "signup.fields.dob")),
        SizedBox(
          height: 3,
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Text(
            LocalizationUtils.getSingleValueString(
                "signup", "signup.fields.dob-helptext"),
            style: TextStyle(fontSize: 12, color: secondaryColor),
          ),
        ),
        SizedBox(height: 8),
        Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: PPFormFields.getNumericFormField(
                        autovalidate: autovalidate,
                        keyboardType: TextInputType.phone,
                        maxLength: 2,
                        focusNode: _monthFnode,
                        textInputAction: TextInputAction.next,
                        onTextChanged: (value) {
                          model.checkProvinceDiscount(value,
                              _dateController.text, _yearController.text);
                          //_formKey.currentState.validate();
                        },
                        validator: (value) =>
                            validateMonth(value, showErr: true),
                        decoration: PPInputDecor.getDecoration(
                                collapsed: false,
                                labelText:
                                    LocalizationUtils.getSingleValueString(
                                        "signup", "signup.fields.dob-month"),
                                hintText:
                                    LocalizationUtils.getSingleValueString(
                                        "signup", "signup.fields.dob-month"))
                            .copyWith(
                          contentPadding: EdgeInsets.only(
                              left: 6, right: 0, top: 0, bottom: 0),
                          errorStyle: TextStyle(height: 1),
                          errorMaxLines: 4,
                        ),
                        controller: _monthController,
                        onFieldSubmitted: (value) {
                          _fieldFocusChange(context, _monthFnode, _dateFnode);
                        }),
                  ),
                  SizedBox(width: MEDIUM_XXX),
                  Expanded(
                    flex: 1,
                    child: PPFormFields.getNumericFormField(
                        autovalidate: autovalidate,
                        keyboardType: TextInputType.phone,
                        maxLength: 2,
                        onTextChanged: (value) {
                          //_formKey.currentState.validate();
                          model.checkProvinceDiscount(_monthController.text,
                              value, _yearController.text);
                        },
                        validator: (value) =>
                            validateDate(value, showErr: true),
                        decoration: PPInputDecor.getDecoration(
                                collapsed: false,
                                labelText:
                                    LocalizationUtils.getSingleValueString(
                                        "signup", "signup.fields.dob-date"),
                                hintText:
                                    LocalizationUtils.getSingleValueString(
                                        "signup", "signup.fields.dob-date"))
                            .copyWith(
                          contentPadding: EdgeInsets.only(
                              left: 6, right: 0, top: 0, bottom: 0),
                          errorStyle: TextStyle(height: 1),
                          errorMaxLines: 4,
                        ),
                        controller: _dateController,
                        textInputAction: TextInputAction.next,
                        focusNode: _dateFnode,
                        onFieldSubmitted: (value) {
                          _fieldFocusChange(context, _dateFnode, _yearFnode);
                        }),
                  ),
                ],
              ),
            ),
            SizedBox(width: MEDIUM_XXX),
            Expanded(
              flex: 2,
              child: PPFormFields.getNumericFormField(
                  autovalidate: autovalidate,
                  keyboardType: TextInputType.phone,
                  maxLength: 4,
                  onTextChanged: (value) {
                    //_formKey.currentState.validate();
                    model.checkProvinceDiscount(
                        _monthController.text, _dateController.text, value);
                  },
                  validator: (value) => validateYear(value, showErr: true),
                  decoration: PPInputDecor.getDecoration(
                          collapsed: false,
                          labelText: LocalizationUtils.getSingleValueString(
                              "signup", "signup.fields.dob-year"),
                          hintText: LocalizationUtils.getSingleValueString(
                              "signup", "signup.fields.dob-year"))
                      .copyWith(
                    contentPadding:
                        EdgeInsets.only(top: 0, bottom: 0, left: 6, right: 90),
                    errorStyle: TextStyle(height: 1),
                    errorMaxLines: 4,
                  ),
                  controller: _yearController,
                  textInputAction: TextInputAction.done,
                  focusNode: _yearFnode,
                  onFieldSubmitted: (value) {
                    onSignUpPressed(context, model);
                  }),
            ),
          ],
        ),
      ],
    );
  }

  String getErrorText() {
    if ((_monthController.text.isEmpty) &&
        (_dateController.text.isEmpty && _yearController.text.isEmpty)) {
      return LocalizationUtils.getSingleValueString(
              "common", "common.label.required") +
          "*";
    } else {
      return LocalizationUtils.getSingleValueString(
          "signup", "signup.fields.dob-error");
    }
  }

  setDateErrorState(bool showError) {
    setState(() {
      showDateError = showError;
    });
  }

  Widget getCombinedNameField() {
    return Row(
      children: <Widget>[
        Expanded(
          child: PPFormFields.getTextField(
              autovalidate: autovalidate,
              textCapitalization: TextCapitalization.words,
              textInputAction: TextInputAction.next,
              controller: _firstnameController,
              focusNode: _firstNameFnode,
              onErrorStr: LocalizationUtils.getSingleValueString(
                      "common", "common.label.required") +
                  "*",
              onTextChanged: (value) {
                // _formKey.currentState.validate();
              },
              decoration: PPInputDecor.getDecoration(
                  labelText: LocalizationUtils.getSingleValueString(
                      "signup", "signup.fields.firstname"),
                  hintText: LocalizationUtils.getSingleValueString(
                      "signup", "signup.fields.firstname")),
              onFieldSubmitted: (value) {
                _fieldFocusChange(context, _firstNameFnode, _lastNameFnode);
              }),
        ),
        SizedBox(
          width: MEDIUM_XXX,
        ),
        Expanded(
          child: PPFormFields.getTextField(
              autovalidate: autovalidate,
              textCapitalization: TextCapitalization.words,
              focusNode: _lastNameFnode,
              controller: _lastnameController,
              onTextChanged: (value) {
                //_formKey.currentState.validate();
              },
              textInputAction: TextInputAction.next,
              onErrorStr: LocalizationUtils.getSingleValueString(
                      "common", "common.label.required") +
                  "*",
              decoration: PPInputDecor.getDecoration(
                  labelText: LocalizationUtils.getSingleValueString(
                      "signup", "signup.fields.lastname"),
                  hintText: LocalizationUtils.getSingleValueString(
                      "signup", "signup.fields.lastname")),
              onFieldSubmitted: (value) {
                _fieldFocusChange(context, _lastNameFnode, _monthFnode);
              }),
        ),
      ],
    );
  }

  Widget getNameField() {
    return Column(
      children: <Widget>[
        PPFormFields.getTextField(
            autovalidate: autovalidate,
            textCapitalization: TextCapitalization.words,
            textInputAction: TextInputAction.next,
            controller: _firstnameController,
            focusNode: _firstNameFnode,
            onErrorStr: LocalizationUtils.getSingleValueString(
                    "common", "common.label.required") +
                "*",
            decoration: PPInputDecor.getDecoration(
                labelText: LocalizationUtils.getSingleValueString(
                    "signup", "signup.fields.firstname"),
                hintText: LocalizationUtils.getSingleValueString(
                    "signup", "signup.fields.firstname")),
            onFieldSubmitted: (value) {
              _fieldFocusChange(context, _firstNameFnode, _lastNameFnode);
            }),
        SizedBox(height: MEDIUM_XXX),
        PPFormFields.getTextField(
            autovalidate: autovalidate,
            textCapitalization: TextCapitalization.words,
            focusNode: _lastNameFnode,
            controller: _lastnameController,
            onErrorStr: LocalizationUtils.getSingleValueString(
                    "common", "common.label.required") +
                "*",
            textInputAction: TextInputAction.next,
            decoration: PPInputDecor.getDecoration(
                labelText: LocalizationUtils.getSingleValueString(
                    "signup", "signup.fields.lastname"),
                hintText: LocalizationUtils.getSingleValueString(
                    "signup", "signup.fields.lastname")),
            onFieldSubmitted: (value) {
              _fieldFocusChange(context, _lastNameFnode, _monthFnode);
            }),
      ],
    );
  }

  getSignUpButton(context, model) {
    if (isInOnboardingFlowSuccess == false && isInOnboardingFlow == false) {
      return Row(
        children: <Widget>[
          SecondaryButton(
            text: LocalizationUtils.getSingleValueString(
                "signup", "signup.transfer.back"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
          PrimaryButton(
              text: LocalizationUtils.getSingleValueString(
                  "signup", "signup.employer.next"),
              onPressed: () {
                onSignUpPressed(context, model);
              })
        ],
      );
    } else if (isInOnboardingFlowSuccess == false) {
      return Row(
        children: <Widget>[
          SecondaryButton(
            text: LocalizationUtils.getSingleValueString(
                "signup", "signup.employer.skip"),
            onPressed: () {
              analyticsEvents.sendAnalyticsEvent(
                  AnalyticsEventConstant.account_employer_skip);
              gotoNextScreen(context);
            },
          ),
          SizedBox(width: PPUIHelper.HorizontalSpaceMedium),
          PrimaryButton(
            text: LocalizationUtils.getSingleValueString(
                "signup", "signup.employer.next"),
            onPressed: selectedRadio != -1
                ? () async {
                    analyticsEvents.sendAnalyticsEvent(
                        AnalyticsEventConstant.account_employer_submit);
                    bool result =
                        await model.updateInvitationCode(_invitationCode);
                    if (result == true) {
                      isInOnboardingFlowSuccess = true;
                      onFail(context, errMessage: model.errorMessage);
                    } else {
                      onFail(context, errMessage: model.errorMessage);
                      await Future.delayed(Duration(seconds: 2));
                      gotoNextScreen(context);
                    }
                  }
                : null,
          )
        ],
      );
    } else {
      return PrimaryButton(
          fullWidth: true,
          text: LocalizationUtils.getSingleValueString(
              "signup", "signup.employer-success.looks-good"),
          onPressed: () {
            analyticsEvents.sendAnalyticsEvent(
                AnalyticsEventConstant.account_employer_onboard_success);
            gotoNextScreen(context);
          });
    }
  }

  void gotoNextScreen(context) {
    if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
      Navigator.pushNamed(context, ChambersRedirect.routeName());
    }
    Navigator.pushNamed(context, TransferWidget.routeName,
        arguments: TransferArguments(source: BaseStepperSource.NEW_USER));
  }

  Widget getTnCCheckbox() {
    TapGestureRecognizer _onTapTandC = TapGestureRecognizer()
      ..onTap = () async {
        if (await canLaunch("https://pocketpills.com/terms-of-use")) {
          await launch("https://pocketpills.com/terms-of-use");
        }
      };
    TapGestureRecognizer _onTapPrivacyPolicy = TapGestureRecognizer()
      ..onTap = () async {
        if (await canLaunch("https://pocketpills.com/terms-of-use")) {
          await launch("https://pocketpills.com/terms-of-use");
        }
      };
    TextStyle ts = TextStyle(
      fontSize: PPUIHelper.FontSizeSmall,
      color: secondaryColor,
      height: 1.4,
    );
    TextStyle tsLink = TextStyle(
      fontSize: PPUIHelper.FontSizeSmall,
      color: secondaryColor,
      height: 1.4,
      decoration: TextDecoration.underline,
    );
    return RichText(
      text: TextSpan(
        text: 'By signing up, I agree to ',
        style: ts,
        children: <TextSpan>[
          TextSpan(
              text: 'Terms of Use', style: tsLink, recognizer: _onTapTandC),
          TextSpan(text: ' and ', style: ts),
          TextSpan(
              text: "Privacy Policy",
              style: tsLink,
              recognizer: _onTapPrivacyPolicy),
        ],
      ),
    );
  }

  String validateMonth(String value, {bool showErr}) {
    if (value == null || value.isEmpty)
      return showErr
          ? LocalizationUtils.getSingleValueString(
                  "common", "common.label.required") +
              "*"
          : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr
          ? LocalizationUtils.getSingleValueString(
              "forgot", "forgot.all.valid-value")
          : "";
    } else if (n < 1 || n > 12) {
      return showErr
          ? LocalizationUtils.getSingleValueString(
              "forgot", "forgot.all.valid-value")
          : "";
    }
    return null;
  }

  String validateDate(String value, {bool showErr}) {
    if (value == null || value.isEmpty)
      return showErr
          ? LocalizationUtils.getSingleValueString(
                  "common", "common.label.required") +
              "*"
          : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr
          ? LocalizationUtils.getSingleValueString(
              "forgot", "forgot.all.valid-value")
          : "";
    } else if (n < 1 || n > 31) {
      return showErr
          ? LocalizationUtils.getSingleValueString(
              "forgot", "forgot.all.valid-value")
          : "";
    }
    return null;
  }

  String validateYear(String value, {bool showErr}) {
    if (value == null || value.isEmpty)
      return showErr
          ? LocalizationUtils.getSingleValueString(
                  "common", "common.label.required") +
              "*"
          : "";
    final n = num.tryParse(value);
    if (n == null) {
      return showErr
          ? LocalizationUtils.getSingleValueString(
              "forgot", "forgot.all.valid-value")
          : "";
    } else if (n < 1900 || n > 2030) {
      return showErr
          ? LocalizationUtils.getSingleValueString(
              "forgot", "forgot.all.valid-value")
          : "";
    }
    return null;
  }

  isDateValid() {
    if (_dateController.text.isNotEmpty &&
        _monthController.text.isNotEmpty &&
        _yearController.text.isNotEmpty) {
      final birthday = DateTime(
          int.parse(_yearController.text ?? ""),
          int.parse(_monthController.text ?? ""),
          int.parse(_dateController.text ?? ""));
      final date2 = DateTime.now();
      final difference = date2.difference(birthday).inDays;
      if (difference >= 6935) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  onSignUpPressed(context, SignUpModel signUpModel) async {
    if ((_monthController.text.isEmpty) ||
        (_dateController.text.isEmpty || _yearController.text.isEmpty)) {
      setDateErrorState(true);
    }
    setState(() {
      autovalidate = true;
    });
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    var connectivityResult = await signUpModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: signUpModel.noInternetConnection);
      return;
    }

    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_signup);

    if (!_formKey.currentState.validate()) {
      onFail(context,
          errMessage: LocalizationUtils.getSingleValueString(
              "signup", "signup.employer.fill-details"));
    } else if (isDateValid() == false) {
      onFail(context,
          errMessage: LocalizationUtils.getSingleValueString(
              "signup", "signup.fields.19-old-error"));
    } else {
      await SmsAutoFill().listenForCode;
      var success = await signUpModel.signup(
        firstName: _firstnameController.text,
        lastName: _lastnameController.text,
        date: _dateController.text,
        month: _monthController.text,
        year: _yearController.text,
      );
      if (success == true) {
        analyticsEvents.sendAnalyticsEvent(
            AnalyticsEventConstant.account_about_you_verification);
        bool checkEmployerSuggestion =
            await signUpModel.getSuggestedEmployers();
        if (checkEmployerSuggestion == true) {
          if (signUpModel.employerSuggestionResponse.suggestionsValidCount >
              0) {
            isInOnboardingFlow = true;
            analyticsEvents.sendAnalyticsEvent(
                AnalyticsEventConstant.account_employer_list);
          } else {
            gotoNextScreen(context);
          }
        } else {
          gotoNextScreen(context);
          dataStore.writeBoolean(DataStoreService.COMPLETE_ABOUT_YOU, true);
        }
      } else {
        analyticsEvents.sendAnalyticsEvent(
            AnalyticsEventConstant.account_about_you_verification_failed);
        onFail(context, errMessage: signUpModel.errorMessage);
      }
    }
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }

  Widget getDosageTiles(SignUpModel model) {
    List<Widget> tiles = [];
    for (int i = 0;
        i < model.employerSuggestionResponse?.suggestedEmployers?.length;
        i++) {
      SuggestedEmployers suggestedEmployer =
          model.employerSuggestionResponse?.suggestedEmployers[i];
      tiles.add(RadioListTile(
        title: Text(
          suggestedEmployer.employerDisplayName,
          style: MEDIUM_XXX_PRIMARY_BOLD,
        ),
        value: i,
        groupValue: selectedRadio,
        onChanged: (val) {
          analyticsEvents.sendAnalyticsEvent(
              AnalyticsEventConstant.account_employer_entered);
          if (suggestedEmployer.invitationCodeDataSet != null &&
              suggestedEmployer.invitationCodeDataSet.length > 0) {
            _invitationCode =
                suggestedEmployer.invitationCodeDataSet[0].invitationCode;
          } else {
            _invitationCode = suggestedEmployer.employerName;
          }
          setSelectedRadio(val);
        },
      ));
    }
    return Container(
        child: Column(
      children: tiles,
    ));
  }
}
