import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/viewmodels/consent_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class CopayRequestView extends StatefulWidget {
  static const routeName = 'copayRequestView';

  Medicine medicine;
  int quantity;

  CopayRequestView({this.medicine, this.quantity});

  @override
  _CopayRequestViewState createState() => _CopayRequestViewState();
}

class _CopayRequestViewState extends BaseState<CopayRequestView> {
  Medicine medicine;
  TextEditingController _quantityController = new TextEditingController();
  int selectedRadio;

  @override
  void initState() {
    super.initState();
    this.medicine = widget.medicine;
    _quantityController.text = widget.quantity?.toString();
    selectedRadio = 1;
    ConsentModel model = ConsentModel();
    getLanguageData(model);
  }

  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
      print("Radio $val");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString("copay", "copay.copay-request.appbar-title"),
        appBar: AppBar(),
      ),
      bottomNavigationBar: Builder(
          builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                  child: Container(
                color: Colors.white,
                child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  SecondaryButton(
                    text: LocalizationUtils.getSingleValueString("common", "common.button.back"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
                  PrimaryButton(
                    text: LocalizationUtils.getSingleValueString("common", "common.button.continue"),
                    onPressed: () {
                      if (selectedRadio == 1) {
                        Navigator.pushNamed(context, UploadPrescription.routeName,
                            arguments: UploadPrescriptionArguments(source: BaseStepperSource.COPAY_REQUEST, medicineName: medicine.name, quantity: widget.quantity));
                      } else {
                        Navigator.pushNamed(context, TransferWidget.routeName,
                            arguments: TransferArguments(source: BaseStepperSource.COPAY_REQUEST, medicineName: medicine.name, quantity: widget.quantity));
                      }
                    },
                  )
                ]),
              ))),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: REGULAR_XXX,
                    ),
                    Flexible(
                        child: Text(
                      LocalizationUtils.getSingleValueString("copay", "copay.copay-request.title"),
                      style: REGULAR_XXX_PRIMARY_BOLD,
                    )),
                    Text(
                      LocalizationUtils.getSingleValueString("copay", "copay.copay-request.description"),
                      style: MEDIUM_XXX_SECONDARY,
                    ),
                    SizedBox(
                      height: REGULAR_XXX,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              medicine.description ?? "",
                              style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                            ),
                            Text(
                              medicine.name ?? "",
                              style: MEDIUM_XXX_PRIMARY_BOLD,
                            ),
                          ],
                        ),
                        Image.network(medicine.getDrugTypeSource(), height: LARGE)
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: MEDIUM_XXX,
              ),
              Padding(
                padding: const EdgeInsets.only(left: MEDIUM_XXX, right: MEDIUM_XXX, bottom: MEDIUM_XXX),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          LocalizationUtils.getSingleValueString("copay", "copay.copay-request.quantity"),
                          style: MEDIUM_XX_PRIMARY_BOLD,
                        ),
                        medicine.description != null
                            ? Text(
                                LocalizationUtils.getSingleValueString("copay", "copay.copay-request.total-no-of") + ' ' + medicine.description,
                                style: MEDIUM_X_SECONDARY_BOLD_MEDIUM,
                              )
                            : SizedBox(
                                height: 0.0,
                              ),
                      ],
                    ),
                    SizedBox(
                      width: REGULAR_XXX,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                      child: Container(
                        color: Colors.white,
                        width: MediaQuery.of(context).size.width * 0.25,
                        height: 52,
                        child: PPFormFields.getNumericFormField(
                            textInputAction: TextInputAction.done,
                            maxLength: 3,
                            controller: _quantityController,
                            centeredText: true,
                            onTextChanged: (value) {},
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(4.0),
                                borderSide: BorderSide(color: brandColor),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(4.0),
                                borderSide: BorderSide(color: tertiaryColor),
                              ),
                              fillColor: Colors.white,
                            )),
                      ),
                    )
                  ],
                ),
              ),
              PPDivider(),
              SizedBox(
                height: MEDIUM_XXX,
              ),
              Padding(
                padding: const EdgeInsets.only(left: MEDIUM_XXX, bottom: SMALL_XXX),
                child: Text(
                  LocalizationUtils.getSingleValueString("copay", "copay.copay-request.select-preference") + ":",
                  style: MEDIUM_XX_SECONDARY_BOLD_MEDIUM,
                ),
              ),
              InkWell(
                onTap: () {
                  selectedRadio == 1 ? setSelectedRadio(2) : setSelectedRadio(1);
                },
                child: Row(
                  children: <Widget>[
                    Radio(
                      value: 1,
                      groupValue: selectedRadio,
                      activeColor: brandColor,
                      onChanged: (val) {
                        setSelectedRadio(val);
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: SMALL_XXX),
                      child: Text(
                        LocalizationUtils.getSingleValueString("copay", "copay.copay-request.upload-myprescription"),
                        style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: SMALL_XXX,
              ),
              InkWell(
                onTap: () {
                  selectedRadio == 1 ? setSelectedRadio(2) : setSelectedRadio(1);
                },
                child: Row(
                  children: <Widget>[
                    Radio(
                      value: 2,
                      groupValue: selectedRadio,
                      activeColor: brandColor,
                      onChanged: (val) {
                        print("Radio $val");
                        setSelectedRadio(val);
                      },
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(left: SMALL_XXX),
                        child: Text(
                          LocalizationUtils.getSingleValueString("copay", "copay.copay-request.transfer-pocketpills"),
                          style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void getLanguageData(ConsentModel model) {
  FutureBuilder(
      future: myFutureMethodOverall(model),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData != null && snapshot.data != null) {
          return null;
        } else if (snapshot.hasError) {
          return ErrorScreen();
        } else {
          return LoadingScreen();
        }
      });
}

Future myFutureMethodOverall(ConsentModel model) async {
  Future<Map<String, dynamic>> future1 = model.getLocalization(["copay", "common"]);
  return await Future.wait([future1]);
}
