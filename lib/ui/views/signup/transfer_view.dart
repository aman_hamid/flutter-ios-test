import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/transfer/pharmacy.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/utils/patient_utils.dart';
import 'package:pocketpills/core/viewmodels/prescription_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/ui/shared/chips/chiplist_sample.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/exit_dialog.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/start/splash_view.dart';
import 'package:pocketpills/ui/views/transfer/transfer_pharmacy_search_view.dart';
import 'package:pocketpills/ui/views/signup/telehealth_signup_bottom_sheet.dart';
import 'package:pocketpills/utils/chambers/chambers_redirect.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';
import 'package:flutter/gestures.dart';
import 'package:pocketpills/ui/shared/pp_avatar.dart';
import 'package:pocketpills/ui/shared/container/pp_container.dart'
    as QuebecContainer;

class TransferWidget extends StatefulWidget {
  static const routeName = 'transfer';
  final BaseStepperSource source;
  final UserPatient userPatient;
  final Function onSuccess;
  final Function onBack;
  final String transferAbTest;
  final String medicineName;
  final int quantity;
  bool chambersFlow = false;

  TransferWidget(
      {Key key,
      this.source = BaseStepperSource.MAIN_SCREEN,
      this.onBack,
      this.onSuccess,
      this.userPatient,
      this.transferAbTest,
      this.medicineName,
      this.quantity,
      this.chambersFlow = false})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return TransferState();
  }
}

class TransferState extends BaseState<TransferWidget> {
  final DataStoreService dataStore = locator<DataStoreService>();
  bool autovalidate = false;

  Key popularSearchKey;
  final double _horizontalSpacing = PPUIHelper.HorizontalSpaceMedium;

  final TextEditingController _commentsController = TextEditingController();

  bool newUserFlow = false;
  bool transferAll = true, pharmacySubmitted = false;
  bool pharmacyName = false,
      pharmacyAddress = false,
      pharmacyPhone = false,
      pharmacyComment = false;

  BuildContext innerContext;

  String province = "";
  String selectedPlaceId = '';
  ChoiceChips chipItem;
  @override
  void initState() {
    super.initState();
    _commentsController.addListener(pharmacyCommentListener);
    popularSearchKey = UniqueKey();
  }

  @override
  void dispose() {
    _commentsController.removeListener(pharmacyCommentListener);
    super.dispose();
  }

  pharmacyCommentListener() {
    if (pharmacyComment == false &&
        widget.source == BaseStepperSource.NEW_USER) {
      pharmacyComment = true;
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.transfer_comment_entered);
    }
  }

  UserPatient getUserPatientTuple() {
    UserPatient up = widget.userPatient != null
        ? widget.userPatient
        : Provider.of<DashboardModel>(context).selectedPatient;
    return up;
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return ExitDialog.show(context) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.source == BaseStepperSource.MAIN_SCREEN ||
              widget.source == BaseStepperSource.ADD_PATIENT ||
              widget.source == BaseStepperSource.CONSENT_FLOW
          ? () async {
              Navigator.pop(context);
              return true;
            }
          : widget.source == BaseStepperSource.NEW_USER
              ? () async {
                  Navigator.pop(context);
                  return true;
                }
              : () => _onBackPressed(context),
      child: ChangeNotifierProvider(
        create: (_) => SignUpTransferModel(
            source: widget.source,
            searchNearBy: true,
            isDialogDisplayed: false),
        child: Consumer<SignUpTransferModel>(builder:
            (BuildContext context, SignUpTransferModel model, Widget child) {
          province = model.quebecCheck;
          return FutureBuilder(
              future: myFutureMethodOverall(model),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getTopView(getName(), model, getTransferHeading(),
                      getTransferDescription(), getTransferButtonDescription());
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        }),
      ),
    );
  }

  Future myFutureMethodOverall(SignUpTransferModel signUpTransferModel) async {
    Future<Map<String, dynamic>> future1 = signUpTransferModel.getLocalization([
      "signup",
      "transfer",
      "dropoff",
      "modal",
      "common",
      "modals",
      "copay"
    ]);
    return await Future.wait([future1]);
  }

  String getTransferHeading() {
    return widget.source != BaseStepperSource.COPAY_REQUEST
        ? getTitleText(PatientUtils.getForGender(
            Provider.of<DashboardModel>(context).selectedPatient))
        : LocalizationUtils.getSingleValueString(
            "signup", "signup.transfer.title-employer");
  }

  String getTransferDescription() {
    return widget.source != BaseStepperSource.COPAY_REQUEST
        ? getDescriptionText(PatientUtils.getForGender(
            Provider.of<DashboardModel>(context).selectedPatient))
        : LocalizationUtils.getSingleValueString(
            "signup", "signup.transfer.description-employer");
  }

  String getTransferButtonDescription() {
    return LocalizationUtils.getSingleValueString(
        "signup", "signup.transfer.search-placeholder");
  }

  String getName() {
    String name = PatientUtils.getYouOrNameTitle(getUserPatientTuple());
    name = '${name[0].toUpperCase()}${name.substring(1)}';
    return name;
  }

  Widget getAppbar(String name, SignUpTransferModel model,
      String transferHeading, String transferDescription) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      return AppBar(
        backgroundColor: Colors.white,
        title: Image.asset('graphics/logo-horizontal-dark.png',
            width: MediaQuery.of(context).size.width * 0.45),
        centerTitle: true,
      );
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN ||
        widget.source == BaseStepperSource.ADD_PATIENT ||
        widget.source == BaseStepperSource.CONSENT_FLOW) {
      return InnerAppBar(
        titleText: name,
        appBar: AppBar(),
        leadingBackButton: () {
          if (widget.source == BaseStepperSource.ADD_PATIENT) {
            onSkipClick(model);
          } else {
            Navigator.of(context).pushNamedAndRemoveUntil(
                DashboardWidget.routeName, (Route<dynamic> route) => false);
          }
        },
      );
    }
    if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      return InnerAppBar(
        titleText: LocalizationUtils.getSingleValueString(
            "signup", "signup.transfer.title-copay-request"),
        appBar: AppBar(),
        leadingBackButton: () {
          Navigator.pop(context);
        },
      );
    }
  }

  Widget getTopView(
      String name,
      SignUpTransferModel model,
      String transferHeading,
      String transferDescription,
      String transferButtonDescription) {
    return BaseScaffold(
      appBar: getAppbar(name, model, transferHeading, transferDescription),
      bottomNavigationBar: getButton(model),
      body: Builder(
        builder: (BuildContext context) {
          innerContext = context;
          return Container(
            child: Builder(
              builder: (BuildContext context) {
                return getMainView(model, transferHeading, transferDescription,
                    transferButtonDescription);
              },
            ),
          );
        },
      ),
    );
  }

  Widget quebecContainer(bool val) {
    return val ? QuebecContainer.PPContainer() : Container();
  }

  Widget getMainView(SignUpTransferModel model, String transferHeading,
      String transferDescription, String transferButtonDescription) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(MEDIUM_XXX),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      //PPTexts.getMainViewHeading(transferHeading),
                      widget.source == BaseStepperSource.NEW_USER
                          ? pharmacistFlow()
                          : PPTexts.getMainViewHeading(transferHeading),
                      widget.source != BaseStepperSource.NEW_USER
                          ? normalFlow(transferDescription)
                          : Container(),
                      widget.source == BaseStepperSource.NEW_USER
                          ? SizedBox(
                              height: REGULAR_XXX,
                            )
                          : Container(),
                      dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) &&
                              widget.source == BaseStepperSource.NEW_USER
                          ? Align(
                              alignment: Alignment.center,
                              child: Column(
                                children: [
                                  FlatButton(
                                    child: Text(
                                      'NOT YOU?',
                                      style: TextStyle(
                                        color: brandColor,
                                      ),
                                    ),
                                    onPressed: () async {
                                      await Provider.of<SignUpModel>(context,
                                              listen: false)
                                          .logout();
                                      dataStore.writeBoolean(
                                          DataStoreService.CHAMBERS_FLOW,
                                          false);
                                      dataStore.writeBoolean(
                                          DataStoreService.REFRESH_DASHBOARD,
                                          true);
                                      Navigator.pushNamedAndRemoveUntil(
                                          context,
                                          SplashView.routeName,
                                          (Route<dynamic> route) => false);
                                    },
                                  ),
                                  SizedBox(height: SMALL_X),
                                ],
                              ),
                            )
                          : Container(),
                      GestureDetector(
                          onTap: () {
                            onClickPharmacySearchScreen(model, "");
                          },
                          child: Hero(
                              tag: 'hero-rectangle',
                              transitionOnUserGestures: true,
                              child: Container(
                                padding: const EdgeInsets.all(MEDIUM_X),
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: brandColor, width: 2),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(SMALL_XX),
                                    )),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      transferButtonDescription.toString(),
                                      style: MEDIUM_XXX_SECONDARY,
                                    ),
                                    Icon(
                                      Icons.search,
                                      color: secondaryColor,
                                    )
                                  ],
                                ),
                              ))),
                      SizedBox(
                        height: MEDIUM_XXX,
                      ),
                      Text(
                        "POPULAR SEARCHES",
                        style: MEDIUM_XXX_SECONDARY_BOLD_MEDIUM,
                      ),
                      SizedBox(
                        height: MEDIUM_XXX,
                      ),
                      Container(child: getChipAddView(model)),
                    ],
                  ),
                ),
              ),
              (checkPharmacySelectedDetails(model))
                  ? Container(
                      color: Color(0x60000000),
                    )
                  : PPContainer.emptyContainer()
            ],
          ),
        ),
        //(checkPharmacySelectedDetails(model)) ? Container() : showNearbyPharmacyListing(model),
        (checkPharmacySelectedDetails(model))
            ? Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius: REGULAR,
                        ),
                      ],
                      color: whiteColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(MEDIUM_XXX),
                        topRight: Radius.circular(MEDIUM_XXX),
                      )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          left: MEDIUM_XXX,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Text(
                                LocalizationUtils.getSingleValueString(
                                    "signup", "signup.transfer.selected-title"),
                                style: MEDIUM_XXX_PRIMARY_BOLD,
                              ),
                            ),
                            TransparentButton(
                              text: LocalizationUtils.getSingleValueString(
                                  "signup", "signup.transfer.selected-remove"),
                              onPressed: () {
                                model.clearPharmacyInfo();
                                analyticsEvents.sendAnalyticsEvent(
                                    AnalyticsEventConstant
                                        .click_transfer_remove_pharmacy);
                              },
                            )
                          ],
                        ),
                      ),
                      PPDivider(),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: MEDIUM_XXX, vertical: REGULAR_X),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              model.pharmacyName,
                              style: MEDIUM_XXX_BLACK_BOLD,
                            ),
                            (model.pharmacyPhoneNumber != null &&
                                    model.pharmacyPhoneNumber != '')
                                ? Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: SMALL,
                                      ),
                                      Text(
                                        model.pharmacyPhoneNumber,
                                        style: MEDIUM_XX_SECONDARY,
                                      ),
                                    ],
                                  )
                                : SizedBox(
                                    height: 0.0,
                                  ),
                            SizedBox(
                              height: SMALL,
                            ),
                            Text(
                              model.pharmacyAddress,
                              style: MEDIUM_XX_SECONDARY,
                            )
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          if (transferAll == true) {
                            setState(() {
                              transferAll = false;
                            });
                          } else {
                            setState(() {
                              transferAll = true;
                            });
                          }
                          analyticsEvents.sendAnalyticsEvent(
                              AnalyticsEventConstant
                                  .transfer_all_medication_entered);
                        },
                        child: Container(
                          color: headerBgColor,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Checkbox(
                                  value: transferAll,
                                  activeColor: brandColor,
                                  onChanged: (bool value) {
                                    setState(() {
                                      transferAll = !transferAll;
                                      analyticsEvents.sendAnalyticsEvent(
                                          AnalyticsEventConstant
                                              .transfer_all_medication_entered);
                                    });
                                  }),
                              Text(
                                LocalizationUtils.getSingleValueString("signup",
                                    "signup.transfer.transferall-label"),
                                style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
                              )
                            ],
                          ),
                        ),
                      ),
                      (province.contains('QUEBEC') == true)
                          ? quebecContainer(true)
                          : quebecContainer(false),
                      PPUIHelper.verticalSpaceMedium(),
                      transferAll ? Row() : getCommentsBox()
                    ],
                  ),
                ),
              )
            : (widget.source == BaseStepperSource.MAIN_SCREEN ||
                    widget.source == BaseStepperSource.COPAY_REQUEST)
                ? SizedBox(
                    height: 0.0,
                  )
                : model.state == ViewState.Busy
                    ? SizedBox(
                        height: 0.0,
                      )
                    : GestureDetector(
                        onTap: () {
                          widget.source == BaseStepperSource.ADD_PATIENT
                              ? onSkipClick(model)
                              : showCallAndChatBottomSheet(context, model);
                          //onSkipClick(model);
                        },
                        child: Container(
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: LARGE_XX),
                              child: Text(
                                LocalizationUtils.getSingleValueString(
                                    "signup", "signup.transfer.skip"),
                                style: MEDIUM_XXX_LINK_BOLD_MEDIUM,
                              ),
                            ),
                          ),
                        ))
      ],
    );
  }

  Widget getChipAddView(SignUpTransferModel model) {
    chipItem = ChoiceChips(
        key: popularSearchKey,
        chipName: model.popularPharmacies,
        selectedItem: (selected) {
          analyticsEvents.sendAnalyticsEvent(
              AnalyticsEventConstant.Transfer_search_popular_selected);
          setState(() {
            onClickPharmacySearchScreen(model, selected);
            print(selected + "test");
          });
        });
    return chipItem;
  }

  Widget pharmacistFlow() {
    return Column(
      children: [
        Center(
          child: Avatar(
              displayImage:
                  "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg"),
        ),
        SizedBox(height: PPUIHelper.VerticalSpaceXMedium),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: Text(
            dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW)
                ? LocalizationUtils.getSingleValueString(
                        "signup", "signup.transfer.description-personalised")
                    .replaceAll("{{name}}",
                        dataStore.readString(DataStoreService.FIRSTNAME))
                : LocalizationUtils.getSingleValueString(
                    "signup", "signup.transfer.description-pharmacist"),
            style: TextStyle(
              color: lightBlue,
              fontWeight: FontWeight.w500,
              fontSize: 16.0,
              height: 1.5,
            ),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  Widget normalFlow(String transferDescription) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PPUIHelper.verticalSpaceXSmall(),
        Text(
          transferDescription,
          style: MEDIUM_XXX_SECONDARY,
        ),
        SizedBox(
          height: REGULAR_XXX,
        )
      ],
    );
  }

  bool checkPharmacySelectedDetails(SignUpTransferModel model) {
    return model.pharmacyName != null &&
        model.pharmacyName != '' &&
        model.pharmacyAddress != null &&
        model.pharmacyAddress != '';
  }

  void onClickPharmacySearchScreen(
      SignUpTransferModel model, String PharmacyNamePopular) {
    Navigator.pushNamed(context, TransferPharmacySearchView.routeName,
        arguments: TransferArguments(
            source: widget.source,
            model: model,
            PharmacyNamePopular: PharmacyNamePopular));
    if (checkSignupFlow()) {
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_search);
    } else {
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.transfer_search);
    }
  }

  Widget showNearbyPharmacyListing(SignUpTransferModel model) {
    return Flexible(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          model.pharmacyPredictions.length > 0
              ? Padding(
                  padding: const EdgeInsets.all(MEDIUM_X),
                  child: Text(
                    LocalizationUtils.getSingleValueString(
                        "signup", "signup.transfer.nearby-title"),
                    style: MEDIUM_XXX_PRIMARY_BOLD_MEDIUM,
                  ),
                )
              : PPContainer.emptyContainer(),
          Container(
            height: 80,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: model.pharmacyPredictions.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  Pharmacy pharmacy = model.pharmacyPredictions[index];
                  return Padding(
                    padding: const EdgeInsets.only(left: MEDIUM_X, right: 0.0),
                    child: InkWell(
                      onTap: () {
                        onClickSelectPharmacy(model, pharmacy);
                      },
                      child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              border: Border.all(color: borderColor),
                              borderRadius: BorderRadius.circular(SMALL_XXX)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                width: MEDIUM_X,
                              ),
                              getAutoCompleteItem(pharmacy),
                              TransparentButton(
                                text: LocalizationUtils.getSingleValueString(
                                    "signup", "signup.transfer.nearby-select"),
                                onPressed: () {
                                  onClickSelectPharmacy(model, pharmacy);
                                },
                              )
                            ],
                          )),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  onClickSelectPharmacy(SignUpTransferModel model, Pharmacy pharmacy) async {
    pharmacySubmitted = true;
    PlaceDetails selectedPlace =
        await model.getPlaceDetails(pharmacy.pharmacyPlaceId);
    List<AddressComponent> addressComponents = selectedPlace.addressComponents;
    AddressComponent addressCompone;
    addressComponents.forEach((value) => {
          addressCompone = value,
          if (ViewConstants.province.containsKey(addressCompone.longName))
            {
              province =
                  ViewConstants.province[addressCompone.longName.toString()]
            },
        });
    if (selectedPlace != null) {
      selectedPlaceId = selectedPlace.placeId;
      model.pharmacyAddress = selectedPlace.formattedAddress;
      model.pharmacyName = selectedPlace.name;
      model.pharmacyPhoneNumber = selectedPlace.formattedPhoneNumber;
      model.province = province;
    }
    if (checkSignupFlow()) {
      analyticsEvents.sendAnalyticsEvent(
          AnalyticsEventConstant.au_transfer_nearby_selected);
    } else {
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.transfer_nearby_selected);
    }
  }

  Widget getCopayRequestBottomButton(SignUpTransferModel model) {
    return Builder(
        builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
                child: Container(
              color: Colors.white,
              child: model.state == ViewState.Busy
                  ? ViewConstants.progressIndicator
                  : Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                      SecondaryButton(
                        text: LocalizationUtils.getSingleValueString(
                            "signup", "signup.transfer.back"),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      SizedBox(width: PPUIHelper.HorizontalSpaceSmall),
                      PrimaryButton(
                        butttonColor: province.contains('QUEBEC') == false
                            ? brandColor
                            : Colors.grey.shade700,
                        text: LocalizationUtils.getSingleValueString(
                            "signup", "signup.upload.continue"),
                        onPressed: province.contains('QUEBEC') == false
                            ? () {
                                onTransferClick(context, model);
                              }
                            : () {},
                      )
                    ]),
            )));
  }

  String checkProvince(SignUpTransferModel model) {
    if (province.contains('QUEBEC') &&
        widget.source == BaseStepperSource.NEW_USER) {
      if (!model.isDialogDisplayed) {
        return LocalizationUtils.getSingleValueString(
            "common", "common.button.transfer");
      } else {
        return LocalizationUtils.getSingleValueString(
            "common", "common.button.next");
      }
    } else if (!province.contains('QUEBEC') &&
        widget.source == BaseStepperSource.NEW_USER) {
      return LocalizationUtils.getSingleValueString(
          "common", "common.button.next");
    }
    return LocalizationUtils.getSingleValueString(
        "common", "common.button.transfer");
  }

  Widget getTransferBottomButton(SignUpTransferModel model) {
    return Builder(
      builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
        child: model.state == ViewState.Busy
            ? ViewConstants.progressIndicator
            : Row(
                children: <Widget>[
                  PrimaryButton(
                      text: (!checkPharmacySelectedDetails(model))
                          ? LocalizationUtils.getSingleValueString(
                              "signup", "signup.transfer.transfer")
                          : checkProvince(model),
                      onPressed: (!checkPharmacySelectedDetails(model))
                          ? () {
                              onTransferClick(context, model);
                            }
                          : (checkPharmacySelectedDetails(model) &&
                                  !province.contains('QUEBEC'))
                              ? () {
                                  onTransferClick(context, model);
                                }
                              : (widget.source != BaseStepperSource.NEW_USER ||
                                      (province.contains('QUEBEC') == true &&
                                          !model.isDialogDisplayed))
                                  ? () {
                                      model.updateDialogDisplayed(true);
                                      showGeneralDialog(
                                          barrierLabel: "Label",
                                          barrierDismissible: true,
                                          barrierColor:
                                              Colors.black.withOpacity(0.5),
                                          transitionDuration:
                                              Duration(milliseconds: 700),
                                          context: context,
                                          pageBuilder: (context, anim1, anim2) {
                                            return Align(
                                              alignment: Alignment.topCenter,
                                              child: Container(
                                                child: Material(
                                                  borderRadius:
                                                      BorderRadius.circular(40),
                                                  color: Colors.red.shade200,
                                                  child: Padding(
                                                    padding: EdgeInsets.all(20),
                                                    child: Text(
                                                      'Coming to Quebec Soon!',
                                                      style: TextStyle(
                                                          fontSize: 16.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.white),
                                                    ),
                                                  ),
                                                ),
                                                margin: EdgeInsets.only(
                                                    top: 100,
                                                    left: 12,
                                                    right: 12),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.rectangle,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            40),
                                                    border: Border.all(
                                                        color:
                                                            Colors.red.shade400,
                                                        style:
                                                            BorderStyle.solid,
                                                        width: 2)),
                                              ),
                                            );
                                          });
                                    }
                                  : () {
                                      if (widget.source ==
                                              BaseStepperSource.NEW_USER &&
                                          model.isDialogDisplayed) {
                                        onTransferClick(context, model);
                                      }
                                    })
                ],
              ),
      ),
    );
  }

  Widget getButton(SignUpTransferModel model) {
    if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      return getCopayRequestBottomButton(model);
    } else {
      return getTransferBottomButton(model);
    }
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(SMALL_XXX),
              topRight: Radius.circular(SMALL_XXX)),
        ),
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          top: _horizontalSpacing,
                          left: PPUIHelper.HorizontalSpaceMedium),
                      child: PPTexts.getHeading(
                          LocalizationUtils.getSingleValueString(
                              "transfer", "transfer.all.sample-photo")),
                    ),
                    PPUIHelper.verticalSpaceMedium(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          'graphics/transfer_refill/transfer_refill_1.png',
                          height: 240,
                          width: (MediaQuery.of(context).size.width - 50) / 2,
                        ),
                        PPUIHelper.horizontalSpaceSmall(),
                        Image.asset(
                          'graphics/transfer_refill/transfer_refill_2.png',
                          height: 240,
                          width: (MediaQuery.of(context).size.width - 50) / 2,
                        )
                      ],
                    ),
                  ],
                ),
                PPUIHelper.verticalSpaceMedium(),
                PPDivider(),
                Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: _horizontalSpacing,
                        vertical: _horizontalSpacing),
                    child: Container(
                        child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(SMALL_XX),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.11),
                                      offset: Offset(0, 2),
                                      blurRadius: 2.0),
                                ]),
                            child: OutlineButton(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(color: tertiaryColor),
                                  borderRadius: BorderRadius.circular(SMALL_XX),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                  width: double.infinity,
                                  alignment: Alignment(0, 0),
                                  child: Text(
                                    LocalizationUtils.getSingleValueString(
                                        "signup",
                                        "signup.transfer.current-pharmacy-close"),
                                  ),
                                ))))),
              ],
            ),
          );
        });
  }

  Widget getAutoCompleteItem(Pharmacy item) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          item.pharmacyName ?? "",
          style: TextStyle(
              fontSize: PPUIHelper.FontSizeMedium,
              color: primaryColor,
              fontWeight: FontWeight.w500),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.4,
          child: Text(
            item.pharmacyAddress ?? "",
            maxLines: 2,
            style: TextStyle(
                height: 1.4,
                fontSize: PPUIHelper.FontSizeSmall,
                color: secondaryColor,
                fontWeight: FontWeight.normal),
          ),
        ),
      ],
    );
  }

  Widget getCommentsBox() {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: _horizontalSpacing),
        child: PPFormFields.getMultiLineTextField(
          controller: _commentsController,
          decoration: PPInputDecor.getDecoration(
              labelText: LocalizationUtils.getSingleValueString(
                  "modals", "modals.order.medications"),
              hintText: LocalizationUtils.getSingleValueString(
                  "signup", "signup.transfer.list-pahramacy-info")),
        ));
  }

  onSkipClick(SignUpTransferModel model) async {
    if (widget.source == BaseStepperSource.NEW_USER ||
        widget.source == BaseStepperSource.ADD_PATIENT) {
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_skipped);
      bool success = await model.uploadPrescriptionSignUp();
      print("here");
      if (success == true) {
        print("success");
        dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, true);
        goToNextScreen(model);
      }
    } else
      Navigator.pop(context);
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      analyticsEvents
          .sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      if (dataStore.readBoolean(DataStoreService.CHAMBERS_FLOW) == true) {
        if (ChambersRedirect.routeName().contains('signUpAlmostDoneWidget')) {
          Navigator.pushNamed(context, ChambersRedirect.routeName(),
              arguments: SignupStepperArguments(
                  source: BaseStepperSource.NEW_USER, chambersFlow: true));
        } else {
          Navigator.pushNamed(context, ChambersRedirect.routeName());
        }
      }
      Navigator.pushNamed(context, SignUpAlmostDoneWidget.routeName,
          arguments:
              SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName,
          arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName,
          arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(
              source: BaseStepperSource.TRANSFER_SCREEN,
              successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName,
          arguments: SourceArguments(source: widget.source));
    }
  }

  showCallAndChatBottomSheet(BuildContext context, SignUpTransferModel model) {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return SkipSignupSheet(
              context, model, widget.source, widget.userPatient, "");
        });
  }

  onTransferClick(
      BuildContext context, SignUpTransferModel signUpTransferModel) async {
    bool success;

    var connectivityResult = await signUpTransferModel.checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      onFail(context, errMessage: signUpTransferModel.noInternetConnection);
      return;
    }

    if (signUpTransferModel.pharmacyName != '' &&
        signUpTransferModel.pharmacyAddress != '' &&
        signUpTransferModel.pharmacyName != null &&
        signUpTransferModel.pharmacyAddress != null) {
      if (!transferAll && _commentsController.text == '') {
        onFail(context,
            errMessage: LocalizationUtils.getSingleValueString(
                "transfer", "transfer.all.list-medications"));
        return;
      }

      success = await signUpTransferModel.uploadPrescription(
          widget.userPatient,
          selectedPlaceId,
          signUpTransferModel.pharmacyName != null
              ? signUpTransferModel.pharmacyName
              : "",
          signUpTransferModel.pharmacyAddress != null
              ? signUpTransferModel.pharmacyAddress
              : "",
          getFormatterPhoneNumber(
              signUpTransferModel.pharmacyPhoneNumber != null
                  ? signUpTransferModel.pharmacyPhoneNumber
                  : "1111111111"),
          transferAll,
          transferAll
              ? null
              : (_commentsController.text != null
                  ? _commentsController.text
                  : ""),
          checkSignupFlow(),
          null,
          signUpTransferModel.province != null
              ? signUpTransferModel.province
              : "",
          widget.medicineName,
          widget.quantity);

      if (success == true) {
        Provider.of<PrescriptionModel>(context).clearData();
        dataStore.writeBoolean(DataStoreService.TRANSFER_DONE, true);
        goToNextScreen(signUpTransferModel);
        if (checkSignupFlow()) {
          analyticsEvents.sendInitiatedCheckoutEvent();
          analyticsEvents.sendAnalyticsEvent(
              AnalyticsEventConstant.account_transfer_new_formfilled);
        } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
          analyticsEvents.sendInitiatedCheckoutEvent();
          analyticsEvents.sendAnalyticsEvent(
              AnalyticsEventConstant.member_transfer_refills_formfilled);
          analyticsEvents.sendAnalyticsEvent(
              AnalyticsEventConstant.member_congratulations);
        }
      } else {
        onFail(context,
            errMessage: LocalizationUtils.getSingleValueString(
                "common", "common.label.error-message"));
      }
    } else {
      setState(() {
        autovalidate = true;
      });
      Navigator.pushNamed(context, TransferPharmacySearchView.routeName,
          arguments: TransferArguments(
              source: BaseStepperSource.MAIN_SCREEN,
              model: signUpTransferModel));
      if (checkSignupFlow()) {
        analyticsEvents
            .sendAnalyticsEvent(AnalyticsEventConstant.au_transfer_search);
      } else {
        analyticsEvents
            .sendAnalyticsEvent(AnalyticsEventConstant.transfer_search);
      }
    }
  }

  bool checkSignupFlow() {
    return widget.source == BaseStepperSource.NEW_USER;
  }

  int getFormatterPhoneNumber(String phoneNumber) {
    try {
      String formattedNumber = phoneNumber
          .replaceAll("\(", "")
          .replaceAll("\)", "")
          .replaceAll(" ", "")
          .replaceAll("-", "")
          .replaceAll("+", "");
      return int.parse(formattedNumber);
    } catch (ec) {
      return -1;
    }
  }

  onBack() {
    return () async {
      if (widget.onBack != null) widget.onBack();
    };
  }

  onSuccess() {
    return () async {
      if (widget.onSuccess != null) widget.onSuccess();
    };
  }
}

String getDescriptionText(String gender) {
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString(
          "transfer", "transfer.all.description-MALE");
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString(
          "transfer", "transfer.all.description-FEMALE");
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString(
          "transfer", "transfer.all.description-other");
      break;
    default:
      return LocalizationUtils.getSingleValueString(
          "transfer", "transfer.all.description");
      break;
  }
}

String getTitleText(String gender) {
  switch (gender) {
    case "MALE":
      return LocalizationUtils.getSingleValueString(
          "transfer", "transfer.all.title-MALE");
      break;
    case "FEMALE":
      return LocalizationUtils.getSingleValueString(
          "transfer", "transfer.all.title-FEMALE");
      break;
    case "OTHER":
      return LocalizationUtils.getSingleValueString(
          "transfer", "transfer.all.title-other");
      break;
    default:
      return LocalizationUtils.getSingleValueString(
          "transfer", "transfer.all.title");
      break;
  }
}
