import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/response/signup/transaction_successful_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';

class TransactionSuccessfulModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<TransactionSuccessfulResponse> _Memoizer = AsyncMemoizer();
  Future<TransactionSuccessfulResponse> _futurePrescription;
  ConnectivityResult connectivityResult;
  TransactionSuccessfulResponse successfulResponse;

  Future<TransactionSuccessfulResponse> getTransactionSuccessData(TransactionSuccessEnum transactionSuccessEnum,
      {int memberPatientId}) async {
    _futurePrescription = this._Memoizer.runOnce(() async {
      return await this.getSuccessScreenData(transactionSuccessEnum, memberPatientId);
    });
    return _futurePrescription;
  }

  clearAsyncMemoizer() {
    _Memoizer = AsyncMemoizer();
    notifyListeners();
  }

  Future<TransactionSuccessfulResponse> getSuccessScreenData(
      TransactionSuccessEnum transactionSuccessEnum, int memberPatientId) async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }
    setState(ViewState.Busy);
    var response;
    switch (transactionSuccessEnum) {
      case TransactionSuccessEnum.SIGN_UP_SUCCESS:
        response = await _api.getSignUpSuccessData();
        break;
      case TransactionSuccessEnum.VITAMINS_SUCCESS:
        response = await _api.getVitaminSuccessData();
        break;
      case TransactionSuccessEnum.ADD_MEMBER_SUCCESS:
        response = await _api.getAddMemberSuccessData(memberPatientId);
        break;
      case TransactionSuccessEnum.ORDER_CHECKOUT_SUCCESS:
        response = await _api.getOrderCheckoutSuccessData();
        break;
      case TransactionSuccessEnum.COPAY_REQUEST_SUCCESS:
        response = await _api.getCopayRequestSuccessData();
        break;
    }

    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<TransactionSuccessfulResponse> baseResponse = BaseResponse.fromJson(response.data);
      if (!baseResponse.status) {
        errorMessage = baseResponse.getErrorMessage();
        return null;
      }
      successfulResponse = baseResponse.response;
      return baseResponse.response;
    } else {
      return null;
    }
  }
}
