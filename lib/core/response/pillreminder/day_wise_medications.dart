import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
import 'package:pocketpills/utils/route/dashboard_route.dart';
part 'day_wise_medications.g.dart';

@JsonSerializable()
class DayWiseMedications {
  String date;
  String firstName;
  String title;
  int missedCount;
  int takenCount;
  int pendingCount;
  List<PocketPackDetails> pocketPackDetails;

  DayWiseMedications(this.date, this.firstName, this.title, this.missedCount, this.takenCount, this.pendingCount,
      this.pocketPackDetails);

  factory DayWiseMedications.fromJson(Map<String, dynamic> json) => _$DayWiseMedicationsFromJson(json);
}
