import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/medicine.dart';
import 'package:pocketpills/core/viewmodels/vitamins/vitamins_catalog_model.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/buttons/secondary_button.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class VitaminsCatalogDetailsWidget extends StatefulWidget {
  static const routeName = 'vitaminsCatalogDetails';

  @override
  _VitaminsCatalogDetailsWidgetState createState() => _VitaminsCatalogDetailsWidgetState();
}

class _VitaminsCatalogDetailsWidgetState extends BaseState<VitaminsCatalogDetailsWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<VitaminsCatalogModel>(
      builder: (BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Widget child) {
        return FutureBuilder(
            future: vitaminsCatalogModel.getLocalization(["vitamins"]),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return _onVitaminsCatalogLoad(context, vitaminsCatalogModel.vitaminsCatalogList[vitaminsCatalogModel.medicineId], vitaminsCatalogModel);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      },
    );
  }

  Widget _onVitaminsCatalogLoad(BuildContext context, Medicine medicine, VitaminsCatalogModel vitaminsCatalogModel) {
    return BaseScaffold(
      appBar: InnerAppBar(titleText: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.title-details"), appBar: AppBar()),
      bottomNavigationBar: Builder(
        builder: (BuildContext context) => PPBottomBars.getButtonedBottomBar(
          child: vitaminsCatalogModel.state == ViewState.Busy
              ? ViewConstants.progressIndicator
              : Row(
                  children: <Widget>[
                    SecondaryButton(
                      text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.back"),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    SizedBox(width: MEDIUM_XXX),
                    (medicine.isInShoppingCart != null && medicine.isInShoppingCart == true)
                        ? PrimaryButton(
                            text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.remove"),
                            onPressed: () {
                              removeVitaminsToCart(context, vitaminsCatalogModel, medicine);
                            },
                          )
                        : PrimaryButton(
                            text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.add-pack"),
                            onPressed: () {
                              addVitaminsToCart(context, vitaminsCatalogModel, medicine);
                            },
                          )
                  ],
                ),
        ),
      ),
      body: Builder(
        builder: (BuildContext context) => Container(
          child: Builder(
            builder: (BuildContext context) {
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: SMALL_XXX,
                    ),
                    Padding(
                      padding: EdgeInsets.all(MEDIUM_XXX),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            medicine.drugName,
                            style: REGULAR_XXX_PRIMARY_BOLD,
                          ),
                          SizedBox(height: MEDIUM),
                          Row(
                            children: <Widget>[
                              CachedNetworkImage(
                                placeholder: (context, url) => CircularProgressIndicator(
                                  strokeWidth: 2,
                                ),
                                imageUrl: medicine.benefitsHeadlineIconUrl,
                                height: MEDIUM_XX,
                              ),
                              SizedBox(
                                width: SMALL_XXX,
                              ),
                              Text(
                                medicine?.benefitsHeadline ?? "",
                                style: MEDIUM_X_PINK,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    PPDivider(),
                    medicine.recommendedDailyDosage != null
                        ? Column(
                            children: <Widget>[
                              SizedBox(height: MEDIUM_X),
                              Padding(
                                padding: const EdgeInsets.only(left: MEDIUM_XXX),
                                child: RichText(
                                  text: TextSpan(
                                    text: LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.recommended") + ": ",
                                    style: MEDIUM_XX_SECONDARY,
                                    children: <TextSpan>[
                                      TextSpan(text: medicine.recommendedDailyDosage, style: MEDIUM_XX_PRIMARY_BOLD),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: MEDIUM_X),
                              PPDivider(),
                            ],
                          )
                        : SizedBox(
                            height: 0.0,
                          ),
                    Padding(
                      padding: EdgeInsets.all(MEDIUM_XXX),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          medicine.benefits != null
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.benefits"),
                                      style: MEDIUM_XXX_PRIMARY_BOLD,
                                    ),
                                    SizedBox(height: SMALL_X),
                                    Text(
                                      medicine.benefits ?? "",
                                      style: MEDIUM_XX_SECONDARY,
                                    ),
                                  ],
                                )
                              : SizedBox(
                                  height: 0.0,
                                ),
                          medicine.naturalSources != null
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(height: MEDIUM_XXX),
                                    Text(
                                      LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.natural-sources"),
                                      style: MEDIUM_XXX_PRIMARY_BOLD,
                                    ),
                                    SizedBox(height: SMALL_X),
                                    Text(
                                      medicine.naturalSources ?? "",
                                      style: MEDIUM_XX_SECONDARY,
                                    ),
                                  ],
                                )
                              : SizedBox(
                                  height: 0.0,
                                ),
                          medicine.supportingStudies != null
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(height: MEDIUM_XXX),
                                    Text(
                                      LocalizationUtils.getSingleValueString("vitamins", "vitamins.product.supporting-studies"),
                                      style: MEDIUM_XXX_PRIMARY_BOLD,
                                    ),
                                    SizedBox(height: SMALL_X),
                                    Text(
                                      medicine.supportingStudies ?? "",
                                      style: MEDIUM_XX_SECONDARY,
                                    ),
                                  ],
                                )
                              : SizedBox(
                                  height: 0.0,
                                ),
                        ],
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void addVitaminsToCart(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Medicine medicine) async {
    bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
      return;
    }
    vitaminsCatalogModel.addMedicineToShoppingCart(medicine);
    Navigator.pop(context);
  }

  void removeVitaminsToCart(BuildContext context, VitaminsCatalogModel vitaminsCatalogModel, Medicine medicine) async {
    bool connectivityResult = await vitaminsCatalogModel.isInternetConnected();
    if (connectivityResult == false) {
      onFail(context, errMessage: vitaminsCatalogModel.noInternetConnection);
      return;
    }
    vitaminsCatalogModel.removeMedicineFromShoppingCart(medicine);
  }
}
