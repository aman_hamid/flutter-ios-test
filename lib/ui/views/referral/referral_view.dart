import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pocketpills/core/response/referral_response.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/referral/referral_contact_sync_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class ReferralView extends StatefulWidget {
  static const routeName = 'referral';

  @override
  _ReferralViewState createState() => _ReferralViewState();
}

class _ReferralViewState extends BaseState<ReferralView> {
  String description;
  List<String> howItWorks;
  List<String> termAndCondition;
  @override
  Widget build(BuildContext context) {
    return Consumer<HomeModel>(
      builder: (BuildContext context, HomeModel homeModel, Widget child) {
        return FutureBuilder(
          future: myFutureMethodOverall(homeModel, context),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData != null && snapshot.data != null) {
              return getMainVew(homeModel, context, snapshot);
            } else if (snapshot.hasError) {
              return ErrorScreen();
            } else {
              return LoadingScreen();
            }
          },
        );
      },
    );
  }

  Future myFutureMethodOverall(HomeModel homeModel, BuildContext context) async {
    Future<ReferralResponse> future1 = homeModel.fetchReferralData(Provider.of<DashboardModel>(context).selectedPatientId);
    Future<Map<String, dynamic>> future2 = homeModel.getLocalization(["referral", "navbar"]);
    return await Future.wait([future1, future2]);
  }

  Widget getMainVew(HomeModel homeModel, BuildContext context, AsyncSnapshot snapshot) {
    setLocalizationData();
    return Scaffold(
        appBar: InnerAppBar(
            titleText: LocalizationUtils.getSingleValueString("navbar", "navbar.common.refer-title"),
            appBar: AppBar(),
            leadingBackButton: () {
              Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
            }),
        body: Builder(
            builder: (context) => SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          decoration: BoxDecoration(color: Color(0xfff7f7f7)),
                          child: Padding(
                              padding: EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
                              child: Column(children: <Widget>[
                                PPUIHelper.verticalSpaceXSmall(),
                                Image.asset(
                                  'graphics/referral/referral_group.png',
                                  width: double.infinity,
                                ),
                                PPUIHelper.verticalSpaceMedium(),
                                PPTexts.getMainViewHeading(LocalizationUtils.getSingleValueString("referral", "referral.all.title")),
                                PPTexts.getSecondaryHeading(description, isBold: false),
                                PPUIHelper.verticalSpaceMedium(),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(LocalizationUtils.getSingleValueString("referral", "referral.all.your-referral"),
                                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16.0, color: primaryColor)),
                                ),
                                PPUIHelper.verticalSpaceXMedium(),
                                getReferralContainer(context, snapshot.requireData[0].referralCode),
                                PPUIHelper.verticalSpaceXMedium(),
                              ]))),
                      Container(
                          child: Padding(
                              padding: EdgeInsets.only(left: PPUIHelper.HorizontalSpaceMedium, right: PPUIHelper.HorizontalSpaceMedium, top: PPUIHelper.HorizontalSpaceSmall),
                              child: Column(
                                children: <Widget>[
                                  PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("referral", "referral.howitworks.title")),
                                  ListView.builder(
                                    primary: false,
                                    shrinkWrap: true,
                                    itemCount: howItWorks.length,
                                    itemBuilder: (BuildContext ctxt, int index) {
                                      return Container(
                                        constraints: BoxConstraints(maxWidth: double.infinity),
                                        child: Column(children: <Widget>[
                                          PPUIHelper.verticalSpaceSmall(),
                                          Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text((index + 1).toString() + ". ", style: TextStyle(height: 1.4, color: secondaryColor, fontSize: PPUIHelper.FontSizeMedium)),
                                              Flexible(
                                                  child: Text(
                                                howItWorks[index],
                                                maxLines: 5,
                                                style: TextStyle(height: 1.4, color: secondaryColor, fontSize: PPUIHelper.FontSizeMedium),
                                              ))
                                            ],
                                          )
                                        ]),
                                      );
                                    },
                                  ),
                                  PPUIHelper.verticalSpaceMedium(),
                                  PPTexts.getSecondaryHeading(LocalizationUtils.getSingleValueString("referral", "referral.terms.title")),
                                  ListView.builder(
                                    primary: false,
                                    shrinkWrap: true,
                                    itemCount: termAndCondition.length,
                                    itemBuilder: (BuildContext ctxt, int index) {
                                      return Container(
                                        constraints: BoxConstraints(maxWidth: double.infinity),
                                        child: Column(children: <Widget>[
                                          PPUIHelper.verticalSpaceSmall(),
                                          Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text((index + 1).toString() + ". ", style: TextStyle(height: 1.4, color: secondaryColor, fontSize: PPUIHelper.FontSizeMedium)),
                                              Flexible(
                                                  child: Text(
                                                termAndCondition[index],
                                                maxLines: 5,
                                                style: TextStyle(height: 1.4, color: secondaryColor, fontSize: PPUIHelper.FontSizeMedium),
                                              ))
                                            ],
                                          )
                                        ]),
                                      );
                                    },
                                  ),
                                  PPUIHelper.verticalSpaceLarge()
                                ],
                              ))),
                    ],
                  ),
                )));
  }

  Widget getReferralContainer(BuildContext context, String referralCode) {
    return Container(
      decoration: BoxDecoration(color: Colors.white, shape: BoxShape.rectangle, borderRadius: BorderRadius.all(Radius.circular(4.0))),
      child: DottedBorder(
          dashPattern: [6, 4],
          strokeWidth: 1,
          color: brandColor,
          child: Padding(
              padding: EdgeInsets.only(left: PPUIHelper.HorizontalSpaceLarge, right: PPUIHelper.HorizontalSpacexSmall),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    referralCode,
                    textAlign: TextAlign.left,
                    style: TextStyle(color: brandColor, fontSize: PPUIHelper.FontSizeXLarge, fontWeight: FontWeight.bold),
                  ),
                  Row(
                    children: <Widget>[
                      TransparentButton(
                        text: LocalizationUtils.getSingleValueString("referral", "referral.all.copy"),
                        bgColor: Colors.white,
                        onPressed: () {
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_referral_code_copy);
                          Clipboard.setData(new ClipboardData(text: referralCode));
                          showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("referral", "referral.all.copied-clipboard"));
                        },
                      ),
                      TransparentButton(
                        text: LocalizationUtils.getSingleValueString("referral", "referral.all.share"),
                        bgColor: Colors.white,
                        onPressed: () {
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_referral_code_share);
                          Navigator.pushNamed(context, ReferralContactSyncView.routeName);
                        },
                      )
                    ],
                  ),
                ],
              ))),
    );
  }

  void setLocalizationData() {
    description = LocalizationUtils.getSingleValueString("referral", "referral.all.desc-1") + ' ' + LocalizationUtils.getSingleValueString("referral", "referral.all.des-2");

    howItWorks = [
      LocalizationUtils.getSingleValueString("referral", "referral.howitworks.point-1"),
      LocalizationUtils.getSingleValueString("referral", "referral.howitworks.point-2"),
      LocalizationUtils.getSingleValueString("referral", "referral.howitworks.point-3"),
    ];
    termAndCondition = [
      LocalizationUtils.getSingleValueString("referral", "referral.terms.point-1"),
      LocalizationUtils.getSingleValueString("referral", "referral.terms.point-2"),
      LocalizationUtils.getSingleValueString("referral", "referral.terms.point-3"),
      LocalizationUtils.getSingleValueString("referral", "referral.terms.point-4"),
      LocalizationUtils.getSingleValueString("referral", "referral.terms.point-5"),
      LocalizationUtils.getSingleValueString("referral", "referral.terms.point-6"),
    ];
  }
}
