import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/viewmodels/profile/add_credit_card_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class PaymentDetailWidget extends StatefulWidget {
  static const routeName = 'paymentdetail';

  PaymentDetailWidget({
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return PaymentDetailState();
  }
}

class PaymentDetailState extends BaseState<PaymentDetailWidget> {
  bool defaultValue = true;

  final HttpApi _api = locator<HttpApi>();

  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  Future<void> addMonerisToken(token) async {
    var res = await _api.addMonerisToken(token);
    if (res != null) {
      Navigator.of(context).pop(true);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider<AddCreditCardModel>(create: (_) => AddCreditCardModel())],
      child: Consumer<AddCreditCardModel>(
        builder: (BuildContext context, AddCreditCardModel creditCardModel, Widget child) {
          return FutureBuilder(
              future: creditCardModel.getLocalization(["payment"]),
              builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
                if (snapshot.hasData != null && snapshot.data != null) {
                  return getMainView(creditCardModel);
                } else if (snapshot.hasError) {
                  return ErrorScreen();
                } else {
                  return LoadingScreen();
                }
              });
        },
      ),
    );
  }

  Widget getMainView(AddCreditCardModel creditCardModel) {
    return Scaffold(
        appBar: AppBar(
          title: Text(LocalizationUtils.getSingleValueString("payment", "payment.label.add-card")),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: FutureBuilder(
          future: creditCardModel.getMonarisHtml(),
          // ignore: missing_return
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            if (creditCardModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
              return NoInternetScreen(
                onClickRetry: creditCardModel.clearAsyncMemorizer,
              );
            }

            if (snapshot.hasData == true && creditCardModel.state == ViewState.Idle && creditCardModel.connectivityResult != ConnectivityResult.none) {
              var unescape = new HtmlUnescape();
              String strHtml = unescape.convert(snapshot.data);
              return WillPopScope(
                  onWillPop: () {
                    Navigator.pop(context);
                  },
                  child: WebviewScaffold(
                    url: Uri.dataFromString(strHtml, mimeType: 'text/html', encoding: Encoding.getByName('utf-8')).toString(),
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(0.1),
                      // here the desired height
                      child: AppBar(
                        title: const Text(''),
                      ),
                    ),
                    withZoom: true,
                    withLocalStorage: true,
                    hidden: true,
                    afterWebLoadComplete: () {
                      flutterWebviewPlugin.onUrlChanged.listen((url) async {
                        if (url != null && url.startsWith("https://static.pocketpills.com/emails/static/pocketpills_logo.html")) {
                          List<String> splitTokenUrl = url.split("?/");
                          if (splitTokenUrl.length > 0) {
                            var token = splitTokenUrl[1].split(" ");
                            addMonerisToken(token[0]);
                          }
                        }
                      });
                    },
                  ));
            } else if (snapshot.hasError && creditCardModel.connectivityResult != ConnectivityResult.none) {
              Crashlytics.instance.log(snapshot.hasError.toString());
              return ErrorScreen();
            }

            if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
              return LoadingScreen();
            }
          },
        ));
  }
}
