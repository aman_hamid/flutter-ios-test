import 'package:json_annotation/json_annotation.dart';
part 'chambers_activate_response.g.dart';

@JsonSerializable()
class ChambersResponse {
  String redirect;
  String signUpFlow;
  String signUpType;
  int userId;
  String userIdentifier;
  String hint;
  ChambersResponse(this.redirect, this.signUpFlow, this.signUpType, this.userId,
      this.userIdentifier, this.hint);

  factory ChambersResponse.fromJson(Map<String, dynamic> json) =>
      _$ChambersResponseFromJson(json);
}
