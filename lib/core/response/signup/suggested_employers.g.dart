// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'suggested_employers.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuggestedEmployers _$SuggestedEmployersFromJson(Map<String, dynamic> json) {
  return SuggestedEmployers(
    json['id'] as int,
    json['disabled'] as bool,
    json['employerName'] as String,
    (json['invitationCodeDataSet'] as List)
        ?.map((e) => e == null
            ? null
            : InvitationCodeDataSet.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['employerDisplayName'] as String,
  );
}

Map<String, dynamic> _$SuggestedEmployersToJson(SuggestedEmployers instance) =>
    <String, dynamic>{
      'id': instance.id,
      'disabled': instance.disabled,
      'employerName': instance.employerName,
      'employerDisplayName': instance.employerDisplayName,
      'invitationCodeDataSet': instance.invitationCodeDataSet,
    };
